import { Box, Flex, SimpleGrid, Stack, Text, Button } from "@chakra-ui/react";
import { Popconfirm, message } from "antd";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { deleteScreen, detailsScreen } from "../../Actions/screenActions";
import { useEffect } from "react";
import { convertIntoDateAndTime } from "../../utils/dateAndTimeUtils";
import { useNavigate } from "react-router-dom";
import {
  SCREEN_DELETE_RESET,
  SCREEN_DETAILS_RESET,
} from "../../Constants/screenConstants";
import { RiDeleteBin6Line } from "react-icons/ri";
import { AiTwotoneEdit } from "react-icons/ai";
import { getAllActiveAndPauseCampaignListByScreenId } from "../../Actions/campaignAction";
import { MyButton } from "../../components/newCommans/MyButtons";

export function ScreenAbout(props: any) {
  const { screenId } = props;
  const dispatch = useDispatch<any>();
  const navigate = useNavigate();

  const screenDelete = useSelector((state: any) => state.screenDelete);
  const {
    // loading: loadingScreenDelete,
    error: errorScreenDelete,
    success: successScreeDelete,
  } = screenDelete;

  const activeAndPauseCampaignListByScreenId = useSelector(
    (state: any) => state.activeAndPauseCampaignListByScreenId
  );
  const {
    // loading: loadingCampaign,
    // error: errorCampaign,
    campaigns: videosList,
  } = activeAndPauseCampaignListByScreenId;

  useEffect(() => {
    if (successScreeDelete) {
      message.success("Screen deleted successfully!");
      dispatch({ type: SCREEN_DELETE_RESET });
      setTimeout(() => {
        navigate("/my/screens");
      }, 0);
    }
    if (errorScreenDelete) {
      message.error(errorScreenDelete);
      dispatch({ type: SCREEN_DELETE_RESET });
    }
  }, [dispatch, navigate, errorScreenDelete, successScreeDelete]);

  const confirm = (e: React.MouseEvent<HTMLElement> | undefined) => {
    dispatch(deleteScreen(screenId));
  };

  const screenDetails = useSelector((state: any) => state.screenDetails);
  const { error: errorScreen, screen } = screenDetails;

  useEffect(() => {
    if (errorScreen) {
      message.error(errorScreen);
      dispatch({ type: SCREEN_DETAILS_RESET });
    }
  }, [errorScreen, dispatch]);

  useEffect(() => {
    dispatch(detailsScreen(screenId));
    dispatch(getAllActiveAndPauseCampaignListByScreenId(screenId));
  }, [screenId, dispatch]);
  return (
    <Stack
      p={{ base: "2", lg: "5" }}
      height="100%"
      width="100%"
      overflowY={"auto"}
    >
      <Flex gap="2" px={{ base: "2", lg: "10" }} flexDir="column">
        <Flex align="center" justifyContent="space-between" p="1">
          <Text
            fontWeight="500"
            fontSize={{ base: "lg", lg: "24px" }}
            m="0"
            color="#131D30"
          >
            About My Screen
          </Text>
          <Flex gap="2">
            <Popconfirm
              title="Delete screen"
              description="Do you really want to delete this screen?"
              onConfirm={confirm}
              okText="Yes"
              cancelText="No"
            >
              <Button
                bgColor="red"
                px={{ base: "4", lg: "4" }}
                borderRadius="28px"
                variant="outline"
                borderColor="#B9B9B9"
                color="#FFFFFF"
                _hover={{ color: "red", bgColor: "#FFFFFF" }}
                leftIcon={<RiDeleteBin6Line size="16px" />}
                isDisabled={videosList?.campaigns > 0 ? true : false}
              >
                Delete
              </Button>
            </Popconfirm>
            <MyButton
              icon={<AiTwotoneEdit size="20px" />}
              label="Edit"
              color="#13AA2B"
              bgColor="#FFFFFF"
              handelClick={() =>
                navigate(`/screen/editScreen/${screenId}`, {
                  state: { type: "Edit" },
                })
              }
            />
          </Flex>
        </Flex>
        <Box py="5" px={{ base: "5", lg: "10" }} bgColor="#FFFFFF">
          <Text fontWeight="700" fontSize="20px" m="0" color="#131D30">
            Basic Details
          </Text>
          <Flex gap="20" pt="3">
            <Stack gap="3">
              <Text fontWeight="400" fontSize="md" m="0" color="#808080">
                Name
              </Text>
              <Text fontWeight="400" fontSize="md" m="0" color="#808080">
                Screen Type
              </Text>{" "}
              <Text fontWeight="400" fontSize="md" m="0" color="#808080">
                Dimension
              </Text>{" "}
              <Text fontWeight="400" fontSize="md" m="0" color="#808080">
                Category
              </Text>{" "}
              <Text fontWeight="400" fontSize="md" m="0" color="#808080">
                On Time
              </Text>
              <Text fontWeight="400" fontSize="md" m="0" color="#808080">
                Off Time
              </Text>
              <Text fontWeight="400" fontSize="md" m="0" color="#808080">
                Daily Rate
              </Text>
            </Stack>
            <Stack gap="3">
              <Text fontWeight="400" fontSize="md" m="0" color="#131D30">
                {screen?.name}
              </Text>
              <Text fontWeight="400" fontSize="md" m="0" color="#131D30">
                {screen?.screenType}
              </Text>
              <Text fontWeight="400" fontSize="md" m="0" color="#131D30">
                {screen?.size?.diagonal} Inches
              </Text>
              <Text fontWeight="400" fontSize="md" m="0" color="#131D30">
                {screen?.category}
              </Text>
              <Text fontWeight="400" fontSize="md" m="0" color="#131D30">
                {convertIntoDateAndTime(screen?.startTime)?.split(",")[2]}
              </Text>
              <Text fontWeight="400" fontSize="md" m="0" color="#131D30">
                {convertIntoDateAndTime(screen?.endTime)?.split(",")[2]}
              </Text>
              <Text fontWeight="400" fontSize="md" m="0" color="#131D30">
                Rs. {screen?.rentPerDay}
              </Text>
            </Stack>
          </Flex>
        </Box>
        <Box py="5" px={{ base: "5", lg: "10" }} bgColor="#FFFFFF">
          <Text fontWeight="700" fontSize="20px" color="#131D30">
            Location
          </Text>
          <SimpleGrid columns={[3, 3, 6]} spacing={{ base: "5", lg: "10" }}>
            <Flex flexDir="column" gap="1">
              <Text fontWeight="400" fontSize="md" m="0" color="#808080">
                Address
              </Text>
              <Text fontWeight="400" fontSize="md" m="0" color="#131D30">
                {screen?.screenAddress}
              </Text>
            </Flex>
            <Flex flexDir="column" gap="1">
              <Text fontWeight="400" fontSize="md" m="0" color="#808080">
                City
              </Text>
              <Text fontWeight="400" fontSize="md" m="0" color="#131D30">
                {screen?.districtCity}
              </Text>
            </Flex>
            <Flex flexDir="column" gap="1">
              <Text fontWeight="400" fontSize="md" m="0" color="#808080">
                State
              </Text>
              <Text fontWeight="400" fontSize="md" m="0" color="#131D30">
                {screen?.stateUT}
              </Text>
            </Flex>
            <Flex flexDir="column" gap="1">
              <Text fontWeight="400" fontSize="md" m="0" color="#808080">
                Landmark
              </Text>
              <Text fontWeight="400" fontSize="md" m="0" color="#131D30">
                {screen?.landMark}
              </Text>
            </Flex>
            <Flex flexDir="column" gap="1">
              <Text fontWeight="400" fontSize="md" m="0" color="#808080">
                Highlights
              </Text>
              <Text fontWeight="400" fontSize="md" m="0" color="#131D30">
                {screen?.screenHighlights?.join(", ")}
              </Text>
            </Flex>
          </SimpleGrid>
        </Box>
        <Box py="5" px={{ base: "5", lg: "10" }} bgColor="#FFFFFF" pb="10">
          <Text fontWeight="700" fontSize="20px" m="0" color="#131D30">
            Slot Details
          </Text>
          <Flex pt="5" gap="20">
            <Flex flexDir="column" gap="1">
              <Text fontWeight="400" fontSize="md" m="0" color="#808080">
                Rent / Day
              </Text>
              <Text fontWeight="400" fontSize="md" m="0" color="#131D30">
                Rs. {screen?.rentPerDay}
              </Text>
            </Flex>
            <Flex flexDir="column" gap="1">
              <Text fontWeight="400" fontSize="md" m="0" color="#808080">
                Total Slot / Day
              </Text>
              <Text fontWeight="400" fontSize="md" m="0" color="#131D30">
                {screen?.slotsPlayPerDay} times
              </Text>
            </Flex>
          </Flex>
        </Box>
      </Flex>
    </Stack>
  );
}
