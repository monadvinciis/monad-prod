import { Box, Flex, Text, Stack, Hide, Show } from "@chakra-ui/react";
import { useDispatch } from "react-redux";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { userScreensList } from "../../Actions/userActions";
import { useSelector } from "react-redux";
import { Alert, FloatButton, Select, message } from "antd";
import { CgScreen } from "react-icons/cg";
import {
  MdCampaign,
  MdOutlineCampaign,
  MdOutlineFileUpload,
  MdOutlineMarkEmailUnread,
} from "react-icons/md";
import { TbAnalyze } from "react-icons/tb";
import { IoMdInformationCircleOutline } from "react-icons/io";
import { OverView } from "./OverView";
import { CampaignsHistory } from "./CampaignsHistory";
import { Analytics } from "./Analytics";
import { ScreenAbout } from "./ScreenAbout";
import { NewRequest } from "./NewRequest";
import { CreateNewCampaignModel } from "./CreateNewCampaignModel";
import { MEDIA_UPLOAD_RESET } from "../../Constants/mediaConstants";
import { CREATE_CAMPAIGN_RESET } from "../../Constants/campaignConstants";
import {
  createCamapaign,
  getAllActiveAndPauseCampaignListByScreenId,
} from "../../Actions/campaignAction";
import { getNumberOfDaysBetweenTwoDates } from "../../utils/dateAndTimeUtils";
import { LuChevronLeft } from "react-icons/lu";
import { MyButton } from "../../components/newCommans/MyButtons";
import { USER_SCREENS_RESET } from "../../Constants/userConstants";
// import { AiOutlineDown } from "react-icons/ai";
import { CgLivePhoto } from "react-icons/cg";
import { MdOutlineNetworkWifi1Bar, MdOutlineNetworkWifi } from "react-icons/md";

export function ScreenDetailPage(props: any) {
  const dispatch = useDispatch<any>();
  const navigate = useNavigate();

  const id = window.location.pathname.split("/")[3];
  const [screenId, setScreenId] = useState<any>(id);
  const [screen, setScreen] = useState<any>(null);
  const [selectedTab, setSelectedTab] = useState<any>(1);
  const [isLoading, setIsLoading] = useState<any>(false);

  const [showName, setShowName] = useState<any>(true);

  //create campaign data

  const createCampaign = useSelector((state: any) => state.createCampaign);
  const {
    loading: loadingSlotBooking,
    error: errorSlotBooking,
    success: successSlotBooking,
  } = createCampaign;

  const mediaUpload = useSelector((state: any) => state.mediaUpload);
  const {
    // loading: loadingMedia,
    media: mediaData,
    success,
    error: errorMedia,
  } = mediaUpload;

  const [campaignData, setCampaignData] = useState<any>();

  const makeCampaignDataAsEmpty = () => {
    setCampaignData({});
  };

  const getScreenOnTime = () => {
    return Math.floor(
      (new Date().getTime() -
        new Date(getRequeirdData()?.lastActive).getTime()) /
        1000
    );
  };

  useEffect(() => {
    if (errorMedia) {
      dispatch({ type: MEDIA_UPLOAD_RESET });
      setIsLoading(false);
      message.error(errorMedia);
      makeCampaignDataAsEmpty();
    }
    if (success && mediaData) {
      slotBookingHandler();
    }
  }, [errorMedia, success, mediaData, dispatch]);

  useEffect(() => {
    if (errorSlotBooking) {
      dispatch({ type: CREATE_CAMPAIGN_RESET });
      dispatch({ type: MEDIA_UPLOAD_RESET });
      setIsLoading(false);
      message.error("Please Try Again, Something went wrong!");
      makeCampaignDataAsEmpty();
    }
    if (successSlotBooking) {
      message.success("Campaign created successfully!");
      dispatch({ type: CREATE_CAMPAIGN_RESET });
      dispatch({ type: MEDIA_UPLOAD_RESET });
      setIsLoading(false);
      makeCampaignDataAsEmpty();
      dispatch(getAllActiveAndPauseCampaignListByScreenId(screenId));
    }
  }, [errorSlotBooking, successSlotBooking, dispatch, screenId]);

  const slotBookingHandler = () => {
    const {
      screenIds,
      campaignName,
      brandName,
      isDefaultCampaign,
      startDateAndTime,
      endDateAndTime,
      url,
      fileType,
      duration,
      fileSize,
    } = campaignData;
    dispatch(
      createCamapaign({
        screenIds,
        campaignName,
        brandName,
        isDefaultCampaign,
        startDateAndTime,
        endDateAndTime,
        noOfDays: getNumberOfDaysBetweenTwoDates(
          startDateAndTime,
          endDateAndTime
        ),
        mediaId: mediaData?._id,
        url,
        fileType,
        duration,
        fileSize,
      })
    );
  };

  const [openCreateCampaignModel, setOpenCreateCampaignModel] =
    useState<any>(false);

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  const userScreens = useSelector((state: any) => state.userScreens);
  const { error: errorScreens, screens } = userScreens;

  const screenOptions = screens?.map((screen: any) => {
    return {
      value: screen?._id,
      label: screen?.name?.toUpperCase(),
      fontSize: 12,
    };
  });

  const getRequeirdData = () => {
    return screens?.find((data: any) => data?._id === screenId);
  };

  useEffect(() => {
    if (errorScreens) {
      message.error("Something went wrong!");
      dispatch({ type: USER_SCREENS_RESET });
    }
    if (screens) {
      setScreen(screens.filter((data: any) => data?._id === screenId));
    }
  }, [screens, errorScreens, dispatch, screenId]);

  useEffect(() => {
    if (!userInfo) {
      navigate("/signin");
    }

    dispatch(userScreensList(userInfo, ""));
  }, [userInfo, navigate, dispatch]);

  const tabOptions = [
    {
      key: 1,
      icon: <CgScreen />,
      label: "Details",
    },

    {
      key: 2,
      icon: <TbAnalyze />,
      label: "Analytics",
    },
    {
      key: 3,
      icon: <MdCampaign />,
      label: "History",
    },
    {
      icon: <MdOutlineMarkEmailUnread />,
      key: 5,
      label: "Requests",
    },
    {
      icon: <IoMdInformationCircleOutline />,
      key: 4,
      label: "About",
    },
  ];

  return (
    <Box w="100%" h="100%" bgColor="#F5F5F5" m="0">
      <CreateNewCampaignModel
        open={openCreateCampaignModel}
        onCancel={() => setOpenCreateCampaignModel(false)}
        screens={screens}
        screenOptions={screenOptions}
        campaignData={campaignData}
        defaultScreen={screenId}
        setCampaignData={(value: any) => setCampaignData(value)}
        setIsLoading={setIsLoading}
      />
      <Flex
        p={{ base: "4", lg: "4" }}
        align="center"
        borderTop="1px"
        borderLeft="1px"
        borderColor="#EBEBEB"
        gap={{ base: "2px", lg: "8px" }}
        pr={{ base: "0", lg: "32px" }}
        pl={{ base: "4", lg: "4" }}
        bgColor="#FFFFFF"
        justifyContent="space-between"
      >
        <Flex align="center" gap="0">
          <LuChevronLeft size="20px" onClick={() => navigate(-1)} />
          <Flex direction={"column"} pl="7">
            <Select
              showSearch
              bordered={false}
              defaultValue={showName ? false : true}
              onChange={(value) => {
                setScreenId(value);
                setShowName(true);
                navigate(`/screen/screenDetail/${value}`);
              }}
              style={{ width: 300 }}
              options={screenOptions}
              value={showName ? null : screenId}
              onClick={() => {
                setShowName(false);
              }}
              onFocus={() => setShowName(false)}
              onMouseEnter={() => setShowName(false)}
              onMouseLeave={() => setShowName(true)}
              onMouseDown={() => setShowName(false)}
              onInputKeyDown={() => setShowName(false)}
              optionFilterProp="children"
              filterOption={(input: any, option: any) =>
                (option?.label ?? "").includes(input)
              }
              filterSort={(optionA: any, optionB: any) =>
                (optionA?.label ?? "")
                  .toLowerCase()
                  .includes((optionB?.label ?? "").toLowerCase())
              }
            />
            {showName && (
              <Flex>
                <Text fontSize="20" fontWeight="600" mt="-8" mb="0">
                  {getRequeirdData()?.name.toUpperCase()}
                </Text>
              </Flex>
            )}
          </Flex>
          <Box>
            <Flex justify="space-between" gap="5" align="center">
              <Flex align="center" justify="center" gap="1">
                {getRequeirdData()?.deviceStatus === "online" ? (
                  <MdOutlineNetworkWifi color="teal" />
                ) : (
                  <MdOutlineNetworkWifi1Bar color="red" />
                )}
                <Text pt="4" fontSize="10px">
                  {getRequeirdData()?.deviceStatus}
                </Text>
              </Flex>
              <Flex align="center" justify="center" gap="2">
                <CgLivePhoto
                  color={
                    !Number.isNaN(getScreenOnTime())
                      ? getScreenOnTime() > 300
                        ? "red"
                        : "green"
                      : "red"
                  }
                />
                <Text pt="4" fontSize="10px">
                  {/* Last seen{" "} */}
                  {!Number.isNaN(getScreenOnTime())
                    ? `${getScreenOnTime()} second ago`
                    : `Not open for long time`}
                </Text>
              </Flex>
            </Flex>
          </Box>
        </Flex>
        <Hide below="md">
          <MyButton
            icon={<MdOutlineFileUpload />}
            handelClick={() => setOpenCreateCampaignModel(true)}
            label="Campaign"
            isLoading={isLoading || loadingSlotBooking}
            loadingText="Creating Campaigns, Plese wait..."
          />
        </Hide>
      </Flex>
      <Show below="md">
        <Flex
          pl={{ base: "4", lg: "8" }}
          align="center"
          border="1px"
          borderColor="#EBEBEB"
          pr={{ base: "4px", lg: "32px" }}
          gap={{ base: "4", lg: "8" }}
          bgColor="#FFFFFF"
          overflowX="auto"
          width="100%"
          className="scrollmenu"
        >
          {tabOptions?.map((option: any, index: any) => {
            return (
              <Stack key={index}>
                <Flex
                  gap="2"
                  py="2"
                  style={{ cursor: "pointer" }}
                  color={selectedTab === option.key ? "#000000" : "#7C7C7C"}
                  fontWeight={selectedTab === option.key ? "700" : "400"}
                  align="center"
                  onClick={() => setSelectedTab(option.key)}
                >
                  {option.icon}
                  <Text m="0" fontSize={{ base: "16px", lg: "16px" }}>
                    {option.label}
                  </Text>
                </Flex>
                <Box
                  height="2px"
                  background={selectedTab === option.key ? "#000000" : ""}
                ></Box>
              </Stack>
            );
          })}
        </Flex>
      </Show>
      <Hide below="md">
        <Flex
          pl={{ base: "4", lg: "8" }}
          align="center"
          border="1px"
          borderColor="#EBEBEB"
          gap={{ base: "8", lg: "8" }}
          bgColor="#FFFFFF"
          overflowX="auto"
          width="100%"
        >
          {tabOptions?.map((option: any, index: any) => {
            return (
              <Stack key={index}>
                <Flex
                  gap="2"
                  py="2"
                  style={{ cursor: "pointer" }}
                  color={selectedTab === option.key ? "#000000" : "#7C7C7C"}
                  fontWeight={selectedTab === option.key ? "700" : "400"}
                  align="center"
                  onClick={() => setSelectedTab(option.key)}
                >
                  {option.icon}
                  <Text m="0" fontSize={{ base: "16px", lg: "16px" }}>
                    {option.label}
                  </Text>
                </Flex>
                <Box
                  height="2px"
                  background={selectedTab === option.key ? "#000000" : ""}
                ></Box>
              </Stack>
            );
          })}
        </Flex>
      </Hide>
      <Flex width="100%" h="83%">
        <Stack width="100%" height="100%" pb="2">
          {isLoading || loadingSlotBooking ? (
            <Alert
              type="info"
              message={"The campaign is creating, wait for some time. ...."}
              banner
            />
          ) : null}
          {selectedTab === 1 ? (
            <OverView screenId={screenId} />
          ) : selectedTab === 2 ? (
            <Analytics screen={screen} screenId={screenId} />
          ) : selectedTab === 3 ? (
            <CampaignsHistory screenId={screenId} />
          ) : selectedTab === 4 ? (
            <ScreenAbout screenId={screenId} />
          ) : selectedTab === 5 ? (
            <NewRequest screenId={screenId} />
          ) : null}
          <Show below="md">
            <FloatButton
              type="primary"
              tooltip={<div>Upload Campaign</div>}
              style={{
                right: 40,
                bottom: 75,
              }}
              icon={<MdOutlineCampaign size="20px" />}
              onClick={() => setOpenCreateCampaignModel(true)}
            ></FloatButton>
          </Show>
        </Stack>
      </Flex>
    </Box>
  );
}
