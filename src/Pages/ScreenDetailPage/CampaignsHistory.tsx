import { Stack } from "@chakra-ui/react";
import { CampaignTable } from "../../components/newCommans";

export function CampaignsHistory(props: any) {
  return (
    <Stack
      p={{ base: "2", lg: "5" }}
      height="100%"
      width="100%"
      overflowY={"auto"}
    >
      <CampaignTable screenId={props?.screenId} />
    </Stack>
  );
}
