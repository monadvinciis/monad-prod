import { Box, Flex, Stack } from "@chakra-ui/react";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import {
  getAllPleaListByScreenId,
  getPendingPleaListByScreenId,
  grantCampaignAllyPlea,
  rejectCampaignAllyPlea,
} from "../../Actions/pleaActions";
import { useDispatch } from "react-redux";
import { ScreenOwnerPlea } from "../../components/newCommans";
import { Skeleton, message } from "antd";
import {
  CAMPAIGN_ALLY_GRANT_RESET,
  CAMPAIGN_ALLY_REJECT_RESET,
} from "../../Constants/pleaConstants";
export function NewRequest(props: any) {
  const dispatch = useDispatch<any>();

  const allPleaListByScreenId = useSelector(
    (state: any) => state.allPleaListByScreenId
  );
  const { pleas, loading: loadingAllPleasByScreenId } = allPleaListByScreenId;

  const pendingPleaListByScreenId = useSelector(
    (state: any) => state.pendingPleaListByScreenId
  );
  const { pleas: pendingPleaRequest, loading: loadingPendingPleaRequest } =
    pendingPleaListByScreenId;

  const getPleaRequest = () => {
    dispatch(getAllPleaListByScreenId(props?.screenId));
    dispatch(getPendingPleaListByScreenId(props?.screenId));
  };

  useEffect(() => {
    dispatch(getAllPleaListByScreenId(props?.screenId));
    dispatch(getPendingPleaListByScreenId(props?.screenId));
  }, [props, dispatch]);

  const campaignAllyPleaGrant = useSelector(
    (state: any) => state.campaignAllyPleaGrant
  );
  const {
    // loading: loadingCampaignPleaGrant,
    success: successCampaignPleaGrant,
    error: errorCampaignPleaGrant,
  } = campaignAllyPleaGrant;

  const campaignAllyPleaReject = useSelector(
    (state: any) => state.campaignAllyPleaReject
  );
  const {
    // loading: loadingCampaignRejectPlea,
    success: successCampaignRejectPlea,
    error: errorCampaignRejectPlea,
  } = campaignAllyPleaReject;

  useEffect(() => {
    if (successCampaignPleaGrant) {
      message.success(
        "You have given access to user to run this campaign on your screen"
      );
      dispatch({ type: CAMPAIGN_ALLY_GRANT_RESET });
      dispatch(getAllPleaListByScreenId(props?.screenId));
      dispatch(getPendingPleaListByScreenId(props?.screenId));
    }
    if (errorCampaignPleaGrant) {
      message.error(errorCampaignPleaGrant);
      dispatch({ type: CAMPAIGN_ALLY_GRANT_RESET });
    }
    if (successCampaignRejectPlea) {
      message.success(
        "You have not given access to user to run this campaign on your screen"
      );
      dispatch({ type: CAMPAIGN_ALLY_REJECT_RESET });
      dispatch(getAllPleaListByScreenId(props?.screenId));
      dispatch(getPendingPleaListByScreenId(props?.screenId));
    }
    if (errorCampaignRejectPlea) {
      message.error(errorCampaignRejectPlea);
      dispatch({ type: CAMPAIGN_ALLY_REJECT_RESET });
    }
  }, [
    successCampaignPleaGrant,
    successCampaignRejectPlea,
    errorCampaignPleaGrant,
    errorCampaignRejectPlea,
    dispatch,
    props,
  ]);

  const handelAcceptCampaignPlea = (plea: any) => {
    dispatch(grantCampaignAllyPlea(plea?._id));
  };
  const handelRejectCampaignPlea = (plea: any) => {
    dispatch(rejectCampaignAllyPlea(plea?._id));
  };

  return (
    <Stack height="100%" width="100%" overflowY={"auto"} bgColor="#F1F1F1">
      <Flex mt="2" width="100%" height="83%">
        <Stack width="100%" height="100%" pb="2">
          <Stack width="100%" overflowY={"auto"}>
            {loadingAllPleasByScreenId || loadingPendingPleaRequest ? (
              <Box p="5">
                <Skeleton avatar active paragraph={{ rows: 2 }} />
                <Skeleton avatar active paragraph={{ rows: 2 }} />
                <Skeleton avatar active paragraph={{ rows: 2 }} />
              </Box>
            ) : (
              <Flex
                flexDir={"column"}
                flexGrow={1}
                overflow={"hidden"}
                width="100%"
              >
                <ScreenOwnerPlea
                  pendingCampaignPleaRequest={pendingPleaRequest}
                  allCampaignPleaRequest={pleas}
                  getPleaRequest={getPleaRequest}
                  loadingPendingPleaRequest={loadingPendingPleaRequest}
                  loadingAllPleas={loadingAllPleasByScreenId}
                  handelAcceptCampaignPlea={handelAcceptCampaignPlea}
                  handelRejectCampaignPlea={handelRejectCampaignPlea}
                />
              </Flex>
            )}
          </Stack>
        </Stack>
      </Flex>
    </Stack>
  );
}
