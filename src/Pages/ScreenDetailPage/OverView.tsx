import {
  Box,
  Button,
  Flex,
  Image,
  SimpleGrid,
  Text,
  Stack,
  Hide,
  Show,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { detailsScreen } from "../../Actions/screenActions";
import { getAllActiveAndPauseCampaignListByScreenId } from "../../Actions/campaignAction";
import { Carousel, message } from "antd";
import { useSelector } from "react-redux";
import { AllActiveCampaign } from "../../components/newCommans";
import { CiLocationOn } from "react-icons/ci";
import { noimage } from "../../assets/Images";
import { SCREEN_DETAILS_RESET } from "../../Constants/screenConstants";
import { ACTIVE_AND_PAUSE_CAMPAIGN_LIST_BY_SCREENID_RESET } from "../../Constants/campaignConstants";
import { campaignSVG } from "../../assets/svg";
import { FiMonitor } from "react-icons/fi";

export function OverView(props: any) {
  const { screenId } = props;
  const dispatch = useDispatch<any>();

  const [selectedIndex, setSelectedIndex] = useState<any>(0);
  const [images, setImages] = useState<any>([]);

  const activeAndPauseCampaignListByScreenId = useSelector(
    (state: any) => state.activeAndPauseCampaignListByScreenId
  );
  const {
    // loading: loadingCampaign,
    error: errorCampaign,
    campaigns: videosList,
  } = activeAndPauseCampaignListByScreenId;

  const screenDetails = useSelector((state: any) => state.screenDetails);
  const { error: errorScreen, screen } = screenDetails;

  useEffect(() => {
    if (errorScreen) {
      // message.error("Something went wrong in finding screen details");
      dispatch({ type: SCREEN_DETAILS_RESET });
    }
    if (errorCampaign) {
      message.error("Something went worng in finding Campaigns");
      dispatch({ type: ACTIVE_AND_PAUSE_CAMPAIGN_LIST_BY_SCREENID_RESET });
    }
    if (screen) {
      const x = [];
      for (let i = 0; i < 4; i++) {
        if (screen?.images[i]) {
          x.push(`https://ipfs.io/ipfs/${screen?.images[i]}`);
        } else {
          x.push(noimage);
        }
      }
      setImages(x);
    }
  }, [errorScreen, errorCampaign, screen, dispatch]);

  useEffect(() => {
    if (screenId) {
      dispatch(detailsScreen(screenId));
      dispatch(getAllActiveAndPauseCampaignListByScreenId(screenId));
    }
  }, [props, screenId, dispatch]);
  return (
    <Stack
      p={{ base: "0", lg: "4" }}
      pb={{ base: "8", lg: "0" }}
      height="100%"
      width="100%"
      overflowY={"auto"}
    >
      <SimpleGrid columns={[1, 1, 2]} spacing={{ base: 2, lg: 4 }}>
        <Box
          p={{ base: "2", lg: "4" }}
          height="100%"
          bgColor="#FFFFFF"
          width="100%"
          borderRadius="9px"
        >
          <Flex gap="8" flexDir="column">
            <Hide below="md">
              <Flex gap="4">
                <Image
                  src={
                    screen?.images?.length !== 0
                      ? images[selectedIndex]
                      : screen?.image
                  }
                  width="70%"
                  height="300px"
                  borderRadius="5px"
                />
                <Flex flexDir="column" gap="4">
                  {images.map((image: any, index: any) => (
                    <Box key={index}>
                      <Image
                        src={image}
                        width="73px"
                        height="60px"
                        borderRadius="5px"
                        border={selectedIndex === index ? "1px" : "0px"}
                        borderColor="red"
                        onClick={() => setSelectedIndex(index)}
                      />
                    </Box>
                  ))}
                </Flex>
              </Flex>
            </Hide>
            <Show below="md">
              <Carousel autoplay>
                {images.map((image: any, index: any) => (
                  <Image
                    key={index}
                    src={image}
                    width="100%"
                    height="258px"
                    borderRadius="4px"
                  />
                ))}
              </Carousel>
            </Show>

            <Box px={{ base: "4", lg: "4" }} pb={{ base: "4", lg: "8" }}>
              <Flex gap="4">
                <Text fontSize="20px" fontWeight="700" color="#131D30" m="0">
                  {screen?.name}
                </Text>
                <Button
                  variant="none"
                  bgColor="#FFFCDD"
                  py="1"
                  borderRadius="25px"
                  color="#FF6B00"
                >
                  {" "}
                  {screen?.ratting || 4.5}
                </Button>
              </Flex>
              <Flex gap="0" py="0" align="center">
                <CiLocationOn color="#069D15" />
                <Text
                  color="#808080"
                  m="0"
                  fontSize="14px"
                >{`${screen?.districtCity}, ${screen?.stateUT}`}</Text>
              </Flex>
              <SimpleGrid
                columns={[2, 2, 2]}
                spacing={{ base: "2", lg: "4" }}
                fontSize={{ base: "14px", lg: "16px" }}
                fontWeight="400"
                pt={2}
              >
                <Stack color="#111111" gap={{ base: "2", lg: "2" }}>
                  <Flex align="center" gap={{ base: "2", lg: "2" }}>
                    <Image src={campaignSVG} />
                    <Text m="0">Active Campaigns</Text>
                  </Flex>
                  <Flex align="center" gap={{ base: "2", lg: "2" }}>
                    <FiMonitor />
                    <Text m="0">Slot play per day</Text>
                  </Flex>
                  <Flex align="center" gap={{ base: "2", lg: "2" }}>
                    <Text m="0">₹</Text>
                    <Text m="0"> Rent Per Day</Text>
                  </Flex>
                </Stack>
                <Stack color="#000000" gap={{ base: "2", lg: "2" }}>
                  {" "}
                  <Text m="0">{videosList?.activeCampaigns?.length}</Text>
                  <Text m="0">{screen?.slotsPlayPerDay} Times</Text>
                  <Text m="0">Rs. {screen?.rentPerDay}</Text>
                </Stack>
              </SimpleGrid>
            </Box>
          </Flex>
        </Box>

        <Stack p={{ base: "4", lg: "0" }}>
          <AllActiveCampaign screenId={screenId} show={"AP"} />
        </Stack>
      </SimpleGrid>
    </Stack>
  );
}
