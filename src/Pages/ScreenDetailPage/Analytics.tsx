import { useEffect, useRef, useState } from "react";
import {
  Box,
  Flex,
  SimpleGrid,
  Stack,
  Text,
  Hide,
  Show,
} from "@chakra-ui/react";
import {
  Chart as ChartJS,
  LinearScale,
  CategoryScale,
  BarElement,
  PointElement,
  LineElement,
  Legend,
  Tooltip,
  LineController,
  BarController,
  Filler,
  ArcElement,
} from "chart.js";
import { Chart, Doughnut, Line } from "react-chartjs-2";
import { useDispatch } from "react-redux";
import { getScreenCamData, getScreenLogs } from "../../Actions/screenActions";
import { useSelector } from "react-redux";
import {
  convertIntoDateAndTime,
  convertToDate,
} from "../../utils/dateAndTimeUtils";
import { ScrollBox } from "../../components/newCommans";
import { ArcSlider } from "../../components/newCommans/ArcSlider";
import { getAllCampaignListByScreenId } from "../../Actions/campaignAction";
import moment from "moment";
import { getUserWalletTransaction } from "../../Actions/walletAction";

ChartJS.register(
  ArcElement,
  LinearScale,
  CategoryScale,
  BarElement,
  PointElement,
  LineElement,
  Legend,
  Tooltip,
  LineController,
  BarController,
  Filler
);

export function Analytics(props: any) {
  const chartRef = useRef<ChartJS>(null);
  const dispatch = useDispatch<any>();
  // const today = moment().startOf("day").toDate();
  const oneWeekAgo = moment().subtract(1, "week").toDate();

  const [audienceLabels, setAudienceLabels] = useState<any>([]);
  const [graphAudienceData, setGraphAudienceData] = useState<any>([]);

  const [playbackLabels, setPlaybackLabels] = useState<any>([]);
  const [graphPlaybackData, setGraphPlaybackData] = useState<any>([]);

  const [brands, setBrands] = useState<any>([]);
  const [brandFreq, setBrandFreq] = useState<any>([]);

  const [revenueLabels, setRevenueLabels] = useState<any>([]);
  const [graphRevenueData, setGraphRevenueData] = useState<any>([]);

  const [campaignRevenueLabels, setCampaignRevenueLabels] = useState<any>([]);

  useEffect(() => {
    dispatch(getScreenCamData(props?.screenId));
    dispatch(getScreenLogs(props?.screenId));
    dispatch(getAllCampaignListByScreenId(props?.screenId));
    dispatch(getUserWalletTransaction());
  }, [dispatch, props]);

  const screenCamDataGet = useSelector((state: any) => state.screenCamDataGet);
  const {
    loading: loadingCamData,
    error: errorCamData,
    data: screenCamData,
  } = screenCamDataGet;

  const screenLogsGet = useSelector((state: any) => state.screenLogsGet);
  const {
    loading: loadingLogs,
    error: errorLogs,
    data: screenLogs,
  } = screenLogsGet;

  const campaignListByScreenId = useSelector(
    (state: any) => state.campaignListByScreenId
  );
  const {
    loading: loadingAllCampaign,
    error: errorAllCampaign,
    campaigns,
  } = campaignListByScreenId;

  const walletTransaction = useSelector(
    (state: any) => state.walletTransaction
  );
  const {
    // loading: loadingWalletTransaction,
    // error: errorWalletTransaction,
    transaction,
  } = walletTransaction;

  function dynamicColors() {
    var r = Math.floor(Math.random() * 255);
    // var b = Math.floor(Math.random() * 255);
    var g = Math.floor(Math.random() * 255);
    return "rgba(" + r + "," + g + "," + 255 + "," + 0.5 + ")";
  }

  function poolColors(a: number) {
    const pool = [];
    for (let i: number = 0; i < a; i++) {
      pool.push(dynamicColors());
    }
    return pool;
  }

  // function filterByDate(data: any, startDate: any, endDate: any) {
  //   return data?.filter((item: any) => {
  //     const date = moment(item.timestamp, "YYYYMMDDHHMMSS").toDate();
  //     return moment(date).isBetween(startDate, endDate, "day", "[]");
  //   });
  // }
  // function datalist({ data }: any) {
  //   var aData: any = {};
  //   const filteredData = data?.filter(
  //     (d: any) =>
  //       new Date(d.timestamp).toDateString() >= new Date(today).toDateString()
  //   );
  //   data
  //     ?.filter(
  //       (d: any) =>
  //         new Date(d.timestamp).toDateString() >= new Date(today).toDateString()
  //     )
  //     .map((b: any) => {
  //       return { time: convertToDate(b.timestamp), people: b.num_people };
  //     })
  //     .forEach((a: any) => {
  //       if (aData[a.time]) {
  //         aData[a.time] = aData[a.time] + a.people;
  //       } else {
  //         aData[a.time] = a.people;
  //       }
  //     });
  //   return filteredData;
  // }

  // const getVideoDuration = (link: any) => {
  //   const video = document.createElement("video");
  //   video.src = link;
  //   let duration;
  //   // "https://bafkreiex3fu3b3gnbivhqo32ib7xulesnhru6wqovh3shhrbl4xc7gac3a.ipfs.w3s.link";
  //   video.onloadedmetadata = () => {
  //     duration = video.duration;
  //   };
  //   return duration;
  // };

  useEffect(() => {
    if (screenCamData) {
      const aData: any = {};
      screenCamData
        ?.filter(
          (d: any) =>
            new Date(d.timestamp).toDateString() >=
            new Date(oneWeekAgo).toDateString()
        )
        ?.map((b: any) => {
          return { time: convertToDate(b.timestamp), people: b.num_people };
        })
        ?.forEach((a: any) => {
          if (aData[a.time]) {
            aData[a.time] = aData[a.time] + a.people;
          } else {
            aData[a.time] = a.people;
          }
        });
      // setAudienceLabels(
      //   screenCamData.map((sc: any) => new Date(sc.timestamp).toDateString())
      // );
      setAudienceLabels(Object.keys(aData));
      setGraphAudienceData(Object.values(aData));
      // datalist({ data: screenCamData });
    }
    if (screenLogs) {
      var screenLogsDates = screenLogs?.allLogs.map((l: any) =>
        convertToDate(new Date(l.playTime))
      );
      // var screenLogsTimes = screenLogs?.allLogs.map((l: any) =>
      //   convertIntoDateAndTime(new Date(l.playTime))
      // );
      var dates: any = {};
      screenLogsDates?.forEach((date: any) => {
        if (dates[date]) {
          dates[date]++;
        } else {
          dates[date] = 1;
        }
      });
      setPlaybackLabels(Object.keys(dates));
      setGraphPlaybackData(Object.values(dates));
    }
    if (campaigns) {
      var brandsHere: any = {};
      campaigns
        ?.map((b: any) => b.brandName)
        ?.forEach((br: any) => {
          if (brandsHere[br]) {
            brandsHere[br]++;
          } else {
            brandsHere[br] = 1;
          }
        });
      setBrands(
        Object.keys(brandsHere).map((b: any) => {
          if (b === "undefined") {
            return "Default";
          } else {
            return b;
          }
        })
      );
      setBrandFreq(Object.values(brandsHere));
    }
    if (transaction) {
      setGraphRevenueData(
        transaction
          ?.filter(
            (tx: any) => tx.txType === "CREDIT" && tx.screen === props?.screenId
          )
          ?.map((t: any) => t.amount)
      );
      setRevenueLabels(
        transaction
          ?.filter(
            (tx: any) => tx.txType === "CREDIT" && tx.screen === props?.screenId
          )
          ?.map((t: any) => convertIntoDateAndTime(t.updatedAt))
      );
      setCampaignRevenueLabels(
        campaigns
          ?.filter((camp: any) => {
            let txn = transaction
              ?.filter(
                (tx: any) =>
                  tx.txType === "CREDIT" && tx.screen === props?.screenId
              )
              ?.map((t: any) => t.campaign);
            if (txn.includes(camp._id)) {
              return camp;
            }
          })
          ?.map((c: any) => c.campaignName)
      );
    }
  }, [screenLogs, screenCamData, campaigns, props, transaction]);

  const audienceData: any = {
    labels: audienceLabels,
    datasets: [
      {
        type: "line",
        label: "Expected Exposure",
        borderColor: "rgb(255, 99, 132)",
        // borderWidth: 2,
        fill: true,
        backgroundColor: "rgba(255, 99, 133, 0.2)",
        data: graphAudienceData,
        lineTension: 0.3,
      },
      {
        type: "bar",
        // label: "Dataset 3",
        // backgroundColor: "rgb(53, 162, 235)",
        data: graphAudienceData,
        // barPercentage: "0.6",
        barThickness: "flex",
        borderRadius: 5,
        backgroundColor: function (context: any) {
          var chart = context.chart;
          var { ctx, chartArea } = chart;

          if (!chartArea) {
            // This case happens on initial chart load
            return null;
          }
          var gradient = ctx.createLinearGradient(0, 0, 0, chart.height);
          gradient.addColorStop(0, "rgba(153, 102, 255, 1)");
          gradient.addColorStop(0.5, "rgba(153, 102, 255, 0.7)");
          gradient.addColorStop(1, "rgba(255, 255, 255, 0.5");
          return gradient;
        },
      },
    ],
  };

  const audienceDataOptions: any = {
    responsive: true,
    plugins: {
      legend: {
        position: "top",
        display: false,
      },
      title: {
        display: true,
        text: "User Gain and Loss",
      },
    },
    scales: {
      x: {
        // Set the category percentage to 100%
        categoryPercentage: 100,
        stacked: true,
        // type: "date",
        ticks: {
          source: "auto",
          // Disabled rotation for performance
          maxRotation: 0,
          autoSkip: true,
        },
      },
      y: {
        // type: "linear",
        // beginAtZero: true,
        title: {
          text: "Expected Exposure",
          display: true,
        },
        // ticks: {
        //   source: "auto",
        //   callback: function (value: any) {
        //     if (Number.isInteger(value)) {
        //       return value;
        //     }
        //   },
        // },
        stacked: true,
      },
    },
  };

  const runtimeData: any = {
    labels: playbackLabels,
    datasets: [
      {
        type: "bar",
        // label: "Dataset 3",
        // backgroundColor: "rgb(53, 162, 235)",
        data: graphPlaybackData.map((d: any) => (d * 20) / 60 / 60),
        // barPercentage: "0.6",
        barThickness: "flex",
        borderRadius: 5,
        backgroundColor: function (context: any) {
          var chart = context.chart;
          var { ctx, chartArea } = chart;

          if (!chartArea) {
            // This case happens on initial chart load
            return null;
          }
          var gradient = ctx.createLinearGradient(0, 0, 0, chart.height);
          gradient.addColorStop(0, "rgba(153, 102, 255, 1)");
          gradient.addColorStop(0.5, "rgba(153, 102, 255, 0.7)");
          gradient.addColorStop(1, "rgba(255, 255, 255, 0.5");
          return gradient;
        },
      },
    ],
  };

  const runtimeDataOptions: any = {
    responsive: true,
    plugins: {
      legend: {
        position: "top",
        display: false,
      },
      title: {
        display: true,
        text: "User Gain and Loss",
      },
    },
    scales: {
      x: {
        // Set the category percentage to 100%
        categoryPercentage: 100,
        reverse: true,
      },
      y: {
        type: "linear",
        beginAtZero: true,
        title: {
          text: "Runtime in hours",
          display: true,
        },
        // ticks: {
        //   callback: function (value: any) {
        //     if (Number.isInteger(value)) {
        //       return value;
        //     }
        //   },
        // },
      },
    },
  };

  const playbackData = {
    labels: playbackLabels,
    datasets: [
      {
        fill: true,
        label: "Daily plaback",
        data: graphPlaybackData,
        borderColor: "rgb(74, 58, 235, 0.5)",
        borderWidth: 1,
        // backgroundColor: "rgba(53, 162, 235, 0.5)",
        lineTension: 0.2,
        backgroundColor: function (context: any) {
          var chart = context.chart;
          var { ctx, chartArea } = chart;

          if (!chartArea) {
            // This case happens on initial chart load
            return null;
          }
          var gradient = ctx.createLinearGradient(0, 0, 0, chart.height);
          gradient.addColorStop(0, "rgba(118, 93, 255)");
          gradient.addColorStop(0.5, "rgba(207, 202, 255, 0.5)");
          gradient.addColorStop(1, "rgba(255, 255, 255, 0.1");
          return gradient;
        },
      },
    ],
  };

  const playbackDataOptions: any = {
    responsive: true,
    plugins: {
      legend: {
        position: "top" as const,
        display: false,
      },
      title: {
        display: true,
        text: "Daily playback count",
      },
    },
    scales: {
      x: {
        reverse: true,
      },
    },
  };

  const brandData = {
    labels: brands,
    datasets: [
      {
        label: "Campaigns deployed",
        data: brandFreq,
        backgroundColor: poolColors(brands?.length),
        borderColor: poolColors(brands?.length).map((br: any) => {
          let regex = /(\d+(?:\.\d+)?)\)$/;
          return br.replace(regex, 0.3 + ")");
        }),
        // borderWidth: 1,
        hoverBackgroundColor: poolColors(brands?.length).map((br: any) => {
          let regex = /(\d+(?:\.\d+)?)\)$/;
          return br.replace(regex, 0.7 + ")");
        }),
        offset: 0, // add this prop
        hoverOffset: 50,
      },
    ],
  };

  const brandOptions = {
    cutout: 90,
    spacing: 1,
    borderWidth: 0,
    borderRadius: 5,
    plugins: {
      legend: {
        display: false,
        responsive: true,
        position: "bottom" as const,
        labels: {
          boxWidth: 36,
          padding: 10,
          font: {
            size: 15,
          },
        },
        // align: "center",
      },
    },
    layout: {
      padding: 25,
    },
  };

  const plugins = [
    {
      id: "plugins",
      beforeDatasetsDraw: function (chart: any) {
        const { ctx, data } = chart;
        const xCoor = chart?.getDatasetMeta(0)?.data[0]?.x;
        const yCoor = chart?.getDatasetMeta(0)?.data[0]?.y;

        ctx.save();
        ctx.font = "bolder 50px sans-serif";
        ctx.fillStyle = "black";
        ctx.textAlign = "center";
        ctx.textBaseline = "middle";
        ctx.fillText(data?.labels?.length, xCoor, yCoor - 15);

        ctx.font = "15px sans-serif";
        ctx.fillStyle = "black";
        ctx.fillText(`Advertisers`, xCoor, yCoor + 25);
      },
    },
  ];

  const revenueData: any = {
    labels: revenueLabels,
    datasets: [
      {
        type: "line",
        label: "Earned Revenue",
        borderColor: "rgb(204, 204, 255)",
        // borderWidth: 2,
        fill: true,
        backgroundColor: "rgb(204, 204, 255, 0.4)",
        data: graphRevenueData,
        lineTension: 0.3,
      },
    ],
  };

  // const revenueDataOptions: any = {
  //   responsive: true,
  //   plugins: {
  //     legend: {
  //       position: "top" as const,
  //       display: false,
  //     },
  //     title: {
  //       display: true,
  //       text: "User Gain and Loss",
  //     },
  //   },
  //   scales: {
  //     x: {
  //       // Set the category percentage to 100%
  //       categoryPercentage: 100,
  //       stacked: true,
  //       // type: "date",
  //       ticks: {
  //         source: "auto",
  //         // Disabled rotation for performance
  //         maxRotation: 0,
  //         autoSkip: true,
  //       },
  //     },
  //     y: {
  //       // type: "linear",
  //       // beginAtZero: true,
  //       title: {
  //         text: "Earned Revenue",
  //         display: true,
  //       },
  //       // ticks: {
  //       //   source: "auto",
  //       //   callback: function (value: any) {
  //       //     if (Number.isInteger(value)) {
  //       //       return value;
  //       //     }
  //       //   },
  //       // },
  //       stacked: true,
  //     },
  //   },
  // };

  const campaignRevenueData: any = {
    labels: campaignRevenueLabels,
    datasets: [
      {
        type: "bar",
        // label: "Dataset 3",
        // backgroundColor: "rgb(53, 162, 235)",
        data: graphRevenueData,
        // barPercentage: "0.6",
        barThickness: "flex",
        borderRadius: 5,
        backgroundColor: function (context: any) {
          var chart = context.chart;
          var { ctx, chartArea } = chart;

          if (!chartArea) {
            // This case happens on initial chart load
            return null;
          }
          var gradient = ctx.createLinearGradient(0, 0, 0, chart.height);
          gradient.addColorStop(0, "rgba(153, 102, 255, 1)");
          gradient.addColorStop(0.5, "rgba(153, 102, 255, 0.7)");
          gradient.addColorStop(1, "rgba(255, 255, 255, 0.5");
          return gradient;
        },
      },
    ],
  };

  const campaignRevenueOptions: any = {
    responsive: true,
    plugins: {
      legend: {
        position: "top",
        display: false,
      },
      title: {
        display: true,
        text: "User Gain and Loss",
      },
    },
    scales: {
      x: {
        // Set the category percentage to 100%
        categoryPercentage: 100,
        reverse: true,
      },
      y: {
        type: "linear",
        beginAtZero: true,
        title: {
          text: "Amount in credits",
          display: true,
        },
        // ticks: {
        //   callback: function (value: any) {
        //     if (Number.isInteger(value)) {
        //       return value;
        //     }
        //   },
        // },
      },
    },
  };

  return (
    <ScrollBox height="70vh">
      <Hide below="md">
        <Box p="2">
          <Flex px="5" gap={2}>
            <Stack p="" width="65%">
              <SimpleGrid gap={2} columns={[3]}>
                <Box
                  m="2"
                  p="5"
                  bgColor="#FFFFFF"
                  justifyContent="flex-start"
                  rounded="xl"
                >
                  <Text pb="0" fontSize="sm">
                    Statics
                  </Text>
                  <Text pt="0" fontSize="md" fontWeight="600">
                    Total Campaigns
                  </Text>
                  {loadingAllCampaign ? (
                    <Text>Loading</Text>
                  ) : errorAllCampaign ? (
                    <Text>Error: {errorAllCampaign}.</Text>
                  ) : (
                    <Stack>
                      <Text fontSize="2xl" fontWeight="600">
                        {campaigns?.length}
                      </Text>
                    </Stack>
                  )}
                  <Text>Campaigns</Text>
                </Box>
                <Box
                  m="2"
                  p="5"
                  bgColor="#FFFFFF"
                  justifyContent="flex-start"
                  rounded="xl"
                >
                  <Text pb="0" fontSize="sm">
                    Statics
                  </Text>
                  <Text pt="0" fontSize="md" fontWeight="600">
                    Total Revenue
                  </Text>
                  <Stack>
                    <Text fontSize="2xl" fontWeight="600">
                      {transaction
                        ?.filter((tx: any) => tx.screen === props?.screenId)
                        ?.map((t: any) => t.amount)
                        ?.reduce((a: any, b: any) => a + b, 0)}
                    </Text>
                  </Stack>
                  <Text>Credits</Text>
                </Box>
                <Box
                  m="2"
                  p="5"
                  bgColor="#FFFFFF"
                  justifyContent="flex-start"
                  rounded="xl"
                >
                  <Text pb="0" fontSize="sm">
                    Statics
                  </Text>
                  <Text pt="0" fontSize="md" fontWeight="600">
                    Total Enquiries
                  </Text>
                  <Stack>
                    <Text fontSize="2xl" fontWeight="600">
                      {props?.screen[0]?.pleas?.length}
                    </Text>
                  </Stack>
                  <Text>Requests</Text>
                </Box>
              </SimpleGrid>
              <Box
                m="2"
                p="5"
                bgColor="#FFFFFF"
                justifyContent="flex-start"
                rounded="lg"
              >
                {loadingLogs ? (
                  <Text>Loading...</Text>
                ) : errorLogs ? (
                  <Text>{errorLogs}</Text>
                ) : (
                  <Stack>
                    <Text fontSize="20px" fontWeight="600">
                      Screen Runtime
                    </Text>
                    <Chart
                      ref={chartRef}
                      type="bar"
                      data={runtimeData}
                      options={runtimeDataOptions}
                    />
                  </Stack>
                )}
              </Box>
              <Box
                m="2"
                p="5"
                bgColor="#FFFFFF"
                justifyContent="flex-start"
                rounded="lg"
              >
                {loadingCamData ? (
                  <Text>Loading...</Text>
                ) : errorCamData ? (
                  <Text>{errorCamData}</Text>
                ) : (
                  <Stack>
                    <Text fontSize="20px" fontWeight="600">
                      Expected Audience Exposure
                    </Text>
                    <Chart
                      ref={chartRef}
                      type="line"
                      data={audienceData}
                      options={audienceDataOptions}
                    />
                  </Stack>
                )}
              </Box>
              <Box
                m="2"
                p="5"
                bgColor="#FFFFFF"
                justifyContent="flex-start"
                rounded="lg"
              >
                <Stack>
                  <Text fontSize="20px" fontWeight="600">
                    Revenue Generated
                  </Text>
                  <Line options={playbackDataOptions} data={revenueData} />
                </Stack>
              </Box>
            </Stack>
            <Stack px="5" width="35%">
              <Box
                m="2"
                p="5"
                bgColor="#FFFFFF"
                justifyContent="flex-start"
                rounded="lg"
              >
                {loadingAllCampaign ? (
                  <Text>Loading...</Text>
                ) : errorAllCampaign ? (
                  <Text>Error: {errorAllCampaign}</Text>
                ) : (
                  <Stack p="2">
                    <Text fontSize="20px" fontWeight="600">
                      Campaigns Executed
                    </Text>
                    <ArcSlider data={campaigns} />
                    <Text m="-5" pb="5" textAlign="center">
                      Active Campaigns
                    </Text>
                  </Stack>
                )}
              </Box>
              <Box
                m="2"
                p="5"
                bgColor="#FFFFFF"
                justifyContent="flex-start"
                rounded="lg"
              >
                <Stack>
                  <Text fontSize="20px" fontWeight="600">
                    Daily playback count
                  </Text>
                  <Line options={playbackDataOptions} data={playbackData} />
                </Stack>
              </Box>
              <Box
                m="2"
                p="5"
                bgColor="#FFFFFF"
                justifyContent="flex-start"
                rounded="lg"
              >
                <Stack>
                  <Text fontSize="20px" fontWeight="600">
                    Revenue Generated
                  </Text>
                  <Chart
                    ref={chartRef}
                    type="bar"
                    data={campaignRevenueData}
                    options={campaignRevenueOptions}
                  />
                </Stack>
              </Box>
              <Box
                m="2"
                p="5"
                bgColor="#FFFFFF"
                justifyContent="flex-start"
                rounded="lg"
              >
                {loadingAllCampaign ? (
                  <Text>Loading...</Text>
                ) : errorAllCampaign ? (
                  <Text>Error: {errorAllCampaign}.</Text>
                ) : (
                  <Stack>
                    <Text fontSize="20px" fontWeight="600">
                      Total Advertisers
                    </Text>
                    {brands?.length > 0 && (
                      <Doughnut
                        data={brandData}
                        options={brandOptions}
                        plugins={plugins}
                      />
                    )}
                    <Stack>
                      {/* <Image src="" alt="" /> */}
                      <Text>Brands Name</Text>

                      {brands.map((b: any, index: any) => (
                        <Flex w="100%" justify="space-between" key={index}>
                          <Text>{b}</Text>
                          <Text>{brandFreq[index]}</Text>
                        </Flex>
                      ))}
                    </Stack>
                  </Stack>
                )}
              </Box>
            </Stack>
          </Flex>
        </Box>
      </Hide>
      <Show below="md">
        <Box
          m="2"
          p="5"
          bgColor="#FFFFFF"
          justifyContent="flex-start"
          rounded="xl"
        >
          <Text pb="0" fontSize="24px" color="red">
            To See Analytic, You have to use Desktop
          </Text>
        </Box>
      </Show>
    </ScrollBox>
  );
}
