import {
  InputGroup,
  Text,
  Box,
  Button,
  Stack,
  Input,
  Flex,
} from "@chakra-ui/react";
import {
  Checkbox,
  Modal,
  Radio,
  RadioChangeEvent,
  Select,
  message,
} from "antd";
import { useEffect, useState } from "react";
import { AiOutlineCloudUpload } from "react-icons/ai";
import { useDispatch } from "react-redux";
import { DateAndTimeInputField } from "../../components/newCommans";
import { uploadMedia } from "../../Actions/mediaActions";
import { MyButton } from "../../components/newCommans/MyButtons";
import { MdOutlineTimer } from "react-icons/md";
import { createCamapaign } from "../../Actions/campaignAction";
import { getNumberOfDaysBetweenTwoDates } from "../../utils/dateAndTimeUtils";
import {
  getVideoDurationFronVideoURL,
  isNumber,
} from "../../utils/utilityFunctions";
import { ShowMediaFile } from "../../components/newCommans/ShowMediaFile";

const options = [
  { label: "Image/Video", value: "Image/Video" },
  { label: "URL", value: "URL" },
];

export function CreateNewCampaignModel(props: any) {
  let hiddenInput: any = null;
  let { screenOptions, setCampaignData, defaultScreen, setIsLoading } = props;
  const dispatch = useDispatch<any>();
  const [campaignOption, setCampaignOption] = useState("Image/Video");

  const [selectedMedia, setSelectedMedia] = useState<any>("");
  const [selectedMediaType, setSelectedMediaType] = useState<any>();
  const [file, setFile] = useState<any>(null);
  const [url, setUrl] = useState<any>("");
  const [campaignName, setCampaignName] = useState<any>("");
  const [campaignDuration, setCampaignDuration] = useState<any>("");
  const [brandName, setBrandName] = useState<any>("");
  const [takeDateRange, setTakeDateRange] = useState<any>(false);
  const [selectedScreens, setSelectedScreens] = useState<any>([]);
  const [startDateAndTime, setStartDateAndTime] = useState<any>(null);
  const [endDateAndTime, setEndDateAndTime] = useState<any>(null);

  const handleSelectOption = ({ target: { value } }: RadioChangeEvent) => {
    if (value == "URL") {
      setFile(null);
      setSelectedMedia(null);
    } else {
      setUrl("");
    }
    setCampaignOption(value);
  };

  const validateSelectedFile = (file: any) => {
    // const MIN_FILE_SIZE = 1024; // 1MB
    let mb = 1000; // mb
    const MAX_FILE_SIZE = mb * 1000 * 1024; // 5MB
    const fileExtension = file.type.split("/")[1];
    const fileSizeKiloBytes = file.size; // convert to kb
    if (fileSizeKiloBytes > MAX_FILE_SIZE) {
      message.error(
        "File size is greater than maximum limit. File size must be less then 50 MB "
      );
      return false;
    }
    if (
      !(
        fileExtension === "mp4" ||
        fileExtension === "jpg" ||
        fileExtension === "jpeg" ||
        fileExtension === "png"
      )
    ) {
      message.error("File format must be .mp4 or jpg or jpeg and png");
      return false;
    } else {
      setSelectedMediaType(fileExtension);
    }
    return true;
  };

  useEffect(() => {
    setSelectedScreens([defaultScreen]);
  }, [defaultScreen]);

  const handelDiscard = () => {
    setSelectedMedia(null);
    setStartDateAndTime(null);
    setEndDateAndTime(null);
    setFile(null);
    setTakeDateRange(false);
    setSelectedScreens([]);
    setCampaignName("");
    setBrandName("");
    props.onCancel();
    setUrl("");
    setCampaignDuration("");
  };

  function isValidUrl(userInput: string) {
    var res = userInput.match(
      /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g
    );
    if (res == null) return false;
    else return true;
  }

  const validateForm = () => {
    if (campaignOption === "Image/Video" && !selectedMedia) {
      message.error("Please Select Media First");
      return false;
    } else if (campaignOption === "URL" && !isValidUrl(url)) {
      message.error("Please enter valid url");
      setUrl("");
      return false;
    } else if (!campaignName) {
      message.error("Please Enter campaign Name");
      return false;
    } else if (
      (campaignOption === "URL" || file?.type.split("/")[0] === "image") &&
      !campaignDuration
    ) {
      message.error("Please set campaign duration in sec");
      return false;
    } else if (
      (campaignOption === "URL" || file?.type.split("/")[0] === "image") &&
      !isNumber(campaignDuration)
    ) {
      message.error("Please Enter only number for campaign duration");
      setCampaignDuration("");
    } else if (!brandName) {
      message.error("Please Enter brand Name");
      return false;
    } else if (selectedScreens?.length === 0) {
      message.error("Please Select atleast one screen");
      return false;
    } else if (takeDateRange && (!startDateAndTime || !endDateAndTime)) {
      message.error("Please Select Date Range");
      return false;
    } else {
      return true;
    }
  };

  function handleFileSelect(file: any) {
    if (validateSelectedFile(file)) {
      const fileURL = URL.createObjectURL(file);
      setSelectedMedia(fileURL);
      if (file?.type?.split("/")[0] === "video") {
        getVideoDurationFronVideoURL(fileURL, (duration: any) => {
          setCampaignDuration(duration);
        });
      }
      setFile(file); // props
    }
  }

  const videoUploadHandler = async (e: any) => {
    setIsLoading(true);
    dispatch(
      uploadMedia({
        title: campaignName,
        // duration: campaignDuration,
        thumbnail:
          "https://bafybeicduvlghzcrjtuxkro7foazucvuyej25rh3humeujbzt7bmio4hsa.ipfs.w3s.link/raily.png",
        fileUrl: file,
        media: "",
      })
    );
  };

  const createCampaignFromURL = () => {
    setIsLoading(true);
    dispatch(
      createCamapaign({
        screenIds: selectedScreens,
        campaignName,
        brandName,
        isDefaultCampaign: !takeDateRange,
        startDateAndTime,
        endDateAndTime,
        noOfDays: getNumberOfDaysBetweenTwoDates(
          startDateAndTime,
          endDateAndTime
        ),
        mediaId: null,
        url: url,
        fileType: "url",
        duration: Number(campaignDuration),
        fileSize: 0,
      })
    );
  };

  const handleNext = (e: any) => {
    if (validateForm()) {
      setCampaignData({
        screenIds: selectedScreens,
        campaignName: campaignName,
        brandName: brandName,
        isDefaultCampaign: !takeDateRange,
        startDateAndTime,
        endDateAndTime,
        url: "",
        fileType: url?.length > 0 ? "url" : file?.type,
        duration: Number(campaignDuration),
        fileSize: file?.size,
      });
      if (url?.length > 0) {
        createCampaignFromURL();
      } else if (
        selectedMediaType === "mp4" ||
        file?.type.split("/")[0] === "image"
      ) {
        videoUploadHandler(e);
      }
      handelDiscard();
    }
  };

  return (
    <Modal
      centered
      open={props?.open}
      onCancel={() => props.onCancel()}
      footer={[]}
      closable={false}
      maskClosable={false}
    >
      <Box px="2">
        <Flex gap="4" flexDir="column">
          <Flex flexDir="column" width="100%" gap="1">
            <Radio.Group
              options={options}
              onChange={handleSelectOption}
              value={campaignOption}
            />
          </Flex>
          {campaignOption === "URL" ? (
            <Flex flexDir="column" width="100%" gap="1">
              <Text fontSize="md" color="#131D30" fontWeight="400" m="0">
                Media URL
              </Text>
              <Input
                width={"100%"}
                height={"45px"}
                placeholder="Enter valid url"
                size="lg"
                type="url"
                fontSize="md"
                borderRadius="md"
                border="1px"
                borderColor="#DADADA"
                color="#555555"
                py="2"
                value={url}
                onChange={(e) => setUrl(e.target.value)}
              />
            </Flex>
          ) : selectedMedia ? (
            <Box>
              <Flex justify="space-between" gap="">
                <Text
                  color="#333333"
                  fontSize={{ base: "xl", lg: "2xl" }}
                  fontWeight="semibold"
                  align="left"
                  m="0"
                >
                  Uploaded media
                </Text>
                <Button
                  type="submit"
                  color="#111111"
                  variant="outline"
                  bgColor="#FFFFFF"
                  _hover={{ color: "#FFFFFF", bgColor: "#111111" }}
                  py="2"
                  fontWeight="600"
                  fontSize={{ base: "sm", lg: "md" }}
                  onClick={() => {
                    setSelectedMedia(null);
                    setFile(null);
                  }}
                >
                  Reset
                </Button>
              </Flex>
              <ShowMediaFile
                url={selectedMedia || url}
                mediaType={file?.type?.split("/")[0] || "url"}
                height="250px"
                width="400px"
              />
            </Box>
          ) : (
            <Box>
              <Text
                color="#000000"
                fontSize={{ base: "lg", lg: "lg" }}
                fontWeight="600"
              >
                Upload content
              </Text>
              <InputGroup>
                <Stack
                  height="112px"
                  width="100%"
                  border="1px"
                  borderRadius="16px"
                  align="center"
                  borderColor="#E4E4E4"
                  onClick={() => hiddenInput.click()}
                >
                  {/* <Stack border="1px solid red" align="center"> */}
                  <Button
                    leftIcon={<AiOutlineCloudUpload size="20px" />}
                    py="2"
                    bgColor="#0EBCF5"
                    borderRadius="20px"
                    fontSize="md"
                    my="9"
                  >
                    Upload a photo or video
                  </Button>
                  {/* </Stack> */}

                  <Input
                    hidden
                    type="file"
                    ref={(el) => (hiddenInput = el)}
                    // accept="image/png, image/jpeg"

                    onChange={(e: any) => handleFileSelect(e.target.files[0])}
                  />
                </Stack>
              </InputGroup>
              <Text
                color="red"
                align="right"
              >{`Max file size less then 50 MB`}</Text>
            </Box>
          )}

          <Flex w="100%" gap="2">
            <Flex flexDir="column" width="100%" gap="1">
              <Text fontSize="md" color="#131D30" fontWeight="400" m="0">
                Campaign Name{" "}
              </Text>
              <Input
                width={campaignOption !== "Image/Video" ? "90%" : "100%"}
                height={"45px"}
                placeholder="campaign name"
                size="lg"
                fontSize="md"
                borderRadius="md"
                border="1px"
                borderColor="#DADADA"
                color="#555555"
                py="2"
                value={campaignName}
                onChange={(e) => setCampaignName(e.target.value)}
              />
            </Flex>
            {(file?.type?.split("/")[0] === "image" ||
              campaignOption === "URL") && (
              <Flex flexDir="column" width="20%" gap="1">
                {/* <Text fontSize="md" color="#131D30" fontWeight="400" m="0">
                  Duration{" "}
                </Text> */}
                <Stack p="0.5" align="center" w="100%">
                  <MdOutlineTimer fontSize="20px" />
                </Stack>
                <Input
                  height={"45px"}
                  size="lg"
                  fontSize="md"
                  borderRadius="md"
                  border="1px"
                  borderColor="#DADADA"
                  color="#555555"
                  py="2"
                  value={campaignDuration}
                  onChange={(e) => setCampaignDuration(e.target.value)}
                />
              </Flex>
            )}
          </Flex>
          <Flex flexDir="column" width="100%" gap="1">
            <Text fontSize="md" color="#131D30" fontWeight="400" m="0">
              Brand Name{" "}
            </Text>
            <Input
              width="100%"
              height={"45px"}
              placeholder="Brand name of the video"
              size="lg"
              fontSize="md"
              borderRadius="md"
              border="1px"
              borderColor="#DADADA"
              color="#555555"
              py="2"
              value={brandName}
              onChange={(e) => setBrandName(e.target.value)}
            />
          </Flex>
          <Flex flexDir="column" width="100%" gap="1">
            <Text fontSize="md" color="#131D30" fontWeight="400" m="0">
              Select Screens
            </Text>
            <Select
              size="large"
              mode="multiple"
              value={selectedScreens}
              onChange={(value) => setSelectedScreens(value)}
              style={{ width: "100%", height: "45px" }}
              options={screenOptions}
              showSearch
              placeholder="Search to Select"
              optionFilterProp="children"
              maxTagCount="responsive"
              filterOption={(input: any, option: any) =>
                (option?.label ?? "").includes(input?.toUpperCase())
              }
              filterSort={(optionA: any, optionB: any) =>
                (optionA?.label ?? "")
                  .toLowerCase()
                  .includes((optionB?.label ?? "").toLowerCase())
              }
            />
          </Flex>

          <Checkbox
            checked={takeDateRange ? takeDateRange : false}
            onChange={(e) => setTakeDateRange(e.target.checked)}
          >
            Do You Want To Add Date Range
          </Checkbox>
          {takeDateRange ? (
            <Flex flexDir="column" gap="1">
              <Text m="0" fontSize="16px" fontWeight="400" color="#131D30">
                Start Date & Time
              </Text>
              <DateAndTimeInputField
                value={startDateAndTime}
                onChange={(value: any) => {
                  setStartDateAndTime(value);
                }}
              />
            </Flex>
          ) : null}
          {takeDateRange ? (
            <Flex flexDir="column" gap="1">
              <Text m="0" fontSize="16px" fontWeight="400" color="#131D30">
                End Date & Time
              </Text>
              <DateAndTimeInputField
                value={endDateAndTime}
                onChange={(value: any) => {
                  setEndDateAndTime(value);
                }}
              />
            </Flex>
          ) : null}
        </Flex>

        <Flex justifyContent="space-between" pt="10">
          <MyButton
            handelClick={handelDiscard}
            label="Cancel"
            color="#FF2E2E"
            bgColor="#FFFFFF"
          />
          <MyButton
            handelClick={handleNext}
            label="Upload"
            color="#111111"
            bgColor="#FFFFFF"
          />
        </Flex>
      </Box>
    </Modal>
  );
}
