import { Box, HStack, Image, Text } from "@chakra-ui/react";
import Axios from "axios";
import React, { useEffect, useState } from "react";
import ReactMapGL, { Marker, Popup } from "react-map-gl";
import { useNavigate } from "react-router-dom";
import { RiMapPinFill } from "react-icons/ri";
import { message } from "antd";

// // The following is required to stop "npm build" from transpiling mapbox code.
// // notice the exclamation point in the import.
// // @ts-ignore
// // eslint-disable-next-line import/no-webpack-loader-syntax, import/no-unresolved
// mapboxgl.workerClass =
//   require("worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker").default;

export function ShowScreensLocationOnMap(props: any) {
  const navigate = useNavigate();
  const listOfScreens = props?.data;
  const [viewState, setViewState] = useState({
    longitude: props
      ? props?.data?.features.filter(
          (pinData: any) => pinData.properties.screen === props?.screen?._id
        )[0]?.geometry?.coordinates[1]
      : 85,
    latitude: props
      ? props?.data?.features.filter(
          (pinData: any) => pinData.properties.screen === props?.screen?._id
        )[0]?.geometry?.coordinates[0]
      : 25,
    zoom: 14,
  });
  const [screenData, setScreenData] = useState<any>(null);
  const [viewSingleScreen, setViewSingleScreen] = useState<any>(false);

  const getSingleScreenData = async (e: any, screenId: any, pinData: any) => {
    try {
      const { data } = await Axios.get(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/screens/${screenId}`
      );
      setScreenData(data);
      setViewSingleScreen(pinData);
    } catch (error: any) {
      message.error(error);
    }
  };

  useEffect(() => {
    if (props) {
      setViewState({
        longitude: props?.data?.features.filter(
          (pinData: any) => pinData.properties.screen === props?.screen?._id
        )[0]?.geometry?.coordinates[1],
        latitude: props?.data?.features.filter(
          (pinData: any) => pinData.properties.screen === props?.screen?._id
        )[0]?.geometry?.coordinates[0],
        zoom: 14,
      });
    }
  }, [props]);

  return (
    <Box
      height={"100%"}
      width="100%"
      color="black.500"
      borderRadius="18px"
      // border="1px solid red"
      overflow="hidden"
    >
      <ReactMapGL
        initialViewState={viewState}
        mapStyle="mapbox://styles/vviicckkyy55/cliox9jwm00q001pg18ppcsq3"
        // mapStyle="https://cdn.jsdelivr.net/gh/osm-in/mapbox-gl-styles@master/osm-mapnik-india-v8.json"
        mapboxAccessToken={
          process.env.REACT_APP_MAPBOX ||
          "pk.eyJ1IjoidnZpaWNja2t5eTU1IiwiYSI6ImNrdW5zaGU3czI0Y3gyeG42YnYxczl1aGQifQ.dxSrDMAyf8hfCrM5WMFnEw"
        }
        onMove={(e) => setViewState(e.viewState)}
        onDblClick={(e) => {
          props?.setLocation({ lat: e.lngLat.lat, lng: e.lngLat.lng });
        }}
        {...viewState}
        // width="100vw"
        // height="100vh"
      >
        {listOfScreens &&
          listOfScreens?.features?.map((singleData: any, index: any) => (
            <Marker
              key={index}
              latitude={singleData?.geometry?.coordinates[0]}
              longitude={singleData?.geometry?.coordinates[1]}
            >
              <RiMapPinFill
                size="40"
                color={
                  singleData.properties.screen === props?.screen?._id
                    ? "#F94449"
                    : "#3BB143"
                }
                onClick={(e) => {
                  getSingleScreenData(
                    e,
                    singleData.properties.screen,
                    singleData
                  );
                }}
              />

              {/* <Image
                src={mapIcon}
                color={"#ffffff"}
                alt="mapIcon"
                onClick={(e) => {
                  getSingleScreenData(
                    e,
                    singleData.properties.screen,
                    singleData
                  );
                }}
              /> */}
            </Marker>
          ))}

        {viewSingleScreen && screenData ? (
          <Popup
            className="map"
            latitude={viewSingleScreen.geometry.coordinates[0]}
            longitude={viewSingleScreen.geometry.coordinates[1]}
            onClose={() => setViewSingleScreen(null)}
            anchor="left"
            closeButton={false}
            focusAfterOpen={true}
          >
            <Box
              border="1px solid #2BB3E0"
              borderRadius="15px"
              bgGradient={[
                "linear-gradient(156.06deg, rgba(255, 255, 255) -1.7%, rgba(255, 255, 255) 102.25%)",
              ]}
              p="3"
              m="-10"
              onClick={() =>
                navigate(`/screen/screenDetail/${screenData?._id}`)
              }
            >
              <HStack>
                <Image
                  width="90px"
                  height="100%"
                  src={screenData?.image}
                  alt="screen image"
                  borderRadius="15px"
                />

                <Box alignItems="left">
                  <Text
                    color="#000000"
                    fontSize="14px"
                    fontWeight="bold"
                    align="left"
                    m="0"
                  >
                    {screenData?.name}
                  </Text>

                  <Text
                    m="0"
                    color="#7D7D7D"
                    fontSize="10px"
                    fontWeight="semibold"
                    align="left"
                  >
                    {screenData?.districtCity}
                  </Text>
                  <Text color="#000000" fontSize="10px" align="left" m="0">
                    Start from:
                  </Text>
                  <Text
                    color="#0EBCF5"
                    fontSize="11px"
                    fontWeight="semibold"
                    align="left"
                    m="0"
                  >
                    {`₹${screenData?.rentPerSlot}/ per slot*`}
                  </Text>
                </Box>
              </HStack>
            </Box>
          </Popup>
        ) : null}
      </ReactMapGL>
    </Box>
  );
}
