import { Button, Form, Input, Modal, message } from "antd";
import { Box } from "@chakra-ui/react";
import React from "react";
import Axios from "axios";
import { useSelector } from "react-redux";

export function RechargeModel(props: any) {
  const [form] = Form.useForm();
  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
  };

  const openPhonePayPage = async (values: any) => {
    try {
      const { data } = await Axios.post(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/userWallet/paymentHandler?amount=${values.amount}&userId=${userInfo?._id}`
      );
      // console.log("data : ", JSON.stringify(data));
      window.open(data.url);
    } catch (error) {
      // console.log("Error : ", JSON.stringify(error));
      message.error(`Something went wrong, ${error}`);
    }
  };
  const onFinish = (values: any) => {
    openPhonePayPage(values);
  };
  return (
    <Modal
      title="Recharge Your Wallet"
      open={props?.open}
      onCancel={() => props.onCancel()}
      footer={[]}
      style={{ top: 100 }}
    >
      <Box px="20" pt="10">
        <Form
          form={form}
          layout="vertical"
          name="control-hooks"
          onFinish={onFinish}
          style={{ maxWidth: 600 }}
        >
          <Form.Item
            name="amount"
            label="Enter Amount To Recharge"
            rules={[{ required: true }]}
          >
            <Input
              type="number"
              placeholder="Enter amount"
              size="large"
              style={{ width: "300px" }}
            />
          </Form.Item>
          <Form.Item {...tailLayout}>
            <Button type="primary" htmlType="submit">
              Recharge
            </Button>
          </Form.Item>
        </Form>
      </Box>
    </Modal>
  );
}
