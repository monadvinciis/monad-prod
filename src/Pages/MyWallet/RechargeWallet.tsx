import { Box, Flex, Text, Stack } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import {
  getUserWalletBalance,
  getUserWalletTransaction,
} from "../../Actions/walletAction";
import {
  CREATE_USER_WALLET_RESET,
  GET_TRANSACTION_LIST_RESET,
  GET_WALLET_BALANCE_RESET,
} from "../../Constants/walletConstants";
import { message } from "antd";
import { TiFlashOutline } from "react-icons/ti";
import { GiWallet } from "react-icons/gi";
import { TransactionList } from "../../components/newCommans";
import { RechargeModel } from "./RechargeModel";
import { MyButton } from "../../components/newCommans/MyButtons";
import { getAllPendingCampaignsByUserId } from "../../Actions/campaignAction";
import { GET_ALL_PENDING_CAMPAIGNS_BY_USER_ID_RESET } from "../../Constants/campaignConstants";

export function RechargeWallet(props: any) {
  const dispatch = useDispatch<any>();
  const [openModel, setOpenModel] = useState<any>(false);
  const [pendingCampaignAmount, setPendingCampaignAmount] = useState<number>(0);

  const createWallet = useSelector((state: any) => state.createWallet);
  const {
    // loading: loadingCretaeWallet,
    success: successCretaeWallet,
    error: errorCreateWallet,
  } = createWallet;

  const walletTransaction = useSelector(
    (state: any) => state.walletTransaction
  );
  const {
    loading: loadingWalletTrasaction,
    error: errroWalletTransaction,
    // success: successWalletTransaction,
    transaction,
  } = walletTransaction;
  const walletBalance = useSelector((state: any) => state.walletBalance);

  const {
    // loading: loadingWalletBalance,
    error: errorWalletBalance,
    // success: successWalletBalance,
    wallet,
  } = walletBalance;

  const allPendingCampaigns = useSelector(
    (state: any) => state.allPendingCampaigns
  );

  const {
    // loading: loadingPendingCampaigns,
    error: errorPendingCampaigns,
    campaigns,
  } = allPendingCampaigns;

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  useEffect(() => {
    if (userInfo?.userWallet) {
      dispatch(getUserWalletBalance());
      dispatch(getUserWalletTransaction());
      dispatch(getAllPendingCampaignsByUserId());
    }
  }, [userInfo, dispatch]);

  useEffect(() => {
    if (successCretaeWallet) {
      dispatch({ type: CREATE_USER_WALLET_RESET });
      dispatch(getUserWalletBalance());
      message.success("User wallet cretaed successfull");
    }
    if (errorCreateWallet) {
      dispatch({ type: CREATE_USER_WALLET_RESET });
      message.error(errorCreateWallet);
    }
    if (errorWalletBalance) {
      message.error(errorWalletBalance);
      dispatch({ type: GET_WALLET_BALANCE_RESET });
    }

    if (errroWalletTransaction) {
      message.error(errroWalletTransaction);
      dispatch({ type: GET_TRANSACTION_LIST_RESET });
    }
    if (errorPendingCampaigns) {
      message.error(errorPendingCampaigns);
      dispatch({ type: GET_ALL_PENDING_CAMPAIGNS_BY_USER_ID_RESET });
    }
    if (campaigns?.length > 0) {
      const amount = campaigns?.reduce(
        (accum: any, campaign: any) => accum + Number(campaign?.vault),
        0
      );
      setPendingCampaignAmount(amount);
    }
  }, [
    successCretaeWallet,
    errorCreateWallet,
    errroWalletTransaction,
    errorWalletBalance,
    errorPendingCampaigns,
    campaigns,
    dispatch,
  ]);

  return (
    <Stack width="100%" overflowY={"auto"} px="2">
      <RechargeModel open={openModel} onCancel={() => setOpenModel(false)} />
      <Box
        px="10"
        py="6"
        bgColor="#FFFFFF"
        width="100%"
        // border="1px"
        borderColor="#E1E1E1"
        gap="7"
      >
        <Flex justifyContent="space-between">
          <Flex align="center" gap="4">
            <Flex flexDir="column">
              <Text color="#000000" fontSize="16px" fontWeight="400" m="0">
                Pending Cost
              </Text>
              <Text color="yellow" fontSize="32px" fontWeight="700" m="0">
                Rs. {pendingCampaignAmount}
              </Text>
            </Flex>
            <Flex flexDir="column">
              <Text color="#000000" fontSize="16px" fontWeight="400" m="0">
                Current Balance
              </Text>
              <Text color="#000000" fontSize="32px" fontWeight="700" m="0">
                Rs. {(wallet?.balance - pendingCampaignAmount)?.toFixed(2)}
              </Text>
            </Flex>
          </Flex>
          <Flex gap="4">
            <MyButton
              icon={<TiFlashOutline size="20px" />}
              label="Recharge"
              color="#00BC84"
              bgColor="#FFFFFF"
              handelClick={() => setOpenModel(true)}
            />
            <MyButton icon={<GiWallet size="20px" />} label="Withdraw" />
          </Flex>
        </Flex>
      </Box>
      <Flex
        flexGrow={1}
        overflow={"hidden"}
        flexDir={"column"}
        p="10"
        bgColor="#FFFFFF"
        width="100%"
        // border="1px"
        borderColor="#E1E1E1"
        gap="7"
      >
        <Text color="#000000" fontSize="20px" fontWeight="400">
          Recent Transaction
        </Text>
        <TransactionList
          data={transaction?.slice(0, 5)}
          loading={loadingWalletTrasaction}
        />
      </Flex>
    </Stack>
  );
}
