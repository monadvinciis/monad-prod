import { Box, Flex, Text, Stack } from "@chakra-ui/react";
// import { MainMenu } from "../../../components/newCommans";
import { Mytabs } from "../../components/newCommans";
import { useEffect, useState } from "react";
import { RechargeWallet } from "./RechargeWallet";
import { TransactionHistory } from "./TransactionHistory";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

const options = ["Wallet", "History"];

export function MyWallet(props: any) {
  const [selectedTab, setSelectedTab] = useState<any>(1);
  const navigate = useNavigate();
  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  useEffect(() => {
    if (!userInfo) {
      navigate("/signin", { state: { path: "/my/campaigns" } });
    }
  }, [userInfo]);

  return (
    <Box
      width="100%"
      height={"100%"}
      // overflow={"hidden"}
      // flexGrow={1}
      bgColor="#F5F5F5"
      m="0"
    >
      <Flex
        width="100%"
        p="3"
        bgColor="#FFFFFF"
        borderTop="1px"
        borderLeft="1px"
        // border="1px solid green"
        borderColor="#EBEBEB"
      >
        <Text px="2" fontWeight="600" fontSize="2xl" m="0">
          Wallet
        </Text>
      </Flex>
      <Flex
        width="100%"
        borderTop="1px"
        borderLeft="1px"
        // border="1px solid green"
        borderColor="#EBEBEB"
      >
        <Mytabs
          selectedTab={selectedTab}
          setSelectedTab={setSelectedTab}
          options={options}
        />
      </Flex>
      <Flex width="100%" height="83%" p="4">
        <Stack width="100%" height="100%" pb="2" pr="10">
          {selectedTab === 1 ? (
            <RechargeWallet />
          ) : selectedTab === 2 ? (
            <TransactionHistory />
          ) : null}
        </Stack>
      </Flex>
    </Box>
  );
}
