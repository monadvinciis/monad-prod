import { Flex, Text } from "@chakra-ui/react";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import {
  getUserWalletBalance,
  getUserWalletTransaction,
} from "../../Actions/walletAction";
import { GET_TRANSACTION_LIST_RESET } from "../../Constants/walletConstants";
import { message } from "antd";
import { TransactionList } from "../../components/newCommans";
const pagination = { pageSize: 6 };

export function TransactionHistory(props: any) {
  const dispatch = useDispatch<any>();
  const walletTransaction = useSelector(
    (state: any) => state.walletTransaction
  );

  const {
    loading: loadingWalletTrasaction,
    error: errroWalletTransaction,
    // success: successWalletTransaction,
    transaction,
  } = walletTransaction;

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  useEffect(() => {
    if (userInfo?.userWallet) {
      dispatch(getUserWalletBalance());
      dispatch(getUserWalletTransaction());
    }
  }, [userInfo, dispatch]);

  useEffect(() => {
    if (errroWalletTransaction) {
      message.error(errroWalletTransaction);
      dispatch({ type: GET_TRANSACTION_LIST_RESET });
    }
  }, [errroWalletTransaction, dispatch]);

  return (
    <Flex
      px="10"
      py="6"
      overflow={"hidden"}
      flexDir="column"
      bgColor="#FFFFFF"
      width="100%"
      borderColor="#E1E1E1"
      gap="7"
    >
      <Text color="#000000" fontSize="20px" fontWeight="400">
        All Transaction
      </Text>
      <TransactionList
        data={transaction}
        loading={loadingWalletTrasaction}
        pagination={pagination}
      />
    </Flex>
  );
}
