import { Box, HStack, Image, Text } from "@chakra-ui/react";
import Axios from "axios";
import React, { useEffect, useState, useCallback } from "react";
import { mapIcon } from "../../assets/svg";
import ReactMapGL, {
  GeolocateControl,
  NavigationControl,
  Marker,
  Popup,
} from "react-map-gl";
import { useNavigate } from "react-router-dom";
import DrawControl from "./DrawControl";
import { isPointInsidePolygon } from "../../utils";
// import { CiMapPin } from "react-icons/ci";

export function ShowScreensLocation(props: any) {
  const navigate = useNavigate();
  const listOfScreens = props?.data;
  // console.log("listOfScreens : ", JSON.stringify(listOfScreens.features));
  // console.log("geometry : ", props.geometry);

  const [viewport, setViewport] = useState({
    longitude: props?.data[0].lng || 85,
    latitude: props?.data[0].lat || 25,
    zoom: props?.zoom || 10,
  });
  const [screenData, setScreenData] = useState<any>(null);
  const [viewSingleScreen, setViewSingleScreen] = useState<any>(false);

  const [features, setFeatures] = useState({});

  useEffect(() => {
    if (listOfScreens?.length > 0) {
      // setPinScreen(screens[selectedBox]);

      const data = listOfScreens?.map((screen: any) => {
        return {
          type: "Feature",
          properties: {
            screen: screen?._id,
          },
          geometry: {
            coordinates: [screen?.lat, screen?.lng],
            type: "Point",
          },
        };
      });
    }
  }, [listOfScreens]);

  const getSingleScreenData = async (e: any, screenId: any, pinData: any) => {
    try {
      const { data } = await Axios.get(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/screens/${screenId}`
      );
      setScreenData(data);
      setViewSingleScreen([pinData?.lat, pinData?.lng]);
    } catch (error) {
      console.log(error);
    }
  };

  const onUpdate = useCallback(
    (e: any) => {
      setFeatures((currFeatures: any) => {
        const newFeatures = { ...currFeatures };

        for (const f of e.features) {
          newFeatures[f.id] = f;
        }
        var mainPoints: any = [];
        for (var i = 0; i < Object.values(newFeatures)?.length; i++) {
          // console.log(Object.values(newFeatures)[i]);
          const featValue: any = Object.values(newFeatures)[i];
          const mapValue: any = listOfScreens.map((screen: any) => [
            screen?.lng,
            screen?.lat,
          ]);
          // console.log(JSON.stringify(featValue?.geometry?.coordinates[0]));
          // console.log(JSON.stringify(mapValue));
          for (const s of mapValue) {
            if (
              isPointInsidePolygon(s, featValue?.geometry?.coordinates[0]) &&
              !mainPoints.includes(
                listOfScreens.filter(
                  (screen: any) => screen.lng === s[0] && screen.lat === s[1]
                )[0]
              )
            ) {
              // console.log(
              //   isPointInsidePolygon(s, featValue?.geometry?.coordinates[0])
              // );
              mainPoints.push(
                listOfScreens.filter(
                  (screen: any) => screen.lng === s[0] && screen.lat === s[1]
                )[0]
              );
            }
          }
        }

        // console.log(mainPoints);
        props?.setSelectedScreen(mainPoints);

        return newFeatures;
      });
    },
    [listOfScreens, props]
  );

  const onDelete = useCallback(
    (e: any) => {
      setFeatures((currFeatures: any) => {
        const newFeatures = { ...currFeatures };
        for (const f of e.features) {
          delete newFeatures[f.id];
        }
        props?.setSelectedScreen([]);
        return newFeatures;
      });
    },
    [props]
  );

  return (
    <Box height="100%" width="100%" color="black.500">
      <ReactMapGL
        {...viewport}
        initialViewState={viewport}
        // onVi
        mapStyle="mapbox://styles/vviicckkyy55/cliox9jwm00q001pg18ppcsq3"
        // mapStyle="https://cdn.jsdelivr.net/gh/osm-in/mapbox-gl-styles@master/osm-mapnik-india-v8.json"
        mapboxAccessToken={
          process.env.REACT_APP_MAPBOX ||
          "pk.eyJ1IjoidnZpaWNja2t5eTU1IiwiYSI6ImNrdW5zaGU3czI0Y3gyeG42YnYxczl1aGQifQ.dxSrDMAyf8hfCrM5WMFnEw"
        }
        onMove={(e) => setViewport(e.viewState)}
        // onClick={(e) => {
        //   console.log(e);
        //   // setDrawPoints()
        // }}
      >
        <Box style={{ position: "absolute", right: 10, top: 10 }}>
          <NavigationControl />
          <GeolocateControl
            positionOptions={{ enableHighAccuracy: true }}
            trackUserLocation
          />
        </Box>
        <DrawControl
          position="top-left"
          displayControlsDefault={false}
          controls={{
            polygon: true,
            trash: true,
          }}
          defaultMode="draw_polygon"
          onCreate={onUpdate}
          onUpdate={onUpdate}
          onDelete={onDelete}
        />

        {listOfScreens &&
          listOfScreens?.map((singleData: any, index: any) => (
            <Marker
              key={index}
              latitude={singleData?.lat}
              longitude={singleData?.lng}
            >
              <Image
                src={mapIcon}
                alt="mapIcon"
                onClick={(e) => {
                  getSingleScreenData(e, singleData._id, singleData);
                }}
              />
            </Marker>
          ))}

        {viewSingleScreen && screenData ? (
          <Popup
            className="map"
            latitude={viewSingleScreen[0]}
            longitude={viewSingleScreen[1]}
            onClose={() => setViewSingleScreen(null)}
            anchor="left"
            closeButton={false}
            focusAfterOpen={true}
          >
            <Box
              border="1px solid #2BB3E0"
              borderRadius="15px"
              bgGradient={[
                "linear-gradient(156.06deg, rgba(255, 255, 255) -1.7%, rgba(255, 255, 255) 102.25%)",
              ]}
              p="3"
              m="-10"
              onClick={() => navigate(`/screenDetails/${screenData?._id}`)}
            >
              <HStack>
                <Image
                  width="90px"
                  height="100%"
                  src={screenData?.image}
                  alt="screen image"
                  borderRadius="15px"
                />

                <Box alignItems="left">
                  <Text
                    color="#000000"
                    fontSize="14px"
                    fontWeight="bold"
                    align="left"
                    m="0"
                  >
                    {screenData?.name}
                  </Text>

                  <Text
                    m="0"
                    color="#7D7D7D"
                    fontSize="10px"
                    fontWeight="semibold"
                    align="left"
                  >
                    {screenData?.districtCity}
                  </Text>
                  <Text color="#000000" fontSize="10px" align="left" m="0">
                    Start from:
                  </Text>
                  <Text
                    color="#0EBCF5"
                    fontSize="11px"
                    fontWeight="semibold"
                    align="left"
                    m="0"
                  >
                    {`₹${screenData?.rentPerSlot}/ per slot*`}
                  </Text>
                </Box>
              </HStack>
            </Box>
          </Popup>
        ) : null}
      </ReactMapGL>
    </Box>
  );
}
