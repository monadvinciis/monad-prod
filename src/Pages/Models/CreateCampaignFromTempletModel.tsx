import { Text, Box, Input, Flex, Stack } from "@chakra-ui/react";
import { Checkbox, Modal, Select, message } from "antd";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { DateAndTimeInputField } from "../../components/newCommans";
import { MyButton } from "../../components/newCommans/MyButtons";
import { createCamapaign } from "../../Actions/campaignAction";
import { getNumberOfDaysBetweenTwoDates } from "../../utils/dateAndTimeUtils";
import { MdOutlineTimer } from "react-icons/md";
import {
  getFileTypeAndFileSizeFromURL,
  getVideoDurationFronVideoURL1,
} from "../../utils/utilityFunctions";
import { ShowMediaFile } from "../../components/newCommans/ShowMediaFile";

export function CreateCampaignFromTempletModel(props: any) {
  let { campaign, screens } = props;
  const dispatch = useDispatch<any>();

  const [campaignName, setCampaignName] = useState<any>("");
  const [brandName, setBrandName] = useState<any>("");
  const [takeDateRange, setTakeDateRange] = useState<any>(false);
  const [selectedScreens, setSelectedScreens] = useState<any>([]);
  const [startDateAndTime, setStartDateAndTime] = useState<any>(null);
  const [endDateAndTime, setEndDateAndTime] = useState<any>(null);
  const [screenOptions, setScreenOptions] = useState<any>([]);
  const [campaignDuration, setCampaignDuration] = useState<any>("");
  const [fileType, setFileType] = useState<any>("");
  const [fileSize, setFileSize] = useState<any>(0);

  // console.log("wewewe : ", campaignName, fileType, campaignDuration, fileSize);
  const updateDefaultVAlue = async () => {
    try {
      setCampaignName(campaign?.campaignName);
      setBrandName(campaign?.brandName);
      // if (campaign?.duration >= 0 && campaign?.fileType && campaign.fileSize) {
      //   setCampaignDuration(campaign.duration);
      //   setFileSize(campaign.fileSize);
      //   setFileType(campaign.fileType);
      // }
      if (campaign?.duration) {
        setCampaignDuration(campaign.duration);
      }
      if (campaign?.fileType !== "URl" || campaign.typ) {
        let data = await getFileTypeAndFileSizeFromURL(campaign?.video);
        setFileSize(data?.size);
        setFileType(data?.type);
        if (data?.type?.split("/")[0] === "video") {
          let duration = await getVideoDurationFronVideoURL1(campaign?.video);
          setCampaignDuration(duration);
        }
      }
    } catch (error) {
      message.error("Please try again");
      props.onCancel();
    }
  };

  useEffect(() => {
    if (campaign) updateDefaultVAlue();
  }, [campaign]);

  const handelDiscard = () => {
    setStartDateAndTime(null);
    setEndDateAndTime(null);
    setTakeDateRange(false);
    setSelectedScreens([]);
    setCampaignName("");
    setBrandName("");
    props.onCancel();
  };

  const validateForm = () => {
    if (!campaignName) {
      message.error("Please Enter campaign Name");
      return false;
    } else if (fileType?.split("/")[0] !== "video" && !campaignDuration) {
      message.error("Please set campaign duration in sec");
      return false;
    } else if (!brandName) {
      message.error("Please Enter brand Name");
      return false;
    } else if (selectedScreens?.length === 0) {
      message.error("Please Select atleast one screen");
      return false;
    } else if (takeDateRange && (!startDateAndTime || !endDateAndTime)) {
      message.error("Please Select Date Range");
      return false;
    } else {
      return true;
    }
  };

  const createCampaignFromURL = () => {
    // console.log("create campaign from url");
    dispatch(
      createCamapaign({
        screenIds: selectedScreens,
        campaignName,
        brandName,
        isDefaultCampaign: !takeDateRange,
        startDateAndTime,
        endDateAndTime,
        noOfDays: getNumberOfDaysBetweenTwoDates(
          startDateAndTime,
          endDateAndTime
        ),
        mediaId: null,
        url: campaign?.video,
        fileType: "url",
        duration: campaignDuration,
        fileSize: 0,
      })
    );
    handelDiscard();
  };

  const createCampaignFromImageOrVideo = () => {
    // console.log("create campaign from image or viodeo");
    dispatch(
      createCamapaign({
        screenIds: selectedScreens,
        campaignName: campaignName,
        brandName: brandName,
        isDefaultCampaign: !takeDateRange,
        startDateAndTime,
        endDateAndTime,
        noOfDays: getNumberOfDaysBetweenTwoDates(
          startDateAndTime,
          endDateAndTime
        ),
        fileType: fileType || "video/mp4",
        duration: campaignDuration,
        fileSize: fileSize || campaign?.fileSize,
        mediaId: campaign?.media,
      })
    );
    handelDiscard();
  };

  const handleNext = (e: any) => {
    if (validateForm()) {
      if (campaign?.fileType === "url") {
        createCampaignFromURL();
      } else {
        createCampaignFromImageOrVideo();
      }
    }
  };
  useEffect(() => {
    const data = screens?.map((screen: any) => {
      return {
        value: screen?._id,
        label: screen?.name?.toUpperCase(),
        fontSize: 12,
      };
    });
    setScreenOptions(data);
  }, [screens]);

  return (
    <Modal
      centered
      open={props?.open}
      onCancel={() => props.onCancel()}
      footer={[]}
      closable={false}
      maskClosable={false}
    >
      <Box>
        <ShowMediaFile
          url={campaign?.video}
          mediaType={fileType !== "url" ? fileType.split("/")[0] : "url"}
          height="250px"
          width="100%"
        />

        <Flex gap="3" flexDir="column">
          <Flex w="100%" py="4">
            <Flex flexDir="column" width="100%" gap="1">
              <Text fontSize="md" color="#131D30" fontWeight="400" m="0">
                Campaign Name{" "}
              </Text>
              <Input
                width={
                  campaign?.fileType?.split("/")[0] === "video" ? "100%" : "90%"
                }
                height={"45px"}
                placeholder="campaign name"
                size="lg"
                fontSize="md"
                borderRadius="md"
                border="1px"
                borderColor="#DADADA"
                color="#555555"
                py="2"
                value={campaignName}
                onChange={(e) => setCampaignName(e.target.value)}
              />
            </Flex>
            {fileType?.split("/")[0] !== "video" && (
              <Flex flexDir="column" width="20%" gap="1">
                <Stack p="0.5" align="center" w="100%">
                  <MdOutlineTimer fontSize="20px" />
                </Stack>
                <Input
                  height={"45px"}
                  size="lg"
                  fontSize="md"
                  borderRadius="md"
                  border="1px"
                  borderColor="#DADADA"
                  color="#555555"
                  py="2"
                  value={campaignDuration}
                  onChange={(e) => setCampaignDuration(e.target.value)}
                />
              </Flex>
            )}
          </Flex>
          <Flex flexDir="column" width="100%" gap="1">
            <Text fontSize="md" color="#131D30" fontWeight="400" m="0">
              Brand Name
            </Text>
            <Input
              width="100%"
              height={"45px"}
              placeholder="Brand name of the video"
              size="lg"
              fontSize="md"
              borderRadius="md"
              border="1px"
              borderColor="#DADADA"
              color="#555555"
              py="2"
              value={brandName}
              onChange={(e) => setBrandName(e.target.value)}
            />
          </Flex>
          <Flex flexDir="column" width="100%" gap="1">
            <Text fontSize="md" color="#131D30" fontWeight="400" m="0">
              Select Screens
            </Text>
            <Select
              size="large"
              mode="multiple"
              value={selectedScreens}
              onChange={(value) => setSelectedScreens(value)}
              style={{ width: "100%", height: "45px" }}
              options={screenOptions}
              showSearch
              placeholder="Search to Select"
              optionFilterProp="children"
              maxTagCount="responsive"
              filterOption={(input: any, option: any) =>
                (option?.label ?? "").includes(input?.toUpperCase())
              }
              filterSort={(optionA: any, optionB: any) =>
                (optionA?.label ?? "")
                  .toLowerCase()
                  .includes((optionB?.label ?? "").toLowerCase())
              }
            />
          </Flex>

          <Checkbox
            checked={takeDateRange ? takeDateRange : false}
            onChange={(e) => setTakeDateRange(e.target.checked)}
          >
            Do You Want To Add Date Range
          </Checkbox>
          {takeDateRange ? (
            <Flex flexDir="column" gap="1">
              <Text m="0" fontSize="16px" fontWeight="400" color="#131D30">
                Start Date & Time
              </Text>
              <DateAndTimeInputField
                value={startDateAndTime}
                onChange={(value: any) => {
                  setStartDateAndTime(value);
                }}
              />
            </Flex>
          ) : null}
          {takeDateRange ? (
            <Flex flexDir="column" gap="1">
              <Text m="0" fontSize="16px" fontWeight="400" color="#131D30">
                End Date & Time
              </Text>
              <DateAndTimeInputField
                value={endDateAndTime}
                onChange={(value: any) => {
                  setEndDateAndTime(value);
                }}
              />
            </Flex>
          ) : null}
        </Flex>

        <Flex justifyContent="space-between" pt="10">
          <MyButton
            handelClick={handelDiscard}
            label="Cancel"
            color="#FF2E2E"
            bgColor="#FFFFFF"
          />
          <MyButton
            handelClick={handleNext}
            label="Upload"
            color="#111111"
            bgColor="#FFFFFF"
          />
        </Flex>
      </Box>
    </Modal>
  );
}
