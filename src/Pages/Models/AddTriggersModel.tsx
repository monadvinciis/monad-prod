import { SimpleGrid, Flex, Text, Button } from "@chakra-ui/react";
import { Modal, message } from "antd";
import { useEffect, useState } from "react";
import { MyButton, TimeInputField } from "../../components/newCommans";

function MyForm(props: any) {
  const handleEndTime = (startTime: any, endTime: any) => {
    if (startTime) {
      if (endTime > startTime) {
        props.handleChangeEndTime(props?.index, endTime);
      } else {
        message.error("End Time Must Be Grator");
      }
    } else {
      message.error("Please Enter Start time first");
    }
  };
  return (
    <SimpleGrid columns={[1, 2]} gap="5">
      <Flex flexDir="column" gap="1">
        <Text m="0" fontSize="13px" fontWeight="400" color="#131D30" pb="1.5">
          Start Time
        </Text>
        <TimeInputField
          value={props.startTime}
          onChange={(value: any) => {
            props.handleChangeStartTime(props?.index, value);
          }}
        />
      </Flex>
      <Flex flexDir="column" gap="1">
        <Text m="0" fontSize="13px" fontWeight="400" color="#131D30" pb="1.5">
          End Time
        </Text>
        <TimeInputField
          value={props.endTime}
          onChange={(value: any) => {
            handleEndTime(props.startTime, value);
          }}
        />
      </Flex>
    </SimpleGrid>
  );
}

export function AddTriggersModel(props: any) {
  const [fields, setFields] = useState([{ id: 1, startTime: "", endTime: "" }]);
  const handleChangeStartTime = (index: number, value: any) => {
    const newFields = [...fields];
    newFields[index].startTime = value;
    setFields(newFields);
  };
  const handleChangeEndTime = (index: number, value: any) => {
    const newFields = [...fields];
    newFields[index].endTime = value;
    setFields(newFields);
  };

  const handleAddField = () => {
    const newFields = [...fields];
    newFields.push({ id: fields.length + 1, startTime: "", endTime: "" });
    setFields(newFields);
  };

  const handleRemoveField = (index: number) => {
    const newFields = fields.filter((field, i) => i !== index);
    setFields(newFields);
  };

  const handelDiscard = () => {
    setFields([{ id: 1, startTime: "", endTime: "" }]);
    props.handleAddTimeTriggers([]);
    props.onCancel();
  };

  useEffect(() => {
    let data = props.timeTriggers?.map((field: any, index: number) => {
      return {
        id: index + 1,
        startTime: field.startTime,
        endTime: field.endTime,
      };
    });
    if (data?.length > 0) {
      setFields(data);
    }
  }, [props]);

  const handleSave = () => {
    let data: any = [];
    if (isValidFormData()) {
      fields.forEach((field: any) => {
        if (field?.startTime && field?.endTime) {
          data.push({
            startTime: field?.startTime,
            endTime: field?.endTime,
          });
        }
      });
      props.handleAddTimeTriggers(data);
      props.onCancel();
    } else {
      message.error(
        "Please Enter Start And End Time, Or Else Remove Unnecessory Input Fields"
      );
    }
  };

  const isValidFormData = () => {
    for (let i = 0; i < fields?.length; i++) {
      if (fields[i].startTime && fields[i]?.endTime) {
        continue;
      } else {
        return false;
      }
    }
    return true;
  };

  return (
    <Modal
      title="Add triggers accornging to your requirements"
      open={props?.open}
      onCancel={() => props.onCancel()}
      footer={[]}
      closable={false}
      maskClosable={false}
    >
      {fields.map((field, index) => (
        <Flex gap="2" align="center" alignItems="end">
          <MyForm
            handleChangeStartTime={handleChangeStartTime}
            handleChangeEndTime={handleChangeEndTime}
            startTime={field.startTime}
            endTime={field.endTime}
            index={index}
            key={field.id}
          />
          <Button
            bgColor="green"
            color="#FFFFFF"
            onClick={handleAddField}
            py="3"
            height="40px"
          >
            Add
          </Button>
          {fields?.length > 1 && (
            <Button
              bgColor="red"
              color="#FFFFFF"
              onClick={() => handleRemoveField(index)}
              py="3"
              height="40px"
            >
              Delete
            </Button>
          )}
        </Flex>
      ))}
      <Flex justifyContent="space-between" pt="10">
        <MyButton
          handelClick={handelDiscard}
          label="Reset & Close"
          color="#FF2E2E"
          bgColor="#FFFFFF"
        />

        <MyButton
          handelClick={handleSave}
          label="Save & Close"
          color="#111111"
          bgColor="#FFFFFF"
        />
      </Flex>
    </Modal>
  );
}
