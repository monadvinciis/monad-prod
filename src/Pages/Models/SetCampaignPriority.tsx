import { Flex, Stack, Text } from "@chakra-ui/react";
import { Input, Modal, message } from "antd";
import { useState } from "react";
import { MyButton } from "../../components/newCommans";
import { isNumber } from "../../utils/utilityFunctions";

export function SetCampaignPriority(props: any) {
  const { campaigns = [], priority, handelSave } = props;
  // console.log("campaigns :", campaigns);

  const [index, setIndex] = useState<any>(priority);

  const findFrequesncy = (arr: any, value: any) => {
    let count = 0;
    for (let x of arr) {
      if (x == value) {
        count++;
      }
    }
    return count;
  };

  const handleInput = (e: any) => {
    setIndex(e.target.value);
  };

  const handelClick = () => {
    const values1 = index
      ?.trim()
      ?.split(",")
      ?.filter((l: any) => {
        if (isNumber(l)) {
          return l;
        }
      })
      ?.map((d: any) => Number(d))
      ?.sort((a: any, b: any) => a - b);
    console.log("values1 : ", values1);
    let isValid = true;
    if (values1?.length > 0) {
      //check same value present in input value or not

      for (let x of values1) {
        if (
          campaigns?.includes(x) ||
          findFrequesncy(values1, x) > 1 ||
          x == 0
        ) {
          isValid = false;
          message.error(
            `You cann't set this value ${x}. Try with different value.`
          );
          setIndex(priority);
          break;
        }
      }

      if (isValid) {
        handelSave(values1?.join(","));
        props.onCancel();
      }
    }
  };
  return (
    <Modal
      title="Set Campaign Priority"
      open={props?.open}
      onCancel={() => props.onCancel()}
      footer={[]}
      closable={false}
      maskClosable={false}
    >
      <Stack>
        <Flex flexDir="column">
          <Text
            color="red"
            fontSize="20px"
          >{`Value must be not in the given list ${campaigns}`}</Text>
          <Text fontSize="18px">
            Please Enter campaign play at position(Ex: 3,4,5)
          </Text>
          <Input
            placeholder="Ex: 4,5,6,8,9"
            onChange={handleInput}
            value={index}
          ></Input>
        </Flex>
        <Flex gap="4" justifyContent="space-around" pt="5">
          <MyButton label="Cancel" handelClick={() => props?.onCancel()} />
          <MyButton label="Save" handelClick={handelClick} />
        </Flex>
      </Stack>
    </Modal>
  );
}
