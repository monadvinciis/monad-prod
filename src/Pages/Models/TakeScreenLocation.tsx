import { Modal } from "antd";
import { MyMap } from "../MyMap";
import { useState } from "react";
import { Box } from "@chakra-ui/react";

export function TakeScreenLocation(props: any) {
  const { setLat, setLng, lat, lng } = props;
  const [jsonData, setJsonData] = useState<any>({
    features: [
      {
        type: "Feature",
        properties: {},
        geometry: {
          coordinates: [lat, lng],
          type: "Point",
        },
      },
    ],
  });

  const [geometry, setGeometry] = useState<any>({
    coordinates: [lat, lng],
  });
  const handleAddPinClick = (location: any) => {
    const { lng, lat } = location;
    setLng(lng);
    setLat(lat);
    setJsonData({
      features: [
        {
          type: "Feature",
          properties: {},
          geometry: {
            coordinates: [lat, lng],
            type: "Point",
          },
        },
      ],
    });
    setGeometry({
      coordinates: [lat, lng],
    });
  };
  return (
    <Modal
      title=""
      open={props?.open}
      onCancel={() => props.onCancel()}
      footer={[]}
      closable={true}
      maskClosable={true}
    >
      <Box height="500px" p="5">
        <MyMap
          data={jsonData}
          geometry={geometry}
          setLocation={handleAddPinClick}
          selectCurrentLocation={true}
          zoom="3"
        />
      </Box>
    </Modal>
  );
}
