import React from "react";
import { Modal, Result, Button } from "antd";
import { Box } from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";
import { MediaContainer } from "../../components/newCommans";

export function CreatedCampaignStatus(props: any) {
  const { campaign } = props;
  const navigate = useNavigate();
  return (
    <Modal
      title=""
      open={props?.open}
      onCancel={() => props.onCancel()}
      footer={[]}
      closable={false}
      maskClosable={false}
    >
      <Box px={{ base: "2", lg: "10" }}>
        <Result
          status="success"
          title="Successfully Created Campaign"
          extra={[
            <Button
              key="buy"
              onClick={() => {
                props.onCancel();
                navigate("/my/campaigns");
              }}
            >
              Go To My Campaigns
            </Button>,
          ]}
        />
        <MediaContainer cid={campaign?.cid} height={"400px"} width={"100%"} />
      </Box>
    </Modal>
  );
}
