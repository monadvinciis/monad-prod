import { Box } from "@chakra-ui/react";
import { GoogleLogin } from "@react-oauth/google";
import { message } from "antd";
import { jwtDecode } from "jwt-decode";

export function LoginWithGoogle(props: any) {
  return (
    <Box>
      <GoogleLogin
        onSuccess={(credentialResponse) => {
          props.onSuccess(jwtDecode(credentialResponse?.credential || ""));
        }}
        onError={() => {
          message.error("Something wend wrong, Please contact to admin");
        }}
        useOneTap
      />
    </Box>
  );
}

// export function OldLoginButton(props: any) {
//   // const login = useGoogleLogin({
//   //   onSuccess: async (tokenResponse) => {
//   //     const res = await Axios.get(
//   //       "https://www.googleapis.com/oauth2/v3/userinfo",
//   //       {
//   //         headers: {
//   //           Authorization: `Bearer ${tokenResponse?.access_token}`,
//   //         },
//   //       }
//   //     );
//   //     // console.log("dddddddata : ", res.data);
//   //     props.onSuccess(res.data);
//   //   },
//   // });
//   return (
//     <Box>
//       <GoogleLogin
//         onSuccess={(credentialResponse) => {
//           console.log(credentialResponse);
//           console.log(jwtDecode(credentialResponse?.credential || ""));
//         }}
//         onError={() => {
//           console.log("Login Failed");
//         }}
//         useOneTap
//       />
//       <Box
//         style={{ cursor: "pointer" }}
//         py="2"
//         width="100%"
//         bgColor="#FFFFFF"
//         color="#333333"
//         // onClick={() => login()}
//         border="1px"
//         rounded="md"
//         textAlign="center"
//       >
//         <IconButton
//           bg="none"
//           pr="5"
//           icon={<FcGoogle size="25px" color="black" />}
//           aria-label="Close"
//         />
//         Log In with Google
//       </Box>
//     </Box>
//   );
// }
