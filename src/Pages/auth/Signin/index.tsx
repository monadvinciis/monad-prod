import React, { useEffect, useState } from "react";
import { Box, Center, Text, Button, Flex, Link } from "@chakra-ui/react";
import { Checkbox, Form, Input, message } from "antd";
import { useLocation, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { googleSignupSignin, signin } from "../../../Actions/userActions";
import { USER_SIGNOUT } from "../../../Constants/userConstants";
import { LoginWithGoogle } from "../LoginWithGoogle";
import "./index.css";

export function Signin(props: any) {
  const location = useLocation();
  const navigate = useNavigate();
  const dispatch = useDispatch<any>();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [email, setEmail] = useState<any>(
    localStorage.getItem("myapp-email") || ""
  );
  const [password, setPassword] = useState<any>(
    localStorage.getItem("myapp-password") || ""
  );
  const [clicked, setClicked] = useState<boolean>(false);

  const userSignin = useSelector((state: any) => state.userSignin);
  const { error, success, userInfo } = userSignin;

  const onFinish = (values: any) => {
    setIsLoading(true);
    dispatch(signin(email, password));
  };

  const onSuccess = async (res: any) => {
    dispatch(
      googleSignupSignin({
        name: res?.name,
        email: res?.email,
        avatar: res?.picture,
      })
    );
  };

  const remember = (e: any) => {
    setClicked(true);
    if (e.target.checked) {
      localStorage.setItem("myapp-email", email);
      localStorage.setItem("myapp-password", password);
    } else {
      localStorage.setItem("myapp-email", "");
      localStorage.setItem("myapp-password", "");
    }
  };

  useEffect(() => {
    if (success) {
      setIsLoading(false);
      if (userInfo?.isMaster || userInfo?.isCreator) {
        navigate(location?.state?.path || "/", {
          state: {
            data: location?.state?.data,
          },
        });
      } else {
        navigate("/setupAccount");
      }
    }
    if (error) {
      setIsLoading(false);
      message.error(error);
      dispatch({ type: USER_SIGNOUT });
    }
  }, [success, error, dispatch, navigate, userInfo, location?.state]);

  return (
    <Box height="100%" bgColor="#F5F5F5">
      <Center py={{ base: "10", lg: 20 }} px={{ base: "2" }}>
        <Box
          px={{ base: "5", lg: "10" }}
          py="10"
          bgColor="#FFFFFF"
          border="1px"
          borderColor="#DDDDDD"
          borderRadius="22px"
          width={{ base: "100%", lg: "474px" }}
        >
          <Text
            color="#949494"
            fontSize={{ base: "xl", lg: "lg" }}
            fontWeight="400"
          >
            Log In To Monad
          </Text>
          <Text
            color="#212121"
            fontSize={{ base: "md", lg: "32px" }}
            fontWeight="700"
          >
            Hello, Welcome Back
          </Text>
          <Form
            layout="vertical"
            name="basic"
            initialValues={{ remember: true }}
            onFinish={onFinish}
            autoComplete="off"
            fields={[
              {
                name: ["email"],
                value: email,
              },
              {
                name: ["password"],
                value: password,
              },
            ]}
          >
            <Form.Item
              label={
                <Text fontSize={{ base: "lg", lg: "xl" }} m="0" color="#333333">
                  Email
                </Text>
              }
              name="email"
              style={{ fontSize: "20px" }}
              rules={[
                { required: true, message: "Please enter your email!" },
                { type: "email" },
              ]}
            >
              <Input
                placeholder="Enter your email"
                onChange={(e) => setEmail(e.target.value)}
                size="large"
              />
            </Form.Item>

            <Form.Item
              label={
                <Text fontSize={{ base: "lg", lg: "xl" }} m="0" color="#333333">
                  Password
                </Text>
              }
              name="password"
              rules={[
                { required: true, message: "Please enter your password!" },
              ]}
            >
              <Input.Password
                placeholder="Enter your password"
                onChange={(e) => setPassword(e.target.value)}
                size="large"
              />
            </Form.Item>

            <Form.Item>
              <Form.Item name="remember" noStyle>
                <Checkbox onClick={remember} checked={clicked}>
                  Remember me
                </Checkbox>
              </Form.Item>

              <Link
                className="login-form-forgot"
                href=""
                onClick={() => navigate("/forgetPassword")}
              >
                Forgot password
              </Link>
            </Form.Item>

            <Form.Item>
              <Button
                type="submit"
                isLoading={isLoading}
                loadingText="Log in"
                width="100%"
                backgroundColor="#000000"
                py="3"
                borderRadius="12px"
                fontWeight="700"
                fontSize={{ base: "xl", lg: "md" }}
              >
                Log in
              </Button>
            </Form.Item>
          </Form>
          <LoginWithGoogle onSuccess={onSuccess} />
          <Flex
            py="5"
            justifyContent="center"
            onClick={() => navigate("/signUp")}
            style={{ cursor: "pointer" }}
          >
            <Text m="0" color="#565656" fontSize="md" fontWeight="400">
              Don’t Have An Account ?
            </Text>
            <Button
              color="#010101"
              variant="null"
              fontWeight="900"
              fontSize="md"
            >
              Create New
            </Button>
          </Flex>
        </Box>
      </Center>
    </Box>
  );
}
