import React, { useEffect, useState } from "react";
import { Box, Center, Text, Button, Link, Flex } from "@chakra-ui/react";
import { Form, Input, message } from "antd";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  googleSignupSignin,
  sendEmailToUser,
} from "../../../Actions/userActions";
import {
  SEND_EMAIL_TO_USER_RESET,
  USER_SIGNOUT,
} from "../../../Constants/userConstants";
import { LoginWithGoogle } from "../LoginWithGoogle";

export function SignUp() {
  const navigate = useNavigate();
  const dispatch = useDispatch<any>();
  const [email, setEmail] = useState<any>("");

  const sendEmail = useSelector((state: any) => state.sendEmail);
  const { message: resMessage, loading, error, success } = sendEmail;

  const userSignin = useSelector((state: any) => state.userSignin);
  const {
    error: errroInGoogleSignin,
    success: successInGoogleSignin,
    userInfo,
  } = userSignin;

  useEffect(() => {
    if (success) {
      message.success(resMessage);
      dispatch({ type: SEND_EMAIL_TO_USER_RESET });
    }
    if (error) {
      message.error(error);
      dispatch({ type: SEND_EMAIL_TO_USER_RESET });
      setEmail("");
    }
    if (successInGoogleSignin) {
      if (userInfo?.isMaster || userInfo?.isCreator) {
        navigate("/");
      } else {
        navigate("/setupAccount");
      }
    }
    if (errroInGoogleSignin) {
      message.error(error);
      dispatch({ type: USER_SIGNOUT });
      setEmail("");
    }
  }, [
    dispatch,
    success,
    error,
    userInfo,
    successInGoogleSignin,
    errroInGoogleSignin,
    navigate,
    resMessage,
  ]);

  const onFinish = (values: any) => {
    dispatch(
      sendEmailToUser({
        email,
        name: email.split("@")[0],
        signup: true,
        forgetPsssword: false,
      })
    );
  };

  const onSuccess = async (res: any) => {
    dispatch(
      googleSignupSignin({
        name: res?.name,
        email: res?.email,
        avatar: res?.picture,
      })
    );
  };

  return (
    <Box height="100%" bgColor="#F5F5F5">
      <Center py={{ base: "10", lg: 20 }} px={{ base: "2" }}>
        <Box
          px={{ base: "5", lg: "10" }}
          py="10"
          bgColor="#FFFFFF"
          border="1px"
          borderColor="#DDDDDD"
          borderRadius="22px"
          width={{ base: "100%", lg: "474px" }}
        >
          <Text
            color="#949494"
            fontSize={{ base: "xl", lg: "lg" }}
            fontWeight="400"
          >
            Log In To Monad
          </Text>
          <Text
            color="#212121"
            fontSize={{ base: "md", lg: "32px" }}
            fontWeight="700"
          >
            Hello, Welcome Back
          </Text>
          {success ? (
            <Link
              color="teal.500"
              href="https://mail.google.com/mail/u/0/?tab=rm&ogbl#inbox"
            >
              click hear to inbox
            </Link>
          ) : null}

          <Form
            layout="vertical"
            name="basic"
            initialValues={{ remember: true }}
            onFinish={onFinish}
            autoComplete="off"
            fields={[
              {
                name: ["email"],
                value: email,
              },
            ]}
          >
            <Form.Item
              label={
                <Text fontSize={{ base: "lg", lg: "xl" }} m="0" color="#333333">
                  Enter email
                </Text>
              }
              name="email"
              rules={[
                { required: true, message: "Please input your email!" },
                { type: "email" },
              ]}
            >
              <Input
                placeholder="Enter your email"
                onChange={(e) => setEmail(e.target.value)}
                size="large"
              />
            </Form.Item>

            <Form.Item>
              <Button
                type="submit"
                width="100%"
                backgroundColor="#2C2C2C"
                py="3"
                borderRadius="12px"
                fontWeight="700"
                fontSize={{ base: "xl", lg: "md" }}
                isLoading={loading}
                loadingText="Sending mail"
              >
                Verify email
              </Button>
            </Form.Item>
          </Form>

          <LoginWithGoogle onSuccess={onSuccess} />
          <Flex
            py="5"
            justifyContent="center"
            onClick={() => navigate("/signin")}
            style={{ cursor: "pointer" }}
          >
            <Text m="0" color="#565656" fontSize="md" fontWeight="400">
              Already have an account ?{" "}
            </Text>
            <Button
              color="#010101"
              variant="null"
              fontWeight="900"
              fontSize="md"
              onClick={() => navigate("/signUp")}
            >
              Log In
            </Button>
          </Flex>
          <Text fontSize="xs" textAlign="left" mt="4" pb="10">
            By logging in you agree to our{" "}
            <Link color="teal.500" href="#">
              Terms of Use{" "}
            </Link>{" "}
            and acknowledge that you have read our{" "}
            <Link color="teal.500" href="#">
              Privacy Policy
            </Link>{" "}
            .
          </Text>
        </Box>
      </Center>
    </Box>
  );
}
