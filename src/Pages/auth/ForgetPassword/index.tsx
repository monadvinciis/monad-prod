import React, { useEffect, useState } from "react";
import { Box, Center, Stack, Text, Button, Link, Flex } from "@chakra-ui/react";
import { Form, Input, message } from "antd";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { sendEmailToUser } from "../../../Actions/userActions";
import { SEND_EMAIL_TO_USER_RESET } from "../../../Constants/userConstants";

export function ForgetPassword(props: any) {
  const navigate = useNavigate();
  const dispatch = useDispatch<any>();
  const [email, setEmail] = useState<any>("");

  //userSignup;
  const sendEmail = useSelector((state: any) => state.sendEmail);
  const { message: resMessage, loading, error, success } = sendEmail;

  useEffect(() => {
    if (success) {
      message.success(resMessage);
      dispatch({ type: SEND_EMAIL_TO_USER_RESET });
    }
    if (error) {
      message.error(error);
      setEmail("");
      dispatch({ type: SEND_EMAIL_TO_USER_RESET });
    }
  }, [success, error, dispatch, resMessage]);

  const onFinish = (values: any) => {
    dispatch(
      sendEmailToUser({
        email,
        name: email.split("@")[0],
        signup: false,
        forgetPsssword: true,
      })
    );
  };

  return (
    <Box height="90%" bgColor="#F5F5F5">
      <Center py={{ base: "10", lg: 20 }} px={{ base: "2" }}>
        <Box
          px={{ base: "5", lg: "10" }}
          py="10"
          bgColor="#FFFFFF"
          border="1px"
          borderColor="#DDDDDD"
          borderRadius="22px"
          width={{ base: "100%", lg: "474px" }}
        >
          <Text
            color="#333333"
            fontSize={{ base: "xl", lg: "2xl" }}
            fontWeight="semibold"
          >
            Forgot password
          </Text>
          {success && (
            <Link
              color="teal.500"
              href="https://mail.google.com/mail/u/0/?tab=rm&ogbl#inbox"
            >
              click hear to inbox
            </Link>
          )}

          <Form
            layout="vertical"
            name="basic"
            initialValues={{ remember: true }}
            onFinish={onFinish}
            autoComplete="off"
            fields={[
              {
                name: ["email"],
                value: email,
              },
            ]}
          >
            <Form.Item
              label={
                <Text fontSize={{ base: "lg", lg: "xl" }} m="0" color="#333333">
                  Enter email
                </Text>
              }
              name="email"
              rules={[
                { required: true, message: "Please input your email!" },
                { type: "email" },
              ]}
            >
              <Input
                placeholder="Enter your email"
                onChange={(e) => setEmail(e.target.value)}
                size="large"
              />
            </Form.Item>

            <Form.Item>
              <Button
                type="submit"
                isLoading={loading}
                loadingText="Sending email"
                width="100%"
                backgroundColor="#2C2C2C"
                py="3"
                borderRadius="12px"
                fontWeight="700"
                fontSize={{ base: "xl", lg: "md" }}
              >
                Send email
              </Button>
            </Form.Item>
          </Form>

          <Stack pt="5">
            <Button
              py="3"
              width="100%"
              bgColor="#FFFFFF"
              color="#2C2C2C"
              borderRadius="12px"
              fontWeight="700"
              border="1px"
              onClick={() => navigate("/signUp")}
              _hover={{ color: "#FFFFFF", bgColor: "#2C2C2C" }}
            >
              Create a new account
            </Button>
          </Stack>
          <Flex
            py="2"
            justifyContent="center"
            onClick={() => navigate("/signin")}
            style={{ cursor: "pointer" }}
          >
            <Text m="0" color="#565656" fontSize="md" fontWeight="400">
              Already have an account ?{" "}
            </Text>
            <Button
              color="#010101"
              variant="null"
              fontWeight="900"
              fontSize="md"
              onClick={() => navigate("/signUp")}
            >
              Log In
            </Button>
          </Flex>
        </Box>
      </Center>
    </Box>
  );
}
