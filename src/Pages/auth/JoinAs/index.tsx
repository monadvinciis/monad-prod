import React, { useState } from "react";
import { Box, Center, Stack, Text, Button } from "@chakra-ui/react";
import { Radio, RadioChangeEvent } from "antd";

export function JoinAs(props: any) {
  const [value, setValue] = useState<any>("normal user");

  const onChange = (e: RadioChangeEvent) => {
    setValue(e.target.value);
  };

  return (
    <Center pt={{ base: 5, lg: 100 }}>
      <Box
        boxShadow="2xl"
        px={{ base: "5", lg: "10" }}
        py="10"
        bgColor="#FBFBFB"
        width={{ base: "100%", lg: "540px" }}
      >
        <Text
          color="#333333"
          fontSize="2xl"
          fontWeight="semibold"
          align="center"
        >
          Join Us As
        </Text>
        <Box p="2">
          <Radio.Group onChange={onChange} value={value}>
            <Stack spacing="5">
              <Radio value={"screen owner"}>
                <Text m="0" fontSize="md" color="#000000">
                  I’m a screen owner to manage my screens
                </Text>
              </Radio>
              <Radio value={"ally"}>
                <Text m="0" fontSize="md" color="#000000">
                  I want to advertising agency to advertise screens
                </Text>
              </Radio>
              <Radio value={"brand"}>
                <Text m="0" fontSize="md" color="#000000">
                  I’m a brand to advertise and issue offers
                </Text>
              </Radio>
              <Radio value={"normal user"}>
                <Text m="0" fontSize="md" color="#000000">
                  I’m, here to take rewards
                </Text>
              </Radio>
            </Stack>
          </Radio.Group>
        </Box>
        <Stack pt="10">
          <Button
            type="submit"
            width="100%"
            backgroundColor="#D7380E"
            py="2"
            fontSize={{ base: "lg", lg: "xl" }}
          >
            Submit
          </Button>
        </Stack>
        <Stack pt="5">
          <Button
            py="3"
            width="100%"
            bgColor="#FFFFFF"
            color="#333333"
            type="submit"
          >
            Skip
          </Button>
        </Stack>
      </Box>
    </Center>
  );
}
