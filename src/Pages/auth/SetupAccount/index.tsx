import React, { useEffect, useState } from "react";
import { Box, Button, Flex, Image, SimpleGrid, Text } from "@chakra-ui/react";
import { Alert, Input, Radio, Select, SelectProps, message } from "antd";
import { FileSelectButton } from "../../../components/newCommans";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { USER_UPDATE_PROFILE_RESET } from "../../../Constants/userConstants";
import { addFileOnWeb3 } from "../../../utils/newWeb3";
import { updateUserProfile } from "../../../Actions/userActions";
import { state } from "../../../utils/utilsData";
import { useNavigate } from "react-router-dom";

const stateOption: SelectProps["options"] = state.map((value: string) => {
  return { value: `${value}`, label: `${value}` };
});

export function SetupAccount(props: any) {
  const dispatch = useDispatch<any>();
  const [error, setError] = useState<any>("");
  const navigate = useNavigate();

  const [value, setValue] = useState<any>(1);
  const [name, setName] = useState<any>("");
  const [avatar, setAvatar] = useState<any>("");
  const [file, setFile] = useState<any>(null);
  const [phone, setPhone] = useState<any>("");
  const [businessName, setBusinessName] = useState<any>("");
  const [districtCity, setDistrictCity] = useState<any>("");
  const [stateUt, setStateUt] = useState<any>("");
  const [address, setAddress] = useState<any>("");
  const [pincode, setPincode] = useState<any>("");
  const [gst, setGst] = useState<any>("");
  const [loading1, setLoading1] = useState<any>(false);
  const [webSiteLink, setWebSiteLink] = useState<any>("");

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  function handleProfileSelect(file: any) {
    const fileThumbnail = URL.createObjectURL(file);
    setAvatar(fileThumbnail);
    setFile(file);
  }

  useEffect(() => {
    setName(userInfo?.name);
    setPhone(userInfo?.phone);
    setAddress(userInfo?.address);
    setStateUt(userInfo?.stateUt);
    setDistrictCity(userInfo?.districtCity);
    setPincode(userInfo?.pincode);
    setAvatar(userInfo?.avatar);
    setBusinessName(userInfo?.businessName);
    setGst(userInfo?.gst);
  }, [userInfo]);

  const userUpdateProfile = useSelector(
    (state: any) => state.userUpdateProfile
  );
  const { error: errorUpdateProfile, success } = userUpdateProfile;

  useEffect(() => {
    if (success) {
      dispatch({ type: USER_UPDATE_PROFILE_RESET });
      message.success("User updated successFull!");
      setLoading1(false);
      navigate("/");
    }
    if (errorUpdateProfile) {
      message.error(errorUpdateProfile);
      dispatch({ type: USER_UPDATE_PROFILE_RESET });
      setLoading1(false);
    }
  }, [dispatch, errorUpdateProfile, success, navigate]);

  const validateForm = () => {
    if (!avatar) {
      setError("Please Upload media");
      return false;
    } else if (!name) {
      setError("Please Enter your name");
      return false;
    } else if (!businessName) {
      setError("Please Enter Business Name");
      return false;
    } else if (!phone) {
      setError("Please Enter Phone Number");
      return false;
    } else if (!address) {
      setError("Please Enter Your Address");
      return false;
    } else if (!districtCity) {
      setError("Please Enter City");
      return false;
    } else if (!state) {
      setError("Please Select State");
      return false;
    } else if (!pincode) {
      setError("Please Enter pincode");
      return false;
    } else {
      return true;
    }
  };

  const handelSave = async () => {
    if (validateForm()) {
      let cid;
      if (file) {
        setLoading1(true);
        cid = await addFileOnWeb3(file);
      }
      dispatch(
        updateUserProfile({
          name,
          phone,
          businessName,
          districtCity,
          stateUt,
          address,
          pincode,
          gst,
          isCreator: value === 2 ? true : false,
          isMaster: value === 1 ? true : false,
          avatar: file ? `https://ipfs.io/ipfs/${cid}` : userInfo?.avatar,
          additionalInfo: {
            webSiteLink,
          },
        })
      );
    }
  };
  return (
    <Box
      height="90%"
      bgColor="#F5F5F5"
      px={{ base: "5", lg: "20" }}
      py={{ base: "5", lg: "5" }}
    >
      <Box
        bgColor="#FFFFFF"
        border="1px"
        borderColor="#DDDDDD"
        borderRadius="8px"
        px={{ base: "5", lg: "10" }}
        py={{ base: "5", lg: "10" }}
      >
        <Flex justifyContent="center">
          {error ? (
            <Alert
              message="Error"
              description={error}
              type="error"
              showIcon
              closable
              style={{ width: "400px" }}
            />
          ) : null}
        </Flex>
        <Flex justifyContent="flex-end">
          <Button
            py="2"
            px="5"
            borderRadius="8px"
            onClick={handelSave}
            isLoading={loading1}
            loadingText="Saving data..."
          >
            Done
          </Button>
        </Flex>
        <Text fontWeight="400" color="#010101" fontSize="lg">
          Set Up Profile
        </Text>
        <Text fontWeight="400" color="#212121" fontSize="32px">
          Tell Us A Bit About You
        </Text>
        <Text fontWeight="400" color="#010101" fontSize="lg">
          Select Role
        </Text>
        <Radio.Group
          onChange={(e) => setValue(e.target.value)}
          value={value}
          size="large"
        >
          <Radio value={1}>Screen Owner</Radio>
          <Radio value={2}>Advertiser</Radio>
        </Radio.Group>
        <Flex pt="5" align="center" gap="10">
          <Image src={avatar} height="90px" width="90px" borderRadius="full" />
          <FileSelectButton handleProfileSelect={handleProfileSelect} />
        </Flex>
        <Flex pt="10" gap="5" flexDir="column">
          <Flex flexDir="column" gap="2">
            <Text color="#131D30" fontSize="16px" fontWeight="300" m="0">
              Business Name*
            </Text>
            <Input
              size="large"
              style={{ width: "300px", color: "#131D30" }}
              value={businessName}
              onChange={(e: any) => setBusinessName(e.target.value)}
            />
          </Flex>
          <SimpleGrid columns={[1, 1, 3]}>
            <Flex flexDir="column" gap="2">
              <Text color="#131D30" fontSize="16px" fontWeight="300" m="0">
                Contact Person Name*
              </Text>
              <Input
                size="large"
                style={{ width: "300px", color: "#131D30" }}
                value={name}
                onChange={(e: any) => setName(e.target.value)}
              />
            </Flex>
            <Flex flexDir="column" gap="2">
              <Text color="#131D30" fontSize="16px" fontWeight="300" m="0">
                Contact Number*
              </Text>
              <Input
                size="large"
                style={{ width: "300px", color: "#131D30" }}
                value={phone}
                onChange={(e: any) => setPhone(e.target.value)}
              />
            </Flex>
          </SimpleGrid>
          <SimpleGrid columns={[1, 2, 3]}>
            <Flex flexDir="column" gap="2">
              <Text color="#131D30" fontSize="16px" fontWeight="300" m="0">
                Register Address*
              </Text>
              <Input
                size="large"
                style={{ width: "300px", color: "#131D30" }}
                value={address}
                onChange={(e: any) => setAddress(e.target.value)}
              />
            </Flex>
            <Flex flexDir="column" gap="2">
              <Text color="#131D30" fontSize="16px" fontWeight="300" m="0">
                State*
              </Text>
              <Select
                size="large"
                defaultValue=""
                value={stateUt}
                onChange={(value: any) => setStateUt(value)}
                style={{ width: "300px", color: "#131D30" }}
                options={stateOption}
                showSearch
                placeholder="Search to Select"
                optionFilterProp="children"
                filterOption={(input: any, option: any) =>
                  (option?.label ?? "").includes(input)
                }
                filterSort={(optionA: any, optionB: any) =>
                  (optionA?.label ?? "")
                    .toLowerCase()
                    .localeCompare((optionB?.label ?? "").toLowerCase())
                }
              />
            </Flex>
            <Flex flexDir="column" gap="2">
              <Text color="#131D30" fontSize="16px" fontWeight="300" m="0">
                City*
              </Text>
              <Input
                size="large"
                style={{ width: "300px", color: "#131D30" }}
                value={districtCity}
                onChange={(e: any) => setDistrictCity(e.target.value)}
              />
            </Flex>
          </SimpleGrid>
          <SimpleGrid columns={[1, 2, 3]}>
            <Flex flexDir="column" gap="2">
              <Text color="#131D30" fontSize="16px" fontWeight="300" m="0">
                PinCode*
              </Text>
              <Input
                size="large"
                style={{ width: "300px", color: "#131D30" }}
                value={pincode}
                onChange={(e: any) => setPincode(e.target.value)}
              />
            </Flex>
            <Flex flexDir="column" gap="2">
              <Text color="#131D30" fontSize="16px" fontWeight="300" m="0">
                GST(optional)
              </Text>
              <Input
                size="large"
                style={{ width: "300px", color: "#131D30" }}
                value={gst}
                onChange={(e: any) => setGst(e.target.value)}
              />
            </Flex>
            <Flex flexDir="column" gap="2">
              <Text color="#131D30" fontSize="16px" fontWeight="300" m="0">
                Website Link(optional)
              </Text>
              <Input
                size="large"
                style={{ width: "300px", color: "#131D30" }}
                value={webSiteLink}
                onChange={(e: any) => setWebSiteLink(e.target.value)}
              />
            </Flex>
          </SimpleGrid>
        </Flex>
      </Box>
    </Box>
  );
}
