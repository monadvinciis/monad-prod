import React, { useEffect, useState } from "react";
import { Box, Center, Text, Button } from "@chakra-ui/react";
import { Form, Input, message } from "antd";
import { signup } from "../../../Actions/userActions";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

export function CreatePassword(props: any) {
  const navigate = useNavigate();
  const dispatch = useDispatch<any>();
  const name = window.location.pathname.split("/")[3];
  const email = window.location.pathname.split("/")[2];
  const [password, setPassword] = useState<any>("");
  const [confirmPassword, setConfirmPassword] = useState<any>("");
  const userSignup = useSelector((state: any) => state.userSignup);
  const { loading, error, success, userInfo } = userSignup;

  const [messageApi, contextHolder] = message.useMessage();
  const passwordError = (error: any) => {
    messageApi.open({
      type: "error",
      content: error,
    });
  };

  useEffect(() => {
    if (success) {
      message.success("Password has changed!");
      setTimeout(() => {
        if (userInfo?.isMaster || userInfo?.isCreator) {
          navigate("/");
        } else {
          navigate("/setupAccount");
        }
      }, 0);
    }
    if (error) {
      message.error(error);
      setPassword("");
      setConfirmPassword("");
    }
  }, [success, error, userInfo, navigate]);

  const onFinish = (values: any) => {
    if (password === confirmPassword) {
      if (password?.length >= 8) {
        dispatch(signup(name, email, password));
      } else {
        passwordError("Password length atleast 8 charactors");
        setPassword("");
        setConfirmPassword("");
      }
    } else {
      passwordError("Password mismatched!, try again");
      setPassword("");
      setConfirmPassword("");
    }
  };

  return (
    <Box height="90%" bgColor="#F5F5F5">
      <Center py={{ base: "10", lg: 20 }} px={{ base: "2" }}>
        {contextHolder}
        <Box
          px={{ base: "5", lg: "10" }}
          py="10"
          bgColor="#FFFFFF"
          border="1px"
          borderColor="#DDDDDD"
          borderRadius="22px"
          width={{ base: "100%", lg: "474px" }}
        >
          <Text
            color="#333333"
            fontSize={{ base: "xl", lg: "2xl" }}
            fontWeight="semibold"
          >
            Create new password
          </Text>
          <Text color="#5B5B5B" fontSize={{ base: "sm", lg: "md" }}>
            Password must be at least 8 characters long.
          </Text>
          <Form
            layout="vertical"
            name="basic"
            initialValues={{ remember: true }}
            onFinish={onFinish}
            autoComplete="off"
            fields={[
              {
                name: ["password"],
                value: password,
              },
              {
                name: ["confirmPassword"],
                value: confirmPassword,
              },
            ]}
          >
            <Form.Item
              label={
                <Text fontSize={{ base: "lg", lg: "xl" }} m="0" color="#333333">
                  New password
                </Text>
              }
              name="password"
              rules={[
                { required: true, message: "Please input your password!" },
              ]}
            >
              <Input.Password
                placeholder="New password"
                onChange={(e) => setPassword(e.target.value)}
                size="large"
                minLength={8}
              />
            </Form.Item>
            <Form.Item
              label={
                <Text fontSize={{ base: "lg", lg: "xl" }} m="0" color="#333333">
                  Confirm password
                </Text>
              }
              name="confirmPassword"
              rules={[
                {
                  required: true,
                  message: "Please input your confirm password!",
                },
              ]}
            >
              <Input.Password
                placeholder="Confirm password"
                onChange={(e) => setConfirmPassword(e.target.value)}
                size="large"
                minLength={8}
              />
            </Form.Item>

            <Form.Item>
              <Button
                type="submit"
                width="100%"
                backgroundColor="#2C2C2C"
                py="3"
                borderRadius="12px"
                fontWeight="700"
                fontSize={{ base: "xl", lg: "md" }}
                isLoading={loading}
                loadingText="Saving password"
              >
                Submit
              </Button>
            </Form.Item>
          </Form>
        </Box>
      </Center>
    </Box>
  );
}
