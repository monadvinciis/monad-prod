import { Box, Text, Flex, Stack } from "@chakra-ui/react";
// import { MainMenu } from "../../../components/newCommans";
import { Mytabs } from "../../components/newCommans";
import { useEffect, useState } from "react";
import { UserProfile } from "./UserProfile";
import { ChangePassword } from "./UserProfile/ChangePassword";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

const options = ["Profile", "Security"];
export function Setting(props: any) {
  const navigate = useNavigate();
  const [selectedTab, setSelectedTab] = useState<any>(1);
  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  useEffect(() => {
    if (!userInfo) {
      navigate("/signin", { state: { path: "/setting" } });
    }
  }, [userInfo]);

  return (
    <Box w="100%" h="100%" bgColor="#F5F5F5" m="0">
      <Flex
        p="3"
        align="center"
        justifyContent="space-between"
        borderTop="1px"
        borderLeft="1px"
        // border="1px solid green"
        borderColor="#EBEBEB"
        pr="20"
        bgColor="#FFFFFF"
      >
        <Text px="2" fontWeight="600" fontSize="2xl" m="0">
          Setting
        </Text>
      </Flex>

      <Flex
        width="100%"
        borderTop="1px"
        borderLeft="1px"
        // border="1px solid green"
        borderColor="#EBEBEB"
      >
        <Mytabs
          selectedTab={selectedTab}
          setSelectedTab={setSelectedTab}
          options={options}
        />
      </Flex>
      <Flex width="100%" height="80%">
        <Stack width="100%" height="900hv" pb="2" overflowY="hidden">
          {selectedTab === 1 ? (
            <UserProfile />
          ) : selectedTab === 2 ? (
            <ChangePassword />
          ) : null}
        </Stack>
      </Flex>
    </Box>
  );
}
