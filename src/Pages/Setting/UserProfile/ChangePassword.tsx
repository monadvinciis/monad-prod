import {
  Box,
  Text,
  Flex,
  SimpleGrid,
  Hide,
  Show,
  Button,
} from "@chakra-ui/react";
import { Input, message } from "antd";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { updateUserPassword } from "../../../Actions/userActions";
import { USER_UPDATE_PASSWORD_RESET } from "../../../Constants/userConstants";
import { MdLockOutline } from "react-icons/md";
import { MyButton } from "../../../components/newCommans/MyButtons";

export function ChangePassword(props: any) {
  const dispatch = useDispatch<any>();
  const [oldPassword, setOldPassword] = useState<any>("");
  const [password, setPassword] = useState<any>("");
  const [confirmPassword, setConfirmPassword] = useState<any>("");

  const userUpdatePassword = useSelector(
    (state: any) => state.userUpdatePassword
  );
  const { loading, error: errorUpdateProfile, success } = userUpdatePassword;

  useEffect(() => {
    if (success) {
      dispatch({ type: USER_UPDATE_PASSWORD_RESET });
      message.success("New Password Added SuccessFull!");
      setOldPassword("");
      setPassword("");
      setConfirmPassword("");
    }
    if (errorUpdateProfile) {
      message.error(errorUpdateProfile);
      dispatch({ type: USER_UPDATE_PASSWORD_RESET });
    }
  }, [dispatch, errorUpdateProfile, success]);

  const handelSave = async () => {
    if (oldPassword) {
      if (password === confirmPassword) {
        if (password?.length >= 8) {
          dispatch(updateUserPassword({ oldPassword, newPassword: password }));
        } else {
          message.error("Password length atleast 8 charactors");
          setPassword("");
          setConfirmPassword("");
        }
      } else {
        message.error("Password mismatched!, try again");
        setPassword("");
        setConfirmPassword("");
      }
    } else {
      message.error("Please Enter Your Old Password!");
    }
  };
  return (
    <Box
      p={{ base: "4", lg: "8" }}
      bgColor="#FFFFFF"
      width="100%"
      borderColor="#E1E1E1"
      gap={{ base: "2", lg: "4" }}
    >
      <Flex justifyContent="space-between">
        <Text color="#131D30" fontSize="24px" fontWeight="500" m="0">
          Change Password
        </Text>
        <Hide below="md">
          <MyButton
            label="Change Password"
            handelClick={handelSave}
            isLoading={loading}
            loadingText="Saving..."
          />
        </Hide>
        <Show below="md">
          <Button
            variant="outline"
            borderRadius="full"
            py="1"
            color="#2A2A2A"
            onClick={handelSave}
          >
            Change Password
          </Button>
        </Show>
      </Flex>
      <Flex
        pt="10"
        flexDir="column"
        gap={{ base: "2", lg: "4" }}
        px={{ base: "4" }}
        width={{ base: "100%", lg: "70%" }}
      >
        <SimpleGrid columns={[1, 2, 2]} spacing={{ base: "2", lg: "4" }}>
          <Flex align="center" gap={{ base: "2", lg: "4" }}>
            <MdLockOutline />
            <Text fontSize="16px" fontWeight="400" color="#131D30" m="0">
              Enter Current Password
            </Text>
          </Flex>

          <Input.Password
            placeholder="Old password"
            onChange={(e) => setOldPassword(e.target.value)}
            size="large"
            minLength={8}
            value={oldPassword}
          />
        </SimpleGrid>
        <SimpleGrid columns={[1, 2, 2]} spacing={{ base: "2", lg: "4" }}>
          <Flex align="center" gap={{ base: "2", lg: "4" }}>
            <MdLockOutline />
            <Text fontSize="16px" fontWeight="400" color="#131D30" m="0">
              Enter New Password
            </Text>
          </Flex>

          <Input.Password
            placeholder="New password"
            onChange={(e) => setPassword(e.target.value)}
            size="large"
            minLength={8}
            value={password}
          />
        </SimpleGrid>
        <SimpleGrid columns={[1, 2, 2]} spacing={{ base: "2", lg: "4" }}>
          <Flex align="center" gap={{ base: "2", lg: "4" }}>
            <MdLockOutline />
            <Text fontSize="16px" fontWeight="400" color="#131D30" m="0">
              Confirm New Password
            </Text>
          </Flex>
          <Input.Password
            placeholder="Confirm New Password"
            onChange={(e) => setConfirmPassword(e.target.value)}
            size="large"
            minLength={8}
            value={confirmPassword}
          />
        </SimpleGrid>
      </Flex>
    </Box>
  );
}
