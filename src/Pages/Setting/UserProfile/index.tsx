import { Box, Flex, Hide, Show } from "@chakra-ui/react";
import { VerticalTabs } from "../../../components/newCommans";
import { FaRegUser } from "react-icons/fa";
import { IoIosInformationCircleOutline } from "react-icons/io";
import { MdOutlineLock } from "react-icons/md";
import { useState } from "react";
import { PersionaInfo } from "./PersionaInfo";
import { AdditionalInfo } from "./AdditionalInfo";
import { ChangePassword } from "./ChangePassword";
import { MobileViewUserProfile } from "./MobileViewUserProfile";

const options = ["Personal", "Aditional", "Password"];
const icons = [
  <FaRegUser />,
  <IoIosInformationCircleOutline />,
  <MdOutlineLock />,
];
export function UserProfile(props: any) {
  const [selectedTab, setSelectedTab] = useState<any>(1);

  return (
    <Box
      py={{ base: "2", lg: "4" }}
      px={{ base: "0", lg: "4" }}
      overflow={"hidden"}
      height={"100%"}
      m="0"
    >
      <Flex
        overflow={"hidden"}
        height={"100%"}
        gap={{ base: "0", lg: "4" }}
        px={{ base: "2", lg: "4" }}
      >
        <Hide below="md">
          <VerticalTabs
            options={options}
            icons={icons}
            selectedTab={selectedTab}
            setSelectedTab={setSelectedTab}
          />

          {selectedTab === 1 ? (
            <PersionaInfo />
          ) : selectedTab === 2 ? (
            <AdditionalInfo />
          ) : selectedTab === 3 ? (
            <ChangePassword />
          ) : null}
        </Hide>
        <Show below="md">
          <MobileViewUserProfile />
        </Show>
      </Flex>
    </Box>
  );
}
