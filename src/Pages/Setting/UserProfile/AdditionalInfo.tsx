import { Box, Text, Flex, SimpleGrid } from "@chakra-ui/react";
import { Input, Select, message } from "antd";
import { useEffect, useState } from "react";
import { CiEdit } from "react-icons/ci";
import { IoBusiness } from "react-icons/io5";
import { useSelector } from "react-redux";
import { LiaSaveSolid } from "react-icons/lia";
import { useDispatch } from "react-redux";
import { updateUserProfile } from "../../../Actions/userActions";
import { USER_UPDATE_PROFILE_RESET } from "../../../Constants/userConstants";
import { FiLink2 } from "react-icons/fi";
import { MyButton } from "../../../components/newCommans/MyButtons";

const options = ["Creative Agency", "Digital Marketing"];
export function AdditionalInfo(props: any) {
  const dispatch = useDispatch<any>();
  const [isDesiable, setIsDesiable] = useState<any>(true);
  const [natureOfBusiness, setNatureOfBusiness] = useState<any>([]);
  const [yearOfExperiance, setYearOfExperiance] = useState<any>(0);
  const [webSiteLink, setWebSiteLink] = useState<any>("");

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  useEffect(() => {
    setNatureOfBusiness(userInfo?.additionalInfo?.natureOfBusiness);
    setYearOfExperiance(userInfo?.additionalInfo?.yearOfExperiance);
    setWebSiteLink(userInfo?.additionalInfo?.webSiteLink);
  }, [userInfo]);

  const userUpdateProfile = useSelector(
    (state: any) => state.userUpdateProfile
  );
  const { loading, error: errorUpdateProfile, success } = userUpdateProfile;

  useEffect(() => {
    if (success) {
      dispatch({ type: USER_UPDATE_PROFILE_RESET });
      message.success("User updated successFull!");
      setIsDesiable(true);
    }
    if (errorUpdateProfile) {
      message.error(errorUpdateProfile);
      dispatch({ type: USER_UPDATE_PROFILE_RESET });
    }
  }, [dispatch, errorUpdateProfile, success]);

  const filteredOptions = options.filter(
    (data: any) => !natureOfBusiness?.includes(data)
  );

  const handelSave = async () => {
    //after save
    dispatch(
      updateUserProfile({
        additionalInfo: {
          natureOfBusiness,
          yearOfExperiance,
          webSiteLink,
        },
      })
    );
  };
  return (
    <Box
      p={{ base: "4", lg: "8" }}
      bgColor="#FFFFFF"
      width="100%"
      // border="1px"
      borderColor="#E1E1E1"
      gap={{ base: "2", lg: "4" }}
    >
      <Flex justifyContent="space-between">
        <Text color="#131D30" fontSize="24px" fontWeight="500" m="0">
          Additional Information
        </Text>

        {isDesiable ? (
          <MyButton
            icon={<CiEdit size="20px" />}
            label="Edit Profile"
            handelClick={() => setIsDesiable(false)}
          />
        ) : (
          <MyButton
            icon={<LiaSaveSolid size="20px" />}
            label="Save"
            handelClick={handelSave}
            isLoading={loading}
            loadingText="Saving..."
          />
        )}
      </Flex>
      <Flex
        pt="10"
        flexDir="column"
        gap={{ base: "2", lg: "4" }}
        width={{ base: "100%", lg: "100%" }}
        px={{ base: "4" }}
      >
        <SimpleGrid columns={[1, 2, 2]} spacing={{ base: "2", lg: "4" }}>
          <Flex align="center" gap={{ base: "2", lg: "4" }}>
            <IoBusiness />
            <Text fontSize="16px" fontWeight="400" color="#131D30" m="0">
              Nature Of Business
            </Text>
          </Flex>

          <Select
            mode="multiple"
            size={"large"}
            maxTagCount="responsive"
            disabled={isDesiable}
            placeholder="Please select"
            value={natureOfBusiness}
            onChange={setNatureOfBusiness}
            style={{ width: "100%", color: "#131D30" }}
            options={filteredOptions.map((item: any) => ({
              value: item,
              label: item,
            }))}
          />
        </SimpleGrid>
        <SimpleGrid columns={[1, 2, 2]} spacing={{ base: "2", lg: "4" }}>
          <Flex align="center" gap={{ base: "2", lg: "4" }}>
            <IoBusiness />
            <Text fontSize="16px" fontWeight="400" color="#131D30" m="0">
              Years Of Exprience
            </Text>
          </Flex>

          <Input
            size="large"
            type="number"
            style={{ color: "#131D30" }}
            disabled={isDesiable}
            value={yearOfExperiance}
            onChange={(e) => setYearOfExperiance(e.target.value)}
          ></Input>
        </SimpleGrid>
        <SimpleGrid columns={[1, 2, 2]} spacing={{ base: "2", lg: "4" }}>
          <Flex align="center" gap={{ base: "2", lg: "4" }}>
            <FiLink2 />
            <Text fontSize="16px" fontWeight="400" color="#131D30" m="0">
              Website Link
            </Text>
          </Flex>
          <Input
            addonBefore="https://"
            size="large"
            style={{ color: "#131D30" }}
            disabled={isDesiable}
            value={webSiteLink}
            onChange={(e) => setWebSiteLink(e.target.value)}
          ></Input>
        </SimpleGrid>
      </Flex>
    </Box>
  );
}
