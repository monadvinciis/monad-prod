import { Stack, Flex, Text, SimpleGrid, Image, Button } from "@chakra-ui/react";
import { Input, Select, message } from "antd";
import { useEffect, useState } from "react";
import { IoBusiness, IoCallOutline, IoLocationOutline } from "react-icons/io5";
import { MdAdd, MdOutlineEmail } from "react-icons/md";
import { PiMinusBold } from "react-icons/pi";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { USER_UPDATE_PROFILE_RESET } from "../../../Constants/userConstants";
import { addFileOnWeb3 } from "../../../utils/newWeb3";
import { updateUserProfile } from "../../../Actions/userActions";
import { FaRegUser } from "react-icons/fa";
import { LuFileSpreadsheet } from "react-icons/lu";
import { FiLink2 } from "react-icons/fi";
import { CiEdit } from "react-icons/ci";
import { LiaSaveSolid } from "react-icons/lia";
import { FileSelectButton } from "../../../components/newCommans";

const options = ["Creative Agency", "Digital Marketing"];

export function MobileViewUserProfile(props: any) {
  const dispatch = useDispatch<any>();
  const [isOpenPertionaInfo, setIsOpenPertionaInfo] = useState<any>(true);
  const [isOpenAdditionInfo, setIsOpenAdditionInfo] = useState<any>(true);

  const [isDesiable, setIsDesiable] = useState<any>(true);
  const [name, setName] = useState<any>("");
  const [avatar, setAvatar] = useState<any>("");
  const [file, setFile] = useState<any>(null);
  const [phone, setPhone] = useState<any>("");
  const [businessName, setBusinessName] = useState<any>("");
  const [districtCity, setDistrictCity] = useState<any>("");
  const [stateUt, setStateUt] = useState<any>("");
  const [address, setAddress] = useState<any>("");
  const [pincode, setPincode] = useState<any>("");
  const [gst, setGst] = useState<any>("");
  const [natureOfBusiness, setNatureOfBusiness] = useState<any>([]);
  const [yearOfExperiance, setYearOfExperiance] = useState<any>(0);
  const [webSiteLink, setWebSiteLink] = useState<any>("");
  const [loading1, setLoading1] = useState<any>(false);

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  function handleProfileSelect(file: any) {
    const fileThumbnail = URL.createObjectURL(file);
    setAvatar(fileThumbnail);
    setFile(file);
  }

  useEffect(() => {
    setName(userInfo?.name);
    setPhone(userInfo?.phone);
    setAddress(userInfo?.address);
    setStateUt(userInfo?.stateUt);
    setDistrictCity(userInfo?.districtCity);
    setPincode(userInfo?.pincode);
    setAvatar(userInfo?.avatar);
    setBusinessName(userInfo?.businessName);
    setGst(userInfo?.gst);
    setNatureOfBusiness(userInfo?.additionalInfo?.natureOfBusiness);
    setYearOfExperiance(userInfo?.additionalInfo?.yearOfExperiance);
    setWebSiteLink(userInfo?.additionalInfo?.webSiteLink);
  }, [userInfo]);

  const userUpdateProfile = useSelector(
    (state: any) => state.userUpdateProfile
  );
  const { loading, error: errorUpdateProfile, success } = userUpdateProfile;

  useEffect(() => {
    if (success) {
      dispatch({ type: USER_UPDATE_PROFILE_RESET });
      message.success("User updated successFull!");
      setLoading1(false);
      setIsDesiable(true);
    }
    if (errorUpdateProfile) {
      message.error(errorUpdateProfile);
      dispatch({ type: USER_UPDATE_PROFILE_RESET });
      setLoading1(false);
    }
  }, [dispatch, errorUpdateProfile, success]);

  const filteredOptions = options.filter(
    (data: any) => !natureOfBusiness?.includes(data)
  );

  const handelSave = async () => {
    //after save
    let cid;
    if (file) {
      setLoading1(true);
      cid = await addFileOnWeb3(file);
    }
    dispatch(
      updateUserProfile({
        name,
        phone,
        businessName,
        districtCity,
        stateUt,
        address,
        pincode,
        gst,
        avatar: file ? `https://ipfs.io/ipfs/${cid}` : userInfo?.avatar,
        additionalInfo: {
          natureOfBusiness,
          yearOfExperiance,
          webSiteLink,
        },
      })
    );
  };

  return (
    <Stack width="100%" gap="2">
      <Stack px="4" py="2" bgColor="#FFFFFF" align="center" gap="0" spacing="0">
        <Flex gap={{ base: "4", lg: "8" }} align="center">
          <Image src={avatar} height="90px" width="90px" borderRadius="full" />
          {!isDesiable ? (
            <FileSelectButton handleProfileSelect={handleProfileSelect} />
          ) : null}
        </Flex>
        <Text fontSize="32px" fontWeight="600" m="0" color="#161616" p="0">
          {userInfo?.name}
        </Text>
        <Text color="#A1A1A1" fontSize="14px" fontWeight="400" m="0" pb="1">
          {userInfo?.email}
        </Text>
        {isDesiable ? (
          <Button
            variant="outline"
            borderRadius="full"
            leftIcon={<CiEdit size="16px" />}
            py="1"
            color="#2A2A2A"
            onClick={() => setIsDesiable(false)}
          >
            Edit
          </Button>
        ) : (
          <Button
            variant="outline"
            borderRadius="full"
            leftIcon={<LiaSaveSolid size="16px" />}
            py="1"
            color="#2A2A2A"
            isLoading={loading1 || loading}
            loadingText="Saving..."
            onClick={handelSave}
          >
            Save
          </Button>
        )}
      </Stack>
      <Stack overflowY="auto">
        <Stack px="4" py="4" pb="4" bgColor="#FFFFFF">
          <Flex
            width="100%"
            color="#1B1B1B"
            justifyContent="space-between"
            align="center"
            onClick={() => {
              setIsOpenPertionaInfo(!isOpenPertionaInfo);
            }}
          >
            <Text fontSize="20px" fontWeight="700" m="0">
              Pertional Information
            </Text>
            {isOpenPertionaInfo ? <PiMinusBold /> : <MdAdd />}
          </Flex>
          {isOpenPertionaInfo ? (
            <Stack color="#4A4A4A" pt="4">
              <SimpleGrid columns={[1, 2, 2]} spacing={{ base: "2", lg: "4" }}>
                <Flex align="center" gap={{ base: "2", lg: "4" }}>
                  <IoBusiness />
                  <Text fontSize="16px" fontWeight="400" m="0">
                    Business Name
                  </Text>
                </Flex>

                <Input
                  size="large"
                  style={{ color: "#131D30" }}
                  disabled={isDesiable}
                  value={businessName}
                  onChange={(e) => setBusinessName(e.target.value)}
                ></Input>
              </SimpleGrid>
              <SimpleGrid columns={[1, 2, 2]} spacing={{ base: "2", lg: "4" }}>
                <Flex align="center" gap={{ base: "2", lg: "4" }}>
                  <MdOutlineEmail />
                  <Text fontSize="16px" fontWeight="400" m="0">
                    Email Id
                  </Text>
                </Flex>
                <Input
                  size="large"
                  style={{ color: "#131D30" }}
                  disabled={true}
                  value={userInfo?.email}
                ></Input>
              </SimpleGrid>
              <SimpleGrid columns={[1, 2, 2]} spacing={{ base: "2", lg: "4" }}>
                <Flex align="center" gap={{ base: "2", lg: "4" }}>
                  <IoCallOutline />
                  <Text fontSize="16px" fontWeight="400" m="0">
                    Your Contact
                  </Text>
                </Flex>
                <Input
                  size="large"
                  style={{ color: "#131D30" }}
                  disabled={isDesiable}
                  value={phone}
                  onChange={(e) => setPhone(e.target.value)}
                ></Input>
              </SimpleGrid>
              <SimpleGrid columns={[1, 2, 2]} spacing={{ base: "2", lg: "4" }}>
                <Flex align="center" gap={{ base: "2", lg: "4" }}>
                  <FaRegUser />
                  <Text fontSize="16px" fontWeight="400" m="0">
                    Your Name
                  </Text>
                </Flex>
                <Input
                  size="large"
                  style={{ color: "#131D30" }}
                  disabled={isDesiable}
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                ></Input>
              </SimpleGrid>
              {isDesiable ? (
                <SimpleGrid
                  columns={[1, 2, 2]}
                  spacing={{ base: "2", lg: "4" }}
                >
                  <Flex align="center" gap={{ base: "2", lg: "4" }}>
                    <IoLocationOutline />
                    <Text fontSize="16px" fontWeight="400" m="0">
                      Your Address
                    </Text>
                  </Flex>
                  <Input
                    size="large"
                    style={{ color: "#131D30" }}
                    disabled={isDesiable}
                    value={`${userInfo?.address}, ${userInfo?.districtCity}, ${userInfo?.stateUt}, ${userInfo?.pincode}`}
                  ></Input>
                </SimpleGrid>
              ) : (
                <>
                  <SimpleGrid
                    columns={[1, 2, 2]}
                    spacing={{ base: "2", lg: "4" }}
                  >
                    <Flex align="center" gap={{ base: "2", lg: "4" }}>
                      <IoLocationOutline />
                      <Text fontSize="16px" fontWeight="400" m="0">
                        Full Address
                      </Text>
                    </Flex>
                    <Input
                      size="large"
                      style={{ color: "#131D30" }}
                      disabled={isDesiable}
                      value={address}
                      onChange={(e) => setAddress(e.target.value)}
                    ></Input>
                  </SimpleGrid>
                  <SimpleGrid
                    columns={[1, 2, 2]}
                    spacing={{ base: "2", lg: "4" }}
                  >
                    <Flex align="center" gap={{ base: "2", lg: "4" }}>
                      <IoLocationOutline />
                      <Text fontSize="16px" fontWeight="400" m="0">
                        City
                      </Text>
                    </Flex>
                    <Input
                      size="large"
                      style={{ color: "#131D30" }}
                      disabled={isDesiable}
                      value={districtCity}
                      onChange={(e) => setDistrictCity(e.target.value)}
                    ></Input>
                  </SimpleGrid>
                  <SimpleGrid
                    columns={[1, 2, 2]}
                    spacing={{ base: "2", lg: "4" }}
                  >
                    <Flex align="center" gap={{ base: "2", lg: "4" }}>
                      <IoLocationOutline />
                      <Text fontSize="16px" fontWeight="400" m="0">
                        State
                      </Text>
                    </Flex>
                    <Input
                      size="large"
                      style={{ color: "#131D30" }}
                      disabled={isDesiable}
                      value={stateUt}
                      onChange={(e) => setStateUt(e.target.value)}
                    ></Input>
                  </SimpleGrid>
                  <SimpleGrid
                    columns={[1, 2, 2]}
                    spacing={{ base: "2", lg: "4" }}
                  >
                    <Flex align="center" gap={{ base: "2", lg: "4" }}>
                      <IoLocationOutline />
                      <Text fontSize="16px" fontWeight="400" m="0">
                        Pincode
                      </Text>
                    </Flex>
                    <Input
                      size="large"
                      style={{ color: "#131D30" }}
                      disabled={isDesiable}
                      value={pincode}
                      onChange={(e) => setPincode(e.target.value)}
                    ></Input>
                  </SimpleGrid>
                </>
              )}
              <SimpleGrid columns={[1, 2, 2]} spacing={{ base: "2", lg: "4" }}>
                <Flex align="center" gap={{ base: "2", lg: "4" }}>
                  <LuFileSpreadsheet />
                  <Text fontSize="16px" fontWeight="400" m="0">
                    Gst Number
                  </Text>
                </Flex>
                <Input
                  size="large"
                  style={{ color: "#131D30" }}
                  disabled={isDesiable}
                  value={gst}
                  onChange={(e) => setGst(e.target.value)}
                ></Input>
              </SimpleGrid>
            </Stack>
          ) : null}
        </Stack>
        <Stack px="4" py="4" pb="4" bgColor="#FFFFFF">
          <Flex
            width="100%"
            color="#1B1B1B"
            justifyContent="space-between"
            align="center"
            onClick={() => {
              setIsOpenAdditionInfo(!isOpenAdditionInfo);
            }}
          >
            <Text fontSize="20px" fontWeight="700" m="0">
              Addition Information
            </Text>
            {isOpenAdditionInfo ? <PiMinusBold /> : <MdAdd />}
          </Flex>
          {isOpenAdditionInfo ? (
            <Stack color="#4A4A4A" pt="4">
              <SimpleGrid columns={[1, 2, 2]} spacing={{ base: "2", lg: "4" }}>
                <Flex align="center" gap={{ base: "2", lg: "4" }}>
                  <IoBusiness />
                  <Text fontSize="16px" fontWeight="400" color="#131D30" m="0">
                    Nature Of Business
                  </Text>
                </Flex>

                <Select
                  mode="multiple"
                  size={"large"}
                  maxTagCount="responsive"
                  disabled={isDesiable}
                  placeholder="Please select"
                  value={natureOfBusiness}
                  onChange={setNatureOfBusiness}
                  style={{ width: "100%", color: "#131D30" }}
                  options={filteredOptions.map((item: any) => ({
                    value: item,
                    label: item,
                  }))}
                />
              </SimpleGrid>
              <SimpleGrid columns={[1, 2, 2]} spacing={{ base: "2", lg: "4" }}>
                <Flex align="center" gap={{ base: "2", lg: "4" }}>
                  <IoBusiness />
                  <Text fontSize="16px" fontWeight="400" color="#131D30" m="0">
                    Years Of Exprience
                  </Text>
                </Flex>

                <Input
                  size="large"
                  type="number"
                  style={{ color: "#131D30" }}
                  disabled={isDesiable}
                  value={yearOfExperiance}
                  onChange={(e) => setYearOfExperiance(e.target.value)}
                ></Input>
              </SimpleGrid>
              <SimpleGrid columns={[1, 2, 2]} spacing={{ base: "2", lg: "4" }}>
                <Flex align="center" gap={{ base: "2", lg: "4" }}>
                  <FiLink2 />
                  <Text fontSize="16px" fontWeight="400" color="#131D30" m="0">
                    Website Link
                  </Text>
                </Flex>
                <Input
                  addonBefore="https://"
                  size="large"
                  style={{ color: "#131D30" }}
                  disabled={isDesiable}
                  value={webSiteLink}
                  onChange={(e) => setWebSiteLink(e.target.value)}
                ></Input>
              </SimpleGrid>
            </Stack>
          ) : null}
        </Stack>
      </Stack>
    </Stack>
  );
}
