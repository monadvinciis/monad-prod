import { Box, Image, Stack, Text, Flex, SimpleGrid } from "@chakra-ui/react";
import { Input, message } from "antd";
import { useEffect, useState } from "react";
import { CiEdit } from "react-icons/ci";
import { FaRegUser } from "react-icons/fa";
import { IoBusiness, IoCallOutline, IoLocationOutline } from "react-icons/io5";
import { MdOutlineEmail } from "react-icons/md";
import { useSelector } from "react-redux";
import { LiaSaveSolid } from "react-icons/lia";
import { LuFileSpreadsheet } from "react-icons/lu";
import { useDispatch } from "react-redux";
import { addFileOnWeb3 } from "../../../utils/newWeb3";
import { updateUserProfile } from "../../../Actions/userActions";
import { USER_UPDATE_PROFILE_RESET } from "../../../Constants/userConstants";
import { FileSelectButton } from "../../../components/newCommans";
import { MyButton } from "../../../components/newCommans/MyButtons";

export function PersionaInfo(props: any) {
  const dispatch = useDispatch<any>();
  const [isDesiable, setIsDesiable] = useState<any>(true);
  const [name, setName] = useState<any>("");
  const [avatar, setAvatar] = useState<any>("");
  const [file, setFile] = useState<any>(null);
  const [phone, setPhone] = useState<any>("");
  const [businessName, setBusinessName] = useState<any>("");
  const [districtCity, setDistrictCity] = useState<any>("");
  const [stateUt, setStateUt] = useState<any>("");
  const [address, setAddress] = useState<any>("");
  const [pincode, setPincode] = useState<any>("");
  const [gst, setGst] = useState<any>("");
  const [loading1, setLoading1] = useState<any>(false);

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  function handleProfileSelect(file: any) {
    const fileThumbnail = URL.createObjectURL(file);
    setAvatar(fileThumbnail);
    setFile(file);
  }

  useEffect(() => {
    setName(userInfo?.name);
    setPhone(userInfo?.phone);
    setAddress(userInfo?.address);
    setStateUt(userInfo?.stateUt);
    setDistrictCity(userInfo?.districtCity);
    setPincode(userInfo?.pincode);
    setAvatar(userInfo?.avatar);
    setBusinessName(userInfo?.businessName);
    setGst(userInfo?.gst);
  }, [userInfo]);

  const userUpdateProfile = useSelector(
    (state: any) => state.userUpdateProfile
  );
  const { loading, error: errorUpdateProfile, success } = userUpdateProfile;

  useEffect(() => {
    if (success) {
      dispatch({ type: USER_UPDATE_PROFILE_RESET });
      message.success("User updated successFull!");
      setLoading1(false);
      setIsDesiable(true);
    }
    if (errorUpdateProfile) {
      message.error(errorUpdateProfile);
      dispatch({ type: USER_UPDATE_PROFILE_RESET });
      setLoading1(false);
    }
  }, [dispatch, errorUpdateProfile, success]);

  const handelSave = async () => {
    //after save
    let cid;
    if (file) {
      setLoading1(true);
      cid = await addFileOnWeb3(file);
    }
    dispatch(
      updateUserProfile({
        name,
        phone,
        businessName,
        districtCity,
        stateUt,
        address,
        pincode,
        gst,
        avatar: file ? `https://ipfs.io/ipfs/${cid}` : userInfo?.avatar,
      })
    );
  };
  return (
    <Box
      p={{ base: "4", lg: "8" }}
      bgColor="#FFFFFF"
      width="100%"
      borderColor="#E1E1E1"
      gap={{ base: "2", lg: "4" }}
    >
      <Flex justifyContent="space-between">
        <Flex gap={{ base: "4", lg: "8" }} align="center">
          <Image src={avatar} height="90px" width="90px" borderRadius="full" />
          {isDesiable ? (
            <Stack>
              <Text color="#131D30" fontSize="24px" fontWeight="700" m="0">
                {userInfo?.name}
              </Text>
              <Text color="#A1A1A1" fontSize="16px" fontWeight="400" m="0">
                {userInfo?.isMaster ? "Screen Owner" : "Advertiser"}
              </Text>
            </Stack>
          ) : (
            <FileSelectButton handleProfileSelect={handleProfileSelect} />
          )}
        </Flex>
        {isDesiable ? (
          <MyButton
            icon={<CiEdit size="20px" />}
            label="Edit Profile"
            handelClick={() => setIsDesiable(false)}
          />
        ) : (
          <MyButton
            icon={<LiaSaveSolid size="20px" />}
            label="Save"
            handelClick={handelSave}
            isLoading={loading || loading1}
            loadingText="Saving..."
          />
        )}
      </Flex>
      <Flex
        pt={{ base: "4", lg: "8" }}
        px={{ base: "4" }}
        flexDir="column"
        gap={{ base: "2", lg: "4" }}
        width={{ base: "100%", lg: "100%" }}
      >
        <SimpleGrid columns={[1, 2, 2]} spacing={{ base: "2", lg: "4" }}>
          <Flex align="center" gap={{ base: "2", lg: "4" }}>
            <IoBusiness />
            <Text fontSize="16px" fontWeight="400" color="#131D30" m="0">
              Business Name
            </Text>
          </Flex>

          <Input
            size="large"
            style={{ color: "#131D30" }}
            disabled={isDesiable}
            value={businessName}
            onChange={(e) => setBusinessName(e.target.value)}
          ></Input>
        </SimpleGrid>
        <SimpleGrid columns={[1, 2, 2]} spacing={{ base: "2", lg: "4" }}>
          <Flex align="center" gap={{ base: "2", lg: "4" }}>
            <MdOutlineEmail />
            <Text fontSize="16px" fontWeight="400" color="#131D30" m="0">
              Email Id
            </Text>
          </Flex>
          <Input
            size="large"
            style={{ color: "#131D30" }}
            disabled={true}
            value={userInfo?.email}
          ></Input>
        </SimpleGrid>
        <SimpleGrid columns={[1, 2, 2]} spacing={{ base: "2", lg: "4" }}>
          <Flex align="center" gap={{ base: "2", lg: "4" }}>
            <IoCallOutline />
            <Text fontSize="16px" fontWeight="400" color="#131D30" m="0">
              Your Contact
            </Text>
          </Flex>
          <Input
            size="large"
            style={{ color: "#131D30" }}
            disabled={isDesiable}
            value={phone}
            onChange={(e) => setPhone(e.target.value)}
          ></Input>
        </SimpleGrid>
        <SimpleGrid columns={[1, 2, 2]} spacing={{ base: "2", lg: "4" }}>
          <Flex align="center" gap={{ base: "2", lg: "4" }}>
            <FaRegUser />
            <Text fontSize="16px" fontWeight="400" color="#131D30" m="0">
              Your Name
            </Text>
          </Flex>
          <Input
            size="large"
            style={{ color: "#131D30" }}
            disabled={isDesiable}
            value={name}
            onChange={(e) => setName(e.target.value)}
          ></Input>
        </SimpleGrid>
        {isDesiable ? (
          <SimpleGrid columns={[1, 2, 2]} spacing={{ base: "2", lg: "4" }}>
            <Flex align="center" gap={{ base: "2", lg: "4" }}>
              <IoLocationOutline />
              <Text fontSize="16px" fontWeight="400" color="#131D30" m="0">
                Your Address
              </Text>
            </Flex>
            <Input
              size="large"
              style={{ color: "#131D30" }}
              disabled={isDesiable}
              value={`${userInfo?.address}, ${userInfo?.districtCity}, ${userInfo?.stateUt}, ${userInfo?.pincode}`}
            ></Input>
          </SimpleGrid>
        ) : (
          <>
            <SimpleGrid columns={[1, 2, 2]} spacing={{ base: "2", lg: "4" }}>
              <Flex align="center" gap={{ base: "2", lg: "4" }}>
                <IoLocationOutline />
                <Text fontSize="16px" fontWeight="400" color="#131D30" m="0">
                  Full Address
                </Text>
              </Flex>
              <Input
                size="large"
                style={{ color: "#131D30" }}
                disabled={isDesiable}
                value={address}
                onChange={(e) => setAddress(e.target.value)}
              ></Input>
            </SimpleGrid>
            <SimpleGrid columns={[1, 2, 2]} spacing={{ base: "2", lg: "4" }}>
              <Flex align="center" gap={{ base: "2", lg: "4" }}>
                <IoLocationOutline />
                <Text fontSize="16px" fontWeight="400" color="#131D30" m="0">
                  City
                </Text>
              </Flex>
              <Input
                size="large"
                style={{ color: "#131D30" }}
                disabled={isDesiable}
                value={districtCity}
                onChange={(e) => setDistrictCity(e.target.value)}
              ></Input>
            </SimpleGrid>
            <SimpleGrid columns={[1, 2, 2]} spacing={{ base: "2", lg: "4" }}>
              <Flex align="center" gap={{ base: "2", lg: "4" }}>
                <IoLocationOutline />
                <Text fontSize="16px" fontWeight="400" color="#131D30" m="0">
                  State
                </Text>
              </Flex>
              <Input
                size="large"
                style={{ color: "#131D30" }}
                disabled={isDesiable}
                value={stateUt}
                onChange={(e) => setStateUt(e.target.value)}
              ></Input>
            </SimpleGrid>
            <SimpleGrid columns={[1, 2, 2]} spacing={{ base: "2", lg: "4" }}>
              <Flex align="center" gap={{ base: "2", lg: "4" }}>
                <IoLocationOutline />
                <Text fontSize="16px" fontWeight="400" color="#131D30" m="0">
                  Pincode
                </Text>
              </Flex>
              <Input
                size="large"
                style={{ color: "#131D30" }}
                disabled={isDesiable}
                value={pincode}
                onChange={(e) => setPincode(e.target.value)}
              ></Input>
            </SimpleGrid>
          </>
        )}
        <SimpleGrid columns={[1, 2, 2]} spacing={{ base: "2", lg: "4" }}>
          <Flex align="center" gap={{ base: "2", lg: "4" }}>
            <LuFileSpreadsheet />
            <Text fontSize="16px" fontWeight="400" color="#131D30" m="0">
              Gst Number
            </Text>
          </Flex>
          <Input
            size="large"
            style={{ color: "#131D30" }}
            disabled={isDesiable}
            value={gst}
            onChange={(e) => setGst(e.target.value)}
          ></Input>
        </SimpleGrid>
      </Flex>
    </Box>
  );
}
