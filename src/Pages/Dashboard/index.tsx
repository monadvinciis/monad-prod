import { Box, Flex, Stack, Text } from "@chakra-ui/react";
import {
  Chart as ChartJS,
  LinearScale,
  CategoryScale,
  BarElement,
  PointElement,
  LineElement,
  Legend,
  Tooltip,
  LineController,
  BarController,
  Filler,
  ArcElement,
} from "chart.js";
import { useSelector } from "react-redux";
import { useEffect, useRef, useState } from "react";
import { useDispatch } from "react-redux";
import { getScreenWiseCampaignStatusData } from "../../Actions/screenActions";
import { message } from "antd";
import { SCREENS_CAMAPIGNS_STATUS_BY_USER_RESET } from "../../Constants/screenConstants";
import { useNavigate } from "react-router-dom";
import {
  getPendingPleaListByScreenOwnerId,
  listAllPleasByUserId,
} from "../../Actions/pleaActions";
import { userScreensList } from "../../Actions/userActions";
import { getUserWalletTransaction } from "../../Actions/walletAction";
import { convertIntoDateAndTime } from "../../utils/dateAndTimeUtils";
import { Master } from "./Master";
import { Creator } from "./Creator";

ChartJS.register(
  ArcElement,
  LinearScale,
  CategoryScale,
  BarElement,
  PointElement,
  LineElement,
  Legend,
  Tooltip,
  LineController,
  BarController,
  Filler
);

export function Dashboard(props: any) {
  // screenWiseCampaignStatus
  const chartRef = useRef<ChartJS>(null);

  const navigate = useNavigate();
  const dispatch = useDispatch<any>();

  const [result, setResult] = useState<any>(null);
  const [revenueLabels, setRevenueLabels] = useState<any>([]);
  const [graphRevenueData, setGraphRevenueData] = useState<any>([]);

  const [budgetLabels, setBudgetLabels] = useState<any>([]);
  const [graphBudgetData, setGraphBudgetData] = useState<any>([]);

  const [campaignStatusLabels, setCampaignStatusLabels] = useState<any>([
    "Active",
    "Completed",
    "Pending",
    "Pause",
    "Deleted",
  ]);
  const [campaignStatusData, setCampaignStatusData] = useState<any>([]);

  const [campaignOwnerLabels, setCampaignOwnerLabels] = useState<any>([
    "Mine",
    "Others",
  ]);
  const [campaignOwnerData, setCampaignOwnerData] = useState<any>([]);

  const [masterView, setMasterView] = useState<any>(true);
  const [creatorView, setCreatorView] = useState<any>(false);

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  const screenWiseCampaignStatus = useSelector(
    (state: any) => state.screenWiseCampaignStatus
  );
  const {
    loading: loadingScreens,
    error: errorScreens,
    data,
  } = screenWiseCampaignStatus;

  const walletTransaction = useSelector(
    (state: any) => state.walletTransaction
  );
  const {
    loading: loadingWalletTransaction,
    error: errorWalletTransaction,
    transaction,
  } = walletTransaction;

  useEffect(() => {
    if (!userInfo) {
      navigate("/signin", { state: { path: "/dashboard" } });
    } else {
      dispatch(listAllPleasByUserId());
      dispatch(getPendingPleaListByScreenOwnerId());
      dispatch(getScreenWiseCampaignStatusData());
      dispatch(userScreensList(userInfo, ""));
      dispatch(getUserWalletTransaction());
      if (
        (userInfo?.isMaster && !userInfo?.isCreator) ||
        (userInfo?.isMaster && userInfo?.isCreator)
      ) {
        setMasterView(true);
        setCreatorView(false);
      }
      if (userInfo?.isCreator && !userInfo?.isMaster) {
        setCreatorView(true);
        setMasterView(false);
      }
    }
  }, [navigate, dispatch, userInfo]);

  useEffect(() => {
    if (errorScreens) {
      message.error(errorScreens);
      dispatch({ type: SCREENS_CAMAPIGNS_STATUS_BY_USER_RESET });
    } else if (data) {
      // console.log(data);
      const temp = {
        totalCampaigns: 0,
        totalActiveCampaigns: 0,
        totalDeletdCampaigns: 0,
        totalCompletedCampaigns: 0,
        totalPendingCampaigns: 0,
        activeCampaigns: [],
        pendingCampaigns: [],
        deletdCampaigns: [],
        completedCampaigns: [],
        screensName: [],
        myCampaigns: 0,
        othersCampaigns: 0,
      };
      let newData = data?.reduce((accum: any, current: any) => {
        accum.totalCampaigns +=
          current.active +
          current.deleted +
          current.pending +
          current.completed;
        accum.myCampaigns += current.myCampaigns;
        accum.othersCampaigns += current.othersCampaigns;
        accum.totalActiveCampaigns += current.active;
        accum.totalDeletdCampaigns += current.deleted;
        accum.totalCompletedCampaigns += current.completed;
        accum.totalPendingCampaigns += current.pending;
        accum.screensName.push(current.screenName);

        accum.activeCampaigns.push(current.active);
        accum.pendingCampaigns.push(current.pending);
        accum.deletdCampaigns.push(current.deleted);
        accum.completedCampaigns.push(current.completed);

        return accum;
      }, temp);
      // console.log(newData);
      setCampaignStatusData([
        newData.totalActiveCampaigns,
        newData.totalCompletedCampaigns,
        newData.totalPendingCampaigns,
        newData.totalDeletdCampaigns,
      ]);
      setCampaignOwnerData([newData.myCampaigns, newData.othersCampaigns]);
      setResult(newData);
      newData = {}; // removing memory size
    }
    if (transaction) {
      // console.log(transaction);
      setGraphRevenueData(
        transaction
          ?.filter((tx: any) => tx.txType === "CREDIT")
          ?.map((t: any) => t.amount)
      );
      setRevenueLabels(
        transaction
          ?.filter((tx: any) => tx.txType === "CREDIT")
          ?.map((t: any) => convertIntoDateAndTime(t.updatedAt))
      );
      setGraphBudgetData(
        transaction
          ?.filter((tx: any) => tx.txType === "DEBIT")
          ?.map((t: any) => t.amount)
      );
      setBudgetLabels(
        transaction
          ?.filter((tx: any) => tx.txType === "DEBIT")
          ?.map((t: any) => convertIntoDateAndTime(t.updatedAt))
      );
    }
  }, [errorScreens, data, transaction]);

  return (
    <Box w="100%" h="100%" bgColor="#F5F5F5" m="0">
      <Flex
        width="100%"
        // height="100%"
        p={{ base: "3", lg: "3" }}
        pl={{ base: "4", lg: "4" }}
        bgColor="#FFFFFF"
        borderTop="1px"
        borderLeft="1px"
        // border="1px solid cyan"
        borderColor="#EBEBEB"
      >
        <Text
          fontWeight="700"
          fontSize={{ base: "lg", lg: "24px" }}
          m="0"
          color="#131D30"
        >
          Hello, {userInfo.name}
        </Text>
      </Flex>
      <Flex width="100%" height="94%">
        <Stack
          width="100%"
          h="100%"
          // p={{ base: "2", lg: "4" }}
          // border="1px solid gray"
        >
          <Stack
            // border="1px solid gray"
            width="100%"
            h="100%"
            p={{ base: "2", lg: "4" }}
            overflowY={"auto"}
          >
            <Flex direction="row" justify="space-around">
              <Stack>
                <Text>Screens</Text>
              </Stack>
              <Stack>
                <Text>Campaigns</Text>
              </Stack>
            </Flex>
            <Flex
              direction="row"
              width="100%"
              justify="space-between"
              gap={{ base: "2", lg: "4" }}
            >
              {masterView && <Master />}
              {creatorView && <Creator />}
            </Flex>

            {/* <SimpleGrid columns={[2, 2, 4]} spacing={{ base: "2", lg: "4" }}>
              {boxdata?.map((data: any, index: any) => (
                <SingleBox data={data} key={index} />
              ))}
            </SimpleGrid>
            <Hide below="md">
              <SimpleGrid
                // border="1px solid red"
                columns={[1, 2]}
                spacing={{ base: "4", lg: "4" }}
                mt="2"
                height="100%"
              >
                <Stack
                  // border="1px solid green"
                  spacing={{ base: "2", lg: "4" }}
                  pb="2"
                >
                  <Box
                    width="100%"
                    bgColor="#FFFFFF"
                    borderRadius="4px"
                    p={{ base: "4", lg: "4" }}
                  >
                    <Flex justify="space-between">
                      <Text
                        fontSize={{ base: "md", lg: "20px" }}
                        fontWeight="600"
                      >
                        Active Screen
                      </Text>
                    </Flex>
                    <Flex justify="space-between" w="100%">
                      {screens && (
                        <SingleScreenForCampaignCreate screen={screens[0]} />
                      )}
                    </Flex>
                  </Box>
                  <Box
                    width="100%"
                    h="30%"
                    bgColor="#FFFFFF"
                    borderRadius="4px"
                    // p={{ base: "4", lg: "4" }}
                  >
                    <Text
                      px={{ base: "4", lg: "4" }}
                      pt={{ base: "4", lg: "4" }}
                      fontWeight="600"
                      fontSize={{ base: "md", lg: "20px" }}
                    >
                      Recent Activity
                    </Text>
                    {loadingPendingPleaRequest ? (
                      <Box p="5">
                        <Skeleton avatar active paragraph={{ rows: 2 }} />
                        <Skeleton avatar active paragraph={{ rows: 2 }} />
                        <Skeleton avatar active paragraph={{ rows: 2 }} />
                      </Box>
                    ) : (
                      <Box height="100%">
                        <PleaList
                          pleaRequest={
                            pendingPleaRequest?.length !== 0
                              ? pendingPleaRequest
                              : allPleaRequest
                          }
                        />
                      </Box>
                    )}
                  </Box>
                  <Box
                    p={{ base: "4", lg: "4" }}
                    my="1"
                    width="100%"
                    bgColor="#FFFFFF"
                    borderRadius="4px"
                  >
                    <Text
                      fontWeight="600"
                      fontSize={{ base: "md", lg: "20px" }}
                    >
                      Revenue Generated
                    </Text>
                    <Line options={revenueDataOptions} data={revenueData} />
                  </Box>
                </Stack>
                <Stack
                  // border="1px solid cyan"
                  spacing={{ base: "2", lg: "4" }}
                  pb="2"
                >
                  <Box
                    p={{ base: "4", lg: "4" }}
                    bgColor="#FFFFFF"
                    justifyContent="flex-start"
                    rounded="lg"
                  >
                    <Stack>
                      <Text
                        fontSize={{ base: "md", lg: "20px" }}
                        fontWeight="600"
                      >
                        Screens Summary
                      </Text>
                      <Bar
                        // ref={chartRef}
                        // type="bar"
                        data={screenLogData}
                        options={screenLogDataOptions}
                      />
                    </Stack>
                  </Box>
                  <Box
                    bgColor="#FFFFFF"
                    justifyContent="flex-start"
                    rounded="lg"
                    p={{ base: "4", lg: "4" }}
                  >
                    <Text
                      fontSize={{ base: "md", lg: "20px" }}
                      fontWeight="600"
                    >
                      Campaigns Summary
                    </Text>
                    <SimpleGrid columns={[2]} spacing="8" px="">
                      <Stack align="center">
                        <Doughnut
                          data={campaignStatusGraphData}
                          options={campaignStatusOptions}
                          plugins={plugins}
                        />
                        <Text fontSize="12px" fontWeight="600">
                          By Status
                        </Text>
                      </Stack>
                      <Stack align="center">
                        <Doughnut
                          data={campaignOwnerGraphData}
                          options={campaignOwnerOptions}
                          plugins={plugins}
                        />
                        <Text fontSize="12px" fontWeight="600">
                          By Owners
                        </Text>
                      </Stack>
                    </SimpleGrid>
                  </Box>
                  <Box
                    p={{ base: "4", lg: "4" }}
                    width="100%"
                    bgColor="#FFFFFF"
                    borderRadius="4px"
                  >
                    <Text
                      fontWeight="600"
                      fontSize={{ base: "md", lg: "20px" }}
                    >
                      Budget Spent
                    </Text>
                    <Line options={budgetDataOptions} data={budgetData} />
                  </Box>
                </Stack>
              </SimpleGrid>
            </Hide>
            <Show below="md">
              <Stack
                // border="1px solid green"
                spacing={{ base: "2", lg: "4" }}
                pb="2"
              >
                <Box
                  width="100%"
                  bgColor="#FFFFFF"
                  borderRadius="4px"
                  p={{ base: "4", lg: "4" }}
                >
                  <Flex justify="space-between">
                    <Text
                      fontSize={{ base: "md", lg: "20px" }}
                      fontWeight="600"
                    >
                      Active Screen
                    </Text>
                    <Text p="1" fontSize="12px">
                      View All
                    </Text>
                  </Flex>
                  <Flex justify="space-between" w="100%">
                    {screens && (
                      <SingleScreenForCampaignCreate screen={screens[0]} />
                    )}
                  </Flex>
                </Box>
                <Box
                  width="100%"
                  h="30%"
                  bgColor="#FFFFFF"
                  borderRadius="4px"
                  // p={{ base: "4", lg: "4" }}
                >
                  <Text
                    px={{ base: "4", lg: "4" }}
                    pt={{ base: "4", lg: "4" }}
                    fontWeight="600"
                    fontSize={{ base: "md", lg: "20px" }}
                  >
                    Recent Activity
                  </Text>
                  {loadingPendingPleaRequest ? (
                    <Box p="5">
                      <Skeleton avatar active paragraph={{ rows: 2 }} />
                      <Skeleton avatar active paragraph={{ rows: 2 }} />
                      <Skeleton avatar active paragraph={{ rows: 2 }} />
                    </Box>
                  ) : (
                    <Box height="100%">
                      <PleaList
                        pleaRequest={
                          pendingPleaRequest?.length !== 0
                            ? pendingPleaRequest
                            : allPleaRequest
                        }
                      />
                    </Box>
                  )}
                </Box>
                <Box
                  p={{ base: "4", lg: "4" }}
                  bgColor="#FFFFFF"
                  justifyContent="flex-start"
                  rounded="lg"
                >
                  <Stack>
                    <Text
                      fontSize={{ base: "md", lg: "20px" }}
                      fontWeight="600"
                    >
                      Screens Summary
                    </Text>
                    <Bar
                      // ref={chartRef}
                      // type="bar"
                      data={screenLogData}
                      options={screenLogDataOptions}
                    />
                  </Stack>
                </Box>
                <Box
                  bgColor="#FFFFFF"
                  justifyContent="flex-start"
                  rounded="lg"
                  p={{ base: "4", lg: "4" }}
                >
                  <Text fontSize={{ base: "md", lg: "20px" }} fontWeight="600">
                    Campaigns Summary
                  </Text>
                  <SimpleGrid columns={[2]} spacing="8" px="">
                    <Stack align="center">
                      <Doughnut
                        data={campaignStatusGraphData}
                        options={campaignStatusOptions}
                        plugins={plugins}
                      />
                      <Text fontSize="12px" fontWeight="600">
                        By Status
                      </Text>
                    </Stack>
                    <Stack align="center">
                      <Doughnut
                        data={campaignOwnerGraphData}
                        options={campaignOwnerOptions}
                        plugins={plugins}
                      />
                      <Text fontSize="12px" fontWeight="600">
                        By Owners
                      </Text>
                    </Stack>
                  </SimpleGrid>
                </Box>
                <Box
                  p={{ base: "4", lg: "4" }}
                  my="1"
                  width="100%"
                  bgColor="#FFFFFF"
                  borderRadius="4px"
                >
                  <Text fontWeight="600" fontSize={{ base: "md", lg: "20px" }}>
                    Revenue Generated
                  </Text>
                  <Line options={revenueDataOptions} data={revenueData} />
                </Box>
                <Box
                  p={{ base: "4", lg: "4" }}
                  width="100%"
                  bgColor="#FFFFFF"
                  borderRadius="4px"
                >
                  <Text fontWeight="600" fontSize={{ base: "md", lg: "20px" }}>
                    Budget Spent
                  </Text>
                  <Line options={budgetDataOptions} data={budgetData} />
                </Box>
              </Stack>
            </Show> */}
          </Stack>
        </Stack>
      </Flex>
    </Box>
  );
}
