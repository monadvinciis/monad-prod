import {
  Box,
  Flex,
  Hide,
  Show,
  SimpleGrid,
  Stack,
  Text,
} from "@chakra-ui/react";
import {
  Chart as ChartJS,
  LinearScale,
  CategoryScale,
  BarElement,
  PointElement,
  LineElement,
  Legend,
  Tooltip,
  LineController,
  BarController,
  Filler,
  ArcElement,
} from "chart.js";
import { Line, Bar, Doughnut } from "react-chartjs-2";
import { useSelector } from "react-redux";
import { useEffect, useRef, useState } from "react";
import { useDispatch } from "react-redux";
import { getScreenWiseCampaignStatusData } from "../../Actions/screenActions";
import { Skeleton, message } from "antd";
import { SCREENS_CAMAPIGNS_STATUS_BY_USER_RESET } from "../../Constants/screenConstants";
import { useNavigate } from "react-router-dom";
import { PleaList } from "../PleaList";
import {
  getPendingPleaListByScreenOwnerId,
  listAllPleasByUserId,
} from "../../Actions/pleaActions";
import { userScreensList } from "../../Actions/userActions";
import { SingleScreenForCampaignCreate } from "../../components/newCommans";
import { MdCampaign } from "react-icons/md";
import { GiPayMoney } from "react-icons/gi";
import { getUserWalletTransaction } from "../../Actions/walletAction";
import { convertIntoDateAndTime } from "../../utils/dateAndTimeUtils";
import { SingleBox } from "./SingleBox";

ChartJS.register(
  ArcElement,
  LinearScale,
  CategoryScale,
  BarElement,
  PointElement,
  LineElement,
  Legend,
  Tooltip,
  LineController,
  BarController,
  Filler
);

export function Creator(props: any) {
  // screenWiseCampaignStatus
  const chartRef = useRef<ChartJS>(null);

  const navigate = useNavigate();
  const dispatch = useDispatch<any>();

  const [result, setResult] = useState<any>(null);
  const [revenueLabels, setRevenueLabels] = useState<any>([]);
  const [graphRevenueData, setGraphRevenueData] = useState<any>([]);

  const [budgetLabels, setBudgetLabels] = useState<any>([]);
  const [graphBudgetData, setGraphBudgetData] = useState<any>([]);

  const [campaignStatusLabels, setCampaignStatusLabels] = useState<any>([
    "Active",
    "Completed",
    "Pending",
    "Pause",
    "Deleted",
  ]);
  const [campaignStatusData, setCampaignStatusData] = useState<any>([]);

  const [campaignOwnerLabels, setCampaignOwnerLabels] = useState<any>([
    "Mine",
    "Others",
  ]);
  const [campaignOwnerData, setCampaignOwnerData] = useState<any>([]);

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  const pendingPleaListByScreenOwner = useSelector(
    (state: any) => state.pendingPleaListByScreenOwner
  );
  const {
    allPleas: pendingPleaRequest,
    loading: loadingPendingPleaRequest,
    error: errorPendingPleaRequest,
  } = pendingPleaListByScreenOwner;

  const allPleasListByUser = useSelector(
    (state: any) => state.allPleasListByUser
  );
  const {
    allPleas: allPleaRequest,
    loading: loadingAllPleaRequest,
    error: errorAllPleaRequest,
  } = allPleasListByUser;

  const screenWiseCampaignStatus = useSelector(
    (state: any) => state.screenWiseCampaignStatus
  );
  const {
    loading: loadingScreens,
    error: errorScreens,
    data,
  } = screenWiseCampaignStatus;

  const userScreens = useSelector((state: any) => state.userScreens);
  const {
    loading: loadingUserScreens,
    error: errorUserScreens,
    screens,
  } = userScreens;

  const walletTransaction = useSelector(
    (state: any) => state.walletTransaction
  );
  const {
    loading: loadingWalletTransaction,
    error: errorWalletTransaction,
    transaction,
  } = walletTransaction;
  useEffect(() => {
    if (!userInfo) {
      navigate("/signin", { state: { path: "/dashboard" } });
    } else {
      dispatch(listAllPleasByUserId());
      dispatch(getPendingPleaListByScreenOwnerId());
      dispatch(getScreenWiseCampaignStatusData());
      dispatch(userScreensList(userInfo, ""));
      dispatch(getUserWalletTransaction());
    }
  }, [navigate, dispatch, userInfo]);

  function dynamicColors() {
    var r = Math.floor(Math.random() * 255);
    var b = Math.floor(Math.random() * 255);
    var g = Math.floor(Math.random() * 255);
    // console.log("rgb(" + r + "," + g + "," + b + ")");
    return "rgba(" + r + "," + g + "," + 255 + "," + 0.5 + ")";
  }

  function poolColors(a: number) {
    const pool = [];
    for (let i: number = 0; i < a; i++) {
      pool.push(dynamicColors());
    }
    return pool;
  }

  useEffect(() => {
    if (errorScreens) {
      message.error(errorScreens);
      dispatch({ type: SCREENS_CAMAPIGNS_STATUS_BY_USER_RESET });
    } else if (data) {
      // console.log(data);
      const temp = {
        totalCampaigns: 0,
        totalActiveCampaigns: 0,
        totalDeletdCampaigns: 0,
        totalCompletedCampaigns: 0,
        totalPendingCampaigns: 0,
        activeCampaigns: [],
        pendingCampaigns: [],
        deletdCampaigns: [],
        completedCampaigns: [],
        screensName: [],
        myCampaigns: 0,
        othersCampaigns: 0,
      };
      let newData = data?.reduce((accum: any, current: any) => {
        accum.totalCampaigns +=
          current.active +
          current.deleted +
          current.pending +
          current.completed;
        accum.myCampaigns += current.myCampaigns;
        accum.othersCampaigns += current.othersCampaigns;
        accum.totalActiveCampaigns += current.active;
        accum.totalDeletdCampaigns += current.deleted;
        accum.totalCompletedCampaigns += current.completed;
        accum.totalPendingCampaigns += current.pending;
        accum.screensName.push(current.screenName);

        accum.activeCampaigns.push(current.active);
        accum.pendingCampaigns.push(current.pending);
        accum.deletdCampaigns.push(current.deleted);
        accum.completedCampaigns.push(current.completed);

        return accum;
      }, temp);
      // console.log(newData);
      setCampaignStatusData([
        newData.totalActiveCampaigns,
        newData.totalCompletedCampaigns,
        newData.totalPendingCampaigns,
        newData.totalDeletdCampaigns,
      ]);
      setCampaignOwnerData([newData.myCampaigns, newData.othersCampaigns]);
      setResult(newData);
      newData = {}; // removing memory size
    }
    if (transaction) {
      // console.log(transaction);
      setGraphRevenueData(
        transaction
          ?.filter((tx: any) => tx.txType === "CREDIT")
          ?.map((t: any) => t.amount)
      );
      setRevenueLabels(
        transaction
          ?.filter((tx: any) => tx.txType === "CREDIT")
          ?.map((t: any) => convertIntoDateAndTime(t.updatedAt))
      );
      setGraphBudgetData(
        transaction
          ?.filter((tx: any) => tx.txType === "DEBIT")
          ?.map((t: any) => t.amount)
      );
      setBudgetLabels(
        transaction
          ?.filter((tx: any) => tx.txType === "DEBIT")
          ?.map((t: any) => convertIntoDateAndTime(t.updatedAt))
      );
    }
  }, [errorScreens, data, transaction]);

  const revenueData: any = {
    labels: revenueLabels,
    datasets: [
      {
        type: "line",
        label: "Earned Revenue",
        borderColor: "rgb(204, 204, 255)",
        // borderWidth: 2,
        fill: true,
        backgroundColor: "rgb(204, 204, 255, 0.4)",
        data: graphRevenueData,
        lineTension: 0.3,
      },
    ],
  };

  const revenueDataOptions: any = {
    responsive: true,
    plugins: {
      legend: {
        position: "top",
        display: false,
      },
      title: {
        display: true,
        text: "User Gain and Loss",
      },
    },
    scales: {
      x: {
        // Set the category percentage to 100%
        categoryPercentage: 100,
        stacked: true,
        reverse: true,
        // type: "date",
        ticks: {
          source: "auto",
          // Disabled rotation for performance
          maxRotation: 0,
          autoSkip: true,
        },
      },
      y: {
        // type: "linear",
        // beginAtZero: true,
        title: {
          text: "Earned Revenue",
          display: true,
        },
        // ticks: {
        //   source: "auto",
        //   callback: function (value: any) {
        //     if (Number.isInteger(value)) {
        //       return value;
        //     }
        //   },
        // },
        stacked: true,
      },
    },
  };

  const budgetData: any = {
    labels: budgetLabels,
    datasets: [
      {
        type: "line",
        label: "Spended Amount",
        borderColor: "rgb(255, 99, 132)",
        // borderWidth: 2,
        fill: true,
        backgroundColor: "rgb(255, 99, 132, 0.4)",
        data: graphBudgetData,
        lineTension: 0.3,
      },
    ],
  };

  const budgetDataOptions: any = {
    responsive: true,
    plugins: {
      legend: {
        position: "top",
        display: false,
      },
      title: {
        display: true,
        text: "User Gain and Loss",
      },
    },
    scales: {
      x: {
        // Set the category percentage to 100%
        categoryPercentage: 100,
        stacked: true,
        reverse: true,
        // type: "date",
        ticks: {
          source: "auto",
          // Disabled rotation for performance
          maxRotation: 0,
          autoSkip: true,
        },
      },
      y: {
        // type: "linear",
        // beginAtZero: true,
        title: {
          text: "Spended Amount",
          display: true,
        },
        // ticks: {
        //   source: "auto",
        //   callback: function (value: any) {
        //     if (Number.isInteger(value)) {
        //       return value;
        //     }
        //   },
        // },
        stacked: true,
      },
    },
  };

  const screenLogData: any = {
    labels: result?.screensName,
    datasets: [
      {
        // type: "bar",
        label: "Active Campaigns",
        // backgroundColor: "rgb(53, 162, 235)",
        data: result?.activeCampaigns,
        // barPercentage: "0.6",
        barThickness: "flex",
        borderRadius: 5,
        backgroundColor: function (context: any) {
          var chart = context.chart;
          var { ctx, chartArea } = chart;

          if (!chartArea) {
            // This case happens on initial chart load
            return null;
          }
          var gradient = ctx.createLinearGradient(0, 0, 0, chart.height);
          gradient.addColorStop(0, "rgba(153, 102, 255, 1)");
          gradient.addColorStop(0.5, "rgba(153, 102, 255, 0.7)");
          gradient.addColorStop(1, "rgba(255, 255, 255, 0.5");
          return gradient;
        },
      },
      {
        // type: "bar",
        label: "Pending Campaigns",
        // backgroundColor: "rgb(53, 162, 200)",
        data: result?.pendingCampaigns,
        // barPercentage: "0.6",
        barThickness: "flex",
        borderRadius: 5,
        backgroundColor: function (context: any) {
          var chart = context.chart;
          var { ctx, chartArea } = chart;

          if (!chartArea) {
            // This case happens on initial chart load
            return null;
          }
          var gradient = ctx.createLinearGradient(0, 0, 0, chart.height);
          gradient.addColorStop(1, "rgba(153, 102, 255, 1)");
          gradient.addColorStop(0.5, "rgba(153, 102, 255, 0.7)");
          gradient.addColorStop(0, "rgba(255, 255, 255, 0.5");
          return gradient;
        },
      },
    ],
  };

  const screenLogDataOptions: any = {
    responsive: true,
    plugins: {
      legend: {
        position: "top",
        display: false,
      },
      title: {
        display: true,
        text: "Screens Summary",
      },
    },
    scales: {
      x: {
        // Set the category percentage to 100%
        categoryPercentage: 100,
        stacked: true,
        // type: "date",
        ticks: {
          callback: function (_: any, val: any) {
            var newthis = this as any;
            // Hide every 2nd tick label
            return newthis.getLabelForValue(val).substring(0, 4);
          },
        },
      },
      y: {
        // type: "linear",
        beginAtZero: true,
        title: {
          text: "Total Campaigns",
          display: true,
        },
        stacked: true,
      },
    },
  };

  const campaignStatusGraphData = {
    labels: campaignStatusLabels,
    datasets: [
      {
        label: "Campaigns Status",
        data: campaignStatusData,
        backgroundColor: poolColors(campaignStatusData?.length),
        borderColor: poolColors(campaignStatusData?.length).map((br: any) => {
          let regex = /(\d+(?:\.\d+)?)\)$/;
          return br.replace(regex, 0.3 + ")");
        }),
        // borderWidth: 1,
        hoverBackgroundColor: poolColors(campaignStatusData?.length).map(
          (br: any) => {
            let regex = /(\d+(?:\.\d+)?)\)$/;
            return br.replace(regex, 0.7 + ")");
          }
        ),
        offset: 0, // add this prop
        hoverOffset: 50,
      },
    ],
  };

  const campaignStatusOptions = {
    cutout: 30,
    spacing: 1,
    borderWidth: 0,
    borderRadius: 5,
    plugins: {
      legend: {
        display: false,
        responsive: true,
        position: "bottom" as const,
        labels: {
          boxWidth: 36,
          padding: 10,
          font: {
            size: 15,
          },
        },
        // align: "center",
      },
    },
    layout: {
      padding: 25,
    },
  };

  const campaignOwnerGraphData = {
    labels: campaignOwnerLabels,
    datasets: [
      {
        label: "Campaigns Owners",
        data: campaignOwnerData,
        backgroundColor: poolColors(campaignOwnerData?.length),
        borderColor: poolColors(campaignOwnerData?.length).map((br: any) => {
          let regex = /(\d+(?:\.\d+)?)\)$/;
          return br.replace(regex, 0.3 + ")");
        }),
        // borderWidth: 1,
        hoverBackgroundColor: poolColors(campaignOwnerData?.length).map(
          (br: any) => {
            let regex = /(\d+(?:\.\d+)?)\)$/;
            return br.replace(regex, 0.7 + ")");
          }
        ),
        offset: 0, // add this prop
        hoverOffset: 50,
      },
    ],
  };

  const campaignOwnerOptions = {
    cutout: 30,
    spacing: 1,
    borderWidth: 0,
    borderRadius: 5,
    plugins: {
      legend: {
        display: false,
        responsive: true,
        position: "bottom" as const,
        labels: {
          boxWidth: 36,
          padding: 10,
          font: {
            size: 15,
          },
        },
        // align: "center",
      },
    },
    layout: {
      padding: 25,
    },
  };

  const plugins = [
    {
      id: "plugins",
      beforeDatasetsDraw: function (chart: any) {
        const { ctx, data } = chart;
        const xCoor = chart?.getDatasetMeta(0)?.data[0]?.x;
        const yCoor = chart?.getDatasetMeta(0)?.data[0]?.y;

        ctx.save();
        // ctx.font = "bolder 50px sans-serif";
        // ctx.fillStyle = "black";
        // ctx.textAlign = "center";
        // ctx.textBaseline = "middle";
        // ctx.fillText(result?.totalCampaigns?.length, xCoor, yCoor - 15);

        // ctx.font = "15px sans-serif";
        // ctx.fillStyle = "black";
        // ctx.fillText(`Advertisers`, xCoor, yCoor + 25);
      },
    },
  ];

  const boxdata = [
    {
      icon: <MdCampaign size="25px" color="teal" />,
      label: "Campaigns",
      value: result?.totalCampaigns || 0,
      path: "/my/campaigns",
    },
    {
      icon: <GiPayMoney size="20px" color="red" />,
      label: "Spent",
      value: transaction
        ?.filter((tx: any) => tx.txType === "DEBIT")
        ?.map((t: any) => t.amount)
        ?.reduce((accumulator: any, current: any) => {
          return accumulator + current;
        }, 0),
      path: "/my/wallet",
    },
  ];
  return (
    <Flex width="100%" height="94%">
      <Stack
        width="100%"
        h="100%"
        // p={{ base: "2", lg: "4" }}
        // border="1px solid gray"
      >
        <Stack
          // border="1px solid gray"
          width="100%"
          h="100%"
          p={{ base: "2", lg: "4" }}
          overflowY={"auto"}
        >
          <SimpleGrid columns={[1, 2]} spacing={{ base: "2", lg: "4" }}>
            {boxdata?.map((data: any, index: any) => (
              <SingleBox data={data} key={index} />
            ))}
          </SimpleGrid>
          <Hide below="md">
            {/* <SimpleGrid
              // border="1px solid red"
              columns={[1, 2]}
              spacing={{ base: "4", lg: "4" }}
              mt="2"
              height="100%"
            > */}
            <Stack
              // border="1px solid green"
              spacing={{ base: "2", lg: "4" }}
              pb="2"
            >
              <Box
                width="100%"
                bgColor="#FFFFFF"
                borderRadius="4px"
                p={{ base: "4", lg: "4" }}
              >
                <Flex justify="space-between">
                  <Text fontSize={{ base: "md", lg: "20px" }} fontWeight="600">
                    Active Screen
                  </Text>
                </Flex>
                <Flex justify="space-between" w="100%">
                  {screens && (
                    <SingleScreenForCampaignCreate screen={screens[0]} />
                  )}
                </Flex>
              </Box>
              <Box
                width="100%"
                h="30%"
                bgColor="#FFFFFF"
                borderRadius="4px"
                // p={{ base: "4", lg: "4" }}
              >
                <Text
                  px={{ base: "4", lg: "4" }}
                  pt={{ base: "4", lg: "4" }}
                  fontWeight="600"
                  fontSize={{ base: "md", lg: "20px" }}
                >
                  Recent Activity
                </Text>
                {loadingPendingPleaRequest ? (
                  <Box p="5">
                    <Skeleton avatar active paragraph={{ rows: 2 }} />
                    <Skeleton avatar active paragraph={{ rows: 2 }} />
                    <Skeleton avatar active paragraph={{ rows: 2 }} />
                  </Box>
                ) : (
                  <Box height="100%">
                    <PleaList
                      pleaRequest={
                        pendingPleaRequest?.length !== 0
                          ? pendingPleaRequest
                          : allPleaRequest
                      }
                    />
                  </Box>
                )}
              </Box>
              <Box
                p={{ base: "4", lg: "4" }}
                my="1"
                width="100%"
                bgColor="#FFFFFF"
                borderRadius="4px"
              >
                <Text fontWeight="600" fontSize={{ base: "md", lg: "20px" }}>
                  Revenue Generated
                </Text>
                <Line options={revenueDataOptions} data={revenueData} />
              </Box>
            </Stack>
            <Stack
              // border="1px solid cyan"
              spacing={{ base: "2", lg: "4" }}
              pb="2"
            >
              <Box
                p={{ base: "4", lg: "4" }}
                bgColor="#FFFFFF"
                justifyContent="flex-start"
                rounded="lg"
              >
                <Stack>
                  <Text fontSize={{ base: "md", lg: "20px" }} fontWeight="600">
                    Screens Summary
                  </Text>
                  <Bar
                    // ref={chartRef}
                    // type="bar"
                    data={screenLogData}
                    options={screenLogDataOptions}
                  />
                </Stack>
              </Box>
              <Box
                bgColor="#FFFFFF"
                justifyContent="flex-start"
                rounded="lg"
                p={{ base: "4", lg: "4" }}
              >
                <Text fontSize={{ base: "md", lg: "20px" }} fontWeight="600">
                  Campaigns Summary
                </Text>
                <SimpleGrid columns={[2]} spacing="8" px="">
                  <Stack align="center">
                    <Doughnut
                      data={campaignStatusGraphData}
                      options={campaignStatusOptions}
                      plugins={plugins}
                    />
                    <Text fontSize="12px" fontWeight="600">
                      By Status
                    </Text>
                  </Stack>
                  <Stack align="center">
                    <Doughnut
                      data={campaignOwnerGraphData}
                      options={campaignOwnerOptions}
                      plugins={plugins}
                    />
                    <Text fontSize="12px" fontWeight="600">
                      By Owners
                    </Text>
                  </Stack>
                </SimpleGrid>
              </Box>
              <Box
                p={{ base: "4", lg: "4" }}
                width="100%"
                bgColor="#FFFFFF"
                borderRadius="4px"
              >
                <Text fontWeight="600" fontSize={{ base: "md", lg: "20px" }}>
                  Budget Spent
                </Text>
                <Line options={budgetDataOptions} data={budgetData} />
              </Box>
            </Stack>
            {/* </SimpleGrid> */}
          </Hide>
          <Show below="md">
            <Stack
              // border="1px solid green"
              spacing={{ base: "2", lg: "4" }}
              pb="2"
            >
              <Box
                width="100%"
                bgColor="#FFFFFF"
                borderRadius="4px"
                p={{ base: "4", lg: "4" }}
              >
                <Flex justify="space-between">
                  <Text fontSize={{ base: "md", lg: "20px" }} fontWeight="600">
                    Active Screen
                  </Text>
                  <Text p="1" fontSize="12px">
                    View All
                  </Text>
                </Flex>
                <Flex justify="space-between" w="100%">
                  {screens && (
                    <SingleScreenForCampaignCreate screen={screens[0]} />
                  )}
                </Flex>
              </Box>
              <Box
                width="100%"
                h="30%"
                bgColor="#FFFFFF"
                borderRadius="4px"
                // p={{ base: "4", lg: "4" }}
              >
                <Text
                  px={{ base: "4", lg: "4" }}
                  pt={{ base: "4", lg: "4" }}
                  fontWeight="600"
                  fontSize={{ base: "md", lg: "20px" }}
                >
                  Recent Activity
                </Text>
                {loadingPendingPleaRequest ? (
                  <Box p="5">
                    <Skeleton avatar active paragraph={{ rows: 2 }} />
                    <Skeleton avatar active paragraph={{ rows: 2 }} />
                    <Skeleton avatar active paragraph={{ rows: 2 }} />
                  </Box>
                ) : (
                  <Box height="100%">
                    <PleaList
                      pleaRequest={
                        pendingPleaRequest?.length !== 0
                          ? pendingPleaRequest
                          : allPleaRequest
                      }
                    />
                  </Box>
                )}
              </Box>
              <Box
                bgColor="#FFFFFF"
                justifyContent="flex-start"
                rounded="lg"
                p={{ base: "4", lg: "4" }}
              >
                <Text fontSize={{ base: "md", lg: "20px" }} fontWeight="600">
                  Campaigns Summary
                </Text>
                <SimpleGrid columns={[2]} spacing="8" px="">
                  <Stack align="center">
                    <Doughnut
                      data={campaignStatusGraphData}
                      options={campaignStatusOptions}
                      plugins={plugins}
                    />
                    <Text fontSize="12px" fontWeight="600">
                      By Status
                    </Text>
                  </Stack>
                  <Stack align="center">
                    <Doughnut
                      data={campaignOwnerGraphData}
                      options={campaignOwnerOptions}
                      plugins={plugins}
                    />
                    <Text fontSize="12px" fontWeight="600">
                      By Owners
                    </Text>
                  </Stack>
                </SimpleGrid>
              </Box>
              <Box
                p={{ base: "4", lg: "4" }}
                width="100%"
                bgColor="#FFFFFF"
                borderRadius="4px"
              >
                <Text fontWeight="600" fontSize={{ base: "md", lg: "20px" }}>
                  Budget Spent
                </Text>
                <Line options={budgetDataOptions} data={budgetData} />
              </Box>
            </Stack>
          </Show>
        </Stack>
      </Stack>
    </Flex>
  );
}
