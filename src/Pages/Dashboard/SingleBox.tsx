import { Box, Center, Stack, Text } from "@chakra-ui/react";
import { Statistic } from "antd";
import { useNavigate } from "react-router-dom";
import CountUp from "react-countup";

export function SingleBox(props: any) {
  const { data } = props;
  const navigate = useNavigate();
  const formatter = (value: any) => <CountUp end={value} separator="," />;

  return (
    <Box
      bgColor="#FFFFFF"
      p={{ base: "4", lg: "4" }}
      borderRadius="4px"
      width=""
      height={{ base: "150px", lg: "200px" }}
      onClick={() => {
        navigate(data?.path);
      }}
    >
      <Stack
        border="1px"
        p="2"
        borderRadius="full"
        height="37px"
        width="37px"
        alignItems="center"
        justifyContent="center"
        color="#131D30"
      >
        {data?.icon}
      </Stack>
      <Center flexDirection="column">
        {/* <Text fontSize={{ base: "30px", lg: "40px" }} fontWeight="400"> */}
        <Statistic
          title=""
          valueStyle={{ fontSize: "40" }}
          value={data?.value || 0}
          precision={2}
          formatter={formatter}
        />
        {/* </Text> */}
        <Text fontSize={{ base: "16px", lg: "16px" }} fontWeight="600">
          {data?.label}
        </Text>
      </Center>
    </Box>
  );
}
