import React, { useState, useEffect } from "react";
import useWebSocket, { ReadyState } from "react-use-websocket";

const WS_URL = "ws://127.0.0.1:3333";

export const Message = () => {
  const [messages, setMessages] = useState<any>([]);
  const [input, setInput] = useState("");
  const [user, setUser] = useState("");

  // useWebSocket(WS_URL, {
  //   onOpen: () => {
  //     console.log("WebSocket connection established.");
  //   },
  // });

  const { sendJsonMessage, lastJsonMessage, readyState } = useWebSocket(
    WS_URL,
    {
      queryParams: { username: "kishan" },
      share: false,
      shouldReconnect: () => true,
    }
  );
  // console.log(lastJsonMessage);
  useEffect(() => {
    console.log("Connection state changed");
    if (readyState === ReadyState.OPEN) {
      sendJsonMessage({
        event: "subscribe",
        data: {
          channel: "general-chatroom",
        },
      });
    }
  }, [readyState]);

  // Run when a new WebSocket message is received (lastJsonMessage)
  // useEffect(() => {
  //   console.log(`Got a new message: ${lastJsonMessage}`);
  // }, [lastJsonMessage]);

  const sendMessage = () => {
    if (input && user) {
      const messageData = {
        user,
        content: input,
      };

      // Send the message to the server
      // socket.emit("chatMessage", messageData);

      // Update local state
      setMessages((prevMessages: any) => [...prevMessages, messageData]);
      setInput("");
      sendJsonMessage({
        event: "subscribe",
        data: {
          channel: "general-chatroom",
          message: messageData,
        },
      });
    }
  };

  return (
    <div>
      <div>
        <input
          type="text"
          placeholder="Enter your username"
          value={user}
          onChange={(e) => setUser(e.target.value)}
        />
      </div>
      <div>
        <ul>
          {messages.map((message: any, index: any) => (
            <li key={index}>
              <strong>{message?.user}:</strong> {message.content}
            </li>
          ))}
        </ul>
      </div>
      <div>
        <input
          type="text"
          placeholder="Type your message"
          value={input}
          onChange={(e) => setInput(e.target.value)}
        />
        <button onClick={sendMessage}>Send</button>
      </div>
    </div>
  );
};
