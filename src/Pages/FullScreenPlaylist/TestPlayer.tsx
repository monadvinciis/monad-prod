/* eslint-disable prettier/prettier */
import React, { useState, useEffect, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";
import { screenVideosList } from "../../Actions/screenActions";
import { Box, Stack } from "@chakra-ui/react";
import Webcam from "react-webcam";
import Axios from "axios";
import useWebSocket, { ReadyState } from "react-use-websocket";

const WS_URL = "ws://127.0.0.1:3333";

const videoConstraints = {
  width: 1920,
  height: 1080,
  facingMode: "user",
  // facingMode: { exact: "environment" }
};

export function TestPlayer() {
  const screenId = window.location.pathname.split("/").splice(-1)[0];
  // console.log(screenId);
  const videoRef = React.useRef<any>(null);
  const activity = React.useRef<any>();
  const timerId = React.useRef<any>(null);

  const [screenPlaylist, setScreenPlaylist] = useState<any>(null);
  const [checkScreenPlaylist, setCheckScreenPlaylist] = useState<any>(null);

  const [isFullScreen, setIsFullScreen] = useState<any>(false);
  const [adIndex, setAdIndex] = useState<any>(
    localStorage.getItem("adIndex")
      ? parseInt(localStorage.getItem("adIndex")!)
      : 0
  );
  const [adVid, setAdVid] = useState<any>(
    "https://bafkreiex3fu3b3gnbivhqo32ib7xulesnhru6wqovh3shhrbl4xc7gac3a.ipfs.w3s.link"
  );

  const webcamRef = React.useRef<Webcam>(null);
  const [img, setImg] = useState<string | null>(null);

  const { sendJsonMessage, lastJsonMessage, readyState } = useWebSocket(
    WS_URL,
    {
      queryParams: { username: screenId },
      share: false,
      shouldReconnect: () => true,
    }
  );
  
  console.log(lastJsonMessage);

  const getScreenVideos = useSelector((state: any) => state.getScreenVideos);
  const {
    data,
    loading: loadingScreenVideos,
    error: errorScreenVideos,
  } = getScreenVideos;

  const dispatch = useDispatch<any>();

  useEffect(() => {
    // console.log('vid: ', vid);
    // navigator.permissions.query({ name: "camera" as PermissionName}).then((permissionObj) => {
    //   console.log(permissionObj.state);
    //   if (permissionObj.state !== "granted") {
    //     navigator.mediaDevices.getUserMedia({ "video": true }).then((media) => {
    //       media.getTracks().forEach((track) => track.stop());
    //     });
    //   }
    // });

    if (localStorage.getItem("adIndex")) {
      // console.log(localStorage.getItem("adIndex"));
    } else {
      localStorage.setItem("adIndex", JSON.stringify(0));
    }
    dispatch(screenVideosList(screenId));

    activity.current = new Date().getTime();
    timerId.current = setInterval(async () => {
      const timeDif = (new Date().getTime() - activity.current) / 1000;
      console.log("timeDif", timeDif);

      if (timeDif > 120) {
        console.log(timeDif);
        dispatch(screenVideosList(screenId));
      }
    }, 60000);
    return () => {
      clearInterval(timerId.current);
    };
  }, [dispatch, screenId]);

  useEffect(() => {
    if (data) {
      // console.log(localStorage.getItem("adIndex"));
      // console.log(data.myScreenVideos.map((v: any) => { return `https://${v.video.split("/").splice(-1)[0]}.ipfs.w3s.link` }));

      setScreenPlaylist(data.myScreenVideos);
      setAdIndex(parseInt(localStorage.getItem("adIndex")!));
    }
  }, [data]);

  useEffect(() => {
    console.log("Connection state changed", data);
    if (data && readyState === ReadyState.OPEN) {
      sendJsonMessage({
        event: "playback",
        data: {},
      });
    }
  }, [data, readyState]);


  const update = useCallback(async () => {
    const { data } = await Axios.get(
      `${process.env.REACT_APP_BLINDS_SERVER}/api/screens/${screenId}/screenVideos`
    );
    setCheckScreenPlaylist(data?.myScreenVideos);
    activity.current = new Date().getTime();
  }, [screenId]);

  const onLoadStart = (e: any) => {
    console.log("ON LOAD START: ", adVid);
    // console.log(data?.myScreenVideos?.length);
  };

  const onLoad = (e: any) => {
    // console.log(`ON LOAD: `, e);
    if (
      checkScreenPlaylist &&
      screenPlaylist?.length !== checkScreenPlaylist?.length
    ) {
      dispatch(screenVideosList(screenId));
    }
  };

  const onError = (e: any) => {
    // console.log(window.location.pathname);
    console.log(`ON ERROR : ${e}`);

    setAdVid("https://bafkreiex3fu3b3gnbivhqo32ib7xulesnhru6wqovh3shhrbl4xc7gac3a.ipfs.w3s.link");
    setTimeout(() => {
      if ("caches" in window) {
        caches.keys().then((names) => {
          // Delete all the cache files
          names.forEach((name) => {
            caches.delete(name);
          });
          // console.log(names);
        });
      }
      dispatch(screenVideosList(screenId));
    }, 30000);
  };

  const onEnd = (e: any) => {
    // setImg("");
    // console.log("on end: ", e);
    const message: any = {
      type: "playbackData",
      command: "Video Ended",
      data: `${screenPlaylist?.map((video: any) => video.video)[adIndex]}`,
      specialScreen: data.screen.specialScreen ? data : null,
    };
    // console.log(window.parent);
    console.log(message);
    // window.parent.postMessage(message, "*");
    window.parent.postMessage(JSON.stringify(message), "*");



    if (screenPlaylist) {
      // window.location.reload();
      // console.log(screenPlaylist?.map((video: any) => { return `https://${video.video.split("/").splice(-1)[0]}.ipfs.w3s.link` })[adIndex]);
      setAdVid(screenPlaylist?.map((video: any) => { return `https://${video.video.split("/").splice(-1)[0]}.ipfs.w3s.link` })[adIndex]);
      if (screenPlaylist?.length > 0) {
        update();

        if (adIndex >= screenPlaylist?.length - 1) {
          setAdIndex(0);
          localStorage.setItem("adIndex", JSON.stringify(0));
        } else {
          setAdIndex(adIndex + 1);
          localStorage.setItem("adIndex", JSON.stringify(adIndex + 1));
        }
        e.target.src = screenPlaylist?.map((video: any) => video.video)[
          adIndex
        ];

        e.target.play();
      }
    }
    console.log('Done Playing');
  };

  const onPlay = () => {
    console.log("playing");

    // capture();

    setTimeout(() => {
      // dispatch(screenVideosList(screenId));
    }, 10000);
  };

  const onEmpty = (e: any) => {
    // console.log(e);
  };

  const onReadyForDisplay = () => {
    setIsFullScreen(isFullScreen);
  };

  const onProgress = (data: any) => {
    // setIsPlaying(true);
    // Video Player will progress continue even if it ends
    // if ((data.seekableDuration - data.currentTime) < 0.5 && files && screenPlaylist && files.length !== screenPlaylist.length && !downloading) {
    if (adIndex === 0 && data.seekableDuration - data.currentTime < 0.2) {
      // onEnd();
    }
  };
  return (
    <Stack height="100%" width="100%">
      .{/* <Text>asdasdas</Text> */}
      {loadingScreenVideos ? (
        "LOading..."
      ) : errorScreenVideos ? (
        `${errorScreenVideos}`
      ) : (
        <Box border="1px solid #83dfe9" height="100%" width="100%">
          {screenPlaylist! && (
            // <iframe
            //   height="100%"
            //   width="100%"
            //   allow="autoplay"
            //   title="video player"
            //   src={screenPlaylist.length !== 0 ? screenPlaylist.map((video: any) => video.video)[adIndex] : adVid}
            // ></iframe>
            <video
              autoPlay={true}
              // controls
              ref={videoRef}
              muted={true}
              onLoadStart={(e) => onLoadStart(e)}
              onLoadedData={(e) => onLoad(e)}
              onEmptied={(e) => onEmpty(e)}
              onError={(e) => onError(e)}
              // onCanPlay={(e) => onCanPlay(e)}
              onPlay={onPlay}
              onProgress={onProgress}
              onEnded={(e) => onEnd(e)}
              poster="https://bafybeieusarrqnozo2avkc5bv2j6ltlrvoyzch7r2v4tqorgyhxnqzwmcu.ipfs.dweb.link/Screenshot%20%289%29.png"
              width="100%"
              height="100%"
              preload="auto"
              className="videoContainer"
            >
              <source
                key={0}
                src={
                  // "https://ipfs.io/ipfs/bafkreiex3fu3b3gnbivhqo32ib7xulesnhru6wqovh3shhrbl4xc7gac3a"
                  adVid
                  // screenPlaylist?.length !== 0
                  //   ? screenPlaylist?.map((video: any) => video.video)[adIndex]
                  //   : adVid
                }
              />
            </video>
          )}
        </Box>
      )}
      <Webcam
        audio={false}
        ref={webcamRef}
        screenshotFormat="image/jpeg"
        // style={{ display: img ? "none" : "block" }}
        style={{
          height: "10%",
          width: "10%",
          zIndex: -1,
          display: "block",
          position: "absolute",
          objectFit: 'fill',
        }}
        videoConstraints={videoConstraints}
      />
      {/* {img && <Image src={img} />}  */}
    </Stack>
  );
}
