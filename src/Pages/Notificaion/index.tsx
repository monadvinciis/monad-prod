import { Box, Flex, Text, Stack } from "@chakra-ui/react";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import {
  getPendingPleaListByScreenOwnerId,
  getTopFivePleaRequestByUserId,
  grantCampaignAllyPlea,
  listAllPleasByUserId,
  rejectCampaignAllyPlea,
} from "../../Actions/pleaActions";
import { message } from "antd";
import {
  ALL_PLEA_LIST_BY_USER_REST,
  CAMPAIGN_ALLY_GRANT_RESET,
  CAMPAIGN_ALLY_REJECT_RESET,
  PENDING_PLEA_LIST_BY_USER_RESET,
} from "../../Constants/pleaConstants";
import { ScreenOwnerPlea } from "../../components/newCommans";

export function Notification(props: any) {
  const navigate = useNavigate();
  const dispatch = useDispatch<any>();
  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  const getPleaRequest = () => {
    dispatch(listAllPleasByUserId());
    dispatch(getPendingPleaListByScreenOwnerId());
  };

  useEffect(() => {
    if (!userInfo) {
      navigate("/signin", { state: { path: "/notification" } });
    } else {
      dispatch(listAllPleasByUserId());
      dispatch(getPendingPleaListByScreenOwnerId());
    }
  }, [navigate, userInfo, dispatch]);

  const pendingPleaListByScreenOwner = useSelector(
    (state: any) => state.pendingPleaListByScreenOwner
  );
  const {
    allPleas: pendingPleaRequest,
    loading: loadingPendingPleaRequest,
    error: errorPendingPleaRequest,
  } = pendingPleaListByScreenOwner;

  const allPleasListByUser = useSelector(
    (state: any) => state.allPleasListByUser
  );
  const {
    allPleas,
    loading: loadingAllPleas,
    error: errorAllPleas,
  } = allPleasListByUser;

  useEffect(() => {
    if (errorAllPleas) {
      message.error(errorAllPleas);
      dispatch({ type: ALL_PLEA_LIST_BY_USER_REST });
    }
    if (errorPendingPleaRequest) {
      message.error(errorPendingPleaRequest);
      dispatch({ type: PENDING_PLEA_LIST_BY_USER_RESET });
    }
  }, [errorAllPleas, errorPendingPleaRequest, dispatch]);

  const campaignAllyPleaGrant = useSelector(
    (state: any) => state.campaignAllyPleaGrant
  );
  const {
    // loading: loadingCampaignPleaGrant,
    success: successCampaignPleaGrant,
    error: errorCampaignPleaGrant,
  } = campaignAllyPleaGrant;

  const campaignAllyPleaReject = useSelector(
    (state: any) => state.campaignAllyPleaReject
  );
  const {
    // loading: loadingCampaignRejectPlea,
    success: successCampaignRejectPlea,
    error: errorCampaignRejectPlea,
  } = campaignAllyPleaReject;

  useEffect(() => {
    if (successCampaignPleaGrant) {
      message.success(
        "You have given access to user to run this campaign on your screen"
      );
      dispatch({ type: CAMPAIGN_ALLY_GRANT_RESET });
      dispatch(listAllPleasByUserId());
      dispatch(getTopFivePleaRequestByUserId());
      dispatch(getPendingPleaListByScreenOwnerId());
    }
    if (errorCampaignPleaGrant) {
      message.error(errorCampaignPleaGrant);
      dispatch({ type: CAMPAIGN_ALLY_GRANT_RESET });
    }
    if (successCampaignRejectPlea) {
      message.success(
        "You have not given access to user to run this campaign on your screen"
      );
      dispatch({ type: CAMPAIGN_ALLY_REJECT_RESET });
      dispatch(getTopFivePleaRequestByUserId());
      dispatch(listAllPleasByUserId());
      dispatch(getPendingPleaListByScreenOwnerId());
    }
    if (errorCampaignRejectPlea) {
      message.error(errorCampaignRejectPlea);
      dispatch({ type: CAMPAIGN_ALLY_REJECT_RESET });
    }
  }, [
    successCampaignPleaGrant,
    successCampaignRejectPlea,
    errorCampaignPleaGrant,
    errorCampaignRejectPlea,
    dispatch,
  ]);

  const handelAcceptCampaignPlea = (plea: any) => {
    dispatch(grantCampaignAllyPlea(plea?._id));
  };
  const handelRejectCampaignPlea = (plea: any) => {
    dispatch(rejectCampaignAllyPlea(plea?._id));
  };

  return (
    <Box w="100%" h="100%" bgColor="#F5F5F5" m="0">
      <Flex
        p="3"
        align="center"
        justifyContent="space-between"
        borderTop="1px"
        borderLeft="1px"
        // border="1px solid green"
        borderColor="#EBEBEB"
        pr="20"
        bgColor="#FFFFFF"
      >
        <Text px="2" fontWeight="600" fontSize="2xl" m="0">
          Notification
        </Text>
      </Flex>
      <Flex mt="2" width="100%" height="83%">
        <Stack width="100%" height="100%" pb="2">
          <Stack width="100%" overflowY={"auto"}>
            <Flex
              flexDir={"column"}
              flexGrow={1}
              overflow={"hidden"}
              width="100%"
            >
              <ScreenOwnerPlea
                pendingCampaignPleaRequest={pendingPleaRequest}
                allCampaignPleaRequest={allPleas}
                getPleaRequest={getPleaRequest}
                loadingPendingPleaRequest={loadingPendingPleaRequest}
                loadingAllPleas={loadingAllPleas}
                handelAcceptCampaignPlea={handelAcceptCampaignPlea}
                handelRejectCampaignPlea={handelRejectCampaignPlea}
              />
            </Flex>
          </Stack>
        </Stack>
      </Flex>
    </Box>
  );
}
