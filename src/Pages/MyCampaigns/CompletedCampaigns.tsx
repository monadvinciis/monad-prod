import { Flex, Hide, Stack } from "@chakra-ui/react";
import { ScreenList, SingleCampaign } from "../../components/newCommans";
import { useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Skeleton } from "antd";
import { ResultNotFound } from "../../components/newCommans";

export function CompletedCampaigns(props: any) {
  const navigate = useNavigate();
  const [screens, setScreens] = useState<any>([]);
  const [campaigns, setCampaigns] = useState<any>([]);
  const [selectedBox, setSelectedBox] = useState<any>(0);

  const handelSelectScreens = (index: any) => {
    setScreens(myVideos[index]?.listOfScreens);
    setCampaigns(myVideos[index]?.listOfCampaigns);
    setSelectedBox(index);
  };

  const userCampaign = useSelector((state: any) => state.userCampaign);
  const {
    loading: loadingMyVideos,
    error: errorMyVideos,
    campaign: myVideos,
  } = userCampaign;

  useEffect(() => {
    if (errorMyVideos) {
      if (errorMyVideos === "Please Signin Again to continue") {
        navigate("/signin");
      }
    } else if (loadingMyVideos === false && myVideos) {
      setScreens(myVideos[0]?.listOfScreens);
      setCampaigns(myVideos[0]?.listOfCampaigns);
    }
  }, [navigate, errorMyVideos, myVideos, loadingMyVideos]);

  return (
    <Stack width="100%" overflowY={"auto"}>
      <Flex h="100%" w="100%" flexGrow={1} gap="2">
        <Stack
          h="100%"
          w={{ base: "100%", lg: "60%" }}
          overflowY={"auto"}
          p="2"
        >
          {loadingMyVideos ? (
            <Stack h="100%" w="100%" pt="5">
              <Skeleton avatar active paragraph={{ rows: 3 }} />{" "}
              <Skeleton avatar active paragraph={{ rows: 3 }} />{" "}
              <Skeleton avatar active paragraph={{ rows: 3 }} />{" "}
              <Skeleton avatar active paragraph={{ rows: 3 }} />{" "}
            </Stack>
          ) : myVideos?.length === 0 && loadingMyVideos === false ? (
            <ResultNotFound path="/" />
          ) : (
            myVideos?.map((data: any, index: any) => (
              <Stack h="100%" p="1" key={index}>
                <SingleCampaign
                  data={data}
                  key={index}
                  index={index}
                  selectedBox={selectedBox}
                  campaignType="Active On"
                  handelSelectScreens={handelSelectScreens}
                  screens={props?.screens}
                  handleSelectCampaignForTemplete={
                    props?.handleSelectCampaignForTemplete
                  }
                />
              </Stack>
            ))
          )}
        </Stack>
        <Hide below="md">
          <Stack width="40%" h="100%" pr="10" py="2">
            <ScreenList screens={screens} campaigns={campaigns} />
          </Stack>
        </Hide>
      </Flex>
    </Stack>
  );
}
