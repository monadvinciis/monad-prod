import { Box, Flex, Hide, Stack } from "@chakra-ui/react";
import {
  ScreenList,
  ScrollBox,
  SingleCampaign,
} from "../../components/newCommans";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { userCampaignsList } from "../../Actions/userActions";
import { CAMPAIGN_STATUS_PENDING } from "../../Constants/campaignConstants";
import { Skeleton } from "antd";
import { ResultNotFound } from "../../components/newCommans";

export function PendingCampaigns(props: any) {
  const dispatch = useDispatch<any>();
  const navigate = useNavigate();
  const [screens, setScreens] = useState<any>([]);
  const [selectedBox, setSelectedBox] = useState<any>(0);

  const handelSelectScreens = (index: any) => {
    setScreens(myVideos[index]?.listOfScreens);
    setSelectedBox(index);
  };

  const userCampaign = useSelector((state: any) => state.userCampaign);
  const {
    loading: loadingMyVideos,
    error: errorMyVideos,
    campaign: myVideos,
  } = userCampaign;
  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  useEffect(() => {
    if (errorMyVideos) {
      if (errorMyVideos === "Please Signin Again to continue") {
        navigate("/signin");
      }
    } else if (loadingMyVideos === false && myVideos) {
      setScreens(myVideos[0]?.listOfScreens);
    }
  }, [navigate, errorMyVideos, myVideos]);

  useEffect(() => {
    if (!userInfo) {
      navigate("/signin");
    } else {
      dispatch(userCampaignsList(userInfo, CAMPAIGN_STATUS_PENDING));
    }
  }, [navigate, dispatch]);

  return (
    <Flex gap="2">
      <Stack width="100%">
        <ScrollBox height="800px">
          <Flex flexDir="column" gap="2" p="3">
            {loadingMyVideos ? (
              <Box pt="5">
                <Skeleton avatar active paragraph={{ rows: 3 }} />{" "}
                <Skeleton avatar active paragraph={{ rows: 3 }} />{" "}
                <Skeleton avatar active paragraph={{ rows: 3 }} />{" "}
                <Skeleton avatar active paragraph={{ rows: 3 }} />{" "}
              </Box>
            ) : myVideos?.length === 0 && loadingMyVideos === false ? (
              <ResultNotFound path="/" />
            ) : (
              myVideos.map((data: any, index: any) => (
                <SingleCampaign
                  data={data}
                  key={index}
                  index={index}
                  selectedBox={selectedBox}
                  campaignType="Active On"
                  handelSelectScreens={handelSelectScreens}
                />
              ))
            )}
          </Flex>
        </ScrollBox>
      </Stack>
      <Hide below="md">
        <Stack width="40%" h="100%" pr="10" py="2">
          <ScreenList screens={screens} />
        </Stack>
      </Hide>
    </Flex>
  );
}
