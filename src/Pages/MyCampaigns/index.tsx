import {
  Box,
  Flex,
  Text,
  Stack,
  Hide,
  Show,
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { LiveCampaigns } from "./LiveCampaigns";
import { CompletedCampaigns } from "./CompletedCampaigns";
import { DeletedCampaigns } from "./DeletedCampaigns";
import { useLocation, useNavigate } from "react-router-dom";
import { AddButton, Mytabs } from "../../components/newCommans";
import { Alert, FloatButton, message } from "antd";
import { MdCampaign } from "react-icons/md";
import { LuChevronLeft } from "react-icons/lu";
import { useDispatch } from "react-redux";
import { userCampaignsList, userScreensList } from "../../Actions/userActions";
import { useSelector } from "react-redux";
import Search from "antd/es/input/Search";
import {
  CAMPAIGN_STATUS_COMPLETD,
  CAMPAIGN_STATUS_DELETED,
  CREATE_CAMPAIGN_RESET,
} from "../../Constants/campaignConstants";
import { CreateCampaignFromTempletModel } from "../Models/CreateCampaignFromTempletModel";

const options = ["Live ", "Completed", "Deleted"];

export function MyCampaigns(props: any) {
  const location = useLocation();
  const navigate = useNavigate();
  const dispatch = useDispatch<any>();
  const [selectedTab, setSelectedTab] = useState<any>(1);
  const [searchString, setSearchString] = useState<any>("");
  const [campaignStatus, setCampaignStatus] = useState<any>("ALL");
  const [openModel, setOpenModel] = useState<any>(false);
  const [campaignForTemplete, setCampaignForTemplet] = useState<any>(null);

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  const onSearch = (value: any, _e: any, info: any) => {
    getCampaignList();
  };

  const handelCreateCampaign = () => {
    navigate("/create-campaign", { state: { path: "/my/campaigns" } });
  };

  const handleCreateCampaignWithMultipleMedia = () => {
    navigate("/newcreatecampaign", { state: { path: "/my/campaigns" } });
  };
  const getCampaignList = () => {
    dispatch(userCampaignsList(userInfo, campaignStatus, searchString));
  };
  const handelSetCampaignStatus = (value: any) => {
    switch (value) {
      case 1:
        setCampaignStatus("ALL");
        break;
      case 2:
        setCampaignStatus(CAMPAIGN_STATUS_COMPLETD);
        break;
      case 3:
        setCampaignStatus(CAMPAIGN_STATUS_DELETED);
        break;
      default:
        setCampaignStatus("ALL");
    }
  };

  const createCampaign = useSelector((state: any) => state.createCampaign);
  const {
    loading: loadingSlotBooking,
    error: errorSlotBooking,
    success: successSlotBooking,
  } = createCampaign;

  const userScreens = useSelector((state: any) => state.userScreens);
  const { screens } = userScreens;

  useEffect(() => {
    if (errorSlotBooking) {
      dispatch({ type: CREATE_CAMPAIGN_RESET });
      message.error(errorSlotBooking);
    }
    if (successSlotBooking) {
      //const { createdCampaign } = uploadedCampaign;
      // if (createdCampaign?.length === 0) {
      //   message.error(
      //     "Campaign All ready present with this media, Try with other media"
      //   );
      // } else {
      //   message.success(
      //     `Campaign created successFull on ${createdCampaign?.length} Screens only`
      //   );
      // }
      message.success("Campaign created successfully!");
      dispatch({ type: CREATE_CAMPAIGN_RESET });
      dispatch(userCampaignsList(userInfo, campaignStatus, ""));
    }
  }, [
    errorSlotBooking,
    successSlotBooking,
    dispatch,
    campaignStatus,
    userInfo,
  ]);

  useEffect(() => {
    if (!userInfo) {
      navigate("/signin");
    }
    dispatch(userCampaignsList(userInfo, campaignStatus, ""));
    if (userInfo?.isMaster) dispatch(userScreensList(userInfo, ""));
  }, [navigate, userInfo, dispatch, campaignStatus]);

  const handleSelectCampaignForTemplete = (campaign: any) => {
    setCampaignForTemplet(campaign);
    setOpenModel(true);
  };

  return (
    <Box w="100%" h="100%" bgColor="#F5F5F5" m="0">
      {/* Model */}
      <CreateCampaignFromTempletModel
        open={openModel}
        onCancel={() => setOpenModel(false)}
        campaign={campaignForTemplete}
        screens={screens}
      />
      <Flex
        p="3"
        align="center"
        justifyContent="space-between"
        borderTop="1px"
        borderLeft="1px"
        borderColor="#EBEBEB"
        pr="10"
        bgColor="#FFFFFF"
      >
        <Flex gap={{ base: "2", lg: "4" }} align="center">
          <LuChevronLeft
            size="20px"
            onClick={() => navigate(location?.state?.path || "/")}
          />
          <Text fontWeight="600" fontSize={{ base: "20px", lg: "2xl" }} m="0">
            Campaigns
          </Text>
        </Flex>
        <Hide below="md">
          <Flex gap={{ base: "2", lg: "4" }}>
            <Search
              placeholder="Search by campaign name"
              allowClear
              onSearch={onSearch}
              onChange={(e) => setSearchString(e.target.value)}
              size="large"
              style={{
                width: 300,
              }}
            />

            {/* <AddButton handelClick={handelCreateCampaign} label="Create" /> */}
            <Menu>
              <MenuButton>
                <AddButton label="Create" />
              </MenuButton>
              <MenuList width="300px">
                <MenuItem onClick={handelCreateCampaign} color="black">
                  <Text my="1" fontSize="12px" fontWeight="700">
                    Create Campaign With Single Media
                  </Text>
                </MenuItem>
                <MenuItem
                  onClick={handleCreateCampaignWithMultipleMedia}
                  color="black"
                  justifyContent="space-between"
                >
                  <Text my="1" fontSize="12px" fontWeight="700">
                    Create Campaign With Multiple Media
                  </Text>
                </MenuItem>
              </MenuList>
            </Menu>
          </Flex>
        </Hide>
      </Flex>
      <Show below="md">
        <Flex
          p="4"
          bgColor="#FFFFFF"
          border="1px"
          borderColor="#EBEBEB"
          borderLeft={"0px"}
        >
          <Search
            placeholder="Search by campaign name"
            allowClear
            onSearch={onSearch}
            onChange={(e) => setSearchString(e.target.value)}
            size="large"
            style={{
              width: 400,
            }}
          />
        </Flex>
      </Show>

      <Flex width="100%" borderTop="1px" borderLeft="1px" borderColor="#EBEBEB">
        <Mytabs
          selectedTab={selectedTab}
          setSelectedTab={(value: any) => {
            setSelectedTab(value);
            handelSetCampaignStatus(value);
          }}
          options={options}
        />
      </Flex>
      <Flex mt="" width="100%" height="80%">
        <Stack width="100%" height="100%" pb="" px="">
          {loadingSlotBooking ? (
            <Alert
              type="info"
              message={"The campaign is creating, wait for some time. ...."}
              banner
            />
          ) : null}
          {selectedTab === 1 ? (
            <LiveCampaigns
              searchString={searchString}
              getCampaignList={getCampaignList}
              screens={screens}
              handleSelectCampaignForTemplete={handleSelectCampaignForTemplete}
            />
          ) : selectedTab === 2 ? (
            <CompletedCampaigns
              searchString={searchString}
              getCampaignList={getCampaignList}
              screens={screens}
              handleSelectCampaignForTemplete={handleSelectCampaignForTemplete}
            />
          ) : selectedTab === 3 ? (
            <DeletedCampaigns
              searchString={searchString}
              getCampaignList={getCampaignList}
              screens={screens}
              handleSelectCampaignForTemplete={handleSelectCampaignForTemplete}
            />
          ) : null}
        </Stack>
        <Show below="md">
          <FloatButton
            type="primary"
            tooltip={<div>Create Screen</div>}
            style={{
              right: 40,
              bottom: 75,
            }}
            icon={<MdCampaign />}
            onClick={handelCreateCampaign}
          ></FloatButton>
        </Show>
      </Flex>
    </Box>
  );
}
