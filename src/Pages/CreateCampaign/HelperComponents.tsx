import { Box, Button, Flex, SimpleGrid, Text } from "@chakra-ui/react";
import { LuChevronLeft } from "react-icons/lu";
import { useNavigate } from "react-router-dom";

export function Header(props: any) {
  const navigate = useNavigate();
  const { location } = props;
  return (
    <Box py="5" px="10" bgColor="#FFFFFF" border="1px" borderColor="#E1E1E1">
      <Flex flexDir="column">
        <Flex gap={{ base: "2", lg: "4" }} align="center">
          <LuChevronLeft
            size="20px"
            onClick={() => navigate(location?.state?.path || "/")}
          />
          <Text fontWeight="600" fontSize={{ base: "20px", lg: "2xl" }} m="0">
            Campaigns
          </Text>
        </Flex>
      </Flex>
    </Box>
  );
}

export function Steps(props: any) {
  return (
    <Box py="5" px="10" bgColor="#FFFFFF" border="1px" borderColor="#E1E1E1">
      <Flex justifyContent="space-between">
        <Flex gap="10">
          {props?.steps?.map((step: any, index: any) => (
            <Flex
              key={index}
              color={
                step.status === "Completed" || step.status === "Active"
                  ? "#04822F"
                  : "#CBCBCB"
              }
              gap="10"
              align="center"
            >
              <Flex key={index} align="center" gap="2">
                {step?.icon}
                <Text fontSize="md" fontWeight="500" m="0">
                  {step?.text}
                </Text>
              </Flex>
              {index !== 2 ? <Box border="2px" px="10px"></Box> : null}
            </Flex>
          ))}
        </Flex>
        <Flex gap="5" pr="10">
          <Button
            borderRadius="10px"
            width="100px"
            px="5"
            py="2"
            variant="outline"
            borderColor="#B9B9B9"
            onClick={() => props?.handleBack()}
          >
            {props?.selectedStep === 1 ? "Cancel" : "Back"}
          </Button>
          {props.selectedStep !== 3 ? (
            <Button
              borderRadius="12px"
              width="100px"
              mr="5"
              px="5"
              py="2"
              onClick={() => props?.handleNext()}
              isDisabled={props?.isDisabled}
            >
              Next
            </Button>
          ) : null}
        </Flex>
      </Flex>
    </Box>
  );
}

export function CroudMobabilityFunction(props: any) {
  const { crowdMobilityType, addOrRemobeCrowdMobilityType } = props;

  return (
    <SimpleGrid columns={[1, 2, 3]} gap="5">
      <Button
        style={{ height: "40px" }}
        variant="outline"
        color={crowdMobilityType?.includes("Moving") ? "#4C4C4C" : "#515151"}
        bgColor={crowdMobilityType?.includes("Moving") ? "#D6FFFF" : "#FAFAFA"}
        fontSize="12"
        p="5"
        my="0.5"
        onClick={() => addOrRemobeCrowdMobilityType("Moving")}
      >
        4 Wheelers
      </Button>
      <Button
        style={{ height: "40px" }}
        variant="outline"
        color={crowdMobilityType?.includes("Walking") ? "#4C4C4C" : "#515151"}
        bgColor={crowdMobilityType?.includes("Walking") ? "#D6FFFF" : "#FAFAFA"}
        fontSize="12"
        p="5"
        my="0.5"
        onClick={() => addOrRemobeCrowdMobilityType("Walking")}
      >
        2 Wheelers
      </Button>
      <Button
        style={{ height: "40px" }}
        variant="outline"
        color={crowdMobilityType?.includes("Sitting") ? "#4C4C4C" : "#515151"}
        bgColor={crowdMobilityType?.includes("Sitting") ? "#D6FFFF" : "#FAFAFA"}
        fontSize="12"
        p="5"
        my="0.5"
        onClick={() => addOrRemobeCrowdMobilityType("Sitting")}
      >
        On Footers
      </Button>
    </SimpleGrid>
  );
}
