import {
  Box,
  Button,
  Flex,
  Input,
  SimpleGrid,
  Text,
  Stack,
  Skeleton,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { AiOutlineRight } from "react-icons/ai";
import type { RadioChangeEvent } from "antd";
import { Select, Radio, message, Alert, Checkbox } from "antd";
import {
  categoryOptions,
  croudMobabilityOption,
  employmentTypeOption,
  screenTypeOptions,
  state,
  stateOption,
  stepsForCeateAndEditScreens,
} from "../../utils/utilsData";
import type { Dayjs } from "dayjs";
import "../index.css";
import { useDispatch } from "react-redux";
import { TakeScreenLocation } from "../Models/TakeScreenLocation";
import {
  InputField,
  ShowInformation,
  TimeInputField,
} from "../../components/newCommans";
import { useNavigate } from "react-router-dom";
import { UploadScreenImages } from "./UploadScreenImages";
import {
  detailsScreen,
  filteredScreenListByName,
  updateScreen,
} from "../../Actions/screenActions";
import { addFileOnWeb3 } from "../../utils/newWeb3";
import { useSelector } from "react-redux";
import {
  FILTERED_SCREEN_LIST_RESET,
  SCREEN_UPDATE_RESET,
} from "../../Constants/screenConstants";
import { isNumber } from "../../utils/utilityFunctions";

export function EditScreen(props: any) {
  const [selectedStep, setSelectedStep] = useState<any>(1);
  const [steps, setSteps] = useState(stepsForCeateAndEditScreens);
  const dispatch = useDispatch<any>();
  const navigate = useNavigate();

  const [loading, setLoading] = useState<any>(false);
  // Error variable
  const [error, setError] = useState<any>("");

  const [images, setImages] = useState<any>([]);
  const [syncCode, setSyncCode] = useState<any>("");
  const [files, setFiles] = useState<any>([null, null, null, null]);
  const [leftImage, setLeftImage] = useState<any>("");
  const [rightImage, setRightImage] = useState<any>("");
  const [frontImage, setFrontImage] = useState<any>("");
  const [bestImage, setBestImage] = useState<any>("");
  const [lng, setLng] = useState<number | undefined>(77.52);
  const [lat, setLat] = useState<number | undefined>(28.33);
  const [openMapModel, setOpenMapMadel] = useState<any>(false);
  const [highlights, setHighlights] = useState<any>("");
  const [kidsFriendly, setKidsFriendly] = useState<any>("Yes");
  const [name, setName] = useState<any>("");
  const [isExide, setIsExide] = useState<boolean>(false);
  const [screenType, setScreenType] = useState<any>("");
  const [screenCategory, setScreenCategory] = useState<any>("");
  const [startTime, setStartTime] = useState<Dayjs | null>(null);
  const [endTime, setEndTime] = useState<any>(null);
  const [rentPerDay, setRentPerDay] = useState<any>("");
  const [slotsPlayPerDay, setSlotsPlayPerDay] = useState<any>("");
  const [screenAddress, setScreenAddress] = useState<any>("");
  const [landMark, setLandmark] = useState<any>("");
  const [districtCity, setDistrictCity] = useState<any>("");
  const [pincode, setPinCode] = useState<any>("");
  const [stateUT, setStateUT] = useState<any>("");
  const [country, setCountry] = useState<any>("India");
  const [regularAudiencePercentage, setRegularAudiencePercentage] =
    useState<any>();
  const [averageDailyFootfall, setAverageDailyFootfall] = useState<any>("");
  const [screenDiagonal, setScreenDiagonal] = useState<any>("");
  const [screenLength, setScreenLength] = useState<any>("");
  const [screenWidth, setScreenWidth] = useState<any>("");
  const [averagePurchasePower, setAveragePurchasePower] = useState<any>({
    start: 1000,
    end: 10000,
  });
  const [averageAgeGroup, setAverageAgeGroup] = useState<any>({
    averageStartAge: "",
    averageEndAge: "",
  });
  const [employmentStatus, setEmploymentStatus] = useState<any>([]);
  const [crowdMobilityType, setCrowdMobilityType] = useState<any>([]);

  const handleChange = (value: string | string[]) => {
    setScreenType(value);
    if (value) {
      setError("");
    }
  };

  const handleEndTime = (value: any) => {
    // let newDate = new Date(new Date(value).getTime() + (5 * 60 + 30) * 60000);
    setEndTime(value);
    setError("");
    //    new Date(new Date(date).getTime() + (5 * 60 + 30) * 60000 - time * 1000)
  };
  const handleStartTime = (value: any) => {
    setStartTime(value);
    setError("");
  };

  const handelSelectKigsFriendly = ({
    target: { value },
  }: RadioChangeEvent) => {
    setKidsFriendly(value);
  };

  const handleBack = () => {
    if (selectedStep > 1) {
      setSelectedStep((previous: any) => previous - 1);
    } else {
      dispatch({ type: FILTERED_SCREEN_LIST_RESET });
      navigate("/my/screens");
    }
  };

  const validateForm = () => {
    if (!name) {
      setError("Please Enter screen Name");
      return false;
    } else if (screens > 0) {
      setError(
        "You cann't create screen with same, Please change your screen name"
      );
      return false;
    } else if (!screenType) {
      setError("Please Select screen type");
      return false;
    } else if (!screenCategory) {
      setError("Please select screen category");
      return false;
    } else if (screenType === "LED TV Screen" && !isNumber(screenDiagonal)) {
      setError("Screen diagonal must be number");
      return false;
    } else if (
      screenType === "Digital Billboard Screen" &&
      !isNumber(screenLength) &&
      !isNumber(screenWidth)
    ) {
      setError("BilBod Screen dimantion must be number");
      return false;
    } else if (!startTime) {
      setError("Please Enter start Time Of Screen");
      return false;
    } else if (!endTime) {
      setError("Please Enter end Time Of Screen");
      return false;
    } else if (!rentPerDay || !isNumber(rentPerDay)) {
      setError("Please enter only number for rent per day");
      return false;
    } else if (!slotsPlayPerDay || !isNumber(slotsPlayPerDay)) {
      setError("Please enter only number for slot play per day");
      return false;
    } else if (!screenAddress) {
      setError("Please enter screen address!");
      return false;
    } else if (!districtCity) {
      setError("Please enter city");
      return false;
    } else if (!state) {
      setError("Select state");
      return false;
    } else if (!pincode || !isNumber(pincode)) {
      setError("Pincode must be length of 6");
      return false;
    } else if (crowdMobilityType?.length === 0) {
      setError("Please select croud mobability type");
      return false;
    } else if (employmentStatus?.length === 0) {
      setError("Select employmnet type");
      return false;
    } else if (!averageDailyFootfall || !isNumber(averageDailyFootfall)) {
      setError("Please enter only number for footfall");
      return false;
    } else if (
      averageAgeGroup?.averageStartAge === "" ||
      averageAgeGroup?.averageEndAge === ""
    ) {
      setError("Please enter age group");
      return false;
    } else {
      setError("");
      return true;
    }
  };

  const validateImages = () => {
    if (leftImage && rightImage && frontImage && bestImage) {
      return true;
    } else {
      setError("Please add all images for screen");
      return false;
    }
  };

  const validateScreenCode = () => {
    if (!syncCode) {
      setError("Enter sync code for screen");
      return false;
    }
    return true;
  };

  const screenId = window.location.pathname.split("/")[3];

  const screenDetails = useSelector((state: any) => state.screenDetails);
  const {
    loading: loadingScreenDetails,
    error: errorScreen,
    screen,
  } = screenDetails;

  const screenUpdate = useSelector((state: any) => state.screenUpdate);
  const {
    // loading: loadingUpdate,
    error: errorUpdate,
    success: successUpdate,
  } = screenUpdate;

  useEffect(() => {
    if (errorScreen) message.error(errorScreen);
  }, [errorScreen]);

  useEffect(() => {
    dispatch(detailsScreen(screenId));
  }, [screenId, dispatch]);

  const handelUpdateScreen = async () => {
    setLoading(true);
    const data = {
      _id: screen?._id,
      startTime,
      endTime,
      landMark,
      districtCity,
      stateUT,
      country,
      rentPerDay,
      slotsPlayPerDay,
      screenHighlights: highlights
        ?.split(",")
        ?.map((data: any) => data.trim().toLowerCase()),
      name: name?.trim(),
      lat,
      lng,
      pincode,
      isExide,
      images: screen.images,
      image: screen.image,
      screenCategory,
      screenDiagonal,
      screenType,
      screenLength,
      screenWidth,
      screenAddress,
      syncCode,
      additionalData: {
        averageDailyFootfall,
        footfallClassification: {
          averagePurchasePower,
          regularAudiencePercentage,
          employmentStatus,
          kidsFriendly,
          crowdMobilityType,
          averageAgeGroup,
        },
      },
    };

    if (files?.length > 0) {
      setLoading(true);
      const cids = [];
      for (let file of files) {
        if (file != null) {
          try {
            const cid = await addFileOnWeb3(file);
            cids.push(cid);
          } catch (error) {
            message.error(`error in uploading file : ${error}`);
          }
        }
      }
      data.images = [...images, ...cids];
      data.image = images ? screen?.image : `https://ipfs.io/ipfs/${cids[0]}`;
      dispatch(updateScreen(data));
    } else {
      data.images = [...images];
      dispatch(updateScreen(data));
    }
  };

  const changeSteps = (value: any) => {
    let data = steps?.map((step: any) => {
      if (step.key <= value) {
        step.status = "Completed";
      } else if (step.key === value) {
        step.status = "Active";
      }
      return step;
    });
    setSteps(data);
  };

  const handleNext = () => {
    if (selectedStep === 1 && validateForm()) {
      setSelectedStep(2);
      changeSteps(2);
    } else if (selectedStep === 2 && validateImages()) {
      setSelectedStep(3);
      changeSteps(3);
    } else if (selectedStep === 3 && validateScreenCode()) {
      handelUpdateScreen();
    }
  };

  useEffect(() => {
    if (screen) {
      setName(screen?.name);
      setStartTime(screen?.startTime);
      setEndTime(screen?.endTime);
      setRentPerDay(screen?.rentPerDay);
      setSlotsPlayPerDay(screen?.slotsPlayPerDay);
      setDistrictCity(screen?.districtCity);
      setCountry(screen?.country);
      setStateUT(screen?.stateUT);
      setLandmark(screen?.landMark);
      setScreenAddress(screen?.screenAddress);
      setHighlights(screen?.screenHighlights?.join(","));
      setScreenCategory(screen?.category);
      setScreenDiagonal(screen?.size?.diagonal);
      setScreenType(screen?.screenType);
      setSyncCode(screen?.screenCode);
      setScreenLength(screen?.size?.length);
      setScreenWidth(screen?.size?.width);
      setPinCode(screen?.pincode);
      setIsExide(screen?.isExide || false);

      let makeImageLinks = screen?.images?.map(
        (cid: any) => `https://ipfs.io/ipfs/${cid}`
      );
      setImages(screen?.images);
      setFrontImage(makeImageLinks[0]);
      setLeftImage(makeImageLinks[1]);
      setRightImage(makeImageLinks[2]);
      setBestImage(makeImageLinks[3]);

      setAverageDailyFootfall(screen?.additionalData?.averageDailyFootfall);
      setAveragePurchasePower(
        screen?.additionalData?.footfallClassification?.averagePurchasePower
      );
      setEmploymentStatus(
        screen?.additionalData?.footfallClassification?.employmentStatus
      );
      setRegularAudiencePercentage(
        screen?.additionalData?.footfallClassification
          ?.regularAudiencePercentage
      );
      setKidsFriendly(
        screen?.additionalData?.footfallClassification?.kidsFriendly
      );
      setAverageAgeGroup(
        screen?.additionalData?.footfallClassification?.averageAgeGroup
      );
      setCrowdMobilityType(
        screen?.additionalData?.footfallClassification?.crowdMobilityType
      );
      setLat(screen.lat);
      setLng(screen?.lng);
    }
  }, [screen]);

  useEffect(() => {
    if (successUpdate) {
      message.success("Screen updated successfully!");
      setLoading(false);
      dispatch({ type: SCREEN_UPDATE_RESET });
      dispatch({ type: FILTERED_SCREEN_LIST_RESET });
      navigate("/my/screens");
    }
    if (errorUpdate) {
      message.error(errorUpdate);
      setLoading(false);
      dispatch({ type: SCREEN_UPDATE_RESET });
      dispatch({ type: FILTERED_SCREEN_LIST_RESET });
    }
  }, [errorUpdate, successUpdate, dispatch, navigate]);

  const validateNumberValueWithLength = (value: any, length: any, cb: any) => {
    if (value?.length <= length) {
      if (isNumber(value) || value === "") {
        cb(value);
      }
      setError("");
    } else {
      setError(`Value must be less then ${length + 1} digits`);
    }
  };

  const validateStringWithLength = (value: any, length: any, cb: any) => {
    if (value?.length <= length) {
      cb(value);
      setError("");
    } else {
      setError(`Value must be less then ${length + 1} charactors`);
    }
  };

  const filterScreenList = useSelector((state: any) => state.filterScreenList);
  const {
    screens, // no of screens with name which user enter
  } = filterScreenList;

  const getScreenCountWithSameName = (value: string) => {
    if (value !== screen?.name) dispatch(filteredScreenListByName(value));
  };

  return (
    <Stack p="0" m="0" width="100%" bgColor="#F5F5F5">
      <TakeScreenLocation
        open={openMapModel}
        onCancel={() => setOpenMapMadel(false)}
        setLat={setLat}
        setLng={setLng}
        lat={lat}
        lng={lng}
      />
      <Flex
        justifyContent="space-between"
        border="1px"
        borderColor="#EBEBEB"
        pr={{ base: "0px", lg: "100px" }}
        bgColor="#FFFFFF"
        align="center"
        gap={4}
      >
        <Flex p="4" align="center" gap={{ base: "2", lg: "8" }} width="100%">
          <Text fontWeight="600" fontSize={{ base: "16px", lg: "2xl" }} m="0">
            Edit Screen
          </Text>
          <Flex
            gap={{ base: "2", lg: "4" }}
            justifyContent="flex-end"
            fontSize={{ base: "10px", lg: "16px" }}
          >
            {steps.map((step: any, index: any) => (
              <Flex key={index} align="center" gap="2">
                <Text
                  fontWeight="500"
                  color={
                    step.status === "Completed" || step.status === "Active"
                      ? "#000000"
                      : "#CBCBCB"
                  }
                  m="0"
                >
                  {step?.text}
                </Text>
                <AiOutlineRight size="16px" color="#A8A8A8" />
              </Flex>
            ))}
          </Flex>
        </Flex>
        <Button
          size="sm"
          fontSize={{ base: "xs", lg: "sm" }}
          height="40px"
          px={{ base: "2", lg: "4" }}
          borderColor="#DDDDDD"
          bgColor="#FFFFFF"
          variant="outline"
          borderRadius="full"
          onClick={handleBack}
        >
          Back
        </Button>
        <Button
          size="sm"
          fontSize={{ base: "xs", lg: "sm" }}
          height="40px"
          px={{ base: "2", lg: "4" }}
          color="#FFFFFF"
          bgColor="#000000"
          _hover={{ bgColor: "#FFFFFF", color: "#000000" }}
          variant="outline"
          onClick={handleNext}
          isLoading={loading}
          borderRadius="full"
          loadingText="Updating details"
        >
          {selectedStep <= 2 ? "Next" : "Finish"}
        </Button>
      </Flex>
      {error ? <Alert type="error" message={error} banner /> : null}
      {loadingScreenDetails ? (
        <Skeleton>
          <Box
            width={"100%"}
            height="100%"
            bgPosition="center"
            bgSize="cover"
            borderRadius="8px"
          ></Box>
        </Skeleton>
      ) : (
        <Stack borderColor="#EBEBEB" overflowY="auto" height="80%">
          {selectedStep === 1 ? (
            // screen details
            <Flex
              flexDir="column"
              gap={{ base: "2", lg: "4" }}
              className="scrollY"
              p={{ base: "2", lg: "4" }}
            >
              {/* basic details */}
              <Box p={{ base: "4", lg: "8" }} bgColor="#FFFFFF" rounded="5">
                <Text fontSize="lg" fontWeight="700" color="#131D30">
                  Basic details*
                </Text>
                <SimpleGrid
                  columns={[1, 2, 3, 4]}
                  spacing={{ base: "2", lg: "4" }}
                >
                  <Flex flexDir="column" gap="2">
                    <Text
                      fontSize="16px"
                      fontWeight="400"
                      color="#131D30"
                      m="0"
                    >
                      Screen Name*
                    </Text>
                    <InputField
                      placeholder="Enter name of screen"
                      value={name}
                      onChange={(value: any) => {
                        getScreenCountWithSameName(value);
                        validateStringWithLength(value, 35, setName);
                      }}
                    />
                    {screen?.name !== name ? (
                      screens > 0 ? (
                        <Text
                          fontSize="12px"
                          fontWeight="600"
                          color="red"
                          m="0"
                        >
                          Screen already present with same name
                        </Text>
                      ) : (
                        <Text
                          fontSize="12px"
                          fontWeight="400"
                          color="green"
                          m="0"
                        >
                          Good
                        </Text>
                      )
                    ) : null}
                  </Flex>
                  <Flex flexDir="column" gap="2">
                    <Text
                      fontSize="16px"
                      fontWeight="400"
                      color="#131D30"
                      m="0"
                    >
                      Screen Type*
                    </Text>

                    <Select
                      size="large"
                      defaultValue=""
                      bordered={false}
                      placeholder="Select screen type"
                      style={{
                        width: "100%",
                        height: "45px",
                        border: "1px solid #00000040",
                        borderRadius: "5px",
                      }}
                      onChange={handleChange}
                      options={screenTypeOptions}
                      value={screenType}
                    />
                  </Flex>
                  <Flex flexDir="column" gap="2">
                    <Text
                      fontSize="16px"
                      fontWeight="400"
                      color="#131D30"
                      m="0"
                    >
                      Category*
                    </Text>
                    <Select
                      size="large"
                      defaultValue=""
                      bordered={false}
                      style={{
                        width: "100%",
                        height: "45px",
                        border: "1px solid #00000040",
                        borderRadius: "5px",
                      }}
                      options={categoryOptions}
                      value={screenCategory}
                      onChange={(value: any) => {
                        setScreenCategory(value);
                        setError("");
                      }}
                    />
                  </Flex>
                  <Flex flexDir="column" gap="2">
                    <Text
                      fontSize="16px"
                      fontWeight="400"
                      color="#131D30"
                      m="0"
                    >
                      Screen dimensions*
                    </Text>
                    <Flex>
                      {screenType === "Digital Billboard Screen" ? (
                        <Flex align="center" gap="2" w="100%">
                          <InputField
                            placeholder="Length"
                            value={screenLength}
                            onChange={(value: any) => {
                              validateNumberValueWithLength(
                                value,
                                3,
                                setScreenLength
                              );
                            }}
                          />
                          <InputField
                            placeholder="Width"
                            value={screenWidth}
                            onChange={(value: any) => {
                              validateNumberValueWithLength(
                                value,
                                3,
                                setScreenWidth
                              );
                            }}
                          />
                          <Text
                            color="#555555"
                            fontSize="sm"
                            fontWeight="bold"
                            m="0"
                          >
                            Ft
                          </Text>
                        </Flex>
                      ) : (
                        <Flex align="center" gap={1} w="100%">
                          <InputField
                            placeholder="Enter dimention ex. 34"
                            value={screenDiagonal}
                            onChange={(value: any) => {
                              validateNumberValueWithLength(
                                value,
                                3,
                                setScreenDiagonal
                              );
                            }}
                          />
                          <Text
                            // w="10%"
                            color="#000000"
                            fontSize="sm"
                            fontWeight=""
                            m="0"
                          >
                            Inch
                          </Text>
                        </Flex>
                      )}
                    </Flex>
                  </Flex>
                  <Flex flexDir="column" gap="2">
                    <Text
                      pb="1.5"
                      fontSize="16px"
                      fontWeight="400"
                      color="#131D30"
                      m="0"
                    >
                      Screen On Time*
                    </Text>
                    <TimeInputField
                      value={startTime}
                      onChange={handleStartTime}
                    />
                  </Flex>
                  <Flex flexDir="column" gap="2">
                    <Text
                      pb="1.5"
                      fontSize="16px"
                      fontWeight="400"
                      color="#131D30"
                      m="0"
                    >
                      Screen Off Time*
                    </Text>
                    <TimeInputField value={endTime} onChange={handleEndTime} />
                  </Flex>
                  <Flex width="100%" gap="2">
                    <Flex flexDir="column" gap="2" width="50%">
                      <Flex align="center" gap="2">
                        <Text
                          fontSize="16px"
                          fontWeight="400"
                          color="#131D30"
                          m="0"
                        >
                          Maximum Slots/Day*
                        </Text>
                        <ShowInformation value="Maximum slots will booked by 1 campaign" />
                      </Flex>
                      <InputField
                        placeholder=""
                        value={slotsPlayPerDay}
                        onChange={(value: any) => {
                          validateNumberValueWithLength(
                            value,
                            4,
                            setSlotsPlayPerDay
                          );
                        }}
                      />
                    </Flex>
                    <Flex flexDir="column" gap="2">
                      <Flex align="center" gap="2">
                        <Text
                          fontSize="16px"
                          fontWeight="400"
                          color="#131D30"
                          m="0"
                        >
                          Campaign can exide?
                        </Text>
                        <ShowInformation value="Campaign will play more then its daily limits" />
                      </Flex>
                      <Checkbox
                        checked={isExide}
                        onChange={(e) => setIsExide(e.target.checked)}
                      ></Checkbox>
                    </Flex>
                  </Flex>
                  <Flex flexDir="column" gap="2">
                    <Flex align="center" gap="2">
                      <Text
                        fontSize="16px"
                        fontWeight="400"
                        color="#131D30"
                        m="0"
                      >
                        Screen rent per day*
                      </Text>
                      <ShowInformation value="Rent of screen for one day" />
                    </Flex>
                    <Flex align="center" gap={1} w="100%">
                      <InputField
                        placeholder=""
                        value={rentPerDay}
                        onChange={(value: any) => {
                          validateNumberValueWithLength(
                            value,
                            3,
                            setRentPerDay
                          );
                        }}
                      />
                      <Text color="#000000" fontSize="sm" fontWeight="" m="0">
                        INR
                      </Text>
                    </Flex>
                  </Flex>
                </SimpleGrid>
              </Box>
              {/* Location */}

              <Box p={{ base: "4", lg: "8" }} bgColor="#FFFFFF" rounded="5">
                <Text fontSize="lg" fontWeight="700" color="#131D30">
                  location*
                </Text>
                <SimpleGrid
                  columns={[1, 2, 4]}
                  spacing={{ base: "2", lg: "4" }}
                >
                  <Flex flexDir="column" gap="2">
                    <Text
                      fontSize="16px"
                      fontWeight="400"
                      color="#131D30"
                      m="0"
                    >
                      Address*
                    </Text>
                    <InputField
                      placeholder="Enter full address"
                      value={screenAddress}
                      onChange={(value: any) => {
                        validateStringWithLength(value, 100, setScreenAddress);
                      }}
                    />
                  </Flex>
                  <Flex flexDir="column" gap="2">
                    <Text
                      fontSize="16px"
                      fontWeight="400"
                      color="#131D30"
                      m="0"
                    >
                      City*
                    </Text>
                    <InputField
                      placeholder="Enter City"
                      value={districtCity}
                      onChange={(value: any) => {
                        validateStringWithLength(value, 70, setDistrictCity);
                      }}
                    />
                  </Flex>
                  <Flex flexDir="column" gap="2">
                    <Text
                      fontSize="16px"
                      fontWeight="400"
                      color="#131D30"
                      m="0"
                    >
                      State*
                    </Text>
                    <Select
                      size="large"
                      value={stateUT}
                      onChange={(value: any) => {
                        setStateUT(value);
                        setError("");
                      }}
                      style={{ width: "100%", height: "45px" }}
                      options={stateOption}
                      showSearch
                      placeholder="Search to Select"
                      optionFilterProp="children"
                      maxTagCount="responsive"
                      filterOption={(input: any, option: any) =>
                        (option?.label ?? "").includes(input)
                      }
                      filterSort={(optionA: any, optionB: any) =>
                        (optionA?.label ?? "")
                          .toLowerCase()
                          .localeCompare((optionB?.label ?? "").toLowerCase())
                      }
                    />
                  </Flex>
                  <Flex flexDir="column" gap="2">
                    <Text
                      fontSize="16px"
                      fontWeight="400"
                      color="#131D30"
                      m="0"
                    >
                      Pin Code
                    </Text>
                    <InputField
                      placeholder="Ex 897656"
                      value={pincode}
                      onChange={(value: any) => {
                        validateNumberValueWithLength(value, 6, setPinCode);
                      }}
                    />
                  </Flex>
                  <Flex flexDir="column" gap="2">
                    <Text
                      fontSize="16px"
                      fontWeight="400"
                      color="#131D30"
                      m="0"
                    >
                      Landmark*
                    </Text>
                    <InputField
                      placeholder="Enter Landmark"
                      value={landMark}
                      onChange={(value: any) => {
                        validateStringWithLength(value, 100, setLandmark);
                      }}
                    />
                  </Flex>
                  <Flex flexDir="column" gap="2">
                    <Flex align="center" gap="2">
                      <Text
                        fontSize="16px"
                        fontWeight="400"
                        color="#131D30"
                        m="0"
                      >
                        HighLights*
                      </Text>
                      <ShowInformation value="Add multiple favorite palce near screen with , ex: Park, Mall,...." />
                    </Flex>
                    <InputField
                      placeholder="Ex Park,Mall"
                      value={highlights}
                      onChange={(value: any) => {
                        validateStringWithLength(value, 100, setHighlights);
                      }}
                    />
                  </Flex>
                  <Flex flexDir="column" gap="2">
                    <Flex align="center" gap="2">
                      <Text
                        fontSize="16px"
                        fontWeight="400"
                        color="#131D30"
                        m="0"
                      >
                        Longitude & Latitude*
                      </Text>
                      <ShowInformation value="Click open map button to set latitude and langitude of screen" />
                    </Flex>
                    <Flex gap="5">
                      <InputField
                        placeholder="Longitude"
                        value={lng}
                        disabled={true}
                      />
                      <InputField
                        placeholder="Latitude"
                        value={lat}
                        disabled={true}
                      />
                      <Button
                        onClick={() => {
                          setOpenMapMadel(true);
                          setError("");
                        }}
                        height="40px"
                        width="100px"
                        bg="#1CA672"
                      >
                        Open Map
                      </Button>
                    </Flex>
                  </Flex>
                </SimpleGrid>
                <Flex gap="2" pt="4">
                  {highlights.split(",")?.map((value: any, index: any) => (
                    <Button
                      key={index}
                      borderRadius="22px"
                      px="4"
                      py="2"
                      m="0"
                      bgColor="#F0F0F0"
                      color="#131D30"
                      fontSize="md"
                    >
                      {value}
                    </Button>
                  ))}
                </Flex>
              </Box>
              {/* impresion */}
              <Box p={{ base: "2", lg: "8" }} bgColor="#FFFFFF">
                <Text fontSize="lg" fontWeight="700" color="#131D30">
                  Impression*
                </Text>
                <SimpleGrid
                  columns={[1, 2, 4]}
                  spacing={{ base: "2", lg: "4" }}
                >
                  <Flex flexDir="column" gap="2">
                    <Text
                      fontSize="16px"
                      fontWeight="400"
                      color="#131D30"
                      m="0"
                    >
                      Crowd Mobility*
                    </Text>
                    <Select
                      size="large"
                      mode="multiple"
                      onChange={(value) => {
                        setCrowdMobilityType(value);
                        setError("");
                      }}
                      style={{ width: "100%", height: "45px" }}
                      options={croudMobabilityOption}
                      value={crowdMobilityType}
                      showSearch
                      placeholder="Search to Select"
                      optionFilterProp="children"
                      maxTagCount="responsive"
                      filterOption={(input: any, option: any) =>
                        (option?.label ?? "").includes(input)
                      }
                    />
                  </Flex>
                  <Flex flexDir="column" gap="2" width="100%">
                    <Text
                      fontSize="16px"
                      fontWeight="400"
                      color="#131D30"
                      m="0"
                    >
                      Employment status*
                    </Text>
                    <Select
                      size="large"
                      mode="multiple"
                      value={employmentStatus}
                      onChange={(value) => {
                        setEmploymentStatus(value);
                        setError("");
                      }}
                      style={{ width: "100%", height: "45px" }}
                      options={employmentTypeOption}
                      showSearch
                      placeholder="Search to Select"
                      optionFilterProp="children"
                      maxTagCount="responsive"
                      filterOption={(input: any, option: any) =>
                        (option?.label ?? "").includes(input)
                      }
                    />
                  </Flex>
                  <Flex flexDir="column" gap="2">
                    <Text
                      fontSize="16px"
                      fontWeight="400"
                      color="#131D30"
                      m="0"
                    >
                      Average Daily Footfall*
                    </Text>
                    <InputField
                      placeholder="Enter Daily Footfall"
                      value={averageDailyFootfall}
                      onChange={(value: any) => {
                        validateNumberValueWithLength(
                          value,
                          6,
                          setAverageDailyFootfall
                        );
                      }}
                    />
                  </Flex>
                  <Flex flexDir="column" gap="2">
                    <Text
                      fontSize="16px"
                      fontWeight="400"
                      color="#131D30"
                      m="0"
                    >
                      Average Age (Start - End)*
                    </Text>
                    <Flex gap="5">
                      <InputField
                        placeholder="Enter Start Age"
                        value={averageAgeGroup?.averageStartAge}
                        onChange={(value: any) => {
                          if (value?.length <= 2) {
                            if (isNumber(value) || value === "") {
                              setAverageAgeGroup({
                                ...averageAgeGroup,
                                averageStartAge: value,
                              });
                            }
                          }
                        }}
                      />
                      <InputField
                        placeholder="Enter End Age"
                        value={averageAgeGroup?.averageEndAge}
                        onChange={(value: any) => {
                          if (
                            (isNumber(value) && value <= 120) ||
                            value === ""
                          ) {
                            setAverageAgeGroup({
                              ...averageAgeGroup,
                              averageEndAge: value,
                            });
                          } else message.error("End age can be 120 only");
                        }}
                      />
                    </Flex>
                  </Flex>
                  <Flex flexDir="column" gap="2">
                    <Text
                      fontSize="16px"
                      fontWeight="400"
                      color="#131D30"
                      m="0"
                    >
                      Kids Friendly*
                    </Text>
                    <Radio.Group
                      options={["Yes", "No"]}
                      onChange={handelSelectKigsFriendly}
                      value={kidsFriendly}
                    />
                  </Flex>
                </SimpleGrid>
              </Box>
            </Flex>
          ) : selectedStep === 2 ? (
            <UploadScreenImages
              files={files}
              setFiles={setFiles}
              frontImage={frontImage}
              bestImage={bestImage}
              rightImage={rightImage}
              leftImage={leftImage}
              setLeftImage={setLeftImage}
              setRightImage={setRightImage}
              setBestImage={setBestImage}
              setFrontImage={setFrontImage}
              images={images}
              setImages={setImages}
            />
          ) : (
            <Flex flexDir="column" gap="2">
              <Box p={{ base: "4", lg: "4" }} bgColor="#FFFFFF">
                <Text fontSize="lg" fontWeight="700" color="#131D30" m="">
                  Enter activation code*
                </Text>
                <Text fontSize="md" fontWeight="400" color="#C3C3C3" m="">
                  Kindly input the screen code currently being displayed on your
                  screen
                </Text>
                <Input
                  width={{ base: "100%", lg: "330px" }}
                  placeholder="Screen code"
                  size="lg"
                  borderRadius="md"
                  fontSize="lg"
                  border="1px"
                  color="#555555"
                  py="2"
                  value={syncCode}
                  onChange={(e: any) => {
                    validateStringWithLength(e.target.value, 6, setSyncCode);
                  }}
                />
              </Box>
            </Flex>
          )}
        </Stack>
      )}
    </Stack>
  );
}
