import {
  Box,
  Button,
  Flex,
  Image,
  Input,
  SimpleGrid,
  Text,
} from "@chakra-ui/react";
import { AiOutlinePlus } from "react-icons/ai";
import { IoIosCloseCircleOutline } from "react-icons/io";

export function UploadScreenImages(props: any) {
  const {
    files,
    setFiles,
    frontImage,
    bestImage,
    rightImage,
    leftImage,
    setLeftImage,
    setRightImage,
    setBestImage,
    setFrontImage,
    images,
    setImages,
  } = props;

  let hiddenInputLeft: any = null;
  let hiddenInputRight: any = null;
  let hiddenInputBest: any = null;

  let hiddenInputFront: any = null;

  function handlePhotoSelectFront(file: any) {
    if (file?.type.split("/")[0] === "image") {
      const fileThumbnail = URL.createObjectURL(file);
      setFrontImage(fileThumbnail);
      let x = files;
      x[0] = file;
      setFiles(x);
    }
  }
  function handlePhotoSelectRight(file: any) {
    if (file?.type.split("/")[0] === "image") {
      const fileThumbnail = URL.createObjectURL(file);
      setRightImage(fileThumbnail);
      let x = files;
      x[2] = file;
      setFiles(x);
    }
  }
  function handlePhotoSelectLeft(file: any) {
    if (file?.type.split("/")[0] === "image") {
      const fileThumbnail = URL.createObjectURL(file);
      setLeftImage(fileThumbnail);
      let x = files;
      x[1] = file;
      setFiles(x);
    }
  }
  function handlePhotoSelectBest(file: any) {
    if (file?.type.split("/")[0] === "image") {
      const fileThumbnail = URL.createObjectURL(file);
      setBestImage(fileThumbnail);
      let x = files;
      x[3] = file;
      setFiles(x);
    }
  }

  return (
    <Flex flexDir="column" gap="2" className="scrollY">
      <Box p={{ base: "4", lg: "8" }} bgColor="#FFFFFF">
        <Text fontSize="lg" fontWeight="700" color="#131D30">
          Upload Screen Image*
        </Text>

        <SimpleGrid
          columns={[2, 2, 4]}
          spacing={{ base: "4", lg: "16" }}
          py={{ base: "2", lg: "2" }}
        >
          <Flex flexDir="column">
            <Text fontWeight="400" fontSize="lg" color="#131D30" m="0">
              Front
            </Text>
            {frontImage ? (
              <Box height={{ base: "190px", lg: "203px" }}>
                <Flex mt="-3" mr="-3" justifyContent="flex-end">
                  <IoIosCloseCircleOutline
                    size="20px"
                    onClick={() => {
                      setFrontImage(null);
                      setImages(
                        images.filter((image: any, index: any) => index !== 0)
                      );
                      let x = files;
                      x[0] = null;
                      setFiles(x);
                    }}
                  />
                </Flex>
                <Image
                  src={frontImage}
                  height={{ base: "190px", lg: "203px" }}
                />
              </Box>
            ) : (
              <Button
                variant="outline"
                border="dashed 1px"
                height="203px"
                fontSize="lg"
                fontWeight="500"
                onClick={() => hiddenInputFront.click()}
                borderColor="#A3A3A3"
                color="#8F8F8F"
                bgColor="#F0F0F0"
                leftIcon={<AiOutlinePlus size="20px" />}
              >
                Add image
              </Button>
            )}

            <Input
              hidden
              type="file"
              ref={(el) => (hiddenInputFront = el)}
              onChange={(e: any) => handlePhotoSelectFront(e.target.files[0])}
            />
          </Flex>
          <Flex flexDir="column">
            <Text fontWeight="400" fontSize="lg" color="#131D30" m="0">
              Left
            </Text>
            {leftImage ? (
              <Box height={{ base: "190px", lg: "203px" }}>
                <Flex mt="-3" mr="-3" justifyContent="flex-end">
                  <IoIosCloseCircleOutline
                    size="20px"
                    onClick={() => {
                      setLeftImage(null);
                      setImages(
                        images.filter((image: any, index: any) => index !== 1)
                      );
                      let x = files;
                      x[1] = null;
                      setFiles(x);
                    }}
                  />
                </Flex>
                <Image
                  src={leftImage}
                  height={{ base: "190px", lg: "203px" }}
                />
              </Box>
            ) : (
              <Button
                variant="outline"
                border="dashed 1px"
                height="203px"
                fontSize="lg"
                fontWeight="500"
                onClick={() => hiddenInputLeft.click()}
                borderColor="#A3A3A3"
                color="#8F8F8F"
                bgColor="#F0F0F0"
                leftIcon={<AiOutlinePlus size="20px" />}
              >
                Add image
              </Button>
            )}

            <Input
              hidden
              type="file"
              ref={(el) => (hiddenInputLeft = el)}
              onChange={(e: any) => handlePhotoSelectLeft(e.target.files[0])}
            />
          </Flex>
          <Flex flexDir="column">
            <Text fontWeight="400" fontSize="lg" color="#131D30" m="0">
              Right
            </Text>
            {rightImage ? (
              <Box height={{ base: "190px", lg: "203px" }}>
                <Flex mt="-3" mr="-3" justifyContent="flex-end">
                  <IoIosCloseCircleOutline
                    size="20px"
                    onClick={() => {
                      setRightImage(null);
                      setImages(
                        images.filter((image: any, index: any) => index !== 2)
                      );

                      let x = files;
                      x[2] = null;
                      setFiles(x);
                    }}
                  />
                </Flex>
                <Image
                  src={rightImage}
                  height={{ base: "190px", lg: "203px" }}
                />
              </Box>
            ) : (
              <Button
                variant="outline"
                border="dashed 1px"
                height="203px"
                fontSize="lg"
                fontWeight="500"
                onClick={() => hiddenInputRight.click()}
                borderColor="#A3A3A3"
                color="#8F8F8F"
                bgColor="#F0F0F0"
                leftIcon={<AiOutlinePlus size="20px" />}
              >
                Add image
              </Button>
            )}

            <Input
              hidden
              type="file"
              ref={(el) => (hiddenInputRight = el)}
              onChange={(e: any) => handlePhotoSelectRight(e.target.files[0])}
            />
          </Flex>
          <Flex flexDir="column">
            <Text fontWeight="400" fontSize="lg" color="#131D30" m="0">
              Best View
            </Text>
            {bestImage ? (
              <Box height={{ base: "190px", lg: "203px" }}>
                <Flex mt="-3" mr="-3" justifyContent="flex-end">
                  <IoIosCloseCircleOutline
                    size="20px"
                    onClick={() => {
                      setBestImage(null);
                      setImages(
                        images.filter((image: any, index: any) => index !== 3)
                      );
                      let x = files;
                      x[3] = null;
                      setFiles(x);
                    }}
                  />
                </Flex>
                <Image
                  src={bestImage}
                  height={{ base: "190px", lg: "203px" }}
                />
              </Box>
            ) : (
              <Button
                variant="outline"
                border="dashed 1px"
                height="203px"
                fontSize="lg"
                fontWeight="500"
                onClick={() => hiddenInputBest.click()}
                borderColor="#A3A3A3"
                color="#8F8F8F"
                bgColor="#F0F0F0"
                leftIcon={<AiOutlinePlus size="20px" />}
              >
                Add image
              </Button>
            )}

            <Input
              hidden
              type="file"
              ref={(el) => (hiddenInputBest = el)}
              onChange={(e: any) => handlePhotoSelectBest(e.target.files[0])}
            />
          </Flex>
        </SimpleGrid>
      </Box>
    </Flex>
  );
}
