import { Button, Flex, Text, Stack, Center } from "@chakra-ui/react";
import { Form, Input, message } from "antd";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { createNewQuery } from "../../Actions/userQueryAction";
import { useEffect } from "react";
import { ADD_NEW_QUERY_RESET } from "../../Constants/queryConstants";
import { useNavigate } from "react-router-dom";

export function ChatForm(props: any) {
  const navigate = useNavigate();
  const dispatch = useDispatch<any>();

  const createQuery = useSelector((state: any) => state.createQuery);
  const { loading, error, success } = createQuery;

  useEffect(() => {
    if (error) {
      message.error(error);
      dispatch({ type: ADD_NEW_QUERY_RESET });
    }
    if (success) {
      message.success("Query Has Send To Admin");
      dispatch({ type: ADD_NEW_QUERY_RESET });
      setTimeout(() => {
        navigate("/");
      }, 0);
    }
  }, [success, error, dispatch, navigate]);

  const onFinish = (values: any) => {
    dispatch(createNewQuery(values));
  };

  return (
    <Stack width="60%" overflowY={"hidden"} px="5">
      <Flex
        // overflow={"hidden"}
        flexGrow={1}
        flexDir="column"
        px="10"
        py="5"
        bgColor="#FFFFFF"
      >
        <Text color="#7C7C7C" fontSize="16px" fontWeight="400" m="0">
          Get In Touch
        </Text>
        <Text color="#000000" fontSize="32px" fontWeight="400" m="">
          let’s Chat ,Reach out To Us
        </Text>
        <Form
          layout="vertical"
          style={{
            flexGrow: 1,
            overflow: "hidden",
            display: "flex",
            flexDirection: "column",
          }}
          name="basic"
          initialValues={{ remember: true }}
          onFinish={onFinish}
          autoComplete="off"
        >
          <Flex flexDir={"column"} flexGrow={1} overflowY={"auto"}>
            <Form.Item
              label={
                <Text fontSize={{ base: "lg", lg: "xl" }} m="0" color="#333333">
                  Name
                </Text>
              }
              name="name"
              style={{ fontSize: "20px" }}
              rules={[{ required: true, message: "Please enter your Name!" }]}
            >
              <Input placeholder="Enter your name" size="large" />
            </Form.Item>

            <Form.Item
              label={
                <Text fontSize={{ base: "lg", lg: "xl" }} m="0" color="#333333">
                  Email
                </Text>
              }
              name="email"
              rules={[
                { required: true, message: "Please enter your Email!" },
                { type: "email" },
              ]}
            >
              <Input placeholder="Enter your email" size="large" />
            </Form.Item>

            <Form.Item
              label={
                <Text fontSize={{ base: "lg", lg: "xl" }} m="0" color="#333333">
                  Subject
                </Text>
              }
              name="subject"
              rules={[
                { required: true, message: "Please enter your Subject!" },
              ]}
            >
              <Input placeholder="Enter Subject" size="large" />
            </Form.Item>
            <Form.Item
              label={
                <Text fontSize={{ base: "lg", lg: "xl" }} m="0" color="#333333">
                  Message
                </Text>
              }
              name="message"
              rules={[
                { required: true, message: "Please enter your Message!" },
              ]}
            >
              <Input.TextArea placeholder="Enter Message" size="large" />
            </Form.Item>
            <Form.Item>
              <Center>
                <Button
                  type="submit"
                  width="50%"
                  mt="1"
                  backgroundColor="#000000"
                  py="3"
                  borderRadius="6px"
                  fontWeight="700"
                  fontSize={{ base: "xl", lg: "lg" }}
                  isLoading={loading}
                  loadingText="Sending to admin..."
                >
                  Send To Admin
                </Button>
              </Center>
            </Form.Item>
          </Flex>
        </Form>
      </Flex>
    </Stack>
  );
}
