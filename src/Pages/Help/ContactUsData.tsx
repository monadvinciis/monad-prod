import { Center, Flex, Image, Text, Stack } from "@chakra-ui/react";
import { HelpSvg } from "../../assets/svg";
import { MdOutlineMail } from "react-icons/md";
import { IoCallOutline, IoLogoWhatsapp } from "react-icons/io5";
import { CiLocationOn } from "react-icons/ci";

export function ContactUsData(props: any) {
  return (
    <Stack width="40%" h="100%" overflowY={"hidden"} pr="10">
      <Flex
        // alignSelf={"stretch"}
        flexGrow={1}
        // overflow={"hidden"}
        flexDir="column"
        gap="3"
      >
        <Flex align="center" bgColor="#FFFFFF" p="10">
          <Image src={HelpSvg} height="" width="" />
        </Flex>
        <Flex align="center" bgColor="#FFFFFF" gap="5" p="3">
          <Center bgColor="#F6F6F6" p="2">
            <MdOutlineMail size="20px" />
          </Center>
          <Flex flexDir="column">
            <Text color="#000000" fontSize="16px" fontWeight="400" m="0">
              Email
            </Text>
            <Text color="#7C7C7C" fontSize="16px" fontWeight="400" m="0">
              office@vinciis.in
            </Text>
          </Flex>
        </Flex>
        <Flex align="center" bgColor="#FFFFFF" gap="5" p="3">
          <Center bgColor="#F6F6F6" p="2">
            <IoCallOutline size="20px" />
          </Center>
          <Flex flexDir="column">
            <Text color="#000000" fontSize="16px" fontWeight="400" m="0">
              Phone
            </Text>
            <Text color="#7C7C7C" fontSize="16px" fontWeight="400" m="0">
              +919758829448
            </Text>
          </Flex>
        </Flex>
        <Flex align="center" bgColor="#FFFFFF" gap="5" p="3">
          <Center bgColor="#F6F6F6" p="2">
            <IoLogoWhatsapp size="20px" color="#075E54" />
          </Center>
          <Flex flexDir="column">
            <Text color="#000000" fontSize="16px" fontWeight="400" m="0">
              Whatsapp
            </Text>
            <Text color="#7C7C7C" fontSize="16px" fontWeight="400" m="0">
              office@vinciis.in
            </Text>
          </Flex>
        </Flex>
        <Flex align="center" bgColor="#FFFFFF" gap="5" p="3">
          <Center bgColor="#F6F6F6" p="2">
            <CiLocationOn size="20px" />
          </Center>
          <Flex flexDir="column">
            <Text color="#000000" fontSize="16px" fontWeight="400" m="0">
              Office Address
            </Text>
            <Text color="#7C7C7C" fontSize="13px" fontWeight="400" m="0">
              Near Sahara Mall, MG Road Metro Station, Gurugram, Haryana,
              122001.
            </Text>
          </Flex>
        </Flex>
      </Flex>
    </Stack>
  );
}
