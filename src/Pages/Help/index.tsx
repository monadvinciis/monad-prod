import { Box, Flex, Text, Stack } from "@chakra-ui/react";
import { ChatForm } from "./ChatForm";
import { ContactUsData } from "./ContactUsData";
import { Mytabs } from "../../components/newCommans";
import { useState } from "react";
import { SeeAllQueryList } from "./SeeAllQueryList";

const options = ["New Query", "See Queries"];

export function Help(props: any) {
  const [selectedTab, setSelectedTab] = useState<any>(1);
  return (
    <Box w="100%" h="100%" bgColor="#F5F5F5" m="0">
      <Flex
        p="3"
        align="center"
        justifyContent="space-between"
        borderTop="1px"
        borderLeft="1px"
        // border="1px solid green"
        borderColor="#EBEBEB"
        pr="20"
        pl="4"
        bgColor="#FFFFFF"
      >
        <Text fontWeight="600" fontSize="2xl" m="0">
          Contact Us
        </Text>
      </Flex>
      <Flex
        width="100%"
        borderTop="1px"
        borderLeft="1px"
        // border="1px solid green"
        borderColor="#EBEBEB"
      >
        <Mytabs
          selectedTab={selectedTab}
          setSelectedTab={setSelectedTab}
          options={options}
        />
      </Flex>
      <Flex mt="" width="100%" height="80%" py="0">
        <Stack width="100%" height="100%">
          <Stack width="100%" h="100%" overflowY={"auto"} py="5">
            {selectedTab === 1 ? (
              <Flex
                alignItems={"stretch"}
                // overflow={"hidden"}
                flexGrow={1}
                gap={2}
              >
                <ChatForm />
                <ContactUsData />
              </Flex>
            ) : (
              <Flex
                alignItems={"stretch"}
                // overflow={"hidden"}
                flexGrow={1}
                pt="3"
                px="2"
                pb="1"
                gap={6}
              >
                <SeeAllQueryList />
              </Flex>
            )}
          </Stack>
        </Stack>
      </Flex>
    </Box>
  );
}
