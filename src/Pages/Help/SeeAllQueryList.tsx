import { Flex, Text, Stack } from "@chakra-ui/react";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { getAllQueryListByUser } from "../../Actions/userQueryAction";
import { Skeleton, Table, message } from "antd";
import type { ColumnsType } from "antd/es/table";
import { GET_ALL_QUERY_BY_USER_RESET } from "../../Constants/queryConstants";

export function SeeAllQueryList(props: any) {
  const dispatch = useDispatch<any>();

  const getQueryByUser = useSelector((state: any) => state.getQueryByUser);
  const { loading, error, queries } = getQueryByUser;

  useEffect(() => {
    if (error) {
      message.error(error);
      dispatch({ type: GET_ALL_QUERY_BY_USER_RESET });
    }
  }, [error, dispatch]);

  useEffect(() => {
    dispatch(getAllQueryListByUser());
  }, [dispatch]);

  const columns: ColumnsType<any> = [
    {
      title: (
        <Text color="#000000" fontSize="lg" fontWeight="400" m="0">
          Subject
        </Text>
      ),
      dataIndex: "subject",
      key: "sunject",
    },
    {
      title: (
        <Text color="#000000" fontSize="lg" fontWeight="400" m="0">
          Message{" "}
        </Text>
      ),
      dataIndex: "message",
      key: "message",
      render: (value) => (
        <Text color="#000000" fontSize="13px" fontWeight="400">
          {value}
        </Text>
      ),
    },

    {
      title: (
        <Text color="#000000" fontSize="lg" fontWeight="400" m="0">
          Status
        </Text>
      ),
      dataIndex: "status",
      key: "status",
    },
  ];

  return (
    <Stack width="100%" overflowY={"auto"}>
      <Flex gap="2" bgColor="">
        {loading ? (
          <Flex flexDir="column">
            <Skeleton avatar active paragraph={{ rows: 2 }} />{" "}
            <Skeleton avatar active paragraph={{ rows: 2 }} />{" "}
            <Skeleton avatar active paragraph={{ rows: 2 }} />{" "}
          </Flex>
        ) : (
          <Stack>
            <Table
              columns={columns}
              dataSource={queries || []}
              loading={loading}
              scroll={{ y: 300 }}
              pagination={{ pageSize: 10 }}
            />
          </Stack>
        )}
      </Flex>
    </Stack>
  );
}
