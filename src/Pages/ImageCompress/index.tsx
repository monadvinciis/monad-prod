import React, { useState } from "react";

const ImageCompressor = () => {
  const [compressedImage, setCompressedImage] = useState<File | null>(null);

  const handleFileChange = async (e: React.ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files?.[0];
    console.log("file : ", file);

    if (!file) return;

    const reader = new FileReader();
    reader.onload = async function (event) {
      const image = new Image();
      image.src = event.target?.result as string;

      image.onload = function () {
        const canvas = document.createElement("canvas");
        const ctx = canvas.getContext("2d");

        canvas.width = 200; // Set your desired width
        canvas.height = (image.height / image.width) * 200;

        ctx?.drawImage(image, 0, 0, canvas.width, canvas.height);

        canvas.toBlob(
          (blob) => {
            if (blob) {
              setCompressedImage(
                new File([blob], file.name, {
                  type: "image/jpeg",
                  lastModified: Date.now(),
                })
              );
            }
          },
          "image/jpeg",
          0.7
        ); // Adjust quality as needed
      };
    };

    reader.readAsDataURL(file);
  };

  return (
    <div>
      <input type="file" onChange={handleFileChange} />
      {compressedImage && (
        <img src={URL.createObjectURL(compressedImage)} alt="Compressed" />
      )}
    </div>
  );
};

export default ImageCompressor;
