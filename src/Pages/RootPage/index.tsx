import { useSelector } from "react-redux";
import { Dashboard } from "../Dashboard";
import { Signin } from "../auth/Signin";

export function RootPage() {
  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  return <div>{userInfo ? <Dashboard /> : <Signin />}</div>;
}
