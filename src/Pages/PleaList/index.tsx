import { Box, Flex, Image, Text, Stack } from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";

export function PleaList(props: any) {
  const navigate = useNavigate();
  const { pleaRequest } = props;
  return (
    <Box
      flexGrow={1}
      // overflow={"auto"}
      // display={"flex"}
      // flexDir={"column"}
      // bgColor="#FFFFFF"
      px="2"
      // borderRadius="11px"
      h="100%"
      // mb="0"
      pb="10"
      // border="1px solid green"
    >
      <Stack
        p="3"
        // pb="2"
        flexGrow={1}
        h="90%"
        overflowY={"auto"}
        // flexDir="column"
        // border="1px solid red"
        gap="2"
      >
        {pleaRequest?.length > 0 &&
          pleaRequest?.map(({ fromUser, plea }: any, index: any) => (
            <Flex
              gap="1"
              key={index}
              p="2"
              align={"left"}
              onClick={() => navigate("/notification")}
              _hover={{
                bgColor: "#F8F8F8",
                boxShadow: "-5px 5px 5px rgba(0, 0, 0, 0.1)", // apply a larger shadow on hover
                transition: "box-shadow 0.3s", // add a smooth transition
                transform: "scale(1.01)",
              }}
            >
              <Image
                src={fromUser?.avatar}
                alt=""
                height="43px"
                width="43px"
                rounded="2xl"
                p="1"
              />
              <Text fontWeight="700" fontSize="sm" m="0" align="left">
                {plea?.remarks[plea?.remarks?.length - 1]}
              </Text>
            </Flex>
          ))}
      </Stack>
    </Box>
  );
}
