import { Flex, Hide, Show, Text } from "@chakra-ui/react";
import "../index.css";
import { useNavigate } from "react-router-dom";
import { ScreenList } from "./ScreenList";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import Search from "antd/es/input/Search";
import { AddButton } from "../../components/newCommans";
import { FloatButton } from "antd";
import { LuChevronLeft } from "react-icons/lu";
import { FiMonitor } from "react-icons/fi";
import { userScreensList } from "../../Actions/userActions";

export function Screens(props: any) {
  const navigate = useNavigate();

  const dispatch = useDispatch<any>();
  const [searchText, setSearchText] = useState<any>("");

  const handleCreateScreen = () => {
    navigate("/screen/create");
  };

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  const onSearch = (value: any, _e: any, info: any) => {
    dispatch(userScreensList(userInfo, searchText));
  };

  useEffect(() => {
    if (!userInfo) {
      navigate("/signin", { state: { path: "/my/screens" } });
    }
  }, [userInfo]);

  return (
    <Flex
      width="100%"
      height="100%"
      overflow={"hidden"}
      gap="0"
      flexGrow={1}
      bgColor="#F5F5F5"
      m="0"
    >
      <Flex overflow={"hidden"} height={"100%"} flexDir="column" width="100%">
        <Flex
          p={{ base: "3", lg: "3" }}
          align="center"
          justifyContent="space-between"
          borderTop="1px"
          borderColor="#EBEBEB"
          pr={{ base: "0", lg: "32px" }}
          pl={{ base: "2", lg: "4" }}
          bgColor="#FFFFFF"
          gap={{ base: "2", lg: "4" }}
        >
          <Flex gap={{ base: "2", lg: "4" }} align="center">
            <LuChevronLeft size="20px" onClick={() => navigate("/")} />
            <Text fontWeight="600" fontSize={{ base: "20px", lg: "2xl" }} m="0">
              Screens
            </Text>
          </Flex>
          <Hide below="md">
            <Flex gap={{ base: "2", lg: "4" }}>
              <Search
                placeholder="Screen Name, Location"
                allowClear
                onSearch={onSearch}
                onChange={(e) => setSearchText(e.target.value)}
                size="large"
                style={{
                  width: 300,
                }}
              />

              <AddButton handelClick={handleCreateScreen} label="Screen" />
            </Flex>
          </Hide>
        </Flex>
        <Show below="md">
          <Flex
            p="4"
            bgColor="#FFFFFF"
            border="1px"
            borderColor="#EBEBEB"
            borderLeft={"0px"}
          >
            <Search
              placeholder="Screen Name, Location"
              allowClear
              onSearch={onSearch}
              onChange={(e) => setSearchText(e.target.value)}
              size="large"
              style={{
                width: 400,
              }}
            />
          </Flex>
        </Show>
        <ScreenList searchText={searchText} />
        <Show below="md">
          <FloatButton
            type="primary"
            tooltip={<div>Create Screen</div>}
            style={{
              right: 40,
              bottom: 75,
            }}
            icon={<FiMonitor />}
            onClick={handleCreateScreen}
          ></FloatButton>
        </Show>
      </Flex>
    </Flex>
  );
}
