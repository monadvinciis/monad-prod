import { Box, Button, Flex, Hide, Skeleton, Text } from "@chakra-ui/react";
import "../index.css";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { deleteScreen } from "../../Actions/screenActions";
import { useEffect, useState } from "react";
import { message } from "antd";
import { SCREEN_DELETE_RESET } from "../../Constants/screenConstants";
import { USER_SCREENS_RESET } from "../../Constants/userConstants";
import { userScreensList } from "../../Actions/userActions";
import { SingleScreen } from "../../components/newCommans";
import { HaveNoScreen } from "../HaveNoScreen";
import { ShowScreensLocationOnMap } from "../ShowScreensLocationOnMap";
import {
  getPendingPleaListByScreenOwnerId,
  listAllPleasByUserId,
} from "../../Actions/pleaActions";
import { PleaList } from "../PleaList";

const data = [1, 2, 3, 4, 5, 6];

export function ScreenList(props: any) {
  const { searchText } = props;
  const navigate = useNavigate();
  const dispatch = useDispatch<any>();
  const [jsonData, setJsonData] = useState<any>(null);
  const [pinScreen, setPinScreen] = useState<any>([]);
  const [selectedBox, setSelectedBox] = useState<any>(0);

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  const userScreens = useSelector((state: any) => state.userScreens);
  const { loading: loadingScreens, error: errorScreens, screens } = userScreens;

  const screenDelete = useSelector((state: any) => state.screenDelete);
  const {
    // loading: loadingScreenDelete,
    error: errorScreenDelete,
    success: successScreeDelete,
  } = screenDelete;

  const pendingPleaListByScreenOwner = useSelector(
    (state: any) => state.pendingPleaListByScreenOwner
  );
  const {
    allPleas: pendingPleaRequest,
    // loading: loadingPendingPleaRequest,
    // error: errorPendingPleaRequest,
  } = pendingPleaListByScreenOwner;

  const allPleasListByUser = useSelector(
    (state: any) => state.allPleasListByUser
  );
  const {
    allPleas: allPleaRequest,
    // loading: loadingAllPleaRequest,
    // error: errorAllPleaRequest,
  } = allPleasListByUser;

  const handleDeleteScreen = (screenId: any) => {
    dispatch(deleteScreen(screenId));
  };

  useEffect(() => {
    if (searchText?.length === 0)
      dispatch(userScreensList(userInfo, searchText));
  }, [searchText, dispatch, userInfo]);

  useEffect(() => {
    if (successScreeDelete) {
      message.success("Screen deleted successfully!");
      dispatch({ type: SCREEN_DELETE_RESET });
      dispatch(userScreensList(userInfo, ""));
    }
    if (errorScreenDelete) {
      message.error(errorScreenDelete);
      dispatch({ type: SCREEN_DELETE_RESET });
    }
  }, [dispatch, navigate, errorScreenDelete, successScreeDelete, userInfo]);

  useEffect(() => {
    if (errorScreens) {
      message.error(errorScreens);
      dispatch({ type: USER_SCREENS_RESET });
      if (errorScreens === "Please Signin Again to continue") {
        navigate("/signin");
      }
    }
  }, [errorScreens, dispatch, navigate]);

  useEffect(() => {
    if (screens?.length > 0) {
      setPinScreen(screens[selectedBox]);
      const data = screens?.map((screen: any) => {
        return {
          type: "Feature",
          properties: {
            screen: screen?._id,
          },
          geometry: {
            coordinates: [screen?.lat, screen?.lng],
            type: "Point",
          },
        };
      });
      setJsonData({
        features: data,
      });
    }
  }, [screens, selectedBox]);

  useEffect(() => {
    if (!userInfo) {
      navigate("/signin");
    }
    dispatch(userScreensList(userInfo, ""));
    dispatch(getPendingPleaListByScreenOwnerId());
    dispatch(listAllPleasByUserId());
  }, [userInfo, navigate, dispatch]);

  const handelSelectScreens = (index: any) => {
    setPinScreen(screens[index]);
    setSelectedBox(index);
  };
  return (
    <Flex
      overflow={"hidden"}
      alignItems={"stretch"}
      flexGrow={1}
      gap={{ base: "2", lg: "2" }}
      // p={{ base: "2", lg: "4" }}
      height="100%"
    >
      <Box flexGrow={1} pb={{ base: "0", lg: "0" }}>
        {loadingScreens ? (
          data?.map((value: any) => (
            <Skeleton key={value}>
              <Box
                width={"100%"}
                height="251px"
                bgPosition="center"
                bgSize="cover"
                borderRadius="8px"
              ></Box>
            </Skeleton>
          ))
        ) : (
          <Flex
            overflowY="auto"
            gap="2"
            flexDirection="column"
            height="100%"
            p={{ base: "2", lg: "4" }}
            // border="1px solid green"
          >
            {screens?.length > 0 &&
              screens?.map((singleScreen: any, index: any) => (
                <SingleScreen
                  screen={singleScreen}
                  key={index}
                  index={index}
                  handelSelectScreens={handelSelectScreens}
                  handleDeleteScreen={handleDeleteScreen}
                />
              ))}
          </Flex>
        )}
        {screens?.length === 0 && <HaveNoScreen />}
      </Box>
      <Hide below="md">
        <Flex
          pr={{ base: "2", lg: "4" }}
          py={{ base: "2", lg: "4" }}
          flexDir="column"
          width="36%"
          gap="4"
        >
          <Box
            // flexShrink={0}
            height="40%"
            p="2"
            bgColor="#FFFFFF"
            borderRadius="18px"
            _hover={{
              boxShadow: "-5px 5px 5px rgba(0, 0, 0, 0.1)", // apply a larger shadow on hover
              transition: "box-shadow 0.3s", // add a smooth transition
              transform: "scale(1.01)",
            }}
          >
            {jsonData ? (
              <Box height="100%" width="100%">
                <ShowScreensLocationOnMap
                  data={jsonData}
                  screen={pinScreen}
                  zoom="14"
                  // height="100%"
                />
              </Box>
            ) : null}
          </Box>
          <Box py="2" height="60%" bg="#ffffff" borderRadius="11px">
            <Flex justifyContent="space-between" pt="3" px="5">
              <Text color="#131D30" fontSize="20px" fontWeight="700">
                Pending Request(
                {pendingPleaRequest?.length || 0})
              </Text>
              <Button
                color="#222020"
                fontSize="sm"
                fontWeight="400"
                py="2"
                px="5"
                bgColor="#FFFFFF"
                variant="null"
                onClick={() => navigate("/notification")}
              >
                View All
              </Button>
            </Flex>
            <PleaList
              pleaRequest={
                pendingPleaRequest?.length !== 0
                  ? pendingPleaRequest
                  : allPleaRequest
              }
            />
          </Box>
        </Flex>
      </Hide>
    </Flex>
  );
}
