import { Box, Button, Center, Image, Text } from "@chakra-ui/react";
import nodata from "../../assets/Images/nodata.jpeg";
import { HiPlus } from "react-icons/hi";
import { useNavigate } from "react-router-dom";

export function HaveNoScreen(props: any) {
  const navigate = useNavigate();

  const handleCreateScree = () => {
    navigate(`/screen/create`);
  };

  return (
    <Box>
      <Center justifyContent="center" alignItems="center" p="200">
        <Center flexDirection="column" alignContent="center">
          <Image src={nodata} alt="nodata" />
          <Text
            color="#131D30"
            fontSize={{ base: "lg", lg: "32px" }}
            fontWeight="700"
            pt="2"
          >
            Add Your Screen
          </Text>
          <Text
            color="#8E8E8E"
            fontSize={{ base: "12px", lg: "20px" }}
            fontWeight="600"
          >
            Your profile is not completed please complete your profile
          </Text>
          <Button
            className="button"
            leftIcon={<HiPlus />}
            onClick={handleCreateScree}
          >
            Add screen
          </Button>
        </Center>
      </Center>
    </Box>
  );
}
