import React, { useState } from "react";
import { Modal, message } from "antd";
import {
  Box,
  Text,
  InputGroup,
  Input,
  Button,
  Stack,
  HStack,
  SimpleGrid,
} from "@chakra-ui/react";
import { useAnimation } from "framer-motion";
import { AiOutlineCloudUpload } from "react-icons/ai";
import { ShowVideos } from "./ShowVideos";
import { getVideoDurationFronVideoURL1 } from "../../utils/utilityFunctions";

export function UploadMedia(props: any) {
  let hiddenInput: any = null;
  const controls = useAnimation();
  const [files, setFiles] = useState<any>([]);
  const [messageApi, contextHolder] = message.useMessage();
  const startAnimation = () => controls.start("hover");
  const stopAnimation = () => controls.stop();

  const validateSelectedFile = (file: any) => {
    // const MIN_FILE_SIZE = 1024; // 1MB
    let mb = 1000; // mb
    const MAX_FILE_SIZE = mb * 1000 * 1024; // 5MB
    const fileExtension = file.type.split("/")[1];
    const fileSizeKiloBytes = file.size; // convert to kb
    if (fileSizeKiloBytes > MAX_FILE_SIZE) {
      message.error(
        "File size is greater than maximum limit. File size must be less then 50 MB "
      );
      return false;
    }
    if (
      !(
        fileExtension === "mp4" ||
        fileExtension === "jpg" ||
        fileExtension === "jpeg" ||
        fileExtension === "png"
      )
    ) {
      message.error("File format must be .mp4 or jpg or jpeg and png");
      return false;
    }
    return true;
  };
  const mediaError = (error: any) => {
    messageApi.open({
      type: "error",
      content: error,
    });
  };

  async function handleMultipleFilesSelect(multipleFiles: any) {
    const data = [];
    for (let file of multipleFiles) {
      if (validateSelectedFile(file)) {
        const fileURL = URL.createObjectURL(file);
        const fileExtension = file.type.split("/")[0];
        const duration =
          file.type.split("/")[0] === "image"
            ? 30
            : await getVideoDurationFronVideoURL1(fileURL);

        data.push({
          file,
          fileURL,
          fileExtension,
          fileType: file.type,
          fileSize: file.size,
          duration: duration,
        });
      }
    }
    setFiles(data);
  }

  const handelDiscard = () => {
    setFiles([]);
    props?.onCancel();
  };

  const handleNext = (e: any) => {
    props?.handleSelectFile(files);
    handelDiscard();
  };

  return (
    <Modal
      title=""
      centered
      open={props?.open}
      onCancel={() => props.onCancel()}
      footer={[]}
      closable={true}
      maskClosable={false}
    >
      <Box>
        {contextHolder}

        {files?.length > 0 ? (
          <Box>
            <Text
              color="#333333"
              fontSize={{ base: "xl", lg: "2xl" }}
              fontWeight="semibold"
              align="center"
            >
              Uploaded media
            </Text>
            <SimpleGrid columns={[2, 2, 2]} spacing="4">
              {files?.length > 0 &&
                files?.map((data: any, index: any) => (
                  <ShowVideos key={index} data={data} />
                ))}
            </SimpleGrid>
            <HStack justifyContent="space-between" pt="5">
              <Button
                fontSize={{ base: "lg", lg: "xl" }}
                variant="null"
                onClick={() => {
                  setFiles([]);
                }}
              >
                Discard
              </Button>
              <Button
                type="submit"
                color="#D7380E"
                variant="outline"
                borderColor="#000000"
                py="2"
                px="10"
                fontSize={{ base: "lg", lg: "xl" }}
                onClick={handleNext}
              >
                Next
              </Button>
            </HStack>
          </Box>
        ) : (
          <Box>
            <Text
              color="#333333"
              fontSize={{ base: "xl", lg: "2xl" }}
              fontWeight="semibold"
              align="center"
            >
              Upload media
            </Text>
            <Text color="#5B5B5B" fontSize={{ base: "lg", lg: "xl" }}>
              Upload content
            </Text>
            <InputGroup>
              <Box
                height="112px"
                width="100%"
                border="1px"
                borderRadius="16px"
                alignItems="center"
                borderColor="#E4E4E4"
                pt="5"
                onClick={() => hiddenInput.click()}
              >
                <Stack align="center">
                  <Button
                    leftIcon={<AiOutlineCloudUpload size="20px" />}
                    py="2"
                    bgColor="#0EBCF5"
                    borderRadius="20px"
                    fontSize="md"
                  >
                    Upload a photo or video
                  </Button>
                </Stack>
                <Input
                  hidden
                  type="file"
                  ref={(el) => (hiddenInput = el)}
                  multiple={true}
                  onDragEnter={startAnimation}
                  onDragLeave={stopAnimation}
                  onChange={(e: any) =>
                    handleMultipleFilesSelect(e.target.files)
                  }
                />
              </Box>
            </InputGroup>
            <Text color="red" align="right">{`Max file size < 10 MB`}</Text>
            <HStack justifyContent="space-between">
              <Button
                fontSize={{ base: "lg", lg: "xl" }}
                variant="null"
                onClick={handelDiscard}
              >
                Discard
              </Button>
            </HStack>
          </Box>
        )}
      </Box>
    </Modal>
  );
}
