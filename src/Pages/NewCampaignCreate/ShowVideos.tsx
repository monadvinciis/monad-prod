import { Box, Image, Stack } from "@chakra-ui/react";

export function ShowVideos(props: any) {
  const { data } = props;
  return (
    <Stack height={{ base: "150px", lg: "150px" }}>
      {data?.fileExtension === "video" ? (
        <Box
          as="video"
          src={data.fileURL}
          autoPlay
          loop
          muted
          display="inline-block"
          borderRadius="8px"
          height={{ base: "140px", lg: "140px" }}
          width="100%"
        ></Box>
      ) : (
        <Image src={data.fileURL} alt="" width="100%" height="100%"></Image>
      )}
    </Stack>
  );
}
