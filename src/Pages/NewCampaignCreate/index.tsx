import {
  Text,
  Box,
  Flex,
  Button,
  InputGroup,
  SimpleGrid,
  Stack,
  InputRightElement,
  Center,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { CgScreen } from "react-icons/cg";
import { MdAdd, MdOutlineCampaign, MdOutlineSummarize } from "react-icons/md";
import {
  Checkbox,
  Popconfirm,
  Select,
  SelectProps,
  Tooltip,
  message,
} from "antd";
import {
  DateAndTimeInputField,
  InputField,
  ScrollBox,
  SingleScreenForCampaignCreate,
} from "../../components/newCommans";
import Axios from "axios";
import { PiMinusBold } from "react-icons/pi";
import { UploadMedia } from "./UploadMedia";
import { useDispatch } from "react-redux";
import { CroudMobabilityFunction, Header, Steps } from "./HelperComponents";
import { GrShop } from "react-icons/gr";
import { IoMdSearch } from "react-icons/io";
import {
  createCamapaignsWithMultipleMediaWithMultipleScreens,
  getScreensBasedOnAudiancesProfile,
} from "../../Actions/campaignForMultipleScreenAction";
import { useSelector } from "react-redux";
import {
  convertIntoDateAndTime,
  getNumberOfDaysBetweenTwoDates,
} from "../../utils/dateAndTimeUtils";
import { RiDeleteBinLine } from "react-icons/ri";
import { useLocation, useNavigate } from "react-router-dom";
import { getUserWalletBalance } from "../../Actions/walletAction";
import { CREATE_CAMPAIGN_FOR_MULTIPLE_SCREEN_RESET } from "../../Constants/campaignForMultipleScreen";
import { userScreensList } from "../../Actions/userActions";
import { MEDIA_UPLOAD_RESET } from "../../Constants/mediaConstants";
import { CreatedCampaignStatus } from "../Models/CreatedCampaignStatus";
import { ShowScreensLocation } from "../MyMap/ShowScreensLocation";
import { CiEdit } from "react-icons/ci";
import { FaSave } from "react-icons/fa";
import { NotificationButton } from "../../components/newCommans/NotificationButton/NotificationButton";
import { addFileOnWeb3 } from "../../utils/newWeb3";
import { ShowVideos } from "./ShowVideos";

const campaignGoalOptions: SelectProps["options"] = [
  // { value: "Audience engagement", label: "Audience engagement" },
  { value: "Audience reach", label: "Audience reach" },
];
const genderOption: SelectProps["options"] = [
  { value: "Male", label: "Male" },
  { value: "Female", label: "Female" },
  { value: "All", label: "All" },
];
const categoryOption: SelectProps["options"] = [
  { value: "OUTDOORS", label: "Out Doors" },
  { value: "INDOORS", label: "In Doors" },
  { value: "RAILWAY", label: "Railway" },
  { value: "HOSPITAL", label: "Hospital" },
  { value: "ERICKSHAW", label: "eRickshaw" },
];
const tabOptions = [
  {
    key: 1,
    label: "My Screens",
  },
  {
    key: 2,
    label: "Explore Screens",
  },
  {
    key: 3,
    label: "Explore Map",
  },
];

export function NewCampaignCreate(props: any) {
  const dispatch = useDispatch<any>();
  const navigate = useNavigate();
  const location = useLocation();
  const [selectedStep, setSelectedStep] = useState<any>(1);
  const [createdCampaign, setCreatedCampaign] = useState<any>();
  const [steps, setSteps] = useState([
    {
      key: 1,
      text: "Details",
      status: "Active",
      icon: <MdOutlineCampaign size="20px" />,
    },
    {
      key: 2,
      text: "Screens",
      status: "process",
      icon: <CgScreen size="20px" />,
    },
    {
      key: 3,
      text: "Summary",
      status: "process",
      icon: <MdOutlineSummarize size="20px" />,
    },
  ]);
  const [selectAllMyScreen, setSelectAllMyScreen] = useState<any>(false);
  const [selectAllSearchScreen, setselectAllSearchScreen] =
    useState<any>(false);
  const [createdMedias, setCreatedMedias] = useState<any>([]);

  const [isOpenUploadMedia, setIsOpenUploadMedia] = useState<any>(true);
  const [isOpenShowStatus, setIsOpenShowStatus] = useState<any>(false);

  const [isOpenCampaignDetails, setIsOpenCampaignDetails] =
    useState<any>(false);
  const [isOpenAudianceProfile, setIsOpenAudianceProfile] =
    useState<any>(false);
  const [openUploadMedia, setOpenUploadMedia] = useState<boolean>(false);
  const [selectedTab, setSelectedTab] = useState<any>(1);
  const [selectedScreensMediaWise, setSelectedScreensMediaWise] = useState<any>(
    []
  );
  // note :selectedScreensMediaWise= [{media : {} , selectedScreens : [{} , {}]} , {media : {} ,selectedScreens ;[{},{}]}]

  const [selectedScreens, setSelectedScreens] = useState<any>([]);
  const [selectedBugdet, setSelectedBugdet] = useState<number>(0);
  const [selectedMediaIndex, setSelectedMediaIndex] = useState<any>(null);
  const [campaignGoal, setCampaignGoal] = useState<any>(null);
  const [campaignName, setCampaignName] = useState<any>("");
  const [brandName, setBrandName] = useState<any>("");
  const [startDateAndTime, setStartDateAndTime] = useState<any>(null);
  const [endDateAndTime, setEndDateAndTime] = useState<any>(null);
  const [budget, setBudget] = useState<number>();
  const [audianceReach, setAaudianceReach] = useState<number>();
  const [gender, setGender] = useState<any>("All");
  const [category, setCategory] = useState<any>("OutDoors");
  const [crowdMobilityType, setCrowdMobilityType] = useState<any>([
    "Sitting",
    "Moving",
    "Walking",
  ]);
  const [loading, setLoading] = useState<boolean>(false);
  const [startAge, setStartAge] = useState<any>(10);
  const [endAge, setEndAge] = useState<any>(80);
  const [cities, setCities] = useState<any>(``);
  const [highlights, setHighlights] = useState<any>(`Appartments`);
  const [files, setFiles] = useState<any>([]);
  // files = [ { file: "", fileExtension: "video" },{}]
  const [isEditBudget, setIsEditBudget] = useState<boolean>(false);

  const walletBalance = useSelector((state: any) => state.walletBalance);
  const {
    // loading: loadingWalletBalance,
    // error: errorWalletBalance,
    // success: successWalletBalance,
    wallet,
  } = walletBalance;

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  const createCampaignForMultipleScreen = useSelector(
    (state: any) => state.createCampaignForMultipleScreen
  );
  const {
    loading: loadingSlotBooking,
    error: errorSlotBooking,
    success: successSlotBooking,
    uploadedCampaign,
  } = createCampaignForMultipleScreen;

  const userScreens = useSelector((state: any) => state.userScreens);
  const { screens } = userScreens;

  const getScreens = useSelector((state: any) => state.getScreens);
  const {
    // loading: loadingFilterScreens,
    // error: errorFilterScreens,
    screens: filteredScreens,
  } = getScreens;

  const handleAddBudget = (value: any) => {
    if (Number.isNaN(value)) {
      message.error("Please enter number only");
    } else if (Number(value) <= wallet?.balance) {
      setBudget(value);
    } else {
      message.warning(
        `You have Rs. ${wallet?.balance} in your wallet, you cann't excide your limit otherwise recharge your wallet first`
      );
    }
  };

  const getTotalNumberOfSelectedScreens = () => {
    return selectedScreensMediaWise?.reduce((accum: any, current: any) => {
      return accum + current?.selectedScreens?.length;
    }, 0);
  };

  const getAllSelectedScreenMediaIndex = (index: number) => {
    return selectedScreensMediaWise[index]?.selectedScreens?.length;
  };

  const handelSelectAllMyScreen = async (value: any, screens: any) => {
    if (selectedMediaIndex === null) {
      message.error("First select media first ,by click on them");
      return;
    }
    let ss = selectedScreensMediaWise[selectedMediaIndex]?.selectedScreens; // return an array
    let ssmw = selectedScreensMediaWise; // ssmw = selected screen media wise // return an object
    if (value) {
      const data = screens?.filter((screen: any) => {
        if (!ss?.find((screen1: any) => screen1?._id == screen?._id)) {
          return screen;
        }
      });
      setSelectedScreens([...selectedScreens, ...data]);
      ssmw[selectedMediaIndex].selectedScreens = [...ss, ...data];
      setSelectedScreensMediaWise(ssmw);
      data.forEach((screen: any) => {
        if (screen?.master != userInfo?._id) {
          handleSetSelectedBudget(screen, value);
        }
      });
    } else {
      const data = ss?.filter((screen: any) => {
        if (!screens.find((screen1: any) => screen1?._id == screen?._id)) {
          return screen;
        }
      });
      setSelectedScreens(data);
      ssmw[selectedMediaIndex].selectedScreens = data;
      setSelectedScreensMediaWise(ssmw);
      screens.forEach((screen: any) => {
        if (screen?.master != userInfo?._id) {
          handleSetSelectedBudget(screen, value);
        }
      });
    }
  };

  useEffect(() => {
    if (errorSlotBooking) {
      message.error(errorSlotBooking);
      dispatch({ type: CREATE_CAMPAIGN_FOR_MULTIPLE_SCREEN_RESET });
    }
    if (successSlotBooking) {
      const { createdCampaign } = uploadedCampaign;
      if (createdCampaign?.length === 0) {
        message.error(
          "Campaign All ready present with this media, Try with other media"
        );
      } else {
        message.success(
          `Campaign created successFull on ${createdCampaign?.length} Screens only`
        );
        setCreatedCampaign(createdCampaign[0]);
        setTimeout(() => setIsOpenShowStatus(true), 0);
      }
      dispatch({ type: CREATE_CAMPAIGN_FOR_MULTIPLE_SCREEN_RESET });
      setFiles([]);
      dispatch({ type: MEDIA_UPLOAD_RESET });
    }
  }, [dispatch, navigate, errorSlotBooking, successSlotBooking]);

  const slotBookingHandler = () => {
    if (createdMedias?.length > 0) {
      const reqBody = {
        campaignName: campaignName,
        startDateAndTime,
        endDateAndTime,
        brandName,
        noOfDays: getNumberOfDaysBetweenTwoDates(
          startDateAndTime,
          endDateAndTime
        ),
        mediesWiseScreens: selectedScreensMediaWise.map((data: any) => {
          return {
            media: data.media,
            fileType: data.fileType,
            fileSize: data.fileSize,
            duration: data.duration,
            selectedScreens: data.selectedScreens.map(
              (screen: any) => screen?._id
            ),
          };
        }),
        additionalInfo: {
          campaignGoal,
          cities,
          highlights,
          budget,
          audianceReach,
          category,
          gender,
          crowdMobilityType,
          ageRange: [startAge, endAge],
        },
      };
      dispatch(createCamapaignsWithMultipleMediaWithMultipleScreens(reqBody));
    } else {
      message.warning("Please wait for some time, Media will uploading");
    }
  };

  useEffect(() => {
    if (!userInfo) {
      navigate("/signin", { state: { path: "/" } });
    }

    if (userInfo?.isMaster) dispatch(userScreensList(userInfo, ""));

    if (userInfo?.userWallet) {
      dispatch(getUserWalletBalance());
    } else {
      message.warning("You have no wallet, Please create new wallet first");
    }
  }, [userInfo, dispatch, navigate]);

  const addOrRemobeCrowdMobilityType = (lable: any) => {
    if (crowdMobilityType?.length > 0) {
      if (crowdMobilityType.includes(lable)) {
        setCrowdMobilityType([
          ...crowdMobilityType.filter((data: any) => data !== lable),
        ]);
      } else {
        setCrowdMobilityType([...crowdMobilityType, lable]);
      }
    } else {
      setCrowdMobilityType([lable]);
    }
  };

  const handleToPay = () => {
    if (wallet?.balance >= grandTotal()) {
      // proceed to create campaign
      slotBookingHandler();
    } else {
      // send notification to user to recharge your wallet
      message.warning(
        "You have not sufficient balance to continue, for continue recharge your wallet"
      );
      setTimeout(() => {
        window.open("/my/wallet", "_blank", "noopener,noreferrer");
      }, 0);
    }
  };

  const handleEndDate = (value: any) => {
    if (!startDateAndTime) {
      message.error("Please select start date first!");
    } else if (new Date(value) <= new Date(startDateAndTime)) {
      message.error("End date must be greator then start Date");
    } else if (getNumberOfDaysBetweenTwoDates(startDateAndTime, value) < 1) {
      message.error("Please select atleast for 1 day or greator");
    } else {
      setEndDateAndTime(value);
    }
  };

  const getValueOfpersentage = (value: number) => {
    return ((Number(getTotalCost()?.toFixed(2)) * value) / 100)?.toFixed(2);
  };

  const grandTotal = () => {
    return Number(
      Number(getValueOfpersentage(18)) +
        Number(getValueOfpersentage(10)) +
        Number(getTotalCost()?.toFixed(2)) -
        Number(getValueOfpersentage(20))
    )?.toFixed(2);
  };
  const handleSetSelectedBudget = (screen: any, value: any) => {
    if (screen?.master !== userInfo?._id) {
      const x = getNumberOfDaysBetweenTwoDates(
        startDateAndTime,
        endDateAndTime
      );
      const amount = Number(x) * Number(screen?.rentPerDay);
      if (value) {
        setSelectedBugdet((previos: any) => Number(previos) + Number(amount));
      } else {
        setSelectedBugdet((previos: any) => Number(previos) - Number(amount));
      }
    }
  };

  const handleSelectScreens = (screen: any, checked: any) => {
    if (selectedMediaIndex === null) {
      message.error("First select media first ,by click on them");
      return;
    }
    let ss = selectedScreensMediaWise[selectedMediaIndex]?.selectedScreens; // return an array
    let ssmw = selectedScreensMediaWise; // ssmw = selected screen media wise // return an object
    if (checked) {
      ss.push(screen);
      ssmw[selectedMediaIndex].selectedScreens = ss;
      setSelectedScreensMediaWise(ssmw);
      setSelectedScreens([...selectedScreens, screen]);
      handleSetSelectedBudget(screen, checked);
    } else {
      const data = ss?.filter((ss1: any) => ss1?._id != screen?._id);
      ssmw[selectedMediaIndex].selectedScreens = data;
      setSelectedScreensMediaWise(ssmw);
      setSelectedScreens(data);
      handleSetSelectedBudget(screen, checked);

      // we need to check this screen from filterscreen or not if yes then select all false
      if (filteredScreens.find((data: any) => data?._id == screen?._id)) {
        setselectAllSearchScreen(false);
      }
    }
  };

  const handleBack = () => {
    if (selectedStep > 1) {
      setSelectedStep((previous: any) => previous - 1);
    } else {
      dispatch({ type: MEDIA_UPLOAD_RESET });
      setSelectedScreensMediaWise([]);
      setFiles([]);
      navigate("/my/campaigns");
    }
  };

  const validateForm = () => {
    if (files?.length === 0) {
      message.error("Please Upload medias first");
      return false;
    } else if (!campaignGoal) {
      message.error("Please select campaign objective");
      return false;
    } else if (!campaignName) {
      message.error("Please Enter campaign title");
      return false;
    } else if (!brandName) {
      message.error("Please Enter brad name");
      return false;
    } else if (!startDateAndTime) {
      message.error("Please Enter Start date");
      return false;
    } else if (!endDateAndTime) {
      message.error("Please Enter End Date");
      return false;
    } else if (Number(budget) < 0 || !budget) {
      message.error("Please Enter you campaign budget");
      return false;
    } else if (Number(audianceReach) < 0 || !audianceReach) {
      message.error("Please Enter you campaign audiance reach");
      return false;
    } else {
      return true;
    }
  };

  const handleNext = () => {
    if (selectedStep === 1) {
      if (validateForm()) {
        setSelectedStep(2);
        let data = steps?.map((step: any) => {
          if (step.key <= 2) {
            step.status = "Completed";
          } else if (step.key === 2) {
            step.status = "Active";
          }
          return step;
        });
        setSteps(data);
      }
    } else if (selectedStep === 2) {
      if (getTotalNumberOfSelectedScreens() > 0) {
        setSelectedStep(3);
        let data = steps?.map((step: any) => {
          if (step.key <= 3) {
            step.status = "Completed";
          } else if (step.key === 3) {
            step.status = "Active";
          }
          return step;
        });
        setSteps(data);
      } else {
        message.error("Please select atleast one screen to continue");
      }
    }
  };

  // const handleSelecteAllScreens = (value: any) => {};

  const handleCreateVideoFromImage = async (formData: any) => {
    try {
      const { data } = await Axios.post(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/createVideoFromImage/${userInfo?._id}`,
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${userInfo?.token}`,
          },
        }
      );
      return Promise.resolve(data);
    } catch (error) {
      return Promise.reject(error);
    }
  };
  const videoUploadHandler = async (fileUrl: any) => {
    try {
      const cid = await addFileOnWeb3(fileUrl);
      let media = `https://ipfs.io/ipfs/${cid}`;

      const { data } = await Axios.post(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/medias/create`,

        {
          userInfo,
          title: "Campaign Name",
          media,
          thumbnail:
            "https://bafybeicduvlghzcrjtuxkro7foazucvuyej25rh3humeujbzt7bmio4hsa.ipfs.w3s.link/raily.png",
        },
        {
          headers: {
            Authorization: `Bearer ${userInfo?.token}`,
          },
        }
      );
      return Promise.resolve(data);
    } catch (error) {
      return Promise.reject(error);
    }
  };

  const handleSelectFile = async (files: any) => {
    setFiles(files);
    setLoading(true);
    const medias = [];
    for (let data of files) {
      if (data.fileExtension === "video" || data.fileExtension === "image") {
        try {
          const media = await videoUploadHandler(data.file);
          medias.push({
            media,
            fileType: data.fileType,
            fileSize: data.fileSize,
            duration: data.duration,
          });
          // if (media != null && media != undefined) {
          //   medias.push(media);
          // }
        } catch (error) {
          message.error(
            "Something went wrong while uploading media, pleac check internet connection or contact to admin"
          );
        }
      }
    }
    setCreatedMedias(medias);
    let ss = medias?.map((data: any) => {
      return {
        media: data.media,
        fileType: data.fileType,
        fileSize: data.fileSize,
        duration: data.duration,
        selectedScreens: [],
      };
    });
    setSelectedScreensMediaWise(ss);
    setLoading(false);
  };

  useEffect(() => {
    dispatch(
      getScreensBasedOnAudiancesProfile({
        numberOfAudiances: 0,
        croudMobability: crowdMobilityType,
        screenHighlights: highlights
          ? highlights?.split(",")?.map((value: any) => value.trim())
          : [],
        ageRange: [startAge, endAge],
        cities,
        category,
        gender,
      })
    );
  }, [
    dispatch,
    crowdMobilityType,
    highlights,
    startAge,
    endAge,
    cities,
    category,
    gender,
  ]);

  const getRemaingBudget = () => {
    return ((budget || 0) - selectedBugdet).toFixed(2);
  };

  const getCampaignWillPlayPerDay = () => {
    let slotsPlayPerDay = 0;
    for (let data of selectedScreensMediaWise) {
      for (let screen of data.selectedScreens) {
        slotsPlayPerDay += Number(screen?.slotsPlayPerDay);
      }
    }
    return slotsPlayPerDay;
  };

  const getCampaignsPericeForOneDay = () => {
    let cost = 0;
    for (let data of selectedScreensMediaWise) {
      for (let screen of data.selectedScreens) {
        if (screen?.master != userInfo?._id) cost += Number(screen?.rentPerDay);
      }
    }
    return cost;
  };

  const getTotalAudianceReach = () => {
    let audianceReach = 0;
    for (let data of selectedScreensMediaWise) {
      for (let screen of data.selectedScreens) {
        audianceReach += Number(screen?.additionalData?.averageDailyFootfall);
      }
    }
    return audianceReach;
  };

  const getTotalCost = () => {
    return (
      Number(getNumberOfDaysBetweenTwoDates(startDateAndTime, endDateAndTime)) *
      getCampaignsPericeForOneDay()
    );
  };

  return (
    <Box bgColor="#F5F5F5" m="0" flexGrow={1} overflowY={"auto"}>
      <UploadMedia
        open={openUploadMedia}
        onCancel={() => setOpenUploadMedia(false)}
        handleSelectFile={handleSelectFile}
      />
      <CreatedCampaignStatus
        open={isOpenShowStatus}
        onCancel={() => setIsOpenShowStatus(false)}
        campaign={createdCampaign}
      />
      <Flex gap="0" height="100%">
        <Box p="0" m="0" width="100%">
          <Header location={location} />
          <Steps
            steps={steps}
            handleBack={handleBack}
            handleNext={handleNext}
            selectedStep={selectedStep}
            isDisabled={false}
          />
          <Stack border="0px" overflowY="auto" height="80%">
            {selectedStep === 1 ? (
              <Stack px="10" py="5" gap="5" pr="10">
                <SimpleGrid columns={[1, 2]} gap="5">
                  {/* Upload media */}
                  <Box bgColor="#FFFFFF" rounded="5" px="10" py="5" mr="5">
                    <Flex
                      color="#131D30"
                      justifyContent="space-between"
                      align="center"
                      onClick={() => {
                        setIsOpenUploadMedia(!isOpenUploadMedia);
                        setIsOpenCampaignDetails(!isOpenCampaignDetails);
                      }}
                    >
                      <Flex align="center" gap="5">
                        <Text fontSize="16" fontWeight="400" m="0">
                          Media Details
                        </Text>
                      </Flex>
                      {isOpenUploadMedia ? <PiMinusBold /> : <MdAdd />}
                    </Flex>
                    {isOpenUploadMedia ? (
                      <Flex align="center" pt="2" gap="2" flexDir="column">
                        <SimpleGrid columns={[2, 2, 2]} spacing="4">
                          {createdMedias?.length > 0 &&
                            createdMedias?.map((media: any, index: any) => (
                              <Stack height="200px" key={index}>
                                <Box
                                  as="video"
                                  src={media?.media}
                                  autoPlay
                                  loop
                                  muted
                                  display="inline-block"
                                  borderRadius="8px"
                                  height="200px"
                                  width="100%"
                                ></Box>
                              </Stack>
                            ))}
                        </SimpleGrid>
                        <Button
                          onClick={() => setOpenUploadMedia(true)}
                          py="3"
                          px="5"
                          width="100%"
                          isLoading={loading}
                          loadingText={"Media uploading stared, Do next task"}
                        >
                          {createdMedias?.length > 0
                            ? "Delete all and add new"
                            : "Upload Media"}
                        </Button>
                      </Flex>
                    ) : null}{" "}
                  </Box>
                  {/* Campaign details */}
                  <Box bgColor="#FFFFFF" rounded="5" px="10" py="5" mr="5">
                    <Flex
                      color="#131D30"
                      justifyContent="space-between"
                      align="center"
                      onClick={() => {
                        setIsOpenCampaignDetails(!isOpenCampaignDetails);
                      }}
                    >
                      <Text fontSize="16" fontWeight="400" m="0">
                        Basic Details
                      </Text>
                      {isOpenCampaignDetails ? <PiMinusBold /> : <MdAdd />}
                    </Flex>
                    {isOpenCampaignDetails ? (
                      <Flex pt="5" gap="10" flexDir="column">
                        <Flex flexDir="column" gap="1">
                          <Text
                            m="0"
                            fontSize="13px"
                            fontWeight="400"
                            color="#131D30"
                          >
                            Campaign Title
                          </Text>
                          <InputField
                            value={campaignName}
                            onChange={(value: any) => setCampaignName(value)}
                          />
                        </Flex>
                        <Flex flexDir="column" gap="1">
                          <Text
                            m="0"
                            fontSize="13px"
                            fontWeight="400"
                            color="#131D30"
                          >
                            Brand Name
                          </Text>
                          <InputField
                            value={brandName}
                            onChange={(value: any) => setBrandName(value)}
                          />
                        </Flex>
                        <SimpleGrid columns={[1, 2]} gap="10">
                          <Flex flexDir="column" gap="1">
                            <Text
                              m="0"
                              fontSize="13px"
                              fontWeight="400"
                              color="#131D30"
                              pb="1.5"
                            >
                              Start Date & Time
                            </Text>
                            <DateAndTimeInputField
                              value={startDateAndTime}
                              onChange={(value: any) => {
                                setStartDateAndTime(value);
                              }}
                            />
                          </Flex>
                          <Flex flexDir="column" gap="1">
                            <Text
                              m="0"
                              fontSize="13px"
                              fontWeight="400"
                              color="#131D30"
                              pb="1.5"
                            >
                              End Date & Time
                            </Text>
                            <DateAndTimeInputField
                              value={endDateAndTime}
                              onChange={(value: any) => {
                                handleEndDate(value);
                              }}
                            />
                          </Flex>
                        </SimpleGrid>
                        <SimpleGrid columns={[1, 2]} gap="10">
                          <Flex flexDir="column" gap="1">
                            <Text
                              m="0"
                              fontSize="13px"
                              fontWeight="400"
                              color="#131D30"
                            >
                              Objective
                            </Text>
                            <Select
                              size="large"
                              defaultValue=""
                              bordered={false}
                              style={{
                                width: "100%",
                                height: "45px",
                                border: "1px solid #00000040",
                                borderRadius: "5px",
                              }}
                              options={campaignGoalOptions}
                              onChange={(value: any) => setCampaignGoal(value)}
                            />
                          </Flex>
                          <SimpleGrid columns={[1, 2]} gap="10">
                            <Flex flexDir="column" gap="1">
                              <Text
                                m="0"
                                fontSize="13px"
                                fontWeight="400"
                                color="#131D30"
                              >
                                Budget
                              </Text>
                              <InputField
                                type="number"
                                value={budget}
                                onChange={(value: any) =>
                                  handleAddBudget(value)
                                }
                                size="md"
                              />
                            </Flex>
                            <Flex flexDir="column" gap="1">
                              <Text
                                m="0"
                                fontSize="13px"
                                fontWeight="400"
                                color="#131D30"
                              >
                                Expected Reach
                              </Text>
                              <InputField
                                value={audianceReach}
                                type="number"
                                onChange={(value: any) =>
                                  setAaudianceReach(value)
                                }
                                size="md"
                              />
                            </Flex>
                          </SimpleGrid>
                        </SimpleGrid>
                      </Flex>
                    ) : null}
                  </Box>
                </SimpleGrid>

                {/* Audiance profile */}
                <Box bgColor="#FFFFFF" rounded="5" px="10" py="5" mr="5">
                  <Flex
                    color="#131D30"
                    justifyContent="space-between"
                    align="center"
                    onClick={() =>
                      setIsOpenAudianceProfile(!isOpenAudianceProfile)
                    }
                  >
                    <Text fontSize="16" fontWeight="400" m="0">
                      Demographic Details
                    </Text>
                    {isOpenAudianceProfile ? (
                      <PiMinusBold
                        onClick={() =>
                          setIsOpenAudianceProfile(!isOpenAudianceProfile)
                        }
                      />
                    ) : (
                      <MdAdd
                        onClick={() =>
                          setIsOpenAudianceProfile(!isOpenAudianceProfile)
                        }
                      />
                    )}
                  </Flex>
                  {isOpenAudianceProfile ? (
                    <Flex pt="10" gap="10" flexDir="column">
                      <SimpleGrid columns={[1, 2, 3]} gap="10">
                        <Flex flexDir="column" gap="1">
                          <Text
                            m="0"
                            fontSize="12"
                            fontWeight="400"
                            color="#131D30"
                          >
                            Category
                          </Text>
                          <Select
                            size="large"
                            defaultValue=""
                            bordered={false}
                            style={{
                              width: "100%",
                              height: "45px",
                              border: "1px solid #00000040",
                              borderRadius: "5px",
                            }}
                            options={categoryOption}
                            value={category}
                            onChange={(value: any) => setCategory(value)}
                          />
                        </Flex>
                        <Flex flexDir="column" gap="1">
                          <Text
                            m="0"
                            fontSize="12"
                            fontWeight="400"
                            color="#131D30"
                          >
                            Gender
                          </Text>
                          <Select
                            size="large"
                            defaultValue=""
                            bordered={false}
                            style={{
                              width: "100%",
                              height: "45px",
                              border: "1px solid #00000040",
                              borderRadius: "5px",
                            }}
                            options={genderOption}
                            value={gender}
                            onChange={(value: any) => setGender(value)}
                          />
                        </Flex>
                        <Flex flexDir="column" gap="1">
                          <Text
                            m="0"
                            fontSize="12"
                            fontWeight="400"
                            color="#131D30"
                          >
                            Age Range (Start - End) years
                          </Text>
                          <SimpleGrid columns={[1, 2]} gap="5">
                            <InputField
                              type="number"
                              // width="100px"
                              value={startAge}
                              onChange={setStartAge}
                            />
                            <InputField
                              type="number"
                              // width="100px"
                              value={endAge}
                              onChange={setEndAge}
                            />
                          </SimpleGrid>
                        </Flex>
                      </SimpleGrid>
                      <SimpleGrid columns={[1, 2, 3]} gap="10">
                        <Flex flexDir="column" gap="1">
                          <Tooltip
                            placement="topLeft"
                            title="Enter cities or state with , to enter multiple cities"
                            arrow={true}
                          >
                            <Text
                              m="0"
                              fontSize="12"
                              fontWeight="400"
                              color="#131D30"
                            >
                              Target Location (Cities/State)
                            </Text>
                          </Tooltip>

                          <InputField
                            placeholder="Ex. Gurgaon, Noida, NCR"
                            value={cities}
                            onChange={setCities}
                          />
                          <Flex gap="3">
                            {cities &&
                              cities
                                ?.split(",")
                                ?.map((city: any, index: any) => (
                                  <Button
                                    key={index}
                                    variant="outline"
                                    borderRadius="29px"
                                    py="2"
                                    color="#515151"
                                    fontWeight="400"
                                    fontSize="14px"
                                  >
                                    {city}
                                  </Button>
                                ))}
                          </Flex>
                        </Flex>
                        <Flex flexDir="column" gap="1">
                          <Text
                            m="0"
                            fontSize="12"
                            fontWeight="400"
                            color="#131D30"
                          >
                            Traffic
                          </Text>
                          <CroudMobabilityFunction
                            crowdMobilityType={crowdMobilityType}
                            addOrRemobeCrowdMobilityType={
                              addOrRemobeCrowdMobilityType
                            }
                          />
                        </Flex>
                        <Flex flexDir="column" gap="1">
                          <Text
                            m="0"
                            fontSize="12"
                            fontWeight="400"
                            color="#131D30"
                          >
                            Highlights
                          </Text>
                          <InputField
                            placeholder="Ex. Mall, park"
                            value={highlights}
                            onChange={setHighlights}
                          />
                          <Flex gap="3">
                            {highlights &&
                              highlights
                                ?.split(",")
                                ?.map((highlight: any, index: any) => (
                                  <Button
                                    key={index}
                                    variant="outline"
                                    borderRadius="29px"
                                    py="2"
                                    color="#515151"
                                    fontWeight="400"
                                    fontSize="14px"
                                  >
                                    {highlight}
                                  </Button>
                                ))}
                          </Flex>
                        </Flex>
                      </SimpleGrid>
                    </Flex>
                  ) : null}
                </Box>
              </Stack>
            ) : // screen selections part
            selectedStep === 2 ? (
              <Flex px="10" py="5" gap="5" flexDir="column">
                <Box bgColor="#FFFFFF" p="10">
                  <SimpleGrid columns={[5, 5, 5]} spacing="4">
                    {files?.length > 0 &&
                      files?.map((data: any, index: any) => (
                        <Flex
                          key={index}
                          p="1"
                          onClick={() => setSelectedMediaIndex(index)}
                          border={selectedMediaIndex === index ? "2px" : "0px"}
                          borderColor={
                            selectedMediaIndex === index ? "#50C4ED" : ""
                          }
                        >
                          <ShowVideos data={data} />

                          <Center
                            borderRadius="100%"
                            bgColor="red"
                            width="25px"
                            height="25px"
                            justifyContent="center"
                            alignContent="center"
                            mt="-3"
                            ml="-3"
                          >
                            <Text
                              color="#FFFFFF"
                              m="0"
                              fontSize="12px"
                              fontWeight="600"
                            >
                              {getAllSelectedScreenMediaIndex(index)}
                            </Text>
                          </Center>
                        </Flex>
                      ))}
                  </SimpleGrid>

                  <Flex justifyContent="space-between">
                    <Text
                      color="#131D30"
                      fontSize="24px"
                      fontWeight="600"
                      m="0"
                    >
                      Select Screens
                    </Text>
                    <Flex gap="3">
                      <NotificationButton
                        icon={<GrShop size="20px" />}
                        value={getTotalNumberOfSelectedScreens() || 0}
                      />
                      <InputGroup>
                        <InputField value={cities} onChange={setCities} />
                        <InputRightElement width="" pl="4" pt="3" mr="3">
                          <IoMdSearch color="#6C6C6C" />
                        </InputRightElement>
                      </InputGroup>
                      <Button borderRadius="12px" px="4" py="2">
                        Filter
                      </Button>
                    </Flex>
                  </Flex>
                  <Flex>
                    <Box width="70%">
                      <Flex gap="5">
                        {tabOptions?.map((option: any, index: any) => (
                          <Flex
                            key={index}
                            gap="4"
                            p="1"
                            style={{ cursor: "pointer" }}
                            color={
                              selectedTab === option.key ? "#000000" : "#7C7C7C"
                            }
                            align="center"
                            borderBottom={
                              selectedTab === option.key ? "4px" : "0px"
                            }
                            borderColor="#000000"
                            onClick={() => setSelectedTab(option.key)}
                          >
                            <Text m="0" fontSize="16px">
                              {`${option.label} `}
                              {index === 0
                                ? screens?.length
                                : filteredScreens?.length}
                            </Text>
                          </Flex>
                        ))}
                      </Flex>
                      {selectedTab === 1 ? (
                        <Box color="#FF4949">
                          {userInfo?.isMaster === true &&
                          screens?.length > 0 ? (
                            <Stack>
                              <Stack pt="4">
                                <Checkbox
                                  checked={selectAllMyScreen}
                                  onChange={(e) => {
                                    setSelectAllMyScreen(e.target.checked);
                                    handelSelectAllMyScreen(
                                      e.target.checked,
                                      screens
                                    );
                                  }}
                                >
                                  Select All
                                </Checkbox>
                              </Stack>
                              <Flex h="100%" flexDir="column" gap="5" pt="5">
                                {screens?.map((screen: any, index: any) => (
                                  <Flex key={index}>
                                    <Checkbox
                                      checked={selectedScreensMediaWise[
                                        selectedMediaIndex
                                      ]?.selectedScreens?.find(
                                        (ss: any) => ss?._id == screen?._id
                                      )}
                                      onChange={(e) =>
                                        handleSelectScreens(
                                          screen,
                                          e.target.checked
                                        )
                                      }
                                    >
                                      <SingleScreenForCampaignCreate
                                        screen={screen}
                                      />
                                    </Checkbox>
                                  </Flex>
                                ))}
                              </Flex>
                            </Stack>
                          ) : (
                            <Text color="#FF4949" pt="5">
                              No Screens Found
                            </Text>
                          )}
                        </Box>
                      ) : selectedTab === 2 && filteredScreens?.length > 0 ? (
                        <Box>
                          <Stack pt="4">
                            <Checkbox
                              checked={selectAllSearchScreen}
                              onChange={(e) => {
                                setselectAllSearchScreen(e.target.checked);
                                handelSelectAllMyScreen(
                                  e.target.checked,
                                  filteredScreens
                                );
                              }}
                            >
                              Select All
                            </Checkbox>
                          </Stack>
                          <Stack
                            h="100%"
                            flexDir="column"
                            gap="5"
                            pt="5"
                            overflowY={"auto"}
                          >
                            {filteredScreens?.map((screen: any, index: any) => (
                              <Flex key={index}>
                                <Checkbox
                                  checked={selectedScreensMediaWise[
                                    selectedMediaIndex
                                  ]?.selectedScreens?.find(
                                    (ss: any) => ss?._id == screen?._id
                                  )}
                                  onChange={(e) =>
                                    handleSelectScreens(
                                      screen,
                                      e.target.checked
                                    )
                                  }
                                >
                                  <SingleScreenForCampaignCreate
                                    screen={screen}
                                  />
                                </Checkbox>
                              </Flex>
                            ))}
                          </Stack>
                        </Box>
                      ) : selectedTab === 3 ? (
                        <Box height="500">
                          {filteredScreens?.length > 0 && (
                            <ShowScreensLocation
                              data={filteredScreens}
                              setSelectedScreensMediaWise={(value: any) =>
                                setSelectedScreensMediaWise(value)
                              }
                            />
                          )}
                        </Box>
                      ) : (
                        <Text color="#FF4949">
                          No Screens Found By your seelcted filter, You can
                          Change your filter and try again
                        </Text>
                      )}
                    </Box>
                    <Box width="40%" p="4" pt="100px">
                      <Text color="#131D30" fontSize="14px" fontWeight="700">
                        Current Balance : Rs. {wallet?.balance}
                      </Text>
                      <Text color="#131D30" fontSize="13px" fontWeight="400">
                        Include All Taxes
                      </Text>
                      {/* Ewallet?.balance */}

                      <Text color="#131D30" fontSize="16px" fontWeight="400">
                        {`Expected Audiance reach : ${audianceReach || 0}`}
                      </Text>
                      <Flex align="center">
                        <Text color="#131D30" fontSize="16px" fontWeight="400">
                          Total Audiance reach :
                        </Text>
                        <Text color="#131D30" fontSize="16px" fontWeight="700">
                          {getTotalAudianceReach()}
                        </Text>
                      </Flex>
                      <Flex gap="2" align="center">
                        <Text
                          color="#131D30"
                          fontSize="16px"
                          fontWeight="400"
                          m="0"
                        >
                          Campaign Budget :
                        </Text>

                        {isEditBudget ? (
                          <Flex align="center" gap="2">
                            <InputField
                              type="number"
                              value={budget}
                              onChange={(value: any) => handleAddBudget(value)}
                              size="sm"
                            />
                            <FaSave
                              color="#131D30"
                              size="35px"
                              onClick={() => setIsEditBudget(false)}
                            />
                          </Flex>
                        ) : (
                          <Flex align="center" justifyContent="center" gap="2">
                            <Text
                              color="#131D30"
                              fontSize="16px"
                              fontWeight="700"
                              m="0"
                            >
                              Rs. {budget || 0}
                            </Text>
                            <CiEdit
                              size="16px"
                              color="#131D30"
                              onClick={() => setIsEditBudget(true)}
                            />
                          </Flex>
                        )}
                      </Flex>

                      <Text
                        color="#131D30"
                        fontSize="16px"
                        fontWeight="400"
                        pt="4"
                      >
                        {`Total selected screens : ${selectedScreensMediaWise?.length}`}
                      </Text>
                      <Text color="#131D30" fontSize="16px" fontWeight="400">
                        {`Campaigns will play : ${getCampaignWillPlayPerDay()} times per day`}
                      </Text>
                      <Text color="#131D30" fontSize="16px" fontWeight="400">
                        {`Cost of per day : Rs ${getCampaignsPericeForOneDay()} `}
                      </Text>
                      <Text color="#131D30" fontSize="16px" fontWeight="400">
                        {`Total cost for ${getNumberOfDaysBetweenTwoDates(
                          startDateAndTime,
                          endDateAndTime
                        )} days : Rs ${getTotalCost()} `}
                      </Text>
                      <Flex gap="2">
                        <Text color="#131D30" fontSize="16px" fontWeight="400">
                          Remaining budget :
                        </Text>
                        <Text
                          color={
                            Number(getRemaingBudget()) > 0
                              ? "#4BC600"
                              : "#FF0000"
                          }
                          fontSize="16px"
                          fontWeight="400"
                        >
                          {`Rs. ${getRemaingBudget()}`}
                        </Text>
                      </Flex>
                      {Number(getRemaingBudget()) < 0 ? (
                        <Text
                          color={
                            Number(getRemaingBudget()) > 0
                              ? "#4BC600"
                              : "#FF0000"
                          }
                          fontSize="14px"
                          fontWeight="700"
                        >
                          Please delete some screen from selected screen to
                          maintain your budget or increase your budget
                        </Text>
                      ) : null}
                    </Box>
                  </Flex>
                </Box>
              </Flex>
            ) : // Order summary
            selectedStep === 3 ? (
              <Flex gap="2" p="5">
                <Flex bgColor="#FFFFFF" p="10" flexDir="column" width="60%">
                  <Text color="#131D30" fontSize="24px" fontWeight="400">
                    Selected Screens media wise
                  </Text>
                  <Text color="#131D30" fontSize="16px" fontWeight="400">
                    Campaign Duration :{" "}
                    {`${convertIntoDateAndTime(
                      startDateAndTime
                    )} - ${convertIntoDateAndTime(endDateAndTime)}`}
                  </Text>
                  <SimpleGrid columns={[5, 5, 5]} spacing="4">
                    {files?.length > 0 &&
                      files?.map((data: any, index: any) => (
                        <Flex
                          key={index}
                          p="1"
                          onClick={() => setSelectedMediaIndex(index)}
                          border={selectedMediaIndex === index ? "2px" : "0px"}
                          borderColor={
                            selectedMediaIndex === index ? "#50C4ED" : ""
                          }
                        >
                          <ShowVideos data={data} />

                          <Center
                            borderRadius="100%"
                            bgColor="red"
                            width="25px"
                            height="25px"
                            justifyContent="center"
                            alignContent="center"
                            mt="-3"
                            ml="-3"
                          >
                            <Text
                              color="#FFFFFF"
                              m="0"
                              fontSize="12px"
                              fontWeight="600"
                            >
                              {getAllSelectedScreenMediaIndex(index)}
                            </Text>
                          </Center>
                        </Flex>
                      ))}
                  </SimpleGrid>
                  <ScrollBox height="600px" border="0px">
                    <Flex flexDir="column" gap="5" pt="5">
                      {selectedScreensMediaWise[
                        selectedMediaIndex || 0
                      ]?.selectedScreens?.map((screen: any, index: any) => (
                        <Flex justifyContent="space-between" key={index}>
                          <SingleScreenForCampaignCreate screen={screen} />
                          <Tooltip
                            placement="bottomLeft"
                            title="Remove"
                            arrow={true}
                          >
                            <Popconfirm
                              title="Remove"
                              description="Do you really want to remove this screens ?"
                              onConfirm={() =>
                                handleSelectScreens(screen, false)
                              }
                              okText="Yes"
                              cancelText="No"
                            >
                              <RiDeleteBinLine color="#FF4949" />
                            </Popconfirm>
                          </Tooltip>
                        </Flex>
                      ))}
                    </Flex>
                  </ScrollBox>
                </Flex>
                <Flex flexDir="column" gap="2" width="40%">
                  <Flex bgColor="#FFFFFF" p="10" flexDir="column">
                    <Text color="#131D30" fontSize="24px" fontWeight="400">
                      Billing
                    </Text>
                    <SimpleGrid columns={[2, 2, 2]} pt="100px">
                      <Stack>
                        <Text m="0">Total</Text>
                        <Text m="0">Platform Fee 10%</Text>
                        <Text m="0">GST 18%</Text>
                        <Text m="0">Discount 20%</Text>
                        <Text m="0">Grand Total</Text>
                      </Stack>
                      <Stack>
                        <Text m="0">Rs. {getTotalCost()?.toFixed(2)}</Text>
                        <Text m="0">Rs. {getValueOfpersentage(10)} </Text>
                        <Text m="0">Rs. {getValueOfpersentage(18)}</Text>
                        <Text m="0">Rs. -{getValueOfpersentage(20)}</Text>
                        <Text m="0">Rs. {grandTotal()}</Text>
                      </Stack>
                    </SimpleGrid>
                    <Flex align="center" pt="10">
                      <Button
                        width="312px"
                        py="3"
                        onClick={handleToPay}
                        isLoading={loading || loadingSlotBooking}
                        loadingText="Saving media, Please wait for few second.."
                      >
                        Proceed To Pay
                      </Button>
                    </Flex>
                  </Flex>
                  <Flex bgColor="#FFFFFF" p="10" flexDir="column" width="100%">
                    <Text m="0">Notes:</Text>
                    <Text color="#172fe6" fontSize="16px">
                      {` You have selected ${
                        selectedScreensMediaWise?.length
                      } screens for running your ad campaigns
                      ${getCampaignWillPlayPerDay()} times per day at a cost of Rs ${getCampaignsPericeForOneDay()} per day for ${getNumberOfDaysBetweenTwoDates(
                        startDateAndTime,
                        endDateAndTime
                      )} days,
                      total amount Rs ${grandTotal()} will be kept on hold from your wallet
                      for this transaction`}
                    </Text>
                  </Flex>
                </Flex>
              </Flex>
            ) : null}
          </Stack>
        </Box>
      </Flex>
    </Box>
  );
}
