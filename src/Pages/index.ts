export { MyMap } from "./MyMap";
export {
  FullScreenPlaylist1,
  TestPlayer,
  PlayListByScreen,
} from "./FullScreenPlaylist";
//auth
export { Signin } from "./auth/Signin";
export { SignUp } from "./auth/SignUp";
export { CreatePassword } from "./auth/CreatePassword";
export { SetupAccount } from "./auth/SetupAccount";
export { JoinAs } from "./auth/JoinAs";
export { ForgetPassword } from "./auth/ForgetPassword";
export { RootPage } from "./RootPage";

// screen owner dashborad
export { Screens } from "./Screens";
export { Dashboard } from "./Dashboard";
export { EditScreen } from "./EditScreen";
export { CreateNewScreen } from "./CreateNewScreen";
export { ScreenDetailPage } from "./ScreenDetailPage";

export { MyCampaigns } from "./MyCampaigns";
export { CreateCampaign } from "./CreateCampaign";
export { NewCampaignCreate } from "./NewCampaignCreate";
export { CampaignDetailsPage } from "./CampaignDetails";
export { Setting } from "./Setting";
export { MyWallet } from "./MyWallet";
export { Help } from "./Help";
export { Notification } from "./Notificaion";

// test
export { Message } from "./FullScreenPlaylist/Message";
