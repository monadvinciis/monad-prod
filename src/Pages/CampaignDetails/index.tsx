import { Box, Flex, Text, Stack } from "@chakra-ui/react";
import { useDispatch } from "react-redux";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { userCampaignsList } from "../../Actions/userActions";
import { useSelector } from "react-redux";
import { Select, Skeleton, message } from "antd";
import { CgScreen } from "react-icons/cg";
import { TbAnalyze } from "react-icons/tb";
import { IoMdInformationCircleOutline } from "react-icons/io";
import { OverView } from "./OverView";
import { Analytics } from "./Analytics";
import { LuChevronLeft } from "react-icons/lu";
import { changeCampaignStatus } from "../../Actions/campaignAction";
import { CAMPAIGN_DELETE_RESET } from "../../Constants/campaignConstants";
import { isEqualTwoMongodbObjectId } from "../../utils/utilityFunctions";

export function CampaignDetailsPage(props: any) {
  const dispatch = useDispatch<any>();
  const navigate = useNavigate();

  const id = window.location.pathname.split("/")[3];
  const [campaignId, setCampaignId] = useState<any>(id);
  const [selectedTab, setSelectedTab] = useState<any>(1);
  const [campaignOptions, setCampaignOptions] = useState<any>([]);

  const [showName, setShowName] = useState<any>(true);

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  const userCampaign = useSelector((state: any) => state.userCampaign);
  const {
    loading: loadingCampaigns,
    error: errorCampaigns,
    campaign: campaignData,
    campaigns,
  } = userCampaign;

  const getRequeirdData = () => {
    const data = campaignData?.find((data: any) =>
      isEqualTwoMongodbObjectId(data?.campaign?._id, campaignId)
    );
    return data;
  };

  useEffect(() => {
    if (errorCampaigns) {
      message.error("Something went wrong!");
    }
    if (campaigns) {
      const campaignOptions = campaigns?.map((camp: any) => {
        return {
          value: camp?._id,
          label: camp?.campaignName?.toUpperCase(),
          fontSize: 30,
        };
      });
      setCampaignOptions(campaignOptions);
    }
  }, [errorCampaigns, campaigns]);

  const changeCampaignsStatus = useSelector(
    (state: any) => state.changeCampaignsStatus
  );
  const { error: errorCampaignDelete, success: changeCampaignStatusStatus } =
    changeCampaignsStatus;

  const handleChangeCampaignStatus = (campaignId: any, status: any) => {
    dispatch(changeCampaignStatus(campaignId, status));
  };

  useEffect(() => {
    if (errorCampaignDelete) {
      message.error(errorCampaignDelete);
      dispatch({
        type: CAMPAIGN_DELETE_RESET,
      });
    } else if (changeCampaignStatusStatus) {
      message.success("status changed successfully");
      dispatch({
        type: CAMPAIGN_DELETE_RESET,
      });
      dispatch(userCampaignsList(userInfo, "ALL"));
      navigate("/my/campaigns");
    }
  }, [
    changeCampaignStatusStatus,
    errorCampaignDelete,
    dispatch,
    userInfo,
    navigate,
  ]);

  useEffect(() => {
    if (!userInfo) {
      navigate("/signin", { state: { path: `/campaigns/details/${id}` } });
    }
    dispatch(userCampaignsList(userInfo, "ALL"));
  }, [userInfo, navigate, dispatch]);

  const tabOptions = [
    {
      key: 1,
      icon: <CgScreen />,
      label: "Details",
    },

    {
      key: 2,
      icon: <TbAnalyze />,
      label: "Analytics",
    },
    {
      icon: <IoMdInformationCircleOutline />,
      key: 3,
      label: "Edit",
    },
  ];

  return (
    <Box w="100%" h="100%" bgColor="#F5F5F5" m="0">
      {/* <MainMenu selectedKey={3} /> */}
      <Flex
        p="5"
        // pl="10"
        align="center"
        borderTop="1px"
        borderLeft="1px"
        borderColor="#EBEBEB"
        // pr="20"
        bgColor="#FFFFFF"
        justifyContent="space-between"
      >
        <Flex align="center" gap="0">
          <LuChevronLeft
            size="20px"
            // onClick={() => navigate(location?.state?.path || "/my/campaigns")
            onClick={() => navigate(-1)}
          />
          <Flex direction={"column"} pl="7">
            <Select
              // size="large"
              showSearch
              bordered={false}
              defaultValue={showName ? false : true}
              onChange={(value) => {
                setCampaignId(value);
                setShowName(true);
                navigate(`/campaigns/details/${value}`);
              }}
              style={{ width: 300 }}
              options={campaignOptions}
              value={showName ? false : campaignId}
              // suffixIcon={showName ? true : true}
              onClick={() => {
                setShowName(false);
              }}
              onFocus={() => setShowName(false)}
              onMouseEnter={() => setShowName(false)}
              onMouseLeave={() => setShowName(true)}
              onMouseDown={() => setShowName(false)}
              onInputKeyDown={() => setShowName(false)}
              optionFilterProp="children"
              filterOption={(input: any, option: any) =>
                (option?.label ?? "").includes(input)
              }
              filterSort={(optionA: any, optionB: any) =>
                (optionA?.label ?? "")
                  .toLowerCase()
                  .includes((optionB?.label ?? "").toLowerCase())
              }
            />
            {showName && (
              <Flex>
                <Text fontSize="20" fontWeight="600" mt="-8" mb="0">
                  {getRequeirdData()?.campaign?.campaignName.toUpperCase()}
                </Text>
              </Flex>
            )}
          </Flex>
        </Flex>
      </Flex>
      <Flex
        pl="10"
        align="center"
        border="1px"
        borderColor="#EBEBEB"
        pr="10"
        bgColor="#FFFFFF"
      >
        {tabOptions?.map((option: any, index: any) => {
          return (
            <Flex
              width="10%"
              key={index}
              gap="2"
              p="4"
              style={{ cursor: "pointer" }}
              color={selectedTab === option.key ? "#000000" : "#7C7C7C"}
              align="center"
              borderBottom={selectedTab === option.key ? "4px" : "0px"}
              borderColor="#000000"
              onClick={() => setSelectedTab(option.key)}
            >
              {option?.icon}
              <Text m="0">{option?.label}</Text>
            </Flex>
          );
        })}
      </Flex>

      {loadingCampaigns ? (
        <Flex p="20">
          <Skeleton active={true} avatar paragraph={{ rows: 4 }} />
        </Flex>
      ) : (
        <Flex mt="2" width="100%" h="83%">
          <Stack width="100%" height="100%" pb="2" pl="5">
            {selectedTab === 1 ? (
              <OverView
                campaign={getRequeirdData()?.campaign}
                screens={getRequeirdData()?.listOfScreens}
                campaigns={getRequeirdData()?.listOfCampaigns}
                handleChangeCampaignStatus={handleChangeCampaignStatus}
              />
            ) : selectedTab === 2 ? (
              <Analytics campaign={campaignData} campaignId={campaignId} />
            ) : null}
          </Stack>
        </Flex>
      )}
    </Box>
  );
}
