import { Stack, Box, Button, Flex, SimpleGrid, Text } from "@chakra-ui/react";
import { Excelexport, ScreenList } from "../../components/newCommans";
import { MediaContainer } from "../../components/newCommans";
import {
  convertIntoDateAndTime,
  getDateAndTimeAfterAdded5Hr,
} from "../../utils/dateAndTimeUtils";
import { Popconfirm, Select, Skeleton, Table } from "antd";
import type { ColumnsType } from "antd/es/table";
import { useDispatch } from "react-redux";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { getScreenLogs } from "../../Actions/screenActions";
import { RiDeleteBin6Line } from "react-icons/ri";
import {
  CAMPAIGN_STATUS_COMPLETD,
  CAMPAIGN_STATUS_DELETED,
} from "../../Constants/campaignConstants";
import { getVideoDurationFronVideoURL } from "../../utils/utilityFunctions";

const wscols = [
  {
    wch: 5,
  },
  {
    wch: 25,
  },
  {
    wch: 25,
  },
  {
    wch: 25,
  },
  {
    wch: 12,
  },
  {
    wch: 25,
  },
  {
    wch: 20,
  },
  {
    wch: 40,
  },
];
export function OverView(props: any) {
  const dispatch = useDispatch<any>();
  const [duration, setDuration] = useState<any>("");
  const { campaign, screens, campaigns, handleChangeCampaignStatus } = props;
  const [screenId, setScreenId] = useState<any>();
  const [logsDataSrc, setLogsDataSrc] = useState<any>([]);

  const screenLogsGet = useSelector((state: any) => state.screenLogsGet);
  const { loading: loadingScreenLogs, data: screenLogs } = screenLogsGet;

  const screenOptions = screens?.map((screen: any) => {
    return {
      value: screen?._id,
      label: screen?.name?.toUpperCase(),
      fontSize: 12,
    };
  });

  const isValidForDelete = () => {
    const data = campaigns?.filter(
      (campaign: any) =>
        campaign?.status !== CAMPAIGN_STATUS_COMPLETD &&
        campaign?.status !== CAMPAIGN_STATUS_DELETED
    );
    if (data?.length > 0) return true;
    else return false;
  };

  const confirm = (e: React.MouseEvent<HTMLElement> | undefined) => {
    const data = campaigns
      ?.filter(
        (campaign: any) =>
          campaign?.status !== CAMPAIGN_STATUS_DELETED &&
          campaign?.status !== CAMPAIGN_STATUS_COMPLETD
      )
      ?.map((singleCampaign: any) => singleCampaign?._id);
    // data == array of campaigns id
    if (data?.length > 0) {
      props?.handleChangeCampaignStatus(data, CAMPAIGN_STATUS_DELETED);
    }
  };

  const getScreenFromScreens = () => {
    return screens?.find((data: any) => data._id == screenId);
  };

  useEffect(() => {
    if (screens && !screenId) {
      setScreenId(screens[0]?._id);
    }
    if (screenId) {
      dispatch(getScreenLogs(screenId));
    }
  }, [dispatch, screenId, screens]);

  function getCampaignLog() {
    const screen = getScreenFromScreens();
    const data = logsDataSrc?.map((log: any, index: any) => {
      return {
        "SN.": index + 1,
        "Start Time": getDateAndTimeAfterAdded5Hr(
          log?.data?.playTime?.split(".")[0],
          duration
        ),
        "End Time": getDateAndTimeAfterAdded5Hr(
          log?.data?.playTime?.split(".")[0]
        ),
        "Campaign Name": campaign?.campaignName,
        "Content Duration": `${duration} Sec`,
        "Screen Name": screen?.name,
        "InterNet Connection": log?.data?.deviceStatus || "Online",
        Location: `${screen?.screenAddress}, ${screen?.districtCity}, ${screen?.stateUT}`,
      };
    });

    return data;
  }

  useEffect(() => {
    getVideoDurationFronVideoURL(campaign?.video, (duration: any) => {
      setDuration(duration);
    });
    if (screenLogs) {
      setLogsDataSrc(
        screenLogs?.allLogs
          ?.filter((l: any) => l.playVideo === campaign?.cid)
          ?.map((log: any) => ({ key: log._id, data: log }))
          ?.sort(
            (a: any, b: any) =>
              new Date(b.data.playTime).getTime() -
              new Date(a.data.playedTime).getTime()
          )
      );
    }
  }, [screenLogs, campaign]);

  const columnsLogs: ColumnsType<any> = [
    {
      title: (
        <Text
          color="#000000"
          fontSize={{ base: "14px", lg: "sm" }}
          fontWeight="400"
          m="0"
        >
          No.
        </Text>
      ),
      dataIndex: "logo",
      key: "logo",
      render: (_, record, index) => <Text>{index + 1}</Text>,
    },
    {
      title: (
        <Text
          color="#000000"
          fontSize={{ base: "14px", lg: "sm" }}
          fontWeight="400"
          m="0"
        >
          Time of playback
        </Text>
      ),
      dataIndex: "logo",
      key: "logo",
      render: (_, record, index) => (
        <Text>
          {convertIntoDateAndTime(
            new Date(
              new Date(record?.data?.playTime?.split(".")[0]).getTime() +
                (5 * 60 + 30) * 60000
            )
          )}
        </Text>
      ),
    },
  ];
  return (
    <Stack width="100%" overflowY={"auto"}>
      <Flex gap="2" h="100%" p="2">
        <Stack width="100%" h="100%" px="4">
          <Flex gap="8" p="2" bgColor="#FFFFFF">
            <MediaContainer
              width="342px"
              height="233px"
              cid={campaign?.cid}
              autoPlay={false}
              rounded="xl"
            />
            <Box>
              <Flex gap="5">
                <Text fontSize="lg" fontWeight="700" color="#131D30" m="0">
                  {campaign?.campaignName}
                </Text>
                <Button
                  variant="none"
                  bgColor="#FFFCDD"
                  py="1"
                  borderRadius="25px"
                  color="#FF6B00"
                >
                  {" "}
                  {campaign?.ratting || 4.0}
                </Button>
              </Flex>
              <SimpleGrid columns={[2, 2, 2]} spacing="4" fontSize="14px">
                <Box>
                  <Text color="#808080" pt="2" m="0">
                    Brand Name
                  </Text>
                  <Text color="#808080" pt="2" m="0">
                    Start Date
                  </Text>
                  <Text color="#808080" pt="2" m="0">
                    End Date
                  </Text>
                  <Text color="#808080" pt="2" m="0">
                    Budget
                  </Text>
                  <Text color="#808080" pt="2" m="0">
                    Video Duration
                  </Text>
                  <Text color="#808080" pt="2" m="0">
                    File Type
                  </Text>
                  <Text color="#808080" pt="2" m="0">
                    File Size
                  </Text>
                </Box>
                <Box>
                  {" "}
                  <Text color="#808080" pt="2" m="0">
                    {campaign?.brandName}
                  </Text>
                  <Text color="#808080" pt="2" m="0">
                    {convertIntoDateAndTime(campaign?.startDate) || 10}
                  </Text>
                  <Text color="#808080" pt="2" m="0">
                    {convertIntoDateAndTime(campaign?.endDate) || 10}
                  </Text>
                  <Text color="#808080" pt="2" m="0">
                    Rs. {campaign?.totalAmount || 0}
                  </Text>
                  <Text color="#808080" pt="2" m="0">
                    {Number(campaign?.duration)?.toFixed(2)} Sec.
                  </Text>
                  <Text color="#808080" pt="2" m="0">
                    {campaign?.fileType}
                  </Text>
                  <Text color="#808080" pt="2" m="0">
                    {campaign?.fileSize} bytes
                  </Text>
                </Box>
              </SimpleGrid>
            </Box>
            {isValidForDelete() && (
              <Stack pt="2" align="right">
                <Popconfirm
                  title="Delete campaigns"
                  description="Do you really want to delete All active or pause campaigns?"
                  onConfirm={confirm}
                  okText="Yes"
                  cancelText="No"
                >
                  <RiDeleteBin6Line color="#D71111" size="16px" />
                </Popconfirm>
              </Stack>
            )}
          </Flex>

          <Stack width="100%" h="100%" pr="" py="2">
            <ScreenList
              screens={screens}
              campaigns={campaigns}
              handleChangeCampaignStatus={handleChangeCampaignStatus}
            />
          </Stack>
        </Stack>
        <Stack pr="10">
          <Box
            p="4"
            bgColor="#FFFFFF"
            width={{ base: "100%", lg: "487px" }}
            justifyContent="flex-start"
          >
            {logsDataSrc?.length > 0 ? (
              <Excelexport
                excelData={getCampaignLog()}
                fileName={`${campaign?.campaignName}`}
                wscols={wscols}
              />
            ) : null}
            <Flex justify="space-between">
              <Text fontSize="16" fontWeight="600" onClick={getCampaignLog}>
                Playback Logs
              </Text>
              <Flex justify="flex-end">
                <Select
                  size="small"
                  showSearch
                  bordered={false}
                  defaultValue={
                    screenOptions ? screenOptions[0]?.label : "Select"
                  }
                  onChange={(value) => {
                    setScreenId(value);
                  }}
                  style={{
                    // width: 300,
                    // fontSize: "20px",
                    margin: "0",
                    // fontWeight: "600px",
                  }}
                  options={screenOptions}
                  value={screenId}
                  // suffixIcon={showName ? null : true}
                  // onClick={() => {
                  //   setShowName(!showName);
                  // }}
                  // onFocus={() => setShowName(false)}
                  // onInputKeyDown={() => setShowName(false)}
                  optionFilterProp="children"
                  filterOption={(input: any, option: any) =>
                    (option?.label ?? "").includes(input)
                  }
                  filterSort={(optionA: any, optionB: any) =>
                    (optionA?.label ?? "")
                      .toLowerCase()
                      .includes((optionB?.label ?? "").toLowerCase())
                  }
                />
                <Text>{`(${logsDataSrc?.length})`}</Text>
              </Flex>
            </Flex>
            <Stack
              // p={{ base: "2", lg: "5" }}
              height="100%"
              width="100%"
              overflowY={"auto"}
            >
              {loadingScreenLogs ? (
                <Skeleton />
              ) : (
                <Table
                  columns={columnsLogs}
                  // dataSource={[]}
                  dataSource={logsDataSrc || []}
                  // onChange={onChange}
                  scroll={{ y: 500 }}
                  pagination={{ pageSize: 7 }}
                />
              )}
            </Stack>
          </Box>
        </Stack>
      </Flex>
      {/* )} */}
    </Stack>
  );
}
