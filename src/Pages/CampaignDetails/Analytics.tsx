import { useEffect, useRef, useState } from "react";
import { Box, SimpleGrid, Stack, Text } from "@chakra-ui/react";
import {
  Chart as ChartJS,
  LinearScale,
  CategoryScale,
  BarElement,
  PointElement,
  LineElement,
  Legend,
  Tooltip,
  LineController,
  BarController,
  Filler,
  ArcElement,
  // ChartArea,
} from "chart.js";
// import { faker } from "@faker-js/faker";
import { Chart, Line } from "react-chartjs-2";
import { useDispatch } from "react-redux";

import { useSelector } from "react-redux";
// import {
//   convertIntoDateAndTime,
//   convertToDate,
// } from "../../../utils/dateAndTimeUtils";
import {
  // getAllCampaignListByScreenId,
  getCampaignCamData,
} from "../../Actions/campaignAction";
import moment from "moment";
import {
  convertIntoDateAndTime,
  convertToDate,
} from "../../utils/dateAndTimeUtils";
import { getUserWalletTransaction } from "../../Actions/walletAction";

ChartJS.register(
  ArcElement,
  LinearScale,
  CategoryScale,
  BarElement,
  PointElement,
  LineElement,
  Legend,
  Tooltip,
  LineController,
  BarController,
  Filler
);

export function Analytics(props: any) {
  const chartRef = useRef<ChartJS>(null);
  const dispatch = useDispatch<any>();
  // console.log(props);
  const today = moment().startOf("day").toDate();
  const oneWeekAgo = moment().subtract(1, "month").toDate();

  const [campaignDuration, setCampaignDuration] = useState<any>(20);
  const [runTimeLabels, setRunTimeLabels] = useState<any>([]);
  const [graphRunTimeData, setGraphRunTimeData] = useState<any>([]);

  const [playbackLabels, setPlaybackLabels] = useState<any>([]);
  const [graphPlaybackData, setGraphPlaybackData] = useState<any>([]);

  const [budgetScreeLabels, setBudgetScreeLabels] = useState<any>([]);

  const [brands, setBrands] = useState<any>([]);
  const [brandFreq, setBrandFreq] = useState<any>([]);

  const [budgetLabels, setBudgetLabels] = useState<any>([]);
  const [graphBudgetData, setGraphBudgetData] = useState<any>([]);

  const campaignCamDataGet = useSelector(
    (state: any) => state.campaignCamDataGet
  );
  const {
    loading: loadingCamData,
    error: errorCamData,
    data: campaignCamData,
  } = campaignCamDataGet;

  const walletTransaction = useSelector(
    (state: any) => state.walletTransaction
  );
  const {
    loading: loadingWalletTransaction,
    error: errorWalletTransaction,
    transaction,
  } = walletTransaction;

  useEffect(() => {
    // console.log(campaignCamData);
    let video = document.createElement("video");
    video.src = `${
      props?.campaign?.filter(
        (camp: any) => camp.campaign._id === props?.campaignId
      )[0].campaign.video
    }`;
    video.addEventListener("loadedmetadata", function () {
      let duration = video.duration;
      setCampaignDuration(duration);
    });

    dispatch(
      getCampaignCamData({
        campaignId: props?.campaign
          ?.map((camp: any) => camp.campaign)
          ?.filter((c: any) => c._id === props?.campaignId)[0]._id,
      })
    );
    dispatch(getUserWalletTransaction());
  }, [dispatch, props]);

  // const screenLogsGet = useSelector((state: any) => state.screenLogsGet);
  // const {
  //   loading: loadingLogs,
  //   error: errorLogs,
  //   data: screenLogs,
  // } = screenLogsGet;

  // const campaignListByScreenId = useSelector(
  //   (state: any) => state.campaignListByScreenId
  // );
  // const {
  //   loading: loadingAllCampaign,
  //   error: errorAllCampaign,
  //   campaigns,
  // } = campaignListByScreenId;

  useEffect(() => {
    if (campaignCamData) {
      const aData: any = {};
      campaignCamData?.allCamData?.camData
        ?.filter(
          (d: any) =>
            new Date(d.timestamp).toDateString() >=
            new Date(oneWeekAgo).toDateString()
        )
        ?.map((b: any) => {
          return { time: convertToDate(b.timestamp), people: b.num_people };
        })
        ?.forEach((a: any) => {
          if (aData[a.time]) {
            aData[a.time] = aData[a.time] + a.people;
          } else {
            aData[a.time] = a.people;
          }
        });
      setPlaybackLabels(Object.keys(aData));
      setGraphPlaybackData(Object.values(aData));
      // console.log(campaignCamData);

      var screenLogsDates = campaignCamData?.allCamData?.playbackLog.map(
        (l: any) => convertToDate(new Date(l.playTime))
      );
      var dates: any = {};
      screenLogsDates?.forEach((date: any) => {
        if (dates[date]) {
          dates[date]++;
        } else {
          dates[date] = 1;
        }
      });
      setRunTimeLabels(Object.keys(dates));
      setGraphRunTimeData(Object.values(dates));
    }
    if (transaction) {
      setGraphBudgetData(
        transaction
          ?.filter(
            (tx: any) =>
              tx.txType === "DEBIT" && tx.campaign === props?.campaignId
          )
          ?.map((t: any) => t.amount)
      );
      setBudgetLabels(
        transaction
          ?.filter(
            (tx: any) =>
              tx.txType === "DEBIT" && tx.campaign === props?.campaignId
          )
          ?.map((t: any) => convertIntoDateAndTime(t.updatedAt))
      );
      setBudgetScreeLabels(
        props?.campaign
          ?.filter((c: any) => c.campaign._id === props?.campaignId)[0]
          ?.listOfScreens?.filter((screen: any) => {
            let txn = transaction
              ?.filter(
                (tx: any) =>
                  tx.txType === "DEBIT" && tx.campaign === props?.campaignId
              )
              ?.map((t: any) => t.screen);
            if (txn.includes(screen._id)) {
              return screen;
            }
          })
          ?.map((s: any) => s.name)
      );
      // console.log(
      //   props?.campaign
      //     ?.filter((c: any) => c.campaign._id === props?.campaignId)[0]
      //     ?.listOfScreens?.filter((screen: any) => {
      //       let txn = transaction
      //         ?.filter(
      //           (tx: any) =>
      //             tx.txType === "DEBIT" && tx.campaign === props?.campaignId
      //         )
      //         ?.map((t: any) => t.screen);
      //       if (txn.includes(screen._id)) {
      //         return screen;
      //       }
      //     })
      //     ?.map((s: any) => s.name)
      // );
    }
  }, [campaignCamData, transaction]);

  const playbackData = {
    labels: playbackLabels,
    datasets: [
      {
        fill: true,
        label: "Daily plaback",
        data: graphPlaybackData,
        borderColor: "rgb(74, 58, 235, 0.5)",
        borderWidth: 1,
        // backgroundColor: "rgba(53, 162, 235, 0.5)",
        lineTension: 0.2,
        backgroundColor: function (context: any) {
          var chart = context.chart;
          var { ctx, chartArea } = chart;

          if (!chartArea) {
            // This case happens on initial chart load
            return null;
          }
          var gradient = ctx.createLinearGradient(0, 0, 0, chart.height);
          gradient.addColorStop(0, "rgba(118, 93, 255)");
          gradient.addColorStop(0.5, "rgba(207, 202, 255, 0.5)");
          gradient.addColorStop(1, "rgba(255, 255, 255, 0.1");
          return gradient;
        },
      },
    ],
  };

  const playbackDataOptions: any = {
    responsive: true,
    plugins: {
      legend: {
        position: "top" as const,
        display: false,
      },
      title: {
        display: true,
        text: "Daily playback count",
      },
    },
    scales: {
      x: {
        reverse: true,
      },
    },
  };

  const runtimeData: any = {
    labels: runTimeLabels,
    datasets: [
      {
        type: "bar",
        // label: "Dataset 3",
        // backgroundColor: "rgb(53, 162, 235)",
        data: graphRunTimeData.map((d: any) => (d * campaignDuration) / 60),
        // barPercentage: "0.6",
        barThickness: "flex",
        borderRadius: 5,
        backgroundColor: function (context: any) {
          var chart = context.chart;
          var { ctx, chartArea } = chart;

          if (!chartArea) {
            // This case happens on initial chart load
            return null;
          }
          var gradient = ctx.createLinearGradient(0, 0, 0, chart.height);
          gradient.addColorStop(0, "rgba(153, 102, 255, 1)");
          gradient.addColorStop(0.5, "rgba(153, 102, 255, 0.7)");
          gradient.addColorStop(1, "rgba(255, 255, 255, 0.5");
          return gradient;
        },
      },
    ],
  };

  const runtimeDataOptions: any = {
    responsive: true,
    plugins: {
      legend: {
        position: "top",
        display: false,
      },
      title: {
        display: true,
        text: "User Gain and Loss",
      },
    },
    scales: {
      x: {
        // Set the category percentage to 100%
        categoryPercentage: 100,
        // reverse: true,
      },
      y: {
        type: "linear",
        beginAtZero: true,
        title: {
          text: "Runtime in minutes",
          display: true,
        },
        // ticks: {
        //   callback: function (value: any) {
        //     if (Number.isInteger(value)) {
        //       return value;
        //     }
        //   },
        // },
      },
    },
  };

  const budgetData: any = {
    labels: budgetLabels,
    datasets: [
      {
        type: "line",
        label: "Spended Amount",
        borderColor: "rgb(255, 99, 132)",
        // borderWidth: 2,
        fill: true,
        backgroundColor: "rgb(255, 99, 132, 0.4)",
        data: graphBudgetData,
        lineTension: 0.3,
      },
    ],
  };

  const budgetDataOptions: any = {
    responsive: true,
    plugins: {
      legend: {
        position: "top",
        display: false,
      },
      title: {
        display: true,
        text: "User Gain and Loss",
      },
    },
    scales: {
      x: {
        // Set the category percentage to 100%
        categoryPercentage: 100,
        stacked: true,
        // type: "date",
        ticks: {
          source: "auto",
          // Disabled rotation for performance
          maxRotation: 0,
          autoSkip: true,
        },
      },
      y: {
        // type: "linear",
        // beginAtZero: true,
        title: {
          text: "Spended Amount",
          display: true,
        },
        // ticks: {
        //   source: "auto",
        //   callback: function (value: any) {
        //     if (Number.isInteger(value)) {
        //       return value;
        //     }
        //   },
        // },
        stacked: true,
      },
    },
  };

  const budgetScreenData: any = {
    labels: budgetScreeLabels,
    datasets: [
      {
        type: "bar",
        // label: "Dataset 3",
        // backgroundColor: "rgb(53, 162, 235)",
        data: graphBudgetData,
        // barPercentage: "0.6",
        barThickness: "flex",
        borderRadius: 5,
        backgroundColor: function (context: any) {
          var chart = context.chart;
          var { ctx, chartArea } = chart;

          if (!chartArea) {
            // This case happens on initial chart load
            return null;
          }
          var gradient = ctx.createLinearGradient(0, 0, 0, chart.height);
          gradient.addColorStop(0, "rgba(153, 102, 255, 1)");
          gradient.addColorStop(0.5, "rgba(153, 102, 255, 0.7)");
          gradient.addColorStop(1, "rgba(255, 255, 255, 0.5");
          return gradient;
        },
      },
    ],
  };

  const budgetScreenDataOptions: any = {
    responsive: true,
    plugins: {
      legend: {
        position: "top",
        display: false,
      },
      title: {
        display: true,
        text: "User Gain and Loss",
      },
    },
    scales: {
      x: {
        // Set the category percentage to 100%
        categoryPercentage: 100,
        // reverse: true,
      },
      y: {
        type: "linear",
        beginAtZero: true,
        title: {
          text: "Amount in credits",
          display: true,
        },
        // ticks: {
        //   callback: function (value: any) {
        //     if (Number.isInteger(value)) {
        //       return value;
        //     }
        //   },
        // },
      },
    },
  };
  return (
    <Stack width="100%" overflowY={"auto"} p="5">
      {loadingCamData ? (
        <Text>Loading...</Text>
      ) : errorCamData ? (
        <Text>{errorCamData}</Text>
      ) : (
        <Stack width="">
          <SimpleGrid gap={2} columns={[4]}>
            <Box
              m="2"
              p="5"
              bgColor="#FFFFFF"
              justifyContent="flex-start"
              rounded="xl"
            >
              <Text pb="0" fontSize="sm">
                Statics
              </Text>
              <Text pt="0" fontSize="md" fontWeight="600">
                Total Screens
              </Text>
              <Stack>
                <Text fontSize="2xl" fontWeight="600">
                  {
                    props?.campaign?.filter(
                      (camp: any) => camp.campaign._id === props?.campaignId
                    )[0]?.listOfScreens?.length
                  }
                </Text>
              </Stack>
              <Text>Screens</Text>
            </Box>
            <Box
              m="2"
              p="5"
              bgColor="#FFFFFF"
              justifyContent="flex-start"
              rounded="xl"
            >
              <Text pb="0" fontSize="sm">
                Statics
              </Text>
              <Text pt="0" fontSize="md" fontWeight="600">
                Total Impressions
              </Text>
              <Stack>
                <Text fontSize="2xl" fontWeight="600">
                  {campaignCamData?.allCamData?.camData
                    .map((k: any) => k.num_people)
                    .reduce((n: any, m: any) => n + m, 0)}
                </Text>
              </Stack>
              <Text>Persons</Text>
            </Box>
            <Box
              m="2"
              p="5"
              bgColor="#FFFFFF"
              justifyContent="flex-start"
              rounded="xl"
            >
              <Text pb="0" fontSize="sm">
                Statics
              </Text>
              <Text pt="0" fontSize="md" fontWeight="600">
                Total Budget
              </Text>
              <Stack>
                <Text fontSize="2xl" fontWeight="600">
                  {
                    props?.campaign
                      ?.map((camp: any) => camp.campaign)
                      ?.filter((c: any) => c._id === props?.campaignId)[0]
                      ?.totalAmount
                  }
                </Text>
              </Stack>
              <Text>Credits</Text>
            </Box>
            <Box
              m="2"
              p="5"
              bgColor="#FFFFFF"
              justifyContent="flex-start"
              rounded="xl"
            >
              <Text pb="0" fontSize="sm">
                Statics
              </Text>
              <Text pt="0" fontSize="md" fontWeight="600">
                Total Runtime
              </Text>
              <Stack>
                <Text fontSize="2xl" fontWeight="600">
                  {Math.trunc(
                    graphRunTimeData
                      ?.map((d: any) => (d * campaignDuration) / 60)
                      ?.reduce((a: any, b: any) => a + b, 0)
                  )}
                </Text>
              </Stack>
              <Text>minutes</Text>
            </Box>
          </SimpleGrid>
          <SimpleGrid columns={[1, 2]}>
            <Box
              m="2"
              p="5"
              bgColor="#FFFFFF"
              justifyContent="flex-start"
              rounded="lg"
            >
              <Stack p="4">
                <Text fontSize="20px" fontWeight="600">
                  Screen Runtime
                </Text>
                <Chart
                  ref={chartRef}
                  type="bar"
                  data={runtimeData}
                  options={runtimeDataOptions}
                />
              </Stack>
            </Box>
            <Box
              m="2"
              p="5"
              bgColor="#FFFFFF"
              justifyContent="flex-start"
              rounded="lg"
            >
              <Stack p="4">
                <Text fontSize="20px" fontWeight="600">
                  Expected Audience Exposure
                </Text>
                <Line options={playbackDataOptions} data={playbackData} />
              </Stack>
            </Box>
          </SimpleGrid>
          <SimpleGrid columns={[1, 2]}>
            <Box
              m="2"
              p="5"
              bgColor="#FFFFFF"
              justifyContent="flex-start"
              rounded="lg"
            >
              <Stack p="4">
                <Text fontSize="20px" fontWeight="600">
                  Budget spended
                </Text>
                <Line options={budgetDataOptions} data={budgetData} />
              </Stack>
            </Box>
            <Box
              m="2"
              p="5"
              bgColor="#FFFFFF"
              justifyContent="flex-start"
              rounded="lg"
            >
              <Stack p="4">
                <Text fontSize="20px" fontWeight="600">
                  Budget spended
                </Text>
                <Chart
                  ref={chartRef}
                  type="bar"
                  data={budgetScreenData}
                  options={budgetScreenDataOptions}
                />
              </Stack>
            </Box>
          </SimpleGrid>
        </Stack>
      )}
    </Stack>
  );
}
