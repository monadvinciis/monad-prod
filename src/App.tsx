import React from "react";
import "./App.css";
import { ChakraProvider } from "@chakra-ui/react";
// import BasicStyle from "./theme/basicStyle";
import { BrowserRouter as Router } from "react-router-dom";
import { theme } from "./theme/Theme.base";
import { PublicRoutes } from "./routes";
import { QueryClient, QueryClientProvider } from "react-query";
import { GoogleOAuthProvider } from "@react-oauth/google";

const queryClient = new QueryClient();

function App() {
  return (
    <GoogleOAuthProvider clientId="509348548420-rrkqee9s78ke31j5lsfv95be38mvfgoe.apps.googleusercontent.com">
      <ChakraProvider theme={theme}>
        <QueryClientProvider client={queryClient}>
          <Router>
            <PublicRoutes />
          </Router>
        </QueryClientProvider>
      </ChakraProvider>
    </GoogleOAuthProvider>
  );
}

export default App;
