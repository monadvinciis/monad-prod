export const PERSENTAGE_DISCOUNT = "Percentage discounts";
export const FLAT_DISCOUNT = "Flat discount";
export const FREEBIE = "Freebie";
export const FREE_SHIPPING = "Free shipping";
export const LOYALTY_POINTS = "Loyalty points";
export const BUY_X_GEY_Y = "Buy X, Get Y";
