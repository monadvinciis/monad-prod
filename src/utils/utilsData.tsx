import { SelectProps } from "antd";
import { CgScreen } from "react-icons/cg";
import { MdOutlineCampaign, MdOutlineSummarize } from "react-icons/md";

export const state = [
  "Andhra Pradesh",
  "Arunachal Pradesh",
  "Assam",
  "Bihar",
  "Chhattisgarh",
  "Goa",
  "Gujarat",
  "Haryana",
  "Himachal Pradesh",
  "Jammu and Kashmir",
  "Jharkhand",
  "Karnataka",
  "Kerala",
  "Madhya Pradesh",
  "Maharashtra",
  "Manipur",
  "Meghalaya",
  "Mizoram",
  "Nagaland",
  "Odisha",
  "Punjab",
  "Rajasthan",
  "Sikkim",
  "Tamil Nadu",
  "Telangana",
  "Tripura",
  "Uttarakhand",
  "Uttar Pradesh",
  "West Bengal",
  "Andaman and Nicobar Islands",
  "Chandigarh",
  "Dadra and Nagar Haveli",
  "Daman and Diu",
  "New Delhi",
  "Lakshadweep",
  "Puducherry",
];

export const screenTypeOptions: SelectProps["options"] = [
  { value: "LED TV Screen", label: "LED TV Screen" },
  { value: "Digital Billboard Screen", label: "Digital Billboard Screen" },
];

export const categoryOptions: SelectProps["options"] = [
  { value: "OUTDOORS", label: "Out Doors" },
  { value: "INDOORS", label: "In Doors" },
  { value: "RAILWAY", label: "Railway" },
  { value: "HOSPITAL", label: "Hospital" },
  { value: "ERICKSHAW", label: "eRickshaw" },
];
export const croudMobabilityOption: SelectProps["options"] = [
  { value: "4 Wheelers", label: "4 Wheelers" },
  { value: "2 Wheelers", label: "2 Wheelers" },
  { value: "On Footers", label: "On Footers" },
];
export const employmentTypeOption: SelectProps["options"] = [
  { value: "Salaried", label: "Salaried" },
  { value: "Businessman", label: "Businessman" },
  { value: "Student", label: "Student" },
  { value: "Others", label: "Others" },
];

export const stateOption: SelectProps["options"] = state.map(
  (value: string) => {
    return { value: `${value}`, label: `${value}` };
  }
);

export const stepsForCeateAndEditScreens = [
  {
    key: 1,
    text: "Screen details",
    status: "Active",
  },
  {
    key: 2,
    text: "Add images",
    status: "process",
  },
  {
    key: 3,
    text: "Activate screen",
    status: "process",
  },
];

export const createCampaignTabOptions = [
  {
    key: 1,
    text: "Details",
    status: "Active",
    icon: <MdOutlineCampaign size="20px" />,
  },
  {
    key: 2,
    text: "Screens",
    status: "process",
    icon: <CgScreen size="20px" />,
  },
  {
    key: 3,
    text: "Summary",
    status: "process",
    icon: <MdOutlineSummarize size="20px" />,
  },
];

export const campaignGoalOptions: SelectProps["options"] = [
  // { value: "Audience engagement", label: "Audience engagement" },
  { value: "Audience reach", label: "Audience reach" },
];
export const genderOption: SelectProps["options"] = [
  { value: "Male", label: "Male" },
  { value: "Female", label: "Female" },
  { value: "All", label: "All" },
];

export const stepsForSelectScreensInCreateCampaign = [
  {
    key: 1,
    label: "My Screens",
  },
  {
    key: 2,
    label: "Explore Screens",
  },
  {
    key: 3,
    label: "Explore Map",
  },
];

export const highlights: SelectProps["options"] = [
  {
    value: "School",
    label: "School",
  },
  {
    value: "Colleges",
    label: "Colleges",
  },
  {
    value: "Swimming pool",
    label: "Swimming pool",
  },
  {
    value: "Appartments",
    label: "Appartments",
  },
  {
    value: "Metro",
    label: "Metro",
  },
  {
    value: "Airports",
    label: "Airports",
  },
  {
    value: "Military",
    label: "Military",
  },
  {
    value: "Tourist attractions",
    label: "Tourist attractions",
  },
  {
    value: "Railways",
    label: "Railways",
  },
  {
    value: "Beachs",
    label: "Beachs",
  },
];
