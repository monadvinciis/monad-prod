import * as Delegation from "@ucanto/core/delegation";
import * as Client from "@web3-storage/w3up-client";
import Axios from "axios";

export async function addFileOnWeb3(file) {
  try {
    // Create a new client
    let startTime = performance.now();
    const client = await Client.create();
    // console.log(client);
    // Fetch the delegation from the backend
    const apiUrl = `${
      process.env.REACT_APP_BLINDS_SERVER
    }/api/web3/w3up-delegation/${client._agent.did()}`;
    // console.log(client._agent.did());

    const { data } = await Axios.get(apiUrl);
    // console.log(data);
    const response = Object.values(data);
    // console.log(response);
    const res = Uint8Array.from(response);
    // console.log(res);
    // Deserialize the delegation
    const delegation = await Delegation.extract(new Uint8Array(res));
    // console.log(delegation);
    if (!delegation.ok) {
      throw new Error("Failed to extract delegation", {
        cause: delegation.error,
      });
    }
    // Add proof that this agent has been delegated capabilities on the space
    const space = await client.addSpace(delegation.ok);
    // console.log(space);
    client.setCurrentSpace(space.did());

    // READY to go!
    const getCid = await client.uploadFile(file);
    // console.log(getCid);
    const cid = JSON.parse(JSON.stringify(getCid));
    // console.log(cid);
    console.log(
      "time taken to get cid file in sec ",
      (performance.now() - startTime) / 1000
    );
    return cid["/"];
  } catch (error) {
    // console.log("Error in uploading file : ", error);
    // console.log(
    //   "time taken to upload file in ms ",
    //   performance.now() - startTime
    // );
    throw new Error(error);
  }
}
