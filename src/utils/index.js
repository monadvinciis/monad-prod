const characters =
  "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

export function generateString(length) {
  let result = " ";
  const charactersLength = characters?.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }

  return result;
}

export const isPWA = () => {
  return window.matchMedia("(display-mode: standalone)").matches;
};

// for checking map drawings
function isRayIntersectEdge(point, vertex1, vertex2) {
  // Get the coordinates of the point and the vertices
  var x = point[0];
  var y = point[1];
  var x1 = vertex1[0];
  var y1 = vertex1[1];
  var x2 = vertex2[0];
  var y2 = vertex2[1];
  // Check if the point is on the same horizontal line as the edge
  if (y1 === y2) {
    return false;
  }
  // Check if the point is below or above the edge
  if (y > Math.max(y1, y2) || y < Math.min(y1, y2)) {
    return false;
  }
  // Check if the point is on the left or right of the edge
  if (x > Math.max(x1, x2)) {
    return false;
  }
  // Calculate the slope and the intercept of the edge
  var slope = (y2 - y1) / (x2 - x1);
  var intercept = y1 - slope * x1;
  // Calculate the x-coordinate of the intersection point
  var xIntersect = (y - intercept) / slope;
  // Check if the intersection point is on the right of the point
  if (xIntersect > x) {
    return true;
  } else {
    return false;
  }
}

export function isPointInsidePolygon(point, polygon) {
  // Initialize the number of intersections to zero
  var intersections = 0;
  // Loop through each edge of the polygon
  for (var i = 0; i < polygon?.length - 1; i++) {
    // Get the current and next vertices of the edge
    var vertex1 = polygon[i];
    var vertex2 = polygon[i + 1];
    // Check if the ray intersects with the edge
    if (isRayIntersectEdge(point, vertex1, vertex2)) {
      // Increment the number of intersections
      intersections++;
    }
  }
  // Return true if the number of intersections is odd, false otherwise
  return intersections % 2 === 1;
}
