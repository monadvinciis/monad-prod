import { signout } from "../Actions/userActions";

export const isTokenExpiredUtils = () => {
  const userInfo = localStorage.getItem("userInfo")
    ? JSON.parse(localStorage.getItem("userInfo"))
    : null;
  if (userInfo) {
    const token = userInfo?.token;
    if (token) {
      // Check if token is expired
      const { exp } = JSON.parse(atob(token.split(".")[1])); // Decode JWT payload
      const expirationTime = exp * 1000; // Convert expiration time to milliseconds
      if (new Date() >= new Date(expirationTime)) {
        return true;
      } else {
        return false;
      }
    }
  } else {
    return false;
  }
};

export const logOutUtils = (dispatch) => {
  dispatch(signout());
  window.open("/signin");
};
