import { isTokenExpiredUtils, logOutUtils } from "./authUtils";

export const isEqualTwoMongodbObjectId = (objId1: any, objId2: any) => {
  if (objId1 && objId2) {
    return objId1.toString() === objId2.toString();
  }
  return false;
};

export const isNumber = (value: any) => {
  var reg = /^\d+$/;
  return reg.test(value);
};

// using callback
export const getVideoDurationFronVideoURL = (url: string, cb: any) => {
  let video = document.createElement("video");
  video.src = url;
  video.addEventListener("loadedmetadata", function () {
    let duration = video.duration;
    // console.log("getVideoDurationFronVideoURL : ", duration);
    cb(duration);
  });
};

// using promise
export const getVideoDurationFronVideoURL1 = (url: string) => {
  return new Promise((resolve, reject) => {
    let video = document.createElement("video");
    video.src = url;
    video.addEventListener("loadedmetadata", function () {
      let duration = video.duration;
      // console.log("getVideoDurationFronVideoURL : ", duration);
      resolve(duration);
    });
  });
};

export const logoutAfterTimeOut = (dispatch: any) => {
  const interval = setInterval(() => {
    if (isTokenExpiredUtils()) {
      logOutUtils(dispatch);
      clearInterval(interval);
    }
  }, 5 * 60 * 1000); // Check every minute
  return () => clearInterval(interval);
};

export const getFileTypeAndFileSizeFromURL = async (url: string) => {
  try {
    const response = await fetch(url);
    const blob = await response.blob();
    return Promise.resolve({
      type: blob?.type,
      size: blob?.size,
    });
  } catch (error) {
    return Promise.reject(error);
  }
};
