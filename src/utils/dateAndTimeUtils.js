//10:00 AM, 24 December 2022
export function convertIntoDateAndTime(string) {
  if (string) {
    let date = new Date(string); // Fri Jan 27 2012 02:21:50 GMT+0530 (India Standard Time)
    date = date.toString();
    date = date.split(" ");
    let am_pm = "";
    let time = date[4].split(":"); // [02,21,50] =
    if (time[0] >= 12) {
      am_pm = "PM";
    } else {
      am_pm = "AM";
    }
    if (time[0] > 12) time[0] = Number(time[0]) % 12;
    return `${date[2]} ${date[1]}, ${date[3]}, ${time[0]}:${time[1]}:${time[2]} ${am_pm}`; // 24 December 2022, 10:00 AM,
  }
}

export function convertToDate(string) {
  let date = new Date(string);
  date = date.toString();
  date = date.split(" ");
  return `${date[2]} ${date[1]} ${date[3]}`;
}

export function getTimeValue(value) {
  let date = new Date(value); // Fri Jan 27 2012 02:21:50 GMT+0530 (India Standard Time)
  date = date.toString();
  date = date.split(" ");
  let time = date[4]; // ("02:21:50");
  return time;
}

export function getNumberOfDaysBetweenTwoDates(date1, date2) {
  date1 = new Date(date1);
  date2 = new Date(date2);

  //calculate time difference
  var time_difference = date2.getTime() - date1.getTime();

  //calculate days difference by dividing total milliseconds in a day
  var days_difference = time_difference / (1000 * 60 * 60 * 24);
  return Math.round(days_difference);
}
export function consvertSecondToHrAndMinutes(value) {
  const hr = (value / 3600).toFixed(0);
  const min = ((value % 3600) / 60).toFixed(0);
  const s = (value % 3600) % 60;
  if (hr > 0) {
    if (min > 0) {
      if (s > 0) {
        return `${hr} hr, ${min} min, ${s} s`;
      } else {
        return `${hr} hr, ${min} min`;
      }
    } else {
      return `${hr} hr, ${min} min`;
    }
  } else {
    if (min > 0) {
      if (s > 0) {
        return `${min} min, ${s} s`;
      } else {
        return `${min} min`;
      }
    } else {
      if (s > 0) {
        return `${s} s`;
      } else {
        return `0 sec`;
      }
    }
  }
}

export const getDateAndTimeAfterAdded5Hr = (date, time = 0) => {
  return convertIntoDateAndTime(
    new Date(new Date(date).getTime() + (5 * 60 + 30) * 60000 - time * 1000)
  );
};
