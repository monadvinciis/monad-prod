import Axios from "axios";

export const uploadFileOnAWSThroughURL = (url: string, file: File) => {
  console.log("uploadFileOnAWSThroughURL called! : ", url, file);
  return new Promise((resolve, reject) => {
    Axios.put(url, file, {
      headers: {
        "Content-Type": file?.type,
      },
    })
      .then(({ data }) => resolve(data))
      .catch(() => reject("Something went wrong on AWS side, try agin latter"));
  });
};
