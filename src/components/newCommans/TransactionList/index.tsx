import { Box, Text } from "@chakra-ui/react";
import Table, { ColumnsType } from "antd/es/table";
import { convertIntoDateAndTime } from "../../../utils/dateAndTimeUtils";
import { useState } from "react";

export function TransactionList(props: any) {
  const [page, setPage] = useState(1);

  const [paginationSize, setPaginationSize] = useState(
    props?.pagination?.pageSize || 6
  );

  const columns: ColumnsType<any> = [
    {
      title: "SN. No.",
      dataIndex: "sn",
      key: "sn",
      render: (_, values, index) => (
        <Text>{(page - 1) * paginationSize + index + 1}</Text>
      ),
      width: "10%",
    },
    {
      title: "Remark",
      dataIndex: "remark",
      key: "remark",
      render: (remark) => (
        <Text _hover={{ color: "blue" }} fontSize="14px" m="0">
          {remark}
        </Text>
      ),
      // width: "40%",
    },
    {
      title: "Amount",
      dataIndex: "amount",
      key: "amount",
      render: (amount, transaction, index) => (
        <Text
          color={transaction?.txType === "CREDIT" ? "#2D9713" : "#990000"}
          fontSize="16px"
          fontWeight="700"
          m="0"
        >
          {transaction?.txType === "CREDIT" ? `+${amount}` : `-${amount}`}
        </Text>
      ),
    },
    {
      title: "Date & Time",
      dataIndex: "txDate",
      key: "txDate",
      render: (text) => <Text>{convertIntoDateAndTime(text)}</Text>,
      width: "15%",
    },
  ];

  return (
    <Box flexGrow={1} overflowY={"auto"}>
      <Table
        pagination={{
          onChange(current, pageSize) {
            setPage(current);
            setPaginationSize(pageSize);
          },
          defaultPageSize: 6,
          hideOnSinglePage: false,
          showSizeChanger: false,
        }}
        columns={columns}
        loading={props?.loading}
        dataSource={props?.data}
        rowKey="Id"
      />
    </Box>
  );
}
