import { Accordion, Button, Flex, Box } from "@chakra-ui/react";
import { useState } from "react";
import { SinglePlea } from "../SinglePlea";
import { Skeleton } from "antd";
import { NoPleaFound } from "../NoPleaFound";

function PendingPleaRequest(props: any) {
  const {
    pendingCampaignPleaRequest,
    getPleaRequest,
    loadingPendingPleaRequest,
    handelRejectCampaignPlea,
    handelAcceptCampaignPlea,
  } = props;
  return (
    <Box flexGrow={1} overflowY={"auto"} width="100%">
      {loadingPendingPleaRequest ? (
        <Box p="5">
          <Skeleton avatar active paragraph={{ rows: 2 }} />
          <Skeleton avatar active paragraph={{ rows: 2 }} />
          <Skeleton avatar active paragraph={{ rows: 2 }} />
        </Box>
      ) : pendingCampaignPleaRequest?.length > 0 ? (
        <Accordion px="2" allowToggle>
          {pendingCampaignPleaRequest.map((data: any, index: any) => (
            <SinglePlea
              data={data}
              key={index}
              getPleaRequest={getPleaRequest}
              handelAcceptCampaignPlea={handelAcceptCampaignPlea}
              handelRejectCampaignPlea={handelRejectCampaignPlea}
            />
          ))}
        </Accordion>
      ) : (
        <NoPleaFound />
      )}
    </Box>
  );
}

function AllPleaRequest(props: any) {
  const {
    allCampaignPleaRequest,
    getPleaRequest,
    loadingAllPleas,
    handelRejectCampaignPlea,
    handelAcceptCampaignPlea,
  } = props;
  return (
    <Box flexGrow={1} overflowY={"auto"} width="100%">
      {loadingAllPleas ? (
        <Box p="5">
          <Skeleton avatar active paragraph={{ rows: 2 }} />
          <Skeleton avatar active paragraph={{ rows: 2 }} />
          <Skeleton avatar active paragraph={{ rows: 2 }} />
        </Box>
      ) : allCampaignPleaRequest?.length ? (
        <Accordion px="2" allowToggle>
          {allCampaignPleaRequest.map((data: any, index: any) => (
            <SinglePlea
              data={data}
              key={index}
              getPleaRequest={getPleaRequest}
              handelAcceptCampaignPlea={handelAcceptCampaignPlea}
              handelRejectCampaignPlea={handelRejectCampaignPlea}
            />
          ))}
        </Accordion>
      ) : (
        <NoPleaFound />
      )}
    </Box>
  );
}

export function ScreenOwnerPlea(props: any) {
  const [selectedOption, setSelectedOption] = useState<any>(2);
  const options = [
    { key: 1, label: "Pending" },
    { key: 2, label: "All" },
  ];
  const {
    pendingCampaignPleaRequest,
    allCampaignPleaRequest,
    getPleaRequest,
    loadingPendingPleaRequest,
    loadingAllPleas,
    handelRejectCampaignPlea,
    handelAcceptCampaignPlea,
  } = props;

  return (
    <Flex
      mb={"4"}
      flexDir={"column"}
      overflow={"hidden"}
      px={{ base: "1", lg: "5" }}
    >
      <Flex gap="3" py="2" pl={{ base: "2px", lg: "30px" }}>
        {options?.map((option: any, index: any) => {
          return (
            <Button
              key={index}
              variant="outline"
              color={option.key === selectedOption ? "#FFFFFF" : "#000000"}
              bgColor={option.key === selectedOption ? "#000000" : "#FFFFFF"}
              py="2"
              px="4"
              borderRadius="55px"
              fontWeight="700"
              fontSize="12px"
              onClick={() => setSelectedOption(option.key)}
            >
              {option?.label}
            </Button>
          );
        })}
      </Flex>
      <Box flexGrow={1} overflowY={"auto"} width="100%">
        {selectedOption === 1 ? (
          <PendingPleaRequest
            pendingCampaignPleaRequest={pendingCampaignPleaRequest}
            getPleaRequest={getPleaRequest}
            loadingPendingPleaRequest={loadingPendingPleaRequest}
            handelAcceptCampaignPlea={handelAcceptCampaignPlea}
            handelRejectCampaignPlea={handelRejectCampaignPlea}
          />
        ) : (
          <AllPleaRequest
            allCampaignPleaRequest={allCampaignPleaRequest}
            getPleaRequest={getPleaRequest}
            loadingAllPleas={loadingAllPleas}
            handelAcceptCampaignPlea={handelAcceptCampaignPlea}
            handelRejectCampaignPlea={handelRejectCampaignPlea}
          />
        )}
      </Box>
    </Flex>
  );
}
