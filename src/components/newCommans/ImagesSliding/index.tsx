import { Box, Image } from "@chakra-ui/react";
import { Carousel } from "antd";

export function ImagesSliding(props: any) {
  const { images } = props;
  return (
    <Box height={props?.height} width={props?.width}>
      <Carousel autoplay>
        {images.map((image: any, index: any) => (
          <Box key={index}>
            <Image
              height={props?.height}
              width={props?.width}
              src={image}
              borderRadius="3px"
              alt="image"
            />
          </Box>
        ))}
      </Carousel>
    </Box>
  );
}
