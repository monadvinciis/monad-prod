import { Box, Flex, SimpleGrid, Text } from "@chakra-ui/react";
import { GoInfo } from "react-icons/go";
import { Dropdown, Popconfirm } from "antd";
import { HiDotsHorizontal } from "react-icons/hi";
import { BsPauseCircleFill } from "react-icons/bs";
import {
  CAMPAIGN_STATUS_DELETED,
  CAMPAIGN_STATUS_HOLD,
  CAMPAIGN_STATUS_PAUSE,
} from "../../../Constants/campaignConstants";
import { MdPlayCircle } from "react-icons/md";
import { RiDeleteBin6Line } from "react-icons/ri";
import { getNumberOfDaysBetweenTwoDates } from "../../../utils/dateAndTimeUtils";
import { useNavigate } from "react-router-dom";
import { SetCampaignPriority } from "../../../Pages/Models/SetCampaignPriority";
import { useState } from "react";
import { MediaContainer } from "../MediaContainer";

export function ActiveCampaign(props: any) {
  const { campaign, handleChangeCampaignStatus, campaigns } = props;
  let data = campaigns || [];
  let data1 = campaign?.atIndex || [];
  const newCampaigns = data?.filter((value: any) => !data1?.includes(value));
  const navigate = useNavigate();

  const [openModel, setModel] = useState<boolean>(false);
  // const revenueGenerated = () => {
  //   if (campaign?.isDefault) {
  //     return 0;
  //   } else {
  //     const totalSlotBooked = campaign?.totalSlotBooked || 0;
  //     const remaininfSlots = campaign?.remaininfSlots || 0;
  //     const rentPerSlot = campaign?.rentPerSlot || 0;
  //     return (totalSlotBooked - remaininfSlots) * rentPerSlot;
  //   }
  // };

  const confirmPause = () => {
    // props?.handleSetPriority(campaign?._id, "");
    handleChangeCampaignStatus(campaign?._id, CAMPAIGN_STATUS_PAUSE);
  };
  const confirmPlay = () => {
    props?.handleSetPriority(campaign?._id, Math.max(...campaigns) + 1);
    handleChangeCampaignStatus(campaign?._id, "Resume");
  };

  const confirmDeleteCamapaign = () => {
    handleChangeCampaignStatus(campaign?._id, CAMPAIGN_STATUS_DELETED);
  };

  const handelSave = (value: any) => {
    props?.handleSetPriority(campaign?._id, value);
  };

  const holdCampaignItemS = [
    {
      key: "1",
      label: (
        <Flex
          gap="2"
          color="#000000"
          align="center"
          fontSize="14px"
          fontWeight="400"
          onClick={() => navigate(`/campaigns/details/${campaign._id}`)}
        >
          <GoInfo />
          <Text m="0">View detail</Text>
        </Flex>
      ),
    },
    {
      key: "2",
      label: (
        <Popconfirm
          title="Delete Campaign"
          description="Do you really want to delete this campaign?"
          onConfirm={confirmDeleteCamapaign}
          okText="Yes"
          cancelText="No"
        >
          <Flex
            gap="2"
            color="#D71111"
            align="center"
            fontSize="14px"
            fontWeight="400"
          >
            <RiDeleteBin6Line />
            <Text m="0">Delete Campaign</Text>
          </Flex>
        </Popconfirm>
      ),
    },
  ];

  const activeOrPauseCampaignItems = [
    {
      key: "1",
      label: (
        <Flex
          gap="2"
          color="#000000"
          align="center"
          fontSize="14px"
          fontWeight="400"
          onClick={() => navigate(`/campaigns/details/${campaign._id}`)}
        >
          <GoInfo />
          <Text m="0">View detail</Text>
        </Flex>
      ),
    },
    {
      key: "2",
      label: (
        <>
          {!(campaign.status === CAMPAIGN_STATUS_PAUSE) ? (
            <Popconfirm
              title="Pause Campaign"
              description="Do you really want to pause this campaign?"
              onConfirm={confirmPause}
              okText="Yes"
              cancelText="No"
            >
              <Flex gap="2" align="center" fontSize="14px" fontWeight="400">
                <BsPauseCircleFill />
                <Text m="0">Pause Campaign</Text>
              </Flex>
            </Popconfirm>
          ) : (
            <Popconfirm
              title="Play Campaign"
              description="Do you really want to play this campaign?"
              onConfirm={confirmPlay}
              okText="Yes"
              cancelText="No"
            >
              <Flex gap="2" align="center" fontSize="14px" fontWeight="400">
                <MdPlayCircle />
                <Text m="0">Play Campaign</Text>
              </Flex>
            </Popconfirm>
          )}
        </>
      ),
    },
    {
      key: "3",
      label: (
        <>
          {campaign?.status === "Active" ? (
            <Flex
              gap="2"
              color="#000000"
              align="center"
              fontSize="14px"
              fontWeight="400"
              onClick={() => setModel(true)}
            >
              <GoInfo />
              <Text m="0">Set New Priority</Text>
            </Flex>
          ) : null}
        </>
      ),
    },
    {
      key: "4",
      label: (
        <Popconfirm
          title="Delete Campaign"
          description="Do you really want to delete this campaign?"
          onConfirm={confirmDeleteCamapaign}
          okText="Yes"
          cancelText="No"
        >
          <Flex
            gap="2"
            color="#D71111"
            align="center"
            fontSize="14px"
            fontWeight="400"
          >
            <RiDeleteBin6Line />
            <Text m="0">Delete Campaign</Text>
          </Flex>
        </Popconfirm>
      ),
    },
  ];

  return (
    <Flex
      justifyContent="space-between"
      bgColor="#FFFFFF"
      p={{ base: "2", lg: "4" }}
      borderRadius="9px"
      minWidth={{ base: "100%", lg: "700px" }}
    >
      <SetCampaignPriority
        open={openModel}
        onCancel={() => setModel(false)}
        handelSave={handelSave}
        campaigns={newCampaigns}
        priority={data1?.length > 0 ? data1?.join(",") : ""}
      />
      <Flex gap="4">
        <MediaContainer
          cid={campaign.cid}
          width={{ base: "130px", lg: "190px" }}
          height={{ base: "111px", lg: "140px" }}
        />

        <Box>
          <Text fontSize="md" fontWeight="700" color="#131D30" m="0">
            {campaign?.campaignName}
          </Text>
          <SimpleGrid
            columns={[2, 2, 2]}
            spacing={{ base: "2", lg: "4" }}
            fontSize={{ base: "11px", lg: "12px" }}
          >
            <Box gap="0">
              <Text color="#808080" m="0">
                Uploaded By
              </Text>
              <Text color="#808080" m="0">
                Brand
              </Text>
              <Text color="#808080" m="0">
                Today Played
              </Text>

              <Text color="#808080" m="0">
                Days Left
              </Text>
              <Text color="#808080" m="0">
                At Index
              </Text>
              <Text color="#808080" m="0">
                File Size
              </Text>
              <Text color="#808080" m="0">
                File Type
              </Text>
              <Text color="#808080" m="0">
                Duration of Campaign
              </Text>
            </Box>
            <Box>
              <Text color="#808080" m="0" width="100%">
                {campaign?.uploadedBy}
              </Text>
              <Text color="#808080" m="0">
                {campaign?.brandName}
              </Text>
              <Text color="#808080" m="0">
                {campaign?.slotPlayedPerDay}
              </Text>
              <Text color="#FD5D5D" m="0" fontWeight="600">
                {campaign?.isDefaultCampaign
                  ? "still"
                  : getNumberOfDaysBetweenTwoDates(
                      campaign?.startDate,
                      campaign?.endDate
                    )}{" "}
                days
              </Text>
              <Text color="#808080" m="0" fontWeight="600">
                {JSON.stringify(campaign?.atIndex)}
              </Text>
              <Text color="#808080" m="0" fontWeight="600">
                {campaign?.fileSize || 0} Kb
              </Text>
              <Text color="#808080" m="0" fontWeight="600">
                {campaign?.fileType}
              </Text>{" "}
              <Text color="#808080" m="0" fontWeight="600">
                {Number(campaign?.duration || 0).toFixed(2)} Sec
              </Text>
            </Box>
          </SimpleGrid>
        </Box>
      </Flex>

      <Flex justifyContent="flex-end" pr="4">
        <Dropdown
          menu={{
            items:
              campaign?.status !== CAMPAIGN_STATUS_HOLD
                ? activeOrPauseCampaignItems
                : holdCampaignItemS,
          }}
          placement="topLeft"
          arrow
        >
          <Box py="2" px="4">
            <HiDotsHorizontal />
          </Box>
        </Dropdown>
      </Flex>
    </Flex>
  );
}
