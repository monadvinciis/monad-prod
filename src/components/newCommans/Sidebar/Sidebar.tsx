import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  Flex,
  Text,
  // IconButton,
  Divider,
  // Avatar,
  // Heading,
  Stack,
  Image,
  Box,
  Hide,
} from "@chakra-ui/react";
import { BsArrowBarLeft, BsArrowBarRight } from "react-icons/bs";
import { useNavigate } from "react-router-dom";
import { CgScreen } from "react-icons/cg";
import { LuWallet } from "react-icons/lu";
import { MdOutlineCampaign } from "react-icons/md";
import { RxDashboard } from "react-icons/rx";
import { SlBell, SlEarphonesAlt } from "react-icons/sl";
import { IoSettingsOutline } from "react-icons/io5";
import { AiOutlinePoweroff } from "react-icons/ai";
import { Logo } from "../../../assets/Images";
import { signout } from "../../../Actions/userActions";

export function Sidebar(props: any) {
  const [sidebarSize, changeSidebarSize] = useState("small");
  const [currentSize, changeCurrentSize] = useState("small");

  const navigate = useNavigate();
  const dispatch = useDispatch<any>();
  // const { selectedKey } = props;
  const [selectedKey, setSelectedKey] = useState(1);

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  const signoutHandler = () => {
    dispatch(signout());
  };

  useEffect(() => {
    if (window.location.pathname.split("/")[1] === "dashboard") {
      setSelectedKey(1);
    } else if (window.location.pathname.split("/")[1] === "screenOwner") {
      setSelectedKey(2);
    } else if (window.location.pathname.split("/")[2] === "campaigns") {
      setSelectedKey(3);
    } else if (window.location.pathname.split("/")[2] === "wallet") {
      setSelectedKey(4);
    } else if (window.location.pathname.split("/")[1] === "notification") {
      setSelectedKey(5);
    } else if (window.location.pathname.split("/")[1] === "contactUs") {
      setSelectedKey(6);
    } else if (window.location.pathname.split("/")[1] === "setting") {
      setSelectedKey(7);
    } else {
      // setSelectedKey(selectedKey);
    }

    // if (!userInfo) {
    //   navigate("/signin");
    // }
  }, [selectedKey]);

  return (
    <Hide below="md">
      <Flex
        pos="sticky"
        // left="5"
        h="100vh"
        // marginTop="2.5vh"
        // border="1px solid black"
        // boxShadow="0 4px 12px 0 rgba(0, 0, 0, 0.05)"
        w={sidebarSize === "small" ? "75px" : "300px"}
        flexDir="column"
        justifyContent="space-between"
        onMouseEnter={() => changeSidebarSize("large")}
        onMouseLeave={() => {
          if (currentSize === "small") {
            changeSidebarSize("small");
          }
        }}
      >
        <Flex
          p="1.5"
          flexDir="column"
          w="100%"
          alignItems={sidebarSize === "small" ? "center" : "flex-start"}
          // as="Sidebar"
        >
          {/* LOGO ICON */}
          <Flex
            // border="1px solid red"
            direction="row"
            py={sidebarSize === "small" ? "" : "0"}
            pl={sidebarSize === "small" ? "3" : "3"}
            w="100%"
            // onClick={() => {
            //   if (sidebarSize === "small") changeSidebarSize("large");
            //   else changeSidebarSize("small");
            // }}
            // onClick={() => navigate("/")}
            justify="space-between"
          >
            <Stack width="36px" height="36px" onClick={() => navigate("/")}>
              <Image
                // pt="0.5"
                src={Logo}
                // width="120px"
                // height="36px"
              />
            </Stack>
            {sidebarSize === "large" ? (
              <Stack
                py="2"
                align="center"
                // border="1px solid red"
                width="36px"
                height="36px"
              >
                {currentSize === "large" ? (
                  <BsArrowBarLeft
                    size="15px"
                    onClick={() => {
                      changeCurrentSize("small");
                      // changeSidebarSize("small");
                    }}
                  />
                ) : (
                  <BsArrowBarRight
                    size="15px"
                    onClick={() => {
                      changeCurrentSize("large");
                      changeSidebarSize("large");
                    }}
                  />
                )}
              </Stack>
            ) : null}
          </Flex>
        </Flex>
        {/* NAME AVATAR BUTTON */}
        <Flex
          // border="1px solid red"
          // pt={sidebarSize === "small" ? "2" : ""}
          // py=""
          px="2"
          width="100%"
          flexDir="column"
        >
          {userInfo && (
            <Flex
              py="3"
              px={sidebarSize === "large" ? "2" : "2"}
              width="100%"
              align="center"
              gap="2"
              borderRadius="5"
              // bgColor="red"
              bgColor="#F7F7F7"
              // border="1px solid #D7D7D7"
              onClick={() => {
                // if (SidebarSize == "small") changeSidebarSize("large");
                // else changeSidebarSize("small");
              }}
            >
              <Image
                width="40px"
                height="40px"
                rounded="100px"
                src={userInfo?.avatar}
              />
              <Flex
                flexDir="column"
                display={sidebarSize === "small" ? "none" : "flex"}
              >
                <Text
                  align="left"
                  fontSize="16px"
                  color="#131D30"
                  fontWeight="500"
                  mb="0"
                >
                  {userInfo?.name}
                </Text>
                <Text
                  align="left"
                  fontSize="14px"
                  color="#B4B4B4"
                  fontWeight="500"
                  mb="0"
                >
                  {userInfo?.isMaster ? "Screen Owner" : "Ad Agency"}
                </Text>
              </Flex>
            </Flex>
          )}
        </Flex>
        {/* MENU ITEM */}
        <Flex flexDir="column" flexGrow={1} gap="2" px="2" py="2">
          {userInfo && (
            <Flex
              cursor={"pointer"}
              _hover={{ bgColor: "#F6F6F6" }}
              bgColor={selectedKey === 1 ? "#FDFDFD" : ""}
              borderRadius="8px"
              onClick={() => {
                setSelectedKey(1);
                navigate("/dashboard");
              }}
            >
              <Box p="0.5" bgColor={selectedKey === 1 ? "#000000" : ""}></Box>
              <Flex
                align="center"
                gap="3"
                px={sidebarSize === "large" ? 4 : 4}
                py="2"
                color={selectedKey === 1 ? "#000000" : "#747474"}
              >
                <RxDashboard size="20px" />

                {sidebarSize === "large" ? (
                  <Text
                    className="text"
                    fontWeight={selectedKey === 1 ? 600 : ""}
                    fontSize="16px"
                  >
                    Dashboard
                  </Text>
                ) : null}
              </Flex>
            </Flex>
          )}
          {userInfo?.isMaster ? (
            <Flex
              cursor={"pointer"}
              _hover={{ bgColor: "#F6F6F6" }}
              bgColor={selectedKey === 2 ? "#FDFDFD" : ""}
              borderRadius="8px"
              onClick={() => {
                setSelectedKey(2);
                navigate("/my/screens");
              }}
            >
              <Box p="0.5" bgColor={selectedKey === 2 ? "#000000" : ""}></Box>
              <Flex
                align="center"
                gap="3"
                px={sidebarSize === "large" ? 4 : 4}
                py="2"
                color={selectedKey === 2 ? "#000000" : "#747474"}
              >
                <CgScreen size="20px" />

                {sidebarSize === "large" ? (
                  <Text
                    className="text"
                    fontWeight={selectedKey === 2 ? 600 : ""}
                    fontSize="16px"
                  >
                    Screens
                  </Text>
                ) : null}
              </Flex>
            </Flex>
          ) : null}
          {userInfo && (
            <Flex
              cursor={"pointer"}
              _hover={{ bgColor: "#F6F6F6" }}
              bgColor={selectedKey === 3 ? "#FDFDFD" : ""}
              borderRadius="8px"
              onClick={() => {
                setSelectedKey(3);
                navigate("/my/campaigns");
              }}
            >
              <Box p="0.5" bgColor={selectedKey === 3 ? "#000000" : ""}></Box>
              <Flex
                align="center"
                gap="3"
                px={sidebarSize === "large" ? 4 : 4}
                py="2"
                color={selectedKey === 3 ? "#000000" : "#747474"}
              >
                <MdOutlineCampaign size="25px" />

                {sidebarSize === "large" ? (
                  <Text
                    className="text"
                    fontWeight={selectedKey === 3 ? 600 : ""}
                    fontSize="16px"
                  >
                    Campaigns
                  </Text>
                ) : null}
              </Flex>
            </Flex>
          )}
          {userInfo && (
            <Flex
              cursor={"pointer"}
              _hover={{ bgColor: "#F6F6F6" }}
              bgColor={selectedKey === 4 ? "#FDFDFD" : ""}
              borderRadius="8px"
              onClick={() => {
                setSelectedKey(4);
                navigate("/my/wallet");
              }}
            >
              <Box p="0.5" bgColor={selectedKey === 4 ? "#000000" : ""}></Box>
              <Flex
                align="center"
                gap="4"
                px={sidebarSize === "large" ? 4 : 4}
                py="2"
                color={selectedKey === 4 ? "#000000" : "#747474"}
              >
                <LuWallet size="20px" />

                {sidebarSize === "large" ? (
                  <Text
                    className="text"
                    fontWeight={selectedKey === 4 ? 600 : ""}
                    fontSize="16px"
                  >
                    Wallet
                  </Text>
                ) : null}
              </Flex>
            </Flex>
          )}
          {userInfo && (
            <Flex
              cursor={"pointer"}
              _hover={{ bgColor: "#F6F6F6" }}
              bgColor={selectedKey === 5 ? "#FDFDFD" : ""}
              borderRadius="8px"
              onClick={() => {
                setSelectedKey(5);
                navigate("/notification");
              }}
            >
              <Box p="0.5" bgColor={selectedKey === 5 ? "#000000" : ""}></Box>
              <Flex
                align="center"
                gap="4"
                px={sidebarSize === "large" ? 4 : 4}
                py="2"
                color={selectedKey === 5 ? "#000000" : "#747474"}
              >
                <SlBell size="20px" />

                {sidebarSize === "large" ? (
                  <Text
                    className="text"
                    fontWeight={selectedKey === 5 ? 600 : ""}
                    fontSize="16px"
                  >
                    Notification
                  </Text>
                ) : null}
              </Flex>
            </Flex>
          )}
          <Flex
            cursor={"pointer"}
            _hover={{ bgColor: "#F6F6F6" }}
            bgColor={selectedKey === 6 ? "#FDFDFD" : ""}
            borderRadius="8px"
            onClick={() => {
              setSelectedKey(6);
              navigate("/contactUs");
            }}
          >
            <Box p="0.5" bgColor={selectedKey === 6 ? "#000000" : ""}></Box>
            <Flex
              align="center"
              gap="4"
              px={sidebarSize === "large" ? 4 : 4}
              py="2"
              color={selectedKey === 6 ? "#000000" : "#747474"}
            >
              <SlEarphonesAlt size="20px" />

              {sidebarSize === "large" ? (
                <Text
                  className="text"
                  fontWeight={selectedKey === 6 ? 600 : ""}
                  fontSize="16px"
                >
                  Contact
                </Text>
              ) : null}
            </Flex>
          </Flex>
          {userInfo && (
            <Flex
              cursor={"pointer"}
              _hover={{ bgColor: "#F6F6F6" }}
              bgColor={selectedKey === 7 ? "#FDFDFD" : ""}
              borderRadius="8px"
              onClick={() => {
                setSelectedKey(7);
                navigate("/setting");
              }}
            >
              <Box p="0.5" bgColor={selectedKey === 7 ? "#000000" : ""}></Box>
              <Flex
                align="center"
                gap="4"
                px={sidebarSize === "large" ? 4 : 4}
                py="2"
                color={selectedKey === 7 ? "#000000" : "#747474"}
              >
                <IoSettingsOutline size="20px" />

                {sidebarSize === "large" ? (
                  <Text
                    className="text"
                    fontWeight={selectedKey === 7 ? 600 : ""}
                    fontSize="16px"
                  >
                    Setting
                  </Text>
                ) : null}
              </Flex>
            </Flex>
          )}
        </Flex>
        {/* {window.location.pathname.split("/")[1] !== "signup" ||
          (window.location.pathname.split("/")[1] !== "signin" && ( */}
        <Flex
          p="5%"
          flexDir="column"
          w="100%"
          alignItems={sidebarSize === "small" ? "center" : "flex-start"}
          mb={4}
        >
          <Divider display={sidebarSize === "small" ? "none" : "none"} />
          <Flex
            align="center"
            gap="4"
            mt={4}
            px={sidebarSize === "large" ? 4 : 4}
            py="2"
            onClick={() => {
              if (userInfo) signoutHandler();
              else navigate("/signin");
            }}
            color={selectedKey === 8 ? "#000000" : "#747474"}
          >
            <AiOutlinePoweroff size="20px" color="#999999" />

            {sidebarSize === "large" ? (
              <Text
                className="text"
                fontWeight={selectedKey === 8 ? 600 : ""}
                fontSize="16px"
              >
                Log Out
              </Text>
            ) : null}
          </Flex>
        </Flex>
        {/* ))} */}
      </Flex>
    </Hide>
  );
}
