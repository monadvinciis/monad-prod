import { Text, Flex, Box, Hide, Show, Stack } from "@chakra-ui/react";
import { BsDot } from "react-icons/bs";
import type { ColumnsType } from "antd/es/table";

import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { Modal, Skeleton, Table } from "antd";
import { getAllCampaignListByScreenId } from "../../../Actions/campaignAction";
import { Excelexport, ResultNotFound } from "../../newCommans";
import { convertIntoDateAndTime } from "../../../utils/dateAndTimeUtils";
import { SingleCampaignHistory } from "../SingleCampaignHistory";
import { getScreenLogs } from "../../../Actions/screenActions";

export function CampaignTable(props: any) {
  const dispatch = useDispatch<any>();
  const { screenId } = props;
  const [page, setPage] = useState(1);

  const [paginationSize, setPaginationSize] = useState(
    props?.pagination?.pageSize || 6
  );

  const [dataSrc, setDataSrc] = useState<any>([]);
  const [campaign, setCampaign] = useState<any>();
  const [history, setHistory] = useState<any>();
  const [logsDataSrc, setLogsDataSrc] = useState<any>([]);

  const campaignListByScreenId = useSelector(
    (state: any) => state.campaignListByScreenId
  );
  const {
    loading: loadingAllCampaign,
    // error: errorAllCampaign,
    campaigns,
  } = campaignListByScreenId;

  const screenLogsGet = useSelector((state: any) => state.screenLogsGet);
  const {
    loading: loadingScreenLogs,
    error: errorScreenLogs,
    data: screenLogs,
  } = screenLogsGet;

  useEffect(() => {
    dispatch(getAllCampaignListByScreenId(screenId));
    dispatch(getScreenLogs(screenId));
  }, [dispatch, screenId]);

  useEffect(() => {
    if (campaigns) {
      setDataSrc(campaigns.map((c: any) => ({ key: c._id, data: c })));
    }
    if (screenLogs) {
      setLogsDataSrc(
        screenLogs?.allLogs
          ?.filter((l: any) => l.playVideo === campaign?.cid)
          ?.map((log: any) => ({ key: log._id, data: log }))
          ?.sort(
            (a: any, b: any) =>
              new Date(b.data.playTime).getTime() -
              new Date(a.data.playedTime).getTime()
          )
      );
    }
  }, [campaigns, screenLogs, campaign?.cid]);

  const columns: ColumnsType<any> = [
    {
      title: (
        <Text
          color="#000000"
          fontSize={{ base: "14px", lg: "lg" }}
          fontWeight="400"
          m="0"
        >
          Video
        </Text>
      ),
      dataIndex: "logo",
      key: "logo",
      render: (_, record) => (
        <Box
          as="video"
          src={`https://ipfs.io/ipfs/${record.data.cid}`}
          width="70px"
          height="70px"
        />
      ),
    },
    {
      title: (
        <Text
          color="#000000"
          fontSize={{ base: "14px", lg: "lg" }}
          fontWeight="400"
          m="0"
        >
          Campaign Name
        </Text>
      ),
      dataIndex: "campaignName",
      key: "campaignName",
      render: (_, record) => (
        <Text
          color="#000000"
          fontSize={{ base: "12px", lg: "md" }}
          fontWeight="400"
        >
          {record.data.campaignName}
        </Text>
      ),
    },
    {
      title: (
        <Text
          color="#000000"
          fontSize={{ base: "14px", lg: "lg" }}
          fontWeight="400"
          m="0"
        >
          Start Date
        </Text>
      ),
      dataIndex: "startDate",
      key: "startDate",
      render: (_, record) => (
        <Text
          color="#000000"
          fontSize={{ base: "12px", lg: "md" }}
          fontWeight="400"
          width={{ base: "100px", lg: "200px" }}
        >
          {convertIntoDateAndTime(record.data.startDate)}
        </Text>
      ),
    },
    {
      title: (
        <Text
          color="#000000"
          fontSize={{ base: "14px", lg: "lg" }}
          fontWeight="400"
          m="0"
        >
          End Date
        </Text>
      ),
      dataIndex: "startDate",
      key: "startDate",
      render: (_, record) => (
        <Text
          color="#000000"
          fontSize={{ base: "12px", lg: "md" }}
          fontWeight="400"
          width={{ base: "100px", lg: "200px" }}
        >
          {record?.data?.isDefaultCampaign
            ? "Default Campaign"
            : convertIntoDateAndTime(record.data.endDate)}
        </Text>
      ),
    },
    {
      title: (
        <Text
          color="#000000"
          fontSize={{ base: "14px", lg: "lg" }}
          fontWeight="400"
          m="0"
        >
          Total slots booked
        </Text>
      ),
      dataIndex: "totalSlotBooked",
      key: "totalSlotBooked",
      render: (_, record) => (
        <Text
          color="#000000"
          fontSize={{ base: "12px", lg: "md" }}
          fontWeight="400"
        >
          {record.data.totalSlotBooked}
        </Text>
      ),
    },
    {
      title: (
        <Text
          color="#000000"
          fontSize={{ base: "14px", lg: "lg" }}
          fontWeight="400"
          m="0"
        >
          Total slots played
        </Text>
      ),
      dataIndex: "totalSlotPlayed",
      key: "totalSlotPlayed",
      render: (_, record) => (
        <Text
          color="#000000"
          fontSize={{ base: "12px", lg: "md" }}
          fontWeight="400"
        >
          {record.data.totalSlotBooked - record.data.remainingSlots}
        </Text>
      ),
    },
    {
      title: (
        <Text
          color="#000000"
          fontSize={{ base: "14px", lg: "lg" }}
          fontWeight="400"
          m="0"
        >
          Remaining slots
        </Text>
      ),
      dataIndex: "remainingSlots",
      key: "remainingSlots",
      render: (_, record) => (
        <Text
          color="#000000"
          fontSize={{ base: "12px", lg: "md" }}
          fontWeight="400"
        >
          {record.data.remainingSlots}
        </Text>
      ),
    },
    {
      title: (
        <Text
          color="#000000"
          fontSize={{ base: "14px", lg: "lg" }}
          fontWeight="400"
          m="0"
        >
          Rent per slot
        </Text>
      ),
      dataIndex: "rentPerSlot",
      key: "rentPerSlot",
      render: (_, record) => (
        <Text
          color="#4339F2"
          fontSize={{ base: "12px", lg: "md" }}
          fontWeight="400"
        >
          ₹ {record.data.rentPerSlot}
        </Text>
      ),
    },
    {
      title: (
        <Text
          color="#000000"
          fontSize={{ base: "14px", lg: "lg" }}
          fontWeight="400"
          m="0"
        >
          Price
        </Text>
      ),
      dataIndex: "totalAmount",
      key: "totalAmount",
      render: (_, record) => (
        <Text
          color="#4339F2"
          fontSize={{ base: "12px", lg: "md" }}
          fontWeight="400"
        >
          ₹ {record.data.totalAmount}
        </Text>
      ),
    },
    {
      title: (
        <Text
          color="#000000"
          fontSize={{ base: "14px", lg: "lg" }}
          fontWeight="400"
          m="0"
        >
          Status
        </Text>
      ),
      dataIndex: "status",
      key: "status",
      render: (_, record) => (
        <Flex>
          <BsDot
            color={
              record.data.status === "Active"
                ? "#00D615"
                : record.data.status === "Deleted"
                ? "#E93A03"
                : "yellow"
            }
            size="20px"
          />
          <Text color="#403F45" fontSize="sm" pl="2">
            {record.data.status}
          </Text>
        </Flex>
      ),
      filters: [
        {
          text: "Active",
          value: "Active",
        },
        {
          text: "Pause",
          value: "Pause",
        },
        {
          text: "Completed",
          value: "Completed",
        },
        {
          text: "Pending",
          value: "Pending",
        },
        {
          text: "Deleted",
          value: "Deleted",
        },
        {
          text: "Hold",
          value: "Hold",
        },
      ],
      onFilter: (value: any, record: any) => {
        return record?.data?.status?.startsWith(value);
      },

      filterSearch: true,
      width: "10%",
    },
  ];

  const columnsLogs: ColumnsType<any> = [
    {
      title: (
        <Text
          color="#000000"
          fontSize={{ base: "14px", lg: "lg" }}
          fontWeight="400"
          m="0"
        >
          No.
        </Text>
      ),
      dataIndex: "logo",
      key: "logo",
      render: (_, values, index) => (
        <Text
          color="#000000"
          fontSize={{ base: "14px", lg: "lg" }}
          fontWeight="400"
          m="0"
        >
          {(page - 1) * paginationSize + index + 1}
        </Text>
      ),
    },
    {
      title: (
        <Text
          color="#000000"
          fontSize={{ base: "14px", lg: "lg" }}
          fontWeight="400"
          m="0"
        >
          Time of playback
        </Text>
      ),
      dataIndex: "logo",
      key: "logo",
      render: (_, record, index) => (
        <Text>
          {/* {new Date(record.data.playTime.split(".")[0]).toLocaleString(
            "en-IN",
            { timeZone: "IST" }
          )} */}
          {convertIntoDateAndTime(
            new Date(
              new Date(record.data.playTime.split(".")[0]).getTime() +
                (5 * 60 + 30) * 60000
            )
          )}
          {/* {record.data.playTime.split(".")[0]} */}
        </Text>
      ),
    },
  ];

  return (
    <Box>
      {loadingAllCampaign ? (
        <Skeleton active />
      ) : dataSrc?.length && campaigns?.length > 0 ? (
        <div>
          {loadingScreenLogs ? (
            <Skeleton active />
          ) : errorScreenLogs ? (
            <Text>Error in getting screen logs: {errorScreenLogs}</Text>
          ) : (
            <Modal
              // style={{}}
              footer={[]}
              centered
              // width={1000}
              open={history}
              onOk={() => setHistory(false)}
              onCancel={() => {
                setPage(1);
                setHistory(false);
              }}
            >
              <Text fontSize="20" fontWeight="600">
                Last Played History
              </Text>
              <Excelexport
                excelData={logsDataSrc?.map((data: any, index: any) => {
                  return {
                    "SN.": index + 1,
                    "Time of playback": convertIntoDateAndTime(
                      data?.data?.playTime?.split(".")[0]
                    ),
                  };
                })}
                fileName={`${campaign?.campaignName}`}
              />
              <Flex justify="space-between">
                <Text color="#000000">{campaign?.campaignName}</Text>
                <Text>
                  {
                    screenLogs?.allLogs.filter(
                      (l: any) => l.playVideo === campaign?.cid
                    )?.length
                  }{" "}
                  times
                </Text>
              </Flex>
              <Table
                columns={columnsLogs}
                dataSource={logsDataSrc}
                // onChange={onChange}
                scroll={{ y: 500 }}
                pagination={{
                  onChange(current, pageSize) {
                    setPage(current);
                    setPaginationSize(pageSize);
                  },
                  defaultPageSize: 7,
                  hideOnSinglePage: false,
                  showSizeChanger: false,
                }}
              />
            </Modal>
          )}
          {/* it will active for mobile view only*/}
          <Show below="md">
            <Stack spacing="2">
              {campaigns?.map((campaign: any, index: any) => (
                <SingleCampaignHistory key={index} campaign={campaign} />
              ))}
            </Stack>
          </Show>
          {/* it will active for desktop view only*/}
          <Hide below="md">
            <Table
              columns={columns}
              dataSource={dataSrc || []}
              scroll={{ x: 300 }}
              pagination={{ pageSize: 6 }}
              onRow={(record: any, rowIndex: any) => {
                return {
                  onClick: (event: any) => {
                    setCampaign(record.data);
                    setHistory(!history);
                  },
                };
              }}
            />
          </Hide>
        </div>
      ) : (
        <ResultNotFound />
      )}
    </Box>
  );
}
