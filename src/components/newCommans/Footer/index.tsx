import { Box, Center, Flex, Show, Stack, Text } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { CgScreen } from "react-icons/cg";
import { FaRegUser } from "react-icons/fa";
import { LuWallet } from "react-icons/lu";
import { MdOutlineCampaign } from "react-icons/md";
import { RxDashboard } from "react-icons/rx";
import { useNavigate } from "react-router-dom";

export function Footer(props: any) {
  const [selectedKey, setSelectedKey] = useState<any>(1);
  const navigate = useNavigate();
  useEffect(() => {
    if (window.location.pathname.split("/")[1] === "dashboard") {
      setSelectedKey(1);
    } else if (window.location.pathname.split("/")[1] === "screenOwner") {
      setSelectedKey(2);
    } else if (window.location.pathname.split("/")[2] === "campaigns") {
      setSelectedKey(3);
    } else if (window.location.pathname.split("/")[2] === "wallet") {
      setSelectedKey(4);
    } else if (window.location.pathname.split("/")[1] === "setting") {
      setSelectedKey(5);
    }

    // if (!userInfo) {
    //   navigate("/signin");
    // }
  }, [selectedKey]);

  return (
    <Show below="md">
      <Flex gap="10" px="3" bgColor="#FFFFFF" className="footer">
        <Stack
          spacing={"4"}
          onClick={() => {
            setSelectedKey(1);
            navigate("/dashboard");
          }}
        >
          <Box
            height="3px"
            bgColor={selectedKey === 1 ? "#000000" : ""}
            borderRadius="14px"
          ></Box>
          <Center
            color={selectedKey === 1 ? "#000000" : "#747474"}
            flexDir="column"
            gap="2"
          >
            <RxDashboard size="20px" />
            <Text fontSize="10px" m="0" fontWeight="400" pb="2">
              Dashboard
            </Text>
          </Center>
        </Stack>
        <Stack
          spacing={"4"}
          onClick={() => {
            setSelectedKey(2);
            navigate("/my/screens");
          }}
        >
          <Box
            height="3px"
            bgColor={selectedKey === 2 ? "#000000" : ""}
            borderRadius="14px"
          ></Box>
          <Center
            color={selectedKey === 2 ? "#000000" : "#747474"}
            flexDir="column"
            gap="2"
          >
            <CgScreen size="20px" />
            <Text fontSize="10px" m="0" fontWeight="400" pb="2">
              Screen
            </Text>
          </Center>
        </Stack>
        <Stack
          spacing={"4"}
          onClick={() => {
            setSelectedKey(3);
            navigate("/my/campaigns");
          }}
        >
          <Box
            height="3px"
            bgColor={selectedKey === 3 ? "#000000" : ""}
            borderRadius="14px"
          ></Box>
          <Center
            color={selectedKey === 3 ? "#000000" : "#747474"}
            flexDir="column"
            gap="2"
          >
            <MdOutlineCampaign size="20px" />
            <Text fontSize="10px" m="0" fontWeight="400" pb="2">
              Campaign
            </Text>
          </Center>
        </Stack>
        <Stack
          spacing={"4"}
          onClick={() => {
            setSelectedKey(4);
            navigate("/my/wallet");
          }}
        >
          <Box
            height="3px"
            bgColor={selectedKey === 4 ? "#000000" : ""}
            borderRadius="14px"
          ></Box>
          <Center
            color={selectedKey === 4 ? "#000000" : "#747474"}
            flexDir="column"
            gap="2"
          >
            <LuWallet size="20px" />
            <Text fontSize="10px" m="0" fontWeight="400" pb="2">
              Wallet
            </Text>
          </Center>
        </Stack>
        <Stack
          spacing={"4"}
          onClick={() => {
            setSelectedKey(5);
            navigate("/setting");
          }}
        >
          <Box
            height="3px"
            bgColor={selectedKey === 5 ? "#000000" : ""}
            borderRadius="14px"
          ></Box>
          <Center
            color={selectedKey === 5 ? "#000000" : "#747474"}
            flexDir="column"
            gap="2"
          >
            <FaRegUser size="20px" />
            <Text fontSize="10px" m="0" fontWeight="400" pb="2">
              Profile
            </Text>
          </Center>
        </Stack>
      </Flex>
    </Show>
  );
}
