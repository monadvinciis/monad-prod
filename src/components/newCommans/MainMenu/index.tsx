import { Box, Flex, Text } from "@chakra-ui/react";
import { useState } from "react";
import { CgScreen } from "react-icons/cg";
import { LuWallet } from "react-icons/lu";
import { MdOutlineCampaign } from "react-icons/md";
import { RxDashboard } from "react-icons/rx";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { SlBell, SlEarphonesAlt } from "react-icons/sl";
import { IoSettingsOutline } from "react-icons/io5";
import { AiOutlinePoweroff } from "react-icons/ai";
import { useDispatch } from "react-redux";
import { signout } from "../../../Actions/userActions";

export function MainMenu(props: any) {
  const navigate = useNavigate();
  const dispatch = useDispatch<any>();
  const [collapsed, setCollapsed] = useState(false);
  const { selectedKey } = props;

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  const signoutHandler = () => {
    dispatch(signout());
    navigate("/");
  };

  return (
    <Flex
      border="1px"
      height={"100%"}
      m="0"
      flexDirection={"column"}
      borderColor="#EBEBEB"
      bgColor="#FFFFFF"
      width={collapsed ? "5%" : "22%"}
    >
      <Flex flexDir="column" flexGrow={1} gap="4" px="5">
        <Flex py="2"></Flex>
        <Flex
          cursor={"pointer"}
          _hover={{ bgColor: "#F6F6F6" }}
          bgColor={selectedKey === 1 ? "#FDFDFD" : ""}
          borderRadius="8px"
          onClick={() => {
            navigate("/dashboard");
          }}
        >
          <Box p="0.5" bgColor={selectedKey === 1 ? "#000000" : ""}></Box>
          <Flex
            align="center"
            gap="3"
            px={collapsed ? 2 : 5}
            py="2"
            color={selectedKey === 1 ? "#000000" : "#747474"}
          >
            <RxDashboard size="20px" />

            {!collapsed ? (
              <Text
                className="text"
                fontWeight={selectedKey === 1 ? 600 : ""}
                fontSize="16px"
              >
                Dashboard
              </Text>
            ) : null}
          </Flex>
        </Flex>
        {userInfo?.isMaster ? (
          <Flex
            cursor={"pointer"}
            _hover={{ bgColor: "#F6F6F6" }}
            bgColor={selectedKey === 2 ? "#FDFDFD" : ""}
            borderRadius="8px"
            onClick={() => {
              navigate("/my/screens");
            }}
          >
            <Box p="0.5" bgColor={selectedKey === 2 ? "#000000" : ""}></Box>
            <Flex
              align="center"
              gap="3"
              px={collapsed ? 2 : 5}
              py="2"
              color={selectedKey === 2 ? "#000000" : "#747474"}
            >
              <CgScreen size="20px" />

              {!collapsed ? (
                <Text
                  className="text"
                  fontWeight={selectedKey === 2 ? 600 : ""}
                  fontSize="16px"
                >
                  Screens
                </Text>
              ) : null}
            </Flex>
          </Flex>
        ) : null}
        <Flex
          cursor={"pointer"}
          _hover={{ bgColor: "#F6F6F6" }}
          bgColor={selectedKey === 3 ? "#FDFDFD" : ""}
          borderRadius="8px"
          onClick={() => {
            navigate("/my/campaigns");
          }}
        >
          <Box p="0.5" bgColor={selectedKey === 3 ? "#000000" : ""}></Box>
          <Flex
            align="center"
            gap="3"
            px={collapsed ? 2 : 5}
            py="2"
            color={selectedKey === 3 ? "#000000" : "#747474"}
          >
            <MdOutlineCampaign size="25px" />

            {!collapsed ? (
              <Text
                className="text"
                fontWeight={selectedKey === 3 ? 600 : ""}
                fontSize="16px"
              >
                Campaigns
              </Text>
            ) : null}
          </Flex>
        </Flex>
        <Flex
          cursor={"pointer"}
          _hover={{ bgColor: "#F6F6F6" }}
          bgColor={selectedKey === 4 ? "#FDFDFD" : ""}
          borderRadius="8px"
          onClick={() => {
            navigate("/my/wallet");
          }}
        >
          <Box p="0.5" bgColor={selectedKey === 4 ? "#000000" : ""}></Box>
          <Flex
            align="center"
            gap="4"
            px={collapsed ? 2 : 5}
            py="2"
            color={selectedKey === 4 ? "#000000" : "#747474"}
          >
            <LuWallet size="20px" />

            {!collapsed ? (
              <Text
                className="text"
                fontWeight={selectedKey === 4 ? 600 : ""}
                fontSize="16px"
              >
                My Wallet
              </Text>
            ) : null}
          </Flex>
        </Flex>
        <Flex
          cursor={"pointer"}
          _hover={{ bgColor: "#F6F6F6" }}
          bgColor={selectedKey === 5 ? "#FDFDFD" : ""}
          borderRadius="8px"
          onClick={() => {
            navigate("/notification");
          }}
        >
          <Box p="0.5" bgColor={selectedKey === 5 ? "#000000" : ""}></Box>
          <Flex
            align="center"
            gap="4"
            px={collapsed ? 2 : 5}
            py="2"
            color={selectedKey === 5 ? "#000000" : "#747474"}
          >
            <SlBell size="20px" />

            {!collapsed ? (
              <Text
                className="text"
                fontWeight={selectedKey === 5 ? 600 : ""}
                fontSize="16px"
              >
                Notification
              </Text>
            ) : null}
          </Flex>
        </Flex>
        <Flex
          cursor={"pointer"}
          _hover={{ bgColor: "#F6F6F6" }}
          bgColor={selectedKey === 6 ? "#FDFDFD" : ""}
          borderRadius="8px"
          onClick={() => {
            navigate("/contactUs");
          }}
        >
          <Box p="0.5" bgColor={selectedKey === 6 ? "#000000" : ""}></Box>
          <Flex
            align="center"
            gap="4"
            px={collapsed ? 2 : 5}
            py="2"
            color={selectedKey === 6 ? "#000000" : "#747474"}
          >
            <SlEarphonesAlt size="20px" />

            {!collapsed ? (
              <Text
                className="text"
                fontWeight={selectedKey === 6 ? 600 : ""}
                fontSize="16px"
              >
                Contact Us
              </Text>
            ) : null}
          </Flex>
        </Flex>
        <Flex
          cursor={"pointer"}
          _hover={{ bgColor: "#F6F6F6" }}
          bgColor={selectedKey === 7 ? "#FDFDFD" : ""}
          borderRadius="8px"
          onClick={() => {
            navigate("/setting");
          }}
        >
          <Box p="0.5" bgColor={selectedKey === 7 ? "#000000" : ""}></Box>
          <Flex
            align="center"
            gap="4"
            px={collapsed ? 2 : 5}
            py="2"
            color={selectedKey === 7 ? "#000000" : "#747474"}
          >
            <IoSettingsOutline size="20px" />

            {!collapsed ? (
              <Text
                className="text"
                fontWeight={selectedKey === 7 ? 600 : ""}
                fontSize="16px"
              >
                Setting
              </Text>
            ) : null}
          </Flex>
        </Flex>
      </Flex>

      <Flex
        cursor={"pointer"}
        flexShrink={0}
        _hover={{ bgColor: "#F6F6F6" }}
        borderRadius="8px"
        onClick={signoutHandler}
        px="5"
        mb={"10px"}
      >
        <Box p="0.5" bgColor={selectedKey === 8 ? "#000000" : ""}></Box>
        <Flex
          align="center"
          gap="4"
          px={collapsed ? 2 : 5}
          py="2"
          color={selectedKey === 8 ? "#000000" : "#747474"}
        >
          <AiOutlinePoweroff size="20px" color="#999999" />

          {!collapsed ? (
            <Text
              className="text"
              fontWeight={selectedKey === 8 ? 600 : ""}
              fontSize="16px"
            >
              Log Out
            </Text>
          ) : null}
        </Flex>
      </Flex>
    </Flex>
  );
}
