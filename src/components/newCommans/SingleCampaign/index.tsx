import { Box, Flex, SimpleGrid, Stack, Text } from "@chakra-ui/react";
import { convertToDate } from "../../../utils/dateAndTimeUtils";
import { useNavigate } from "react-router-dom";
import { Dropdown } from "antd";
import { HiDotsHorizontal } from "react-icons/hi";
import { GoInfo } from "react-icons/go";
import { useSelector } from "react-redux";
import { MediaContainer } from "../MediaContainer";

export function SingleCampaign(props: any) {
  const { data } = props;
  const { campaign, listOfScreens } = data;
  const navigate = useNavigate();

  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  const items = [
    {
      key: "1",
      label: (
        <Flex
          gap="2"
          color="#000000"
          align="center"
          fontSize="14px"
          fontWeight="400"
          onClick={() =>
            navigate(
              `/campaigns/details/${
                data?.listOfCampaigns?.filter(
                  (camp: any) => camp.status === "Active"
                )?.length > 0
                  ? data?.listOfCampaigns
                      ?.filter((camp: any) => camp.status === "Active")
                      ?.map((c: any) => c._id)[0]
                  : campaign._id
              }`
            )
          }
        >
          <GoInfo />
          <Text m="0">View detail</Text>
        </Flex>
      ),
    },
    userInfo?.isMaster
      ? {
          key: "2",
          label: (
            <Flex
              gap="2"
              color="#000000"
              align="center"
              fontSize="14px"
              fontWeight="400"
              onClick={() => props?.handleSelectCampaignForTemplete(campaign)}
            >
              <Text m="0">Create campaign from templet</Text>
            </Flex>
          ),
        }
      : null,
  ];

  return (
    <Stack>
      <Box
        bgColor="#FFFFFF"
        py="3"
        px="2"
        borderRadius="8px"
        width="100%"
        onClick={() => props?.handelSelectScreens(props?.index)}
        _hover={{
          boxShadow: "-5px 5px 5px rgba(0, 0, 0, 0.1)", // apply a larger shadow on hover
          transition: "box-shadow 0.3s", // add a smooth transition
          transform: "scale(1.01)",
        }}
        onDoubleClick={() => {
          navigate(
            `/campaigns/details/${
              data?.listOfCampaigns?.filter(
                (camp: any) => camp.status === "Active"
              )?.length > 0
                ? data?.listOfCampaigns
                    ?.filter((camp: any) => camp.status === "Active")
                    ?.map((c: any) => c._id)[0]
                : campaign._id
            }`
          );
        }}
        backgroundColor={
          props?.index === props?.selectedBox ? "#ebf1f2" : "#FFFFFF"
        }
      >
        <Flex gap={{ base: "2", lg: "8" }}>
          <Stack
            width={{ base: "142px", lg: "224px" }}
            height={{ base: "120px", lg: "174px" }}
          >
            <MediaContainer
              cid={campaign.cid}
              width={{ base: "142px", lg: "224px" }}
              height={{ base: "120px", lg: "174px" }}
            />
            {/* {campaign?.fileType.split("/")[0] === "video" ||
            campaign?.fileType === "" ? (
              <Box
                as="video"
                src={campaign?.video || campaign?.thumbnail}
                // alt={campaign?.thumbnail}
                width={{ base: "142px", lg: "224px" }}
                height={{ base: "120px", lg: "174px" }}
                borderRadius="5"
                objectFit="cover"
                controls={false}
                // onClick={() =>
                //   navigate(`/campaigns/details/${campaign._id}`)
                // }
              />
            ) : campaign?.fileType.split("/")[0] === "image" ? (
              <Image
                src={campaign?.video || campaign?.thumbnail}
                alt="Image"
                width={{ base: "142px", lg: "224px" }}
                height={{ base: "120px", lg: "174px" }}
              ></Image>
            ) : null} */}
          </Stack>
          <Box width="100%">
            <Flex justifyContent="space-between">
              <Text fontSize="md" fontWeight="700" color="#131D30" m="0">
                {campaign?.campaignName}
              </Text>

              <Dropdown
                menu={{
                  items,
                }}
                placement="bottomRight"
              >
                <Box pr="8" pt="1">
                  <HiDotsHorizontal
                    size="20px"
                    color={
                      campaign?.status === "Active"
                        ? "green"
                        : campaign?.status === "Complete"
                        ? "green"
                        : campaign?.status === "Pending"
                        ? "orange"
                        : campaign?.status === "Pause"
                        ? "red"
                        : "black"
                    }
                  />
                </Box>
              </Dropdown>
            </Flex>

            <SimpleGrid
              columns={[2, 2, 2]}
              spacing={{ base: "2", lg: "8" }}
              fontSize={{ base: "12px", lg: "sm" }}
            >
              <Box gap="0">
                <Text color="#808080" m="0">
                  BrandName
                </Text>
                <Text color="#808080" m="0">
                  Start Date
                </Text>
                <Text color="#808080" m="0">
                  End Date
                </Text>
                <Text color="#808080" m="0">
                  Budget
                </Text>

                <Text color="#808080" m="0">
                  Selected Screens
                </Text>
              </Box>
              <Box>
                <Text color="#808080" m="0" fontWeight="600">
                  {campaign?.brandName || "DEFAULT CAMPAIGN"}
                </Text>
                <Text color="#808080" m="0" fontWeight="600">
                  {convertToDate(campaign?.startDate)}
                </Text>

                <Text color="#808080" m="0" fontWeight="600">
                  {campaign?.isDefault
                    ? "DEFAULT CAMPAIGN"
                    : convertToDate(campaign?.endDate)}{" "}
                </Text>
                <Text color="#808080" m="0" fontWeight="600">
                  ₹ {campaign?.totalAmount}
                </Text>
                <Text color="#808080" m="0" fontWeight="600">
                  {listOfScreens?.length} Screen
                </Text>
              </Box>
            </SimpleGrid>
          </Box>
        </Flex>
      </Box>
    </Stack>
  );
}
