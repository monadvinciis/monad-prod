import { Box, Image, SimpleGrid, Text, Flex } from "@chakra-ui/react";
import { CiLocationOn } from "react-icons/ci";

export function SingleScreenForCampaignCreate(props: any) {
  const { screen } = props;
  // const handleClick = () => {
  //   window.open(
  //     `/screen/screenDetail/${screen?._id}`,
  //     "_blank",
  //     "noopener,noreferrer"
  //   );
  // };
  return (
    <Flex gap="4">
      <Image
        src={
          screen?.images
            ? `https://ipfs.io/ipfs/${screen.images[0]}`
            : screen?.image
        }
        width={{ base: "159px", lg: "179px" }}
        height="132px"
        borderRadius="4px"
        // onClick={handleClick}
      />
      <Box fontSize="sm" fontWeight="400" color="#808080">
        <Text
          fontSize={{ base: "md", lg: "lg" }}
          fontWeight="700"
          color="#131D30"
          m="0"
          // onClick={handleClick}
        >
          {screen?.name}
        </Text>
        <Flex gap="0" align="center">
          <CiLocationOn color="#069D15" />
          <Text
            m="0"
            fontSize={{ base: "12", lg: "14" }}
          >{`${screen?.districtCity}, ${screen?.stateUT}`}</Text>
        </Flex>
        <SimpleGrid
          columns={[2, 2, 2]}
          spacing={{ base: "4", lg: "8" }}
          fontSize={{ base: "12px", lg: "md" }}
        >
          <Box>
            <Text m="0" fontSize="12">
              Slot Play Per Day
            </Text>
            <Text m="0" fontSize="12">
              Rent Per Day
            </Text>
            <Text m="0" fontSize="12">
              Avg. Foot Fall
            </Text>
          </Box>
          <Box>
            <Text m="0" fontSize="12">
              {screen?.slotsPlayPerDay} times
            </Text>
            <Text m="0" fontSize="12">
              Rs. {screen?.rentPerDay}
            </Text>
            <Text m="0" fontSize="12">
              {screen?.additionalData?.averageDailyFootfall || 0}
            </Text>
          </Box>
        </SimpleGrid>
      </Box>
    </Flex>
  );
}
