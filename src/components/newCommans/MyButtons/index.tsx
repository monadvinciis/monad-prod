import { Button, Flex, Text } from "@chakra-ui/react";

export function AddButton(props: any) {
  const { handelClick, label } = props;
  return (
    <Button
      borderRadius="full"
      color="#000000"
      bg="#FFFFFF"
      height="37px"
      border="1px"
      borderColor="#000000"
      _hover={{ color: "#FFFFFF", bgColor: "#000000" }}
      onClick={handelClick}
    >
      <Flex align="center" gap={1}>
        <Text m="0" fontSize="24px">
          +
        </Text>
        <Text m="0" fontSize="md">
          {label}
        </Text>
      </Flex>
    </Button>
  );
}

export function BackButton(props: any) {
  return <Button></Button>;
}

export function NextButton(props: any) {
  return <Button></Button>;
}

export function MyButton(props: any) {
  const {
    handelClick,
    label,
    icon,
    color,
    bgColor,
    isDisabled,
    isLoading,
    loadingText,
  } = props;

  return (
    <Button
      variant="outline"
      borderRadius="full"
      color={color || "#000000"}
      bg={bgColor || "#FFFFFF"}
      height="37px"
      px="4"
      _hover={{ color: bgColor || "#FFFFFF", bgColor: color || "#000000" }}
      onClick={handelClick}
      leftIcon={icon || null}
      isDisabled={isDisabled || false}
      isLoading={isLoading || false}
      loadingText={loadingText || ""}
    >
      <Text m="0" fontSize="md">
        {label}
      </Text>
    </Button>
  );
}
