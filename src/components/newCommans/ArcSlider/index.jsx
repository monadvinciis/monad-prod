import { Stack } from "@chakra-ui/react";
import { useState } from "react";
import { RoundSlider } from "mz-react-round-slider";

export const ArcSlider = (props) => {
  const [acamp, setAcamp] = useState([
    {
      value: props?.data?.filter((d) => d.status === "Active")?.length,
    },
  ]);
  const [ocamp, setOcamp] = useState(
    props?.data?.filter((d) => d.status === "Active")?.length +
      props?.data?.filter((d) => d.status === "Pause")?.length
  );
  return (
    <Stack border="" align="center" height="200px">
      <RoundSlider
        pathStartAngle={180}
        pathEndAngle={0}
        max={ocamp}
        min={0}
        // pathRadius={150}
        pathThickness={25}
        pathBgColor={"#E5EAFC"}
        // pathInnerBgColor={"#efefef"}
        // pathBorder={2}
        // pathBorderColor={"#28586c"}
        textOffsetY={0}
        textFontSize={24}
        textSuffix={" "}
        textPrefix={" "}
        pointers={acamp}
        textBelow={"Out of 7 listed campaigns"}
        pointerBorder={5}
        pointerBorderColor={"#ffffff"}
        pointerRadius={20}
        pointer
        SvgDefs={
          <>
            <linearGradient id="pointer" x1="0%" y1="0%" x2="0%" y2="100%">
              <stop offset="0%" stopColor="#4A3AFF" />
              <stop offset="100%" stopColor="#4A3AFF" />
            </linearGradient>

            <linearGradient
              id="pointer-selected"
              x1="0%"
              y1="0%"
              x2="0%"
              y2="100%"
            >
              <stop offset="0%" stopColor="#4A3AFF" />
              <stop offset="100%" stopColor="#4A3AFF" />
            </linearGradient>

            <linearGradient id="connection" x1="0%" y1="0%" x2="0%" y2="100%">
              <stop offset="0%" stopColor="#4A3AFF" />
              <stop offset="100%" stopColor="#4A3AFF50" />
            </linearGradient>
          </>
        }
        pointerBgColor={"url(#pointer)"}
        pointerBgColorSelected={"url(#pointer-selected)"}
        connectionBgColor={"url(#connection)"}
        // disabled={true}
        keyboardDisabled={true}
        mousewheelDisabled={true}
        // onChange={setPointers}
      />
      {/* <Text>Active Campaigns</Text> */}
    </Stack>
  );
};
