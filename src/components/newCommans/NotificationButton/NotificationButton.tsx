import { Center, Text, Flex } from "@chakra-ui/react";

export function NotificationButton(props: any) {
  const { icon, value } = props;
  return (
    <Flex>
      <Center
        border="1px"
        borderColor="#D2D2D2"
        borderRadius="100%"
        width="44px"
        height="44px"
        justifyContent="center"
        alignContent="center"
      >
        {icon}
      </Center>
      <Center
        borderRadius="100%"
        bgColor="red"
        width="25px"
        height="25px"
        justifyContent="center"
        alignContent="center"
        mt="-3"
        ml="-3"
      >
        <Text color="#FFFFFF" m="0" fontSize="12px" fontWeight="600">
          {value}
        </Text>
      </Center>
    </Flex>
  );
}

export function PleaRequestButton(props: any) {
  const { icon, value } = props;
  return (
    <Flex>
      <Center
        width="34px"
        height="34px"
        justifyContent="center"
        alignContent="center"
      >
        {icon}
      </Center>
      <Center
        borderRadius="100%"
        bgColor="red"
        width="15px"
        height="15px"
        justifyContent="center"
        alignContent="center"
        mt="-1"
        ml="-3"
      >
        <Text color="#FFFFFF" m="0" fontSize="12px" fontWeight="600">
          {value}
        </Text>
      </Center>
    </Flex>
  );
}
