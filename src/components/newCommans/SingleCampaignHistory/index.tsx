import { Stack, Text, Flex, SimpleGrid, Box } from "@chakra-ui/react";
import { convertIntoDateAndTime } from "../../../utils/dateAndTimeUtils";

export function SingleCampaignHistory(props: any) {
  const { campaign } = props;
  return (
    <Stack p="4" bgColor="#FFFFFF">
      <Text m="0" fontSize="md" fontWeight="700" color="#56585C">
        {campaign?.campaignName}
      </Text>
      <Text m="0" fontSize="12px" fontWeight="300" color="#909090">
        Start Date - {convertIntoDateAndTime(campaign?.startDate)}
      </Text>
      <Flex gap="4">
        <Box
          as="video"
          src={campaign?.video}
          width={{ base: "167px", lg: "316px" }}
          height={{ base: "120px", lg: "212px" }}
        />
        <SimpleGrid
          columns={[2, 2, 2]}
          spacing={{ base: "8", lg: "8" }}
          fontSize={{ base: "12px", lg: "sm" }}
        >
          <Stack>
            <Text color="#808080" m="0">
              Price -{" "}
            </Text>
            <Text color="#808080" m="0">
              Status -{" "}
            </Text>

            <Text color="#808080" m="0">
              Slot Booked -{" "}
            </Text>
            <Text color="#808080" m="0">
              Total Played -{" "}
            </Text>
          </Stack>
          <Stack>
            <Text color="#808080" m="0">
              ₹{campaign?.vault}
            </Text>
            <Text
              color={
                campaign?.status === "Active"
                  ? "#1D7D14"
                  : campaign?.status === "Deleted"
                  ? "#E93A03"
                  : "yellow"
              }
              m="0"
            >
              {campaign?.status}
            </Text>
            <Text color="#808080" m="0">
              {campaign?.totalSlotBooked}
            </Text>
            <Text color="#808080" m="0">
              {campaign?.totalSlotBooked - campaign?.remainingSlots}
            </Text>
            <Text color="#FD5D5D" m="0" fontWeight="600"></Text>
          </Stack>
        </SimpleGrid>
      </Flex>
    </Stack>
  );
}
