import { Box, Text, Flex, Image, Hide, Stack } from "@chakra-ui/react";
import { Dropdown } from "antd";
import { CiLocationOn } from "react-icons/ci";
import { HiDotsHorizontal } from "react-icons/hi";
// import { RiDeleteBin6Line } from "react-icons/ri";
import { useNavigate } from "react-router-dom";
import { GoInfo } from "react-icons/go";
import { useEffect, useState } from "react";
import { noimage } from "../../../assets/Images";

export function SingleScreen(props: any) {
  const { screen } = props;
  const [images, setImages] = useState<any>([]);
  const navigate = useNavigate();

  const numberFormat = (value: any) => {
    return new Intl.NumberFormat("en-IN").format(value);
  };
  useEffect(() => {
    if (screen) {
      const x = [];
      for (let i = 0; i < 4; i++) {
        if (screen?.images[i]) {
          x.push(`https://ipfs.io/ipfs/${screen?.images[i]}`);
        } else {
          x.push(noimage);
        }
      }
      setImages(x);
    }
  }, [screen]);

  const confirm = (e: React.MouseEvent<HTMLElement> | undefined) => {
    props?.handleDeleteScreen(screen?._id);
  };

  const items = [
    {
      key: "1",
      label: (
        <Flex
          gap="2"
          color="#000000"
          align="center"
          fontSize="14px"
          fontWeight="400"
          onClick={() =>
            navigate(`/screen/screenDetail/${screen?._id}`, {
              state: { path: "/my/screens" },
            })
          }
        >
          <GoInfo />
          <Text m="0">View detail</Text>
        </Flex>
      ),
    },
    // {
    //   key: "2",
    //   label: (
    //     <Popconfirm
    //       title="Delete screen"
    //       description="Do you really want to delete this screen?"
    //       onConfirm={confirm}
    //       okText="Yes"
    //       cancelText="No"
    //     >
    //       <Flex
    //         gap="2"
    //         color="#D71111"
    //         align="center"
    //         fontSize="14px"
    //         fontWeight="400"
    //       >
    //         <RiDeleteBin6Line />
    //         <Text m="0">Remove Screen</Text>
    //       </Flex>
    //     </Popconfirm>
    //   ),
    // },
  ];
  return (
    <Box
      p="2"
      borderRadius="12px"
      border="1px"
      borderColor="#EBEBEB"
      bgColor="#FFFFFF"
      width="100%"
      onClick={() => props?.handelSelectScreens(props?.index)}
      // onClick={() => navigate(`/screen/screenDetail/${screen?._id}`)}
      _hover={{
        boxShadow: "-5px 5px 5px rgba(0, 0, 0, 0.1)", // apply a larger shadow on hover
        transition: "box-shadow 0.3s", // add a smooth transition
        transform: "scale(1.01)",
      }}
      onDoubleClick={() => {
        navigate(`/screen/screenDetail/${screen?._id}`, {
          state: { path: "/my/screens" },
        });
      }}
    >
      <Flex gap="4">
        <Stack
          height={{ base: "96px", lg: "164px" }}
          width={{ base: "120px", lg: "244px" }}
          borderRadius="9px"
          p="0"
          m="0"
        >
          <Image
            src={images[0]}
            borderRadius="9px"
            height={{ base: "96px", lg: "164px" }}
            width={{ base: "120px", lg: "244px" }}
          />
        </Stack>

        <Box width="100%">
          <Flex justifyContent="space-between">
            <Text
              fontSize={{ base: "16px", lg: "20px" }}
              fontWeight="700"
              color="#131D30"
              m="0"
            >
              {screen.name}
            </Text>
            {/* <Hide below="md"> */}
            <Dropdown
              menu={{
                items,
              }}
              placement="bottomRight"
            >
              <Box py="2" px="5">
                <HiDotsHorizontal />
              </Box>
            </Dropdown>
            {/* </Hide> */}
          </Flex>
          <Flex>
            <CiLocationOn color="#069D15" />
            <Text
              color="#808080"
              m="0"
              fontSize={{ base: "12px", lg: "14px" }}
            >{`${screen?.districtCity}, ${screen?.stateUT}`}</Text>
          </Flex>
          <Hide below="md">
            <Flex gap="5" fontSize={{ base: "12px", lg: "14px" }}>
              <Text m="0" color="#000000">
                Avg. Daily Footfall -
              </Text>
              <Text m="0" color="#000000" fontWeight="600">
                {numberFormat(screen?.additionalData?.averageDailyFootfall) ||
                  ""}
              </Text>
            </Flex>
            <Flex gap="3" fontSize={{ base: "12px", lg: "14px" }}>
              <Text m="0" color="#000000">
                Playing Frequency -
              </Text>
              <Text m="0" color="#000000" fontWeight="600">
                {screen?.slotsPlayPerDay} Slot play / day
              </Text>
            </Flex>
          </Hide>
          <Flex gap="5" fontSize={{ base: "12px", lg: "14px" }}>
            <Text m="0" color="#000000">
              Deployment Cost -
            </Text>
            <Text m="0" color="#000000" fontWeight="600">
              ₹ {numberFormat(screen?.rentPerDay)}/day
            </Text>
          </Flex>
        </Box>
      </Flex>
    </Box>
  );
}
