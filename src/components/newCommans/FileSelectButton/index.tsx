import { Box, Button, Input } from "@chakra-ui/react";

export function FileSelectButton(props: any) {
  let handelInput: any = null;
  const { handleProfileSelect } = props;
  return (
    <Box>
      <Button
        borderRadius="24px"
        bgColor="#FFFFFF"
        variant="outline"
        borderColor="#E5E5E5"
        height="35px"
        px="5"
        onClick={() => handelInput.click()}
      >
        Upload
      </Button>
      <Input
        hidden
        type="file"
        ref={(el) => (handelInput = el)}
        accept="image/png, image/jpeg"
        onChange={(e: any) => handleProfileSelect(e.target.files[0])}
      />
    </Box>
  );
}
