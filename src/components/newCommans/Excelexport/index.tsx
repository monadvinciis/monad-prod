import { Button, message } from "antd";
import * as FileSaver from "file-saver";
import XLSX from "sheetjs-style";

export function Excelexport({ excelData, fileName, wscols = [] }: any) {
  const fileType =
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset-UTF-8";
  const fileExtension = ".xlsx";

  const exportToExcel = async () => {
    try {
      const wscols1 = wscols || [
        {
          wch: 12,
        },
        {
          wch: 30,
        },
      ];

      var ws = XLSX.utils.aoa_to_sheet([[`For Campaign: ${fileName}`]]);
      ws["!cols"] = wscols1;

      XLSX.utils.sheet_add_json(ws, excelData, {
        skipHeader: false,
        origin: "A2", //ok -1
      });
      const wb = { Sheets: { data: ws }, SheetNames: ["data"] };
      const excelBuffer = XLSX.write(wb, { bookType: "xlsx", type: "array" });
      const data = new Blob([excelBuffer], { type: fileType });
      FileSaver.saveAs(data, fileName + fileExtension);
    } catch (error) {
      message.error(`Error in exportToExcel function, ${error}`);
    }
  };

  return <Button onClick={exportToExcel}>Export file</Button>;
}
