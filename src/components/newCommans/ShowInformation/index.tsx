import { Text } from "@chakra-ui/react";
import { Popover } from "antd";
import { IoInformationCircleOutline } from "react-icons/io5";

export function ShowInformation(props: any) {
  const value = props?.value;
  return (
    <div>
      <Popover
        content={
          <Text fontSize={"14px"} color="blue" m="0" p="0">
            {value}
          </Text>
        }
        trigger="click"
        arrow={true}
      >
        <IoInformationCircleOutline />
      </Popover>
    </div>
  );
}
