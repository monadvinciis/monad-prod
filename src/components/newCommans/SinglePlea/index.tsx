import {
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Box,
  Flex,
  Image,
  Stack,
  Text,
  Button,
  Hide,
  Show,
} from "@chakra-ui/react";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { MediaContainer } from "../../newCommans";
import { Popconfirm, message } from "antd";
import {
  getDateAndTimeAfterAdded5Hr,
  getNumberOfDaysBetweenTwoDates,
} from "../../../utils/dateAndTimeUtils";
import {
  grantScreenAllyPlea,
  rejectScreenAllyPlea,
} from "../../../Actions/screenActions";
import {
  SCREEN_ALLY_GRANT_RESET,
  SCREEN_ALLY_REJECT_RESET,
} from "../../../Constants/screenConstants";
import { PLEA_STATUS_PENDING } from "../../../Constants/pleaConstants";

export function CampaignAllyPlea(props: any) {
  const { data, handelRejectCampaignPlea, handelAcceptCampaignPlea } = props;
  const { plea, fromUser, campaign } = data;

  return (
    <Box py="1">
      <AccordionItem>
        <AccordionButton
          px="2"
          py="2"
          width="100%"
          borderRadius="8px"
          bgColor="#FFFFFF"
          _expanded={{ bg: "#E1E1E1", color: "#010101" }}
        >
          <Flex gap="3" align="start">
            <Image
              src={fromUser.avatar}
              alt=""
              height="30px"
              width="30px"
              rounded="2xl"
            />
            <Text fontWeight="700" fontSize={{ base: "12px", lg: "md" }} m="0">
              {`${plea?.remarks[plea?.remarks?.length - 1]}.`}
            </Text>
            <Text
              fontSize={{ base: "sm", lg: "sm" }}
              m="0"
              flexGrow="1"
              pr="5"
              color={"blue"}
              align="end"
            >
              {getDateAndTimeAfterAdded5Hr(plea.createdAt)}
            </Text>
          </Flex>

          <Text
            fontSize={{ base: "sm", lg: "md" }}
            m="0"
            flexGrow="1"
            align="end"
            pr="5"
            color={
              plea.status === "Approved"
                ? "green"
                : plea.status === "Pending"
                ? "#F7CB73"
                : "red"
            }
          >
            {plea.status}
          </Text>

          <AccordionIcon />
        </AccordionButton>
        <AccordionPanel px="0" py="1">
          <Box
            bgColor="#FFFFFF"
            width="100%"
            borderRadius="8px"
            borderColor="#E1E1E1"
          >
            <Flex justifyContent="space-between" p="5">
              <Flex gap="5" fontSize={{ base: "12px", lg: "md" }}>
                <MediaContainer
                  cid={plea?.video?.split("/").slice(4)[0]}
                  width={{ base: "205px", lg: "316px" }}
                  height={{ base: "169px", lg: "212px" }}
                  autoPlay={false}
                  rounded="xl"
                />
                <Flex gap={{ base: "5", lg: "20" }} pt="3">
                  <Stack gap="2">
                    <Text fontWeight="400" m="0" color="#808080">
                      User
                    </Text>
                    <Text fontWeight="400" m="0" color="#808080">
                      Contact
                    </Text>{" "}
                    <Text fontWeight="400" m="0" color="#808080">
                      Campaign
                    </Text>{" "}
                    <Text fontWeight="400" m="0" color="#808080">
                      Brand
                    </Text>{" "}
                    <Text fontWeight="400" m="0" color="#808080">
                      Budget
                    </Text>
                    <Text fontWeight="400" m="0" color="#808080">
                      Duration
                    </Text>
                  </Stack>
                  <Stack gap="2">
                    <Text fontWeight="400" m="0" color="#131D30">
                      {fromUser.name}
                    </Text>
                    <Text fontWeight="400" m="0" color="#131D30">
                      {fromUser.phone}
                    </Text>
                    <Text fontWeight="400" m="0" color="#131D30">
                      {campaign?.campaignName || "Campaign Deleted"}
                    </Text>
                    <Text fontWeight="400" m="0" color="#131D30">
                      {campaign?.brandName || "Campaign Deleted"}
                    </Text>
                    <Text fontWeight="400" m="0" color="#131D30">
                      {`Rs.${campaign?.totalAmount}` || "Campaign Deleted"}
                    </Text>
                    <Text fontWeight="400" m="0" color="#131D30">
                      {`${getNumberOfDaysBetweenTwoDates(
                        campaign?.startDate,
                        campaign?.endDate
                      )} days` || "Campaign Deleted"}
                    </Text>
                  </Stack>
                </Flex>
              </Flex>
              {plea?.status === PLEA_STATUS_PENDING ? (
                <Hide below="md">
                  <Flex align="center" gap="3">
                    <Popconfirm
                      title="Reject request"
                      description="Do you really want to reject this campaign request?"
                      onConfirm={() => handelRejectCampaignPlea(plea)}
                      okText="Yes"
                      cancelText="No"
                    >
                      <Button py="2" bgColor="red">
                        Reject
                      </Button>
                    </Popconfirm>

                    <Button
                      py="2"
                      onClick={() => {
                        handelAcceptCampaignPlea(plea);
                      }}
                      bgColor="green"
                    >
                      Accept
                    </Button>
                  </Flex>
                </Hide>
              ) : null}
            </Flex>
            {plea?.status === PLEA_STATUS_PENDING ? (
              <Show below="md">
                <Flex display="flex" gap="4" p="4">
                  <Popconfirm
                    title="Reject request"
                    description="Do you really want to reject this campaign request?"
                    onConfirm={() => handelRejectCampaignPlea(plea)}
                    okText="Yes"
                    cancelText="No"
                  >
                    <Button
                      variant="outline"
                      py="2"
                      width="100%"
                      color="#EA0000"
                    >
                      Reject
                    </Button>
                  </Popconfirm>
                  <Button
                    width="100%"
                    py="2"
                    onClick={() => {
                      handelAcceptCampaignPlea(plea);
                    }}
                    bgColor="#2BBB76"
                  >
                    Accept
                  </Button>
                </Flex>
              </Show>
            ) : null}
          </Box>
        </AccordionPanel>
      </AccordionItem>
    </Box>
  );
}

export function ScreenAllyPlea(props: any) {
  const { data } = props;
  const { plea, fromUser } = data;
  const dispatch = useDispatch<any>();

  const screenAllyPleaGrant = useSelector(
    (state: any) => state.screenAllyPleaGrant
  );
  const {
    // loading: loadingScreenAllyPleaGrant,
    error: errorScreenAllyPleaGrant,
    success: successScreenAllyPleaGrant,
  } = screenAllyPleaGrant;

  const screenAllyPleaReject = useSelector(
    (state: any) => state.screenAllyPleaReject
  );
  const {
    // loading: loadingScreenAllyPleaReject,
    error: errorScreenAllyPleaReject,
    success: successScreenAllyPleaReject,
  } = screenAllyPleaReject;

  const handelAcceptPlea = () => {
    // console.log("handelAcceptPlea called! : ", pleaId);
    dispatch(grantScreenAllyPlea(plea?._id));
  };
  const handelRejectPlea = () => {
    // console.log("handelRejectPlea called!");
    dispatch(rejectScreenAllyPlea(plea?._id));
  };
  useEffect(() => {
    // for scrren plea
    if (successScreenAllyPleaGrant) {
      message.success(
        "You have given access to brand to direct deploy campaign on your screen"
      );
      dispatch({ type: SCREEN_ALLY_GRANT_RESET });
    }
    if (errorScreenAllyPleaGrant) {
      message.error(errorScreenAllyPleaGrant);
      dispatch({ type: SCREEN_ALLY_GRANT_RESET });
    }
    if (successScreenAllyPleaReject) {
      message.success(
        "You have not given access to brand to direct deploy campaign on your screen"
      );
      dispatch({ type: SCREEN_ALLY_REJECT_RESET });
    }
    if (errorScreenAllyPleaReject) {
      message.error(errorScreenAllyPleaReject);
      dispatch({ type: SCREEN_ALLY_REJECT_RESET });
    }
  }, [
    dispatch,
    successScreenAllyPleaGrant,
    successScreenAllyPleaReject,
    errorScreenAllyPleaGrant,
    errorScreenAllyPleaReject,
  ]);
  return (
    <Box py="1">
      <AccordionItem>
        <AccordionButton
          px="2"
          py="2"
          width="100%"
          borderRadius="8px"
          // border="1px solid #E1E1E1"
          bgColor="#FFFFFF"
          _expanded={{ bg: "E1E1E1", color: "#010101" }}
        >
          <Flex gap="3" align="center">
            <Image
              src={fromUser.avatar}
              alt=""
              height="30px"
              width="30px"
              rounded="2xl"
            />
            <Text fontWeight="700" fontSize="md" m="0">
              {plea?.remarks[plea?.remarks?.length - 1]}
            </Text>
          </Flex>
          <Text fontSize="md" m="0" flexGrow="1" align="end" pr="5">
            {plea.status}
          </Text>

          <AccordionIcon />
        </AccordionButton>
        <AccordionPanel px="0" py="1">
          {plea?.status === PLEA_STATUS_PENDING ? (
            <Box
              bgColor="#FFFFFF"
              width="100%"
              borderRadius="8px"
              // border="1px"
              borderColor="#E1E1E1"
            >
              <Flex justifyContent="space-between" p="5">
                <Flex align="center" gap="3">
                  <Button
                    py="2"
                    onClick={() => {
                      handelAcceptPlea();
                    }}
                    bgColor="green"
                  >
                    Accept
                  </Button>
                  <Button
                    py="2"
                    onClick={() => {
                      handelRejectPlea();
                    }}
                    bgColor="red"
                  >
                    Reject
                  </Button>
                </Flex>
              </Flex>
            </Box>
          ) : null}
        </AccordionPanel>
      </AccordionItem>
    </Box>
  );
}

export function SinglePlea(props: any) {
  const {
    data,
    getPleaRequest,
    handelRejectCampaignPlea,
    handelAcceptCampaignPlea,
  } = props;
  const { plea } = data;

  return (
    <>
      {plea?.pleaType === "SCREEN_ALLY_PLEA" ? (
        <ScreenAllyPlea
          data={data}
          handelAcceptCampaignPlea={handelAcceptCampaignPlea}
          handelRejectCampaignPlea={handelRejectCampaignPlea}
        />
      ) : plea?.pleaType === "CAMPAIGN_ALLY_PLEA" ? (
        <CampaignAllyPlea
          data={data}
          getPleaRequest={getPleaRequest}
          handelAcceptCampaignPlea={handelAcceptCampaignPlea}
          handelRejectCampaignPlea={handelRejectCampaignPlea}
        />
      ) : null}
    </>
  );
}
