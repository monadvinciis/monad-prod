import { FormControl, FormErrorMessage, Input } from "@chakra-ui/react";

export function InputField(props: any) {
  const isError = props?.value === "";

  return (
    <FormControl isInvalid={false}>
      <Input
        width={props?.width ? props?.width : "100%"}
        style={{ height: "45px" }}
        placeholder={props?.placeholder ? props?.placeholder : ""}
        size="lg"
        fontSize="md"
        borderRadius="5px"
        border="1px"
        color="#000000"
        borderColor="#00000040"
        type={props?.type ? props.type : "text"}
        py="2"
        value={props?.value}
        disabled={props?.disabled ? props?.disabled : false}
        onChange={(e) => {
          props?.onChange(e.target.value);
        }}
        _hover={{ borderColor: "#000000" }}
      />
      {isError ? <FormErrorMessage>{props?.error}</FormErrorMessage> : null}
    </FormControl>
  );
}
