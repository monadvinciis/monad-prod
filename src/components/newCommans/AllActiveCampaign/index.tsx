import { Flex, Stack, Text } from "@chakra-ui/react";
import { message } from "antd";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import {
  changeCampaignPriority,
  changeCampaignStatus,
  getAllActiveAndPauseCampaignListByScreenId,
} from "../../../Actions/campaignAction";
import { ActiveCampaign } from "../ActiveCampaign";
import {
  CAMPAIGN_DELETE_RESET,
  CHANGE_CAMPAIGN_PRIORITY_RESET,
} from "../../../Constants/campaignConstants";

export function AllActiveCampaign(props: any) {
  const { screenId, show } = props;
  const dispatch = useDispatch<any>();
  const status = show || "Active";

  const [campaingsIndex, setCampaignsIndex] = useState<any>([]);

  const activeAndPauseCampaignListByScreenId = useSelector(
    (state: any) => state.activeAndPauseCampaignListByScreenId
  );
  const {
    // loading: loadingCampaign,
    error: errorCampaign,
    campaigns,
  } = activeAndPauseCampaignListByScreenId;

  const changeCampaignsStatus = useSelector(
    (state: any) => state.changeCampaignsStatus
  );
  const {
    // loading: loadingCampaignDelete,
    error: errorCampaignDelete,
    success: changeCampaignStatusStatus,
  } = changeCampaignsStatus;

  const handleChangeCampaignStatus = (campaignId: any, status: any) => {
    dispatch(changeCampaignStatus([campaignId], status));
  };
  const campaignsPriority = useSelector(
    (state: any) => state.campaignsPriority
  );
  const {
    // loading: loadingCampaignDelete,
    error: errorCampaignsPriority,
    success: successCampaignsPriority,
  } = campaignsPriority;

  useEffect(() => {
    // console.log("set campaign --------- : ", campaingsIndex);
    if (campaigns?.activeCampaigns?.length > 0) {
      let x = [];
      let camp = campaigns?.activeCampaigns;
      for (let campaign of camp) {
        let pp = campaign?.atIndex || [];
        for (let index of pp) {
          x.push(index);
        }
      }
      x.sort((a: any, b: any) => a - b);
      setCampaignsIndex(x);
    }
  }, [campaigns]);

  useEffect(() => {
    if (errorCampaignDelete) {
      message.error(errorCampaignDelete);
      dispatch({
        type: CAMPAIGN_DELETE_RESET,
      });
    } else if (changeCampaignStatusStatus) {
      message.success("status change successfully");
      dispatch({
        type: CAMPAIGN_DELETE_RESET,
      });
      dispatch(getAllActiveAndPauseCampaignListByScreenId(screenId));
    } else if (errorCampaignsPriority) {
      message.error(errorCampaignsPriority);
      dispatch({ type: CHANGE_CAMPAIGN_PRIORITY_RESET });
    } else if (successCampaignsPriority) {
      message.success("Successfully set campaign priority");
      dispatch({ type: CHANGE_CAMPAIGN_PRIORITY_RESET });
      dispatch(getAllActiveAndPauseCampaignListByScreenId(screenId));
    }
  }, [
    dispatch,
    changeCampaignStatusStatus,
    errorCampaignDelete,
    screenId,
    successCampaignsPriority,
    errorCampaignsPriority,
  ]);

  useEffect(() => {
    if (errorCampaign) message.error(errorCampaign);
  }, [errorCampaign]);

  useEffect(() => {
    if (screenId) {
      dispatch(getAllActiveAndPauseCampaignListByScreenId(screenId));
    }
  }, [dispatch, screenId]);

  const handleSetPriority = (campaignId: any, index: number) => {
    dispatch(changeCampaignPriority(campaignId, index));
  };

  return (
    <Flex
      overflowY={"auto"}
      flexDir="column"
      height="100%"
      gap={{ base: "2", lg: "4" }}
    >
      {show === "AP" ? (
        <Stack>
          <Text mb="0" fontSize={{ base: "20px", lg: "24px" }} fontWeight="700">
            Active Campaigns
          </Text>
          {campaigns?.activeCampaigns?.map((campaign: any, index: any) => {
            return (
              <ActiveCampaign
                campaign={campaign}
                key={index}
                handleChangeCampaignStatus={handleChangeCampaignStatus}
                handleSetPriority={handleSetPriority}
                campaigns={campaingsIndex || []}
              />
            );
          })}
          {campaigns?.pauseCampaigns?.length > 0 && (
            <Text
              mb="0"
              fontSize={{ base: "20px", lg: "24px" }}
              fontWeight="700"
            >
              Paused Campaigns{" "}
              {/* {`(${
                campaigns?.filter((camp: any) => camp.status === "Pause").length
              })`} */}
            </Text>
          )}
          {campaigns?.pauseCampaigns?.length > 0 &&
            campaigns?.pauseCampaigns?.map((campaign: any, index: any) => {
              return (
                <ActiveCampaign
                  campaign={campaign}
                  key={index}
                  handleChangeCampaignStatus={handleChangeCampaignStatus}
                  handleSetPriority={handleSetPriority}
                  campaigns={campaingsIndex || []}
                />
              );
            })}
          {campaigns?.holdCampaigns?.length > 0 && (
            <Text
              mb="0"
              fontSize={{ base: "20px", lg: "24px" }}
              fontWeight="700"
            >
              Hold Campaigns{" "}
              {/* {`(${
                campaigns?.filter((camp: any) => camp.status === "Pause").length
              })`} */}
            </Text>
          )}
          {campaigns?.holdCampaigns?.length > 0 &&
            campaigns?.holdCampaigns?.map((campaign: any, index: any) => {
              return (
                <ActiveCampaign
                  campaign={campaign}
                  key={index}
                  handleChangeCampaignStatus={handleChangeCampaignStatus}
                  handleSetPriority={handleSetPriority}
                />
              );
            })}
        </Stack>
      ) : (
        <Stack>
          {campaigns?.length > 0 &&
            campaigns
              ?.filter((camp: any) => camp.status === status)
              ?.map((campaign: any, index: any) => {
                return (
                  <ActiveCampaign
                    campaign={campaign}
                    key={index}
                    handleChangeCampaignStatus={handleChangeCampaignStatus}
                    handleSetPriority={handleSetPriority}
                  />
                );
              })}
        </Stack>
      )}
    </Flex>
  );
}
