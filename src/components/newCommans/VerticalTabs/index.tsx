import { Text, Flex, Stack } from "@chakra-ui/react";

export function VerticalTabs(props: any) {
  const { options, icons, selectedTab, setSelectedTab } = props;
  const tabOptions = options?.map((option: any, index: number) => {
    return {
      key: index + 1,
      label: option,
      icon: icons[index],
    };
  });

  return (
    <Stack
      p="5"
      bgColor="#FFFFFF"
      width="308px"
      // border="1px"
      borderColor="#E1E1E1"
      gap="7"
    >
      <Flex pt="5"></Flex>
      {tabOptions?.map((tabOption: any, index: number) => (
        <Flex
          key={index}
          align="center"
          gap="3"
          style={{ cursor: "pointer" }}
          borderLeft={selectedTab === tabOption.key ? "5px" : "0px"}
          color={selectedTab === tabOption.key ? "#131D30" : "#A1A1A1"}
          onClick={() => setSelectedTab(tabOption.key)}
          borderColor="#000000"
        >
          {tabOption?.icon}
          <Text fontSize="16px" fontWeight="500" m="0">
            {tabOption?.label}
          </Text>
        </Flex>
      ))}
      <Flex>{}</Flex>
    </Stack>
  );
}
