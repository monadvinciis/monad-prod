import { Accordion, Button, Flex, Box } from "@chakra-ui/react";
import { useState } from "react";
import { ScreenAllyPlea } from "../SinglePlea";
import { ResultNotFound } from "../../newCommans";

function PendingPleaRequest(props: any) {
  const { pendingScreenPleaRequest, getPleaRequest } = props;
  return (
    <Box flexGrow={1} overflowY={"auto"} width="100%">
      {pendingScreenPleaRequest?.length ? (
        <Accordion px="2" allowToggle>
          {pendingScreenPleaRequest.map((data: any, index: any) => (
            <ScreenAllyPlea
              data={data}
              key={index}
              getPleaRequest={getPleaRequest}
            />
          ))}
        </Accordion>
      ) : (
        <ResultNotFound />
      )}
    </Box>
  );
}

function AllPleaRequest(props: any) {
  const { allScreenPleaRequest, getPleaRequest } = props;
  return (
    <Box flexGrow={1} overflowY={"auto"} width="100%">
      {allScreenPleaRequest?.length ? (
        <Accordion px="2" allowToggle>
          {allScreenPleaRequest.map((data: any, index: any) => (
            <ScreenAllyPlea
              data={data}
              key={index}
              getPleaRequest={getPleaRequest}
            />
          ))}
        </Accordion>
      ) : (
        <ResultNotFound />
      )}
    </Box>
  );
}

export function ScreenPleaTab(props: any) {
  const [selectedOption, setSelectedOption] = useState<any>(1);
  const options = [
    { key: 1, label: "Pending" },
    { key: 2, label: "All" },
  ];
  const { pendingScreenPleaRequest, allScreenPleaRequest } = props;

  return (
    <Flex mb={"4"} flexDir={"column"} overflow={"hidden"} px="5">
      <Flex gap="3" py="2">
        {options?.map((option: any, index: any) => {
          return (
            <Button
              key={index}
              variant="outline"
              color={option.key === selectedOption ? "#131313" : "#AEAEAE"}
              bgColor="#FFFFFF"
              py="2"
              px="10"
              borderRadius="55px"
              fontWeight="700"
              fontSize="sm"
              onClick={() => setSelectedOption(option.key)}
            >
              {option?.label}
            </Button>
          );
        })}
      </Flex>
      <Box flexGrow={1} overflowY={"auto"} width="100%">
        {selectedOption === 1 ? (
          <PendingPleaRequest
            pendingScreenPleaRequest={
              pendingScreenPleaRequest?.SCREEN_ALLY_PLEA
            }
          />
        ) : (
          <AllPleaRequest
            allScreenPleaRequest={allScreenPleaRequest?.SCREEN_ALLY_PLEA}
          />
        )}
      </Box>
    </Flex>
  );
}
