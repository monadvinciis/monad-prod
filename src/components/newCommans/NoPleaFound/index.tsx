import { Center, Image, Text } from "@chakra-ui/react";
import { notificationSVG } from "../../../assets/svg";
import { MyButton } from "../MyButtons";
import { useNavigate } from "react-router-dom";

export function NoPleaFound(props: any) {
  const navigate = useNavigate();
  return (
    <Center pt="64px" flexDir="column">
      <Image src={notificationSVG} />
      <Text
        fontSize={{ base: "20px", lg: "24px" }}
        fontWeight="600"
        color="#414141"
        m="0"
        mt="8px"
      >
        No notifications
      </Text>
      <Text
        fontSize={{ base: "15px", lg: "20px" }}
        fontWeight="400"
        color="#838282"
        m="0"
        mt="8px"
      >
        You have no notifications yet.
      </Text>
      <Text
        fontSize={{ base: "15px", lg: "20px" }}
        fontWeight="400"
        color="#838282"
        m="0"
        my="8px"
      >
        Please come back later.
      </Text>
      <MyButton
        label="Go Back"
        handelClick={() => navigate("/")}
        color="#FFFFFF"
        bgColor="#000000"
      />
    </Center>
  );
}
