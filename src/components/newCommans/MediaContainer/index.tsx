import { Box, Image, Skeleton } from "@chakra-ui/react";
// import { getMediaType } from "../../../services/utils";
import { useMedia } from "../../../hooks";

export const MediaContainer = ({
  cid,
  height,
  width,
  autoPlay,
  rounded,
}: any) => {
  const { data: media, isLoading } = useMedia({ id: cid });
  let contentType = media?.fileType;

  let ipfsUrl: any = media?.url;
  // let contentType = getMediaType(media?.headers["content-type"]);

  // if (cid?.split(":").splice(0)[0] === "https") {
  //   ipfsUrl = cid;
  //   contentType = "iframe";
  // } else if (cid?.split(":").splice(0)[0] !== "https" && contentType) {
  //   // const { data: media, isLoading, isError };
  //   ipfsUrl = media?.request["responseURL"];
  // } else if (contentType === undefined) {
  //   ipfsUrl = `https://ipfs.io/ipfs/${cid}`;
  // }

  const IframeContainer = () => (
    <Box
      as="iframe"
      src={ipfsUrl}
      // onLoad={() =>
      //   triggerPort(media.request["responseURL"].split("/").slice()[4])
      // }
      objectFit="fill"
      rounded={rounded || 5}
      border={0}
      boxSize="100%"
      width={width}
      height={height}
    />
  );
  const YouTubeContainer = () => (
    <iframe width={width} height={height} src={ipfsUrl}></iframe>
  );
  const ImageContainer = () => (
    <Image
      src={ipfsUrl}
      boxSize="100%"
      rounded={rounded || 5}
      objectFit="fill"
      width={width}
      height={height}
    />
  );
  const VideoContainer = () => (
    <Box
      as="video"
      autoPlay={autoPlay}
      loop
      controls
      muted
      boxSize="100%"
      rounded={rounded || 5}
      p="0"
      objectFit="fill"
      width={width}
      height={height}
    >
      <source src={ipfsUrl} />
    </Box>
  );

  const renderContainer = () => {
    switch (contentType) {
      case "image":
        return <ImageContainer />;
      case "video":
        return <VideoContainer />;
      case "iframe":
        return <IframeContainer />;
      default:
        return <YouTubeContainer />;
    }
  };
  return (
    <Box rounded={rounded} height={height} width={width} overflow="hidden">
      {isLoading ? (
        <Skeleton>
          <Box height={height} width={width}></Box>{" "}
        </Skeleton>
      ) : (
        renderContainer()
      )}
    </Box>
  );
};
