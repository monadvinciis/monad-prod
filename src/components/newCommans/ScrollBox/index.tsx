import { Box } from "@chakra-ui/react";

export function ScrollBox(props: any) {
  return (
    <Box
      borderColor="#EBEBEB"
      overflowY="auto"
      overflow={"scroll"}
      flexGrow={props.flexGrow}
      flexDir={"column"}
      width={props?.width || "100%"}
      css={{
        "&::-webkit-scrollbar": {
          width: "1px",
        },
        "&::-webkit-scrollbar-track": {
          width: "1px",
        },
        "&::-webkit-scrollbar-thumb": {
          background: "#C4C4C450",
          borderRadius: "24px",
        },
      }}
    >
      {props?.children}
    </Box>
  );
}
