import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  DateTimePicker,
  TimePicker,
} from "@material-ui/pickers";
import {
  IconButton as MiuiIconButton,
  InputAdornment,
} from "@material-ui/core";
import { BsCalendar2Date, BsClock } from "react-icons/bs";
import { FormControl, FormErrorMessage } from "@chakra-ui/react";

export function DateAndTimeInputField(props: any) {
  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <DateTimePicker
        // className="datetimepicker"
        style={{
          width: "100%",
        }}
        disablePast
        size="small"
        inputVariant="outlined"
        value={props?.value}
        onChange={(value: any) => props?.onChange(value)}
        InputProps={{
          endAdornment: (
            <InputAdornment position="start">
              <MiuiIconButton>
                <BsCalendar2Date />
              </MiuiIconButton>
            </InputAdornment>
          ),
        }}
      />
    </MuiPickersUtilsProvider>
  );
}

export function TimeInputField(props: any) {
  const isError = props?.value === "";
  return (
    <FormControl isInvalid={isError}>
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <TimePicker
          // className="datetimepicker"
          style={{
            width: "100%",
          }}
          size="small"
          inputVariant="outlined"
          value={props?.value}
          disabled={props.disabled || false}
          onChange={(value: any) => props?.onChange(value)}
          format="hh:mm a"
          InputProps={{
            endAdornment: (
              <InputAdornment position="start">
                <MiuiIconButton>
                  <BsClock />
                </MiuiIconButton>
              </InputAdornment>
            ),
          }}
        />
      </MuiPickersUtilsProvider>
      {isError ? <FormErrorMessage>{props?.error}</FormErrorMessage> : null}
    </FormControl>
  );
}
