import { Box, Button, Center, Image, Text } from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";

export function ResultNotFound(props: any) {
  const navigate = useNavigate();
  return (
    <Center pt="20">
      <Box
        boxShadow={"2xl"}
        width="400px"
        alignContent="center"
        borderRadius="16px"
        px="20"
        py="10"
      >
        <Center flexDirection="column">
          <Image
            src="https://static.vecteezy.com/system/resources/previews/005/073/071/original/user-not-found-account-not-register-concept-illustration-flat-design-eps10-modern-graphic-element-for-landing-page-empty-state-ui-infographic-icon-vector.jpg"
            alt="Not found"
            height="200px"
            width="200px"
          />
          <Text fontSize="24px" fontWeight="600" color="#000000">
            Result Not Found
          </Text>
          <Text fontSize="sm" fontWeight="400" color="#4A4A4A" align="center">
            Whoops .. this information is not availabe for a movement
          </Text>
          <Button
            variant="outline"
            color="#FFFFFF"
            bgColor="#1D5D9B"
            py="3"
            px="10"
            borderRadius="56px"
            onClick={() => navigate(props?.path)}
          >
            Go Back
          </Button>
        </Center>
      </Box>
    </Center>
  );
}
