import { Box, Image, Stack } from "@chakra-ui/react";

export function ShowMediaFile(props: any) {
  const { url, mediaType, width, height } = props;

  return (
    <Stack py="2" borderRadius="8px">
      {mediaType === "url" ? (
        <Box
          as="iframe"
          src={url}
          objectFit="cover"
          width={width}
          height={height}
        />
      ) : mediaType === "image" ? (
        <Image
          src={url}
          alt="Campaign Image"
          maxHeight="250px"
          width={width}
          height={height}
        ></Image>
      ) : (
        <Stack width={width} height={height}>
          <Box
            as="video"
            src={url}
            autoPlay
            loop
            muted
            display="inline-block"
            width={width}
            height={height}
          ></Box>
        </Stack>
      )}
    </Stack>
  );
}
