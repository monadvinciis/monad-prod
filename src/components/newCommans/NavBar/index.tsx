import {
  Box,
  Button,
  Center,
  Flex,
  Image,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Show,
  Stack,
  Text,
} from "@chakra-ui/react";
import { Monad } from "../../../assets/svg";
import { useSelector } from "react-redux";
import { BsChevronDown } from "react-icons/bs";
import { Link as RouterLink, useNavigate } from "react-router-dom";
import { useEffect, useRef } from "react";
import { CheckPWAInstalled, InstallPWA } from "../PwaPopup";
import { isPWA } from "../../../utils";
import { useDispatch } from "react-redux";
import { signout } from "../../../Actions/userActions";
import { getTopFivePleaRequestByUserId } from "../../../Actions/pleaActions";
import { PleaRequestButton } from "../NotificationButton/NotificationButton";
import { BiSolidBell } from "react-icons/bi";
import { isTokenExpiredUtils } from "../../../utils/authUtils";
// import useWebSocket, { ReadyState } from "react-use-websocket";
const WS_URL = "ws://127.0.0.1:3333";

export function NavBar(props: any) {
  const btnRef = useRef(null);
  const dispatch = useDispatch<any>();
  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;
  const navigate = useNavigate();
  // const [pleas, setPleas] = useState<any>([]);

  const signoutHandler = () => {
    dispatch(signout());
    navigate("/signin");
  };
  // const { sendJsonMessage, lastJsonMessage, readyState } = useWebSocket(
  //   WS_URL,
  //   {
  //     queryParams: { username: userInfo?._id },
  //     share: false,
  //     shouldReconnect: () => true,
  //   }
  // );

  // useEffect(() => {
  //   if (userInfo && lastJsonMessage) {
  //     setPleas(
  //       Object.values(lastJsonMessage || {})?.filter(
  //         (plea: any) => plea.username === userInfo?._id
  //       )[0]?.state?.data || []
  //     );
  //   }
  // }, [lastJsonMessage, userInfo]);

  // useEffect(() => {
  //   if (userInfo && readyState === ReadyState.OPEN) {
  //     console.log("connection establish for getting new request");
  //     sendJsonMessage({
  //       event: "getNewPleaRequest",
  //       data: [],
  //     });
  //   }
  // }, [readyState, userInfo]);

  const topFivePleaRequest = useSelector(
    (state: any) => state.topFivePleaRequest
  );
  const {
    pleas,
    loading: loadingAllPleas,
    // error: errorAllPleas,
  } = topFivePleaRequest;

  const content = (
    <Box zIndex={1} bgColor="">
      <Box>
        {pleas?.map((plea: any, index: any) => (
          <Text
            p="1"
            _hover={{ bg: "rgba(14, 188, 245, 0.3)" }}
            key={index}
            fontSize="14px"
            fontWeight="700"
            onClick={() => navigate(`/notification`)}
          >
            {plea?.remarks[plea?.remarks?.length - 1]}
          </Text>
        ))}
        <Center p="1">
          <Button
            type="submit"
            color="#000000"
            variant="outline"
            bgColor="#FFFFFF"
            _hover={{ color: "#FFFFFF", bgColor: "#000000" }}
            py="2"
            px="10"
            mt="2"
            fontWeight="600"
            fontSize={{ base: "sm", lg: "md" }}
            onClick={() => navigate(`/notification`)}
          >
            See all
          </Button>
        </Center>
      </Box>
    </Box>
  );

  useEffect(() => {
    if (userInfo) {
      dispatch(getTopFivePleaRequestByUserId());
    }
  }, [dispatch, userInfo]);

  useEffect(() => {
    if (isTokenExpiredUtils()) {
      signoutHandler();
    }
  }, [window.location.pathname]);
  return (
    <Flex w="100%" pos="sticky" h="5%" px="4" pt="2" zIndex={1}>
      <Show below="md">
        <Flex align="center" pl="2">
          <Image
            src={Monad}
            width="80px"
            height="25px"
            onClick={() => navigate("/")}
          />
        </Flex>
      </Show>
      <Flex w="100%" pl="2" justifyContent="flex-end">
        {!userInfo ? (
          <Stack flexDirection="row" align="center">
            <Button
              as={RouterLink}
              to={`/signin`}
              size="sm"
              fontSize={{ base: "xs", lg: "sm" }}
              ref={btnRef}
              py="2"
              px={{ base: "2", lg: "4" }}
              color="#000000"
              bgColor="#FFFFFF"
              _hover={{ bgColor: "#000000", color: "#FFFFFF" }}
              borderRadius="8px"
              variant="outline"
            >
              Login
            </Button>
            <Button
              as={RouterLink}
              to={`/signup`}
              size="sm"
              fontSize={{ base: "xs", lg: "sm" }}
              py="2"
              px={{ base: "2", lg: "4" }}
              color="#FFFFFF"
              bgColor="#000000"
              _hover={{ bgColor: "#FFFFFF", color: "#000000" }}
              variant="outline"
              borderRadius="8px"
            >
              Create Account
            </Button>
            {isPWA() ? <CheckPWAInstalled /> : <InstallPWA />}
          </Stack>
        ) : (
          <Flex gap="2" pr={{ base: "0px", lg: "32px" }} align="center">
            <Menu>
              <MenuButton>
                <PleaRequestButton
                  icon={<BiSolidBell size="20px" />}
                  value={pleas?.length || 0}
                />
              </MenuButton>
              {pleas?.length === 0 ? null : (
                <MenuList width="400px">{content}</MenuList>
              )}
            </Menu>
            <Image
              src={userInfo?.avatar}
              rounded="45px"
              width="30px"
              height="30px"
            />
            <Menu>
              <MenuButton>
                <BsChevronDown size="20px" fontWeight="1" color="#403F49" />
              </MenuButton>
              <MenuList width="300px">
                <MenuItem
                  as={RouterLink}
                  to={`/setting`}
                  color="black"
                  justifyContent="space-between"
                >
                  <Text my="1" textAlign="center">
                    Account
                  </Text>
                  <Image
                    height="45px"
                    width="45px"
                    rounded="45px"
                    src={userInfo?.avatar}
                  />
                </MenuItem>
                <MenuItem
                  color="black"
                  justifyContent="space-between"
                  onClick={signoutHandler}
                >
                  <Text my="1" textAlign="center">
                    Log Out
                  </Text>
                </MenuItem>
              </MenuList>
            </Menu>
          </Flex>
        )}
      </Flex>
    </Flex>
  );
}
