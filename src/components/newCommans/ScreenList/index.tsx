import { Box, Text, Flex, Stack, Image } from "@chakra-ui/react";
import { MdCircle } from "react-icons/md";
import {
  CAMPAIGN_STATUS_ACTIVE,
  CAMPAIGN_STATUS_COMPLETD,
  CAMPAIGN_STATUS_DELETED,
  CAMPAIGN_STATUS_PAUSE,
  CAMPAIGN_STATUS_PENDING,
} from "../../../Constants/campaignConstants";
import { CiLocationOn } from "react-icons/ci";
import { Popconfirm } from "antd";
import { RiDeleteBin6Line } from "react-icons/ri";

export function ScreenList(props: any) {
  const { screens, campaigns } = props;

  const getColor = (campaign: any) => {
    const status = campaign?.status;
    const color =
      status === CAMPAIGN_STATUS_ACTIVE
        ? "green"
        : status === CAMPAIGN_STATUS_COMPLETD
        ? "green"
        : status === CAMPAIGN_STATUS_PENDING
        ? "orange"
        : status === CAMPAIGN_STATUS_PAUSE
        ? "red"
        : "#D71111";
    return color;
  };

  const getCampaign = (screen: any) => {
    return campaigns?.find((c: any) => c.screen === screen._id);
  };

  const confirm = (
    e: React.MouseEvent<HTMLElement> | undefined,
    campaign: any
  ) => {
    props?.handleChangeCampaignStatus([campaign], CAMPAIGN_STATUS_DELETED);
  };
  const isValidForDelete = (screen: any) => {
    const campaign = getCampaign(screen);
    if (
      campaign?.status === CAMPAIGN_STATUS_COMPLETD ||
      campaign?.status === CAMPAIGN_STATUS_DELETED
    ) {
      return false;
    } else return true;
  };

  return (
    <Box
      bgColor="#FFFFFF"
      py="5"
      px="5"
      borderRadius="8px"
      // border="1px"
      borderColor="#E1E1E1"
      width="100%"
      height="100%"
    >
      <Text color="#131D30" fontSize="20px" fontWeight="600" pl="4">
        On {screens?.length} screens
      </Text>

      <Stack overflowY={"auto"} gap="4">
        {screens?.map((screen: any, index: any) => (
          <Flex key={index} justifyContent="space-between">
            <Flex gap="5">
              <Image
                src={
                  screen?.images?.length !== 0
                    ? `https://ipfs.io/ipfs/${screen.images[0]}`
                    : `${screen.image}`
                }
                // alt={noimage}
                width="91px"
                height="67px"
                borderRadius="5px"
              />
              <Flex flexDir="column" p="0" m="0">
                <Text m="0" fontSize="20px" color="#131D30" fontWeight="400">
                  {screen?.name}
                </Text>
                <Flex gap="1" py="1" align="center">
                  <CiLocationOn color="#069D15" />
                  <Text
                    color="#808080"
                    fontSize="14px"
                    fontWeight="400"
                    m="0"
                  >{`${screen?.districtCity}, ${screen?.stateUT}`}</Text>
                </Flex>
                <Text color="#808080" fontSize="14px" fontWeight="400" m="0">
                  {/* Budget Rs. 3,000 */}
                </Text>
              </Flex>
            </Flex>
            {/* {campaigns?.length > 0 ? ( */}
            <Flex gap="8" width="30%" align="center" justifyContent="flex-end">
              <Flex gap="2" align="center">
                <MdCircle color={getColor(getCampaign(screen))} />
                <Text m="0" fontSize="14px">
                  {getCampaign(screen)?.status}
                </Text>
              </Flex>
              {isValidForDelete(screen) && (
                <Popconfirm
                  title="Delete campaign"
                  description="Do you really want to delete campaign from this screen?"
                  onConfirm={(e) => confirm(e, getCampaign(screen))}
                  okText="Yes"
                  cancelText="No"
                >
                  <RiDeleteBin6Line color="#D71111" size="16px" />
                </Popconfirm>
              )}
            </Flex>
            {/* ) : null} */}
          </Flex>
        ))}
      </Stack>
    </Box>
  );
}
