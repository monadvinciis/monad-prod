// /api/userQuery
import Axios from "axios";
import {
  ADD_NEW_QUERY_FAIL,
  ADD_NEW_QUERY_REQUEST,
  ADD_NEW_QUERY_SUCCESS,
  GET_ALL_QUERY_BY_USER_FAIL,
  GET_ALL_QUERY_BY_USER_REQUEST,
  GET_ALL_QUERY_BY_USER_SUCCESS,
} from "../Constants/queryConstants";

export const createNewQuery = (requestBody) => async (dispatch, getState) => {
  dispatch({
    type: ADD_NEW_QUERY_REQUEST,
    payload: requestBody,
  });
  try {
    const {
      userSignin: { userInfo },
    } = getState();
    const { data } = await Axios.post(
      `${process.env.REACT_APP_BLINDS_SERVER}/api/userQuery/create`,
      requestBody,
      {
        headers: {
          Authorization: `Bearer ${userInfo?.token}`,
        },
      }
    );
    dispatch({
      type: ADD_NEW_QUERY_SUCCESS,
      payload: data,
    });

    localStorage.setItem("userInfo", JSON.stringify(data));
  } catch (error) {
    dispatch({
      type: ADD_NEW_QUERY_FAIL,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    });
  }
};

export const getAllQueryListByUser = () => async (dispatch, getState) => {
  dispatch({
    type: GET_ALL_QUERY_BY_USER_REQUEST,
    payload: {},
  });
  try {
    const {
      userSignin: { userInfo },
    } = getState();
    const { data } = await Axios.get(
      `${process.env.REACT_APP_BLINDS_SERVER}/api/userQuery/user`,
      {
        headers: {
          Authorization: `Bearer ${userInfo?.token}`,
        },
      }
    );
    dispatch({
      type: GET_ALL_QUERY_BY_USER_SUCCESS,
      payload: data,
    });

    localStorage.setItem("userInfo", JSON.stringify(data));
  } catch (error) {
    dispatch({
      type: GET_ALL_QUERY_BY_USER_FAIL,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    });
  }
};
