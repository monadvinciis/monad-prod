import Axios from "axios";
import {
  CREATE_CAMPAIGN_REQUEST,
  CREATE_CAMPAIGN_SUCCESS,
  CREATE_CAMPAIGN_FAIL,
  CAMPAIGN_LIST_REQUEST,
  CAMPAIGN_LIST_SUCCESS,
  CAMPAIGN_LIST_FAIL,
  CAMPAIGN_DETAILS_FAIL,
  CAMPAIGN_DETAILS_REQUEST,
  CAMPAIGN_DETAILS_SUCCESS,
  CAMPAIGN_LIST_BY_SCREENID_FAIL,
  CAMPAIGN_LIST_BY_SCREENID_REQUEST,
  CAMPAIGN_LIST_BY_SCREENID_SUCCESS,
  CAMPAIGN_DELETE_REQUEST,
  CAMPAIGN_DELETE_FAIL,
  CAMPAIGN_DELETE_SUCCESS,
  ACTIVE_CAMPAIGN_LIST_BY_SCREENID_REQUEST,
  ACTIVE_CAMPAIGN_LIST_BY_SCREENID_SUCCESS,
  ACTIVE_CAMPAIGN_LIST_BY_SCREENID_FAIL,
  FILTERED_CAMPAIGN_LIST_REQUEST,
  FILTERED_CAMPAIGN_LIST_SUCCESS,
  FILTERED_CAMPAIGN_LIST_FAIL,
  CAMPAIGNS_WITH_SCREENS_REQUESR,
  CAMPAIGNS_WITH_SCREENS_SUCCESS,
  CAMPAIGNS_WITH_SCREENS_RESET,
  CAMPAIGN_LOGS_REQUEST,
  CAMPAIGN_LOGS_SUCCESS,
  CAMPAIGN_LOGS_FAIL,
  ACTIVE_AND_PAUSE_CAMPAIGN_LIST_BY_SCREENID_REQUEST,
  ACTIVE_AND_PAUSE_CAMPAIGN_LIST_BY_SCREENID_SUCCESS,
  ACTIVE_AND_PAUSE_CAMPAIGN_LIST_BY_SCREENID_FAIL,
  ACTIVE_AND_PAUSE_CAMPAIGN_LIST_BY_USERID_REQUEST,
  ACTIVE_AND_PAUSE_CAMPAIGN_LIST_BY_USERID_SUCCESS,
  ACTIVE_AND_PAUSE_CAMPAIGN_LIST_BY_USERID_FAIL,
  CAMPAIGN_LIST_BY_USERID_REQUEST,
  CAMPAIGN_LIST_BY_USERID_SUCCESS,
  CAMPAIGN_LIST_BY_USERID_FAIL,
  ACTIVE_CAMPAIGN_LIST_BY_USERID_REQUEST,
  ACTIVE_CAMPAIGN_LIST_BY_USERID_SUCCESS,
  ACTIVE_CAMPAIGN_LIST_BY_USERID_FAIL,
  GET_ALL_PENDING_CAMPAIGNS_BY_USER_ID_FAIL,
  GET_ALL_PENDING_CAMPAIGNS_BY_USER_ID_REQUEST,
  GET_ALL_PENDING_CAMPAIGNS_BY_USER_ID_SUCCESS,
  CHANGE_CAMPAIGN_PRIORITY_REQUEST,
  CHANGE_CAMPAIGN_PRIORITY_SUCCESS,
  CHANGE_CAMPAIGN_PRIORITY_FAIL,
} from "../Constants/campaignConstants.js";

export const createCamapaign = (resquestBody) => async (dispatch, getState) => {
  dispatch({
    type: CREATE_CAMPAIGN_REQUEST,
    payload: resquestBody,
  });
  const {
    userSignin: { userInfo },
  } = getState();
  try {
    const {
      screenIds,
      mediaId,
      campaignName,
      isDefaultCampaign,
      brandName,
      startDateAndTime: startDate,
      endDateAndTime: endDate,
      noOfDays,
      url = "",
      fileType = "",
      duration = "",
      fileSize = "",
    } = resquestBody;
    const { data } = await Axios.post(
      `${process.env.REACT_APP_BLINDS_SERVER}/api/campaign/create`,
      {
        user: userInfo,
        screenIds,
        mediaId,
        campaignName,
        brandName,
        isDefaultCampaign,
        startDate,
        endDate,
        noOfDays,
        url,
        fileType,
        duration,
        fileSize,
      },
      {
        headers: {
          Authorization: `Bearer ${userInfo?.token}`,
        },
      }
    );
    dispatch({
      type: CREATE_CAMPAIGN_SUCCESS,
      payload: data,
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message
        ? error.response.data.message
        : error.message;
    dispatch({
      type: CREATE_CAMPAIGN_FAIL,
      payload: message,
    });
  }
};

export const getCampaignList = () => async (dispatch) => {
  dispatch({ type: CAMPAIGN_LIST_REQUEST });
  try {
    const { data } = await Axios.get(
      `${process.env.REACT_APP_BLINDS_SERVER}/api/campaign/all`
    );
    dispatch({ type: CAMPAIGN_LIST_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: CAMPAIGN_LIST_FAIL,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    });
  }
};

export const getCampaignsAlongWithScreens =
  (cid, campaignName) => async (dispatch) => {
    dispatch({ type: CAMPAIGNS_WITH_SCREENS_REQUESR });
    try {
      const { data } = await Axios.get(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/campaign/${cid}/${campaignName}`
      );
      dispatch({ type: CAMPAIGNS_WITH_SCREENS_SUCCESS, payload: data });
    } catch (error) {
      dispatch({
        type: CAMPAIGNS_WITH_SCREENS_RESET,
        payload:
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message,
      });
    }
  };

export const filteredCampaignListDateWise =
  (startDateHere, endDateHere, screenId) => async (dispatch, getState) => {
    dispatch({ type: FILTERED_CAMPAIGN_LIST_REQUEST });
    try {
      const {
        userSignin: { userInfo },
      } = getState();
      const { data } = await Axios.get(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/campaign/filterCampaignByDate/${startDateHere}/${endDateHere}/${userInfo?._id}/${screenId}`
      );
      //console.log("all campaign data : ", data);
      dispatch({ type: FILTERED_CAMPAIGN_LIST_SUCCESS, payload: data });
    } catch (error) {
      dispatch({
        type: FILTERED_CAMPAIGN_LIST_FAIL,
        payload:
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message,
      });
    }
  };

export const getCampaignDetail = (campaignId) => async (dispatch) => {
  dispatch({
    type: CAMPAIGN_DETAILS_REQUEST,
    payload: campaignId,
  });

  try {
    const { data } = await Axios.get(
      `${process.env.REACT_APP_BLINDS_SERVER}/api/campaign/${campaignId}`
    );
    dispatch({
      type: CAMPAIGN_DETAILS_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: CAMPAIGN_DETAILS_FAIL,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    });
  }
};

// By screen Id
export const getAllActiveAndPauseCampaignListByScreenId =
  (screenId) => async (dispatch) => {
    dispatch({
      type: ACTIVE_AND_PAUSE_CAMPAIGN_LIST_BY_SCREENID_REQUEST,
      payload: screenId,
    });

    try {
      const { data } = await Axios.get(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/campaign/${screenId}/screen/all/activeOrPauser`
      );
      dispatch({
        type: ACTIVE_AND_PAUSE_CAMPAIGN_LIST_BY_SCREENID_SUCCESS,
        payload: data,
      });
    } catch (error) {
      dispatch({
        type: ACTIVE_AND_PAUSE_CAMPAIGN_LIST_BY_SCREENID_FAIL,
        payload:
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message,
      });
    }
  };

export const getAllCampaignListByScreenId = (screenId) => async (dispatch) => {
  dispatch({
    type: CAMPAIGN_LIST_BY_SCREENID_REQUEST,
    payload: screenId,
  });

  try {
    const { data } = await Axios.get(
      `${process.env.REACT_APP_BLINDS_SERVER}/api/campaign/${screenId}/screen/all`
    );
    dispatch({
      type: CAMPAIGN_LIST_BY_SCREENID_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: CAMPAIGN_LIST_BY_SCREENID_FAIL,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    });
  }
};

export const getAllActiveCampaignListByScreenId =
  (screenId) => async (dispatch) => {
    dispatch({
      type: ACTIVE_CAMPAIGN_LIST_BY_SCREENID_REQUEST,
      payload: screenId,
    });

    try {
      const { data } = await Axios.get(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/campaign/${screenId}/screen/all/active`
      );
      dispatch({
        type: ACTIVE_CAMPAIGN_LIST_BY_SCREENID_SUCCESS,
        payload: data,
      });
    } catch (error) {
      dispatch({
        type: ACTIVE_CAMPAIGN_LIST_BY_SCREENID_FAIL,
        payload:
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message,
      });
    }
  };

export const getAllActiveAndPauseCampaignListByUserId =
  (userId) => async (dispatch) => {
    dispatch({
      type: ACTIVE_AND_PAUSE_CAMPAIGN_LIST_BY_USERID_REQUEST,
      payload: userId,
    });

    try {
      const { data } = await Axios.get(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/campaign/${userId}/user/all/activeOrPauser`
      );
      dispatch({
        type: ACTIVE_AND_PAUSE_CAMPAIGN_LIST_BY_USERID_SUCCESS,
        payload: data,
      });
    } catch (error) {
      dispatch({
        type: ACTIVE_AND_PAUSE_CAMPAIGN_LIST_BY_USERID_FAIL,
        payload:
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message,
      });
    }
  };

export const getAllCampaignListByUserId = (userId) => async (dispatch) => {
  dispatch({
    type: CAMPAIGN_LIST_BY_USERID_REQUEST,
    payload: userId,
  });

  try {
    const { data } = await Axios.get(
      `${process.env.REACT_APP_BLINDS_SERVER}/api/campaign/${userId}/user/all`
    );
    dispatch({
      type: CAMPAIGN_LIST_BY_USERID_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: CAMPAIGN_LIST_BY_USERID_FAIL,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    });
  }
};

export const getAllActiveCampaignListByUserId =
  (screenId) => async (dispatch) => {
    dispatch({
      type: ACTIVE_CAMPAIGN_LIST_BY_USERID_REQUEST,
      payload: screenId,
    });

    try {
      const { data } = await Axios.get(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/campaign/${screenId}/user/all/active`
      );
      dispatch({
        type: ACTIVE_CAMPAIGN_LIST_BY_USERID_SUCCESS,
        payload: data,
      });
    } catch (error) {
      dispatch({
        type: ACTIVE_CAMPAIGN_LIST_BY_USERID_FAIL,
        payload:
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message,
      });
    }
  };

export const changeCampaignStatus =
  (campaignIds, status) => async (dispatch, getState) => {
    dispatch({
      type: CAMPAIGN_DELETE_REQUEST,
      payload: { campaignIds, status },
    });
    // console.log("changeCampaignStatus called!");
    try {
      const {
        userSignin: { userInfo },
      } = getState();

      const { data } = await Axios.put(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/campaign/changeStatus/${userInfo?._id}/campaign/${status}`,
        {
          campaignIds,
        },
        {
          headers: {
            Authorization: `Bearer ${userInfo?.token}`,
          },
        }
      );
      dispatch({
        type: CAMPAIGN_DELETE_SUCCESS,
        payload: data,
      });
      // console.log({ data });
    } catch (error) {
      dispatch({
        type: CAMPAIGN_DELETE_FAIL,
        payload:
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message,
      });
    }
  };

export const getCampaignListForBrand =
  (brandId) => async (dispatch, getState) => {
    dispatch({
      type: "CAMPAIGN_LIST_BRAND_REQUEST",
      payload: { brandId },
    });
    try {
      const {
        userSignin: { userInfo },
      } = getState();

      const { data } = await Axios.get(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/campaign/brandCampaign/${brandId}`,
        {
          headers: {
            Authorization: `Bearer ${userInfo?.token}`,
          },
        }
      );
      dispatch({
        type: "CAMPAIGN_LIST_BRAND_SUCCESS",
        payload: data,
      });
      // console.log({ data });
    } catch (error) {
      dispatch({
        type: "CAMPAIGN_LIST_BRAND_FAIL",
        payload:
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message,
      });
    }
  };

export const getCampaignLogs =
  ({ start = 0, end = 50, screenId, cid }) =>
  async (dispatch, getState) => {
    dispatch({
      type: CAMPAIGN_LOGS_REQUEST,
      payload: { start, end, screenId, cid },
    });
    try {
      const {
        userSignin: { userInfo },
      } = getState();

      const { data } = await Axios.get(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/campaign/campaignLogs?screenId=${screenId}&cid=${cid}&start=${start}&end=${end}`,
        {
          headers: {
            Authorization: `Bearer ${userInfo?.token}`,
          },
        }
      );
      dispatch({
        type: CAMPAIGN_LOGS_SUCCESS,
        payload: data,
      });
      // console.log({ data });
    } catch (error) {
      dispatch({
        type: CAMPAIGN_LOGS_FAIL,
        payload:
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message,
      });
    }
  };

export const getAllPendingCampaignsByUserId =
  () => async (dispatch, getState) => {
    dispatch({
      type: GET_ALL_PENDING_CAMPAIGNS_BY_USER_ID_REQUEST,
      payload: {},
    });
    try {
      const {
        userSignin: { userInfo },
      } = getState();

      const { data } = await Axios.get(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/campaign/${userInfo?._id}/user/getAllPendingCampaignsByUserId`,
        {
          headers: {
            Authorization: `Bearer ${userInfo?.token}`,
          },
        }
      );

      dispatch({
        type: GET_ALL_PENDING_CAMPAIGNS_BY_USER_ID_SUCCESS,
        payload: data,
      });
    } catch (error) {
      dispatch({
        type: GET_ALL_PENDING_CAMPAIGNS_BY_USER_ID_FAIL,
        payload:
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message,
      });
    }
  };

export const getCampaignCamData =
  ({ start = 0, end = 50, campaignId }) =>
  async (dispatch, getState) => {
    dispatch({
      type: "CAMPAIGN_CAM_DATA_REQUEST",
      payload: { start, end, campaignId },
    });
    try {
      const {
        userSignin: { userInfo },
      } = getState();

      const { data } = await Axios.get(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/campaign/camData/campaign?campaignId=${campaignId}&start=${start}&end=${end}`,
        {
          headers: {
            Authorization: `Bearer ${userInfo?.token}`,
          },
        }
      );
      dispatch({
        type: "CAMPAIGN_CAM_DATA_SUCCESS",
        payload: data,
      });
      // console.log({ data });
    } catch (error) {
      dispatch({
        type: "CAMPAIGN_CAM_DATA_FAIL",
        payload:
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message,
      });
    }
  };

export const changeCampaignPriority =
  (campaignId, index) => async (dispatch, getState) => {
    dispatch({
      type: CHANGE_CAMPAIGN_PRIORITY_REQUEST,
      payload: { campaignId, index },
    });
    // console.log("changeCampaignStatus called!");
    try {
      const {
        userSignin: { userInfo },
      } = getState();

      const { data } = await Axios.put(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/campaign/setPriority/${campaignId}/${index}`,
        {
          headers: {
            Authorization: `Bearer ${userInfo?.token}`,
          },
        }
      );
      dispatch({
        type: CHANGE_CAMPAIGN_PRIORITY_SUCCESS,
        payload: data,
      });
      // console.log({ data });
    } catch (error) {
      dispatch({
        type: CHANGE_CAMPAIGN_PRIORITY_FAIL,
        payload:
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message,
      });
    }
  };
