import Axios from "axios";

export const getFileUrlToUploadFileOnAWS =
  (contentType) => async (dispatch, getState) => {
    dispatch({
      type: "GET_URL_TO_UPLOAD_FILE_ON_AWS_REQUEST",
      payload: { contentType },
    });

    const {
      userSignin: { userInfo },
    } = getState();
    try {
      const { data } = await Axios.post(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/aws/getURLForFileUplaod`,
        { contentType },
        {
          headers: {
            Authorization: `Bearer ${userInfo?.token}`,
          },
        }
      );
      dispatch({
        type: "GET_URL_TO_UPLOAD_FILE_ON_AWS_SUCCESS",
        payload: data,
      });
      //   console.log(
      //     "time taken to upload media file in sec ",
      //     (performance.now() - startTime) / 1000
      //   );
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: "GET_URL_TO_UPLOAD_FILE_ON_AWS_FAIL",
        payload: message,
      });
    }
  };
