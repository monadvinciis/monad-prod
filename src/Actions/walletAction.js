import Axios from "axios";
import {
  CREATE_USER_WALLET_FAIL,
  CREATE_USER_WALLET_REQUEST,
  CREATE_USER_WALLET_SUCCESS,
  GET_TRANSACTION_LIST_FAIL,
  GET_TRANSACTION_LIST_REQUEST,
  GET_TRANSACTION_LIST_SUCCESS,
  GET_WALLET_BALANCE_FAIL,
  GET_WALLET_BALANCE_REQUEST,
  GET_WALLET_BALANCE_SUCCESS,
  RECHARGE_WALLET_FAIL,
  RECHARGE_WALLET_REQUEST,
  RECHARGE_WALLET_SUCCESS,
} from "../Constants/walletConstants";
import { USER_SIGNIN_SUCCESS } from "../Constants/userConstants";

export const createUserWallet = () => async (dispatch, getState) => {
  dispatch({
    type: CREATE_USER_WALLET_REQUEST,
    payload: {},
  });
  const {
    userSignin: { userInfo },
  } = getState();

  try {
    const { data } = await Axios.post(
      `${process.env.REACT_APP_BLINDS_SERVER}/api/userWallet/create/${userInfo?._id}`,
      {
        headers: {
          Authorization: `Bearer ${userInfo?.token}`,
        },
      }
    );
    dispatch({
      type: CREATE_USER_WALLET_SUCCESS,
      payload: data,
    });
    dispatch({
      type: USER_SIGNIN_SUCCESS,
      payload: data,
    });

    localStorage.setItem("userInfo", JSON.stringify(data));
  } catch (error) {
    const message =
      error.response && error.response.data.message
        ? error.response.data.message
        : error.message;
    dispatch({
      type: CREATE_USER_WALLET_FAIL,
      payload: message,
    });
  }
};

export const rechargeUserWallet = (amount) => async (dispatch, getState) => {
  dispatch({
    type: RECHARGE_WALLET_REQUEST,
    payload: { amount },
  });
  const {
    userSignin: { userInfo },
  } = getState();

  try {
    const { data } = await Axios.post(
      `${process.env.REACT_APP_BLINDS_SERVER}/api/userWallet/addTransactions/?userId=${userInfo?._id}&amount=${amount}`,
      {
        headers: {
          Authorization: `Bearer ${userInfo?.token}`,
        },
      }
    );
    dispatch({
      type: RECHARGE_WALLET_SUCCESS,
      payload: data,
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message
        ? error.response.data.message
        : error.message;
    dispatch({
      type: RECHARGE_WALLET_FAIL,
      payload: message,
    });
  }
};

export const getUserWalletBalance = () => async (dispatch, getState) => {
  dispatch({
    type: GET_WALLET_BALANCE_REQUEST,
    payload: {},
  });
  const {
    userSignin: { userInfo },
  } = getState();

  try {
    const { data } = await Axios.get(
      `${process.env.REACT_APP_BLINDS_SERVER}/api/userWallet/walletDetails/${userInfo?.userWallet}`,
      {
        headers: {
          Authorization: `Bearer ${userInfo?.token}`,
        },
      }
    );
    dispatch({
      type: GET_WALLET_BALANCE_SUCCESS,
      payload: data,
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message
        ? error.response.data.message
        : error.message;
    dispatch({
      type: GET_WALLET_BALANCE_FAIL,
      payload: message,
    });
  }
};

export const getUserWalletTransaction = () => async (dispatch, getState) => {
  dispatch({
    type: GET_TRANSACTION_LIST_REQUEST,
    payload: {},
  });
  const {
    userSignin: { userInfo },
  } = getState();

  try {
    const { data } = await Axios.get(
      `${process.env.REACT_APP_BLINDS_SERVER}/api/userWallet/allTransactions/${userInfo?.userWallet}`,
      {
        headers: {
          Authorization: `Bearer ${userInfo?.token}`,
        },
      }
    );
    dispatch({
      type: GET_TRANSACTION_LIST_SUCCESS,
      payload: data,
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message
        ? error.response.data.message
        : error.message;
    dispatch({
      type: GET_TRANSACTION_LIST_FAIL,
      payload: message,
    });
  }
};
