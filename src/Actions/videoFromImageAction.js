import {
  MEDIA_UPLOAD_FAIL,
  MEDIA_UPLOAD_REQUEST,
  MEDIA_UPLOAD_SUCCESS,
} from "../Constants/mediaConstants";
import Axios from "axios";

export const createVideoFromImage =
  ({ formData, duration }) =>
  async (dispatch, getState) => {
    dispatch({
      type: MEDIA_UPLOAD_REQUEST,
      payload: formData,
    });

    const {
      userSignin: { userInfo },
    } = getState();
    try {
      const { data } = await Axios.post(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/createVideoFromImage/${duration}/${userInfo?._id}`,
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${userInfo?.token}`,
          },
        }
      );

      dispatch({
        type: MEDIA_UPLOAD_SUCCESS,
        payload: data,
      });
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: MEDIA_UPLOAD_FAIL,
        payload: message,
      });
    }
  };
