import Axios from "axios";

export const changeUserAsCreator =
  ({
    creatorName,
    socialId,
    aboutCreator,
    logo,
    images,
    phone,
    email,
    instagramId,
    facebookId,
  }) =>
  async (dispatch, getState) => {
    dispatch({
      type: "CREATE_CREATOR_REQUEST",
      payload: {
        creatorName,
        socialId,
        aboutCreator,
        logo,
        images,
        phone,
        email,
        instagramId,
        facebookId,
      },
    });
    const {
      userSignin: { userInfo },
    } = getState();

    try {
      const { data } = await Axios.post(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/creators/${userInfo?._id}/create`,
        {
          user: userInfo,
          creatorName,
          socialId,
          aboutCreator,
          logo,
          images,
          phone,
          email,
          instagramId,
          facebookId,
        },
        {
          headers: {
            Authorization: `Bearer ${userInfo?.token}`,
          },
        }
      );
      dispatch({
        type: "CREATE_CREATOR_SUCCESS",
        payload: data,
      });
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: "CREATE_CREATOR_FAIL",
        payload: message,
      });
    }
  };

export const getCreatorDetailsById =
  (creatorId) => async (dispatch, getState) => {
    dispatch({
      type: "GET_CREATOR_DETAILS_REQUEST",
      payload: {},
    });
    const {
      userSignin: { userInfo },
    } = getState();

    try {
      const { data } = await Axios.get(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/creators/details/${creatorId}`,
        {
          headers: {
            Authorization: `Bearer ${userInfo?.token}`,
          },
        }
      );
      dispatch({
        type: "GET_CREATOR_DETAILS_SUCCESSION",
        payload: data,
      });
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: "GET_CREATOR_DETAILS_FAIL",
        payload: message,
      });
    }
  };

export const updateCreatorDetails =
  ({
    creatorName,
    socialId,
    aboutCreator,
    logo,
    images,
    phone,
    email,
    instagramId,
    facebookId,
  }) =>
  async (dispatch, getState) => {
    dispatch({
      type: "UPDATE_CREATOR_REQUEST",
      payload: {
        creatorName,
        socialId,
        aboutCreator,
        logo,
        images,
        phone,
        email,
        instagramId,
        facebookId,
      },
    });
    const {
      userSignin: { userInfo },
    } = getState();

    try {
      const { data } = await Axios.put(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/creators/update/${userInfo?.creator[0]}`,
        {
          user: userInfo,
          creatorName,
          socialId,
          aboutCreator,
          logo,
          images,
          phone,
          email,
          instagramId,
          facebookId,
        },
        {
          headers: {
            Authorization: `Bearer ${userInfo?.token}`,
          },
        }
      );
      dispatch({
        type: "UPDATE_CREATOR_SUCCESS",
        payload: data,
      });
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: "UPDATE_CREATOR_FAIL",
        payload: message,
      });
    }
  };
