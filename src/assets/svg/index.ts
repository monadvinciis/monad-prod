export { default as mapIcon } from "./icons/31377-map-marker.svg";
export { default as college } from "./icons/college 1.svg";
export { default as military } from "./icons/military-rank 1.svg";
export { default as tarin } from "./icons/train 1.svg";
export { default as plain } from "./icons/plane 1.svg";
export { default as turist } from "./icons/tourist.svg";
export { default as appartment } from "./icons/appartment (1) 1.svg";
export { default as railayStation } from "./icons/railway-station 1.svg";
export { default as beach } from "./icons/sun-umbrella 1.svg";
export { default as nav } from "./icons/nav.svg";
export { default as smimmer } from "./icons/swimmer 1.svg";
export { default as handSake } from "./icons/handsake.svg";
export { default as audiance } from "./icons/audiance.svg";
export { default as budget } from "./icons/budget.svg";
export { default as goal } from "./icons/goal.svg";
export { default as locationSvg } from "./icons/location.svg";
export { default as media } from "./icons/media.svg";
export { default as people } from "./icons/people.svg";
export { default as timer } from "./icons/timer.svg";
// export { default as campaign } from "./icons/campaignName.svg";
export { default as persent } from "./icons/persent.svg";
export { default as subscription } from "./icons/subscription.svg";

//coupon
export { default as caseback } from "./icons/caseback.svg";
export { default as caseoffer } from "./icons/caseoffer.svg";
export { default as casebonus } from "./icons/casebonus.svg";

export { default as fashion } from "./icons/fashion.svg";
export { default as movies } from "./icons//movies.svg";
export { default as adventurer } from "./icons/adventurer.svg";

export { default as discount } from "./icons/discount.svg";
export { default as oneplus } from "./icons/oneplus.svg";
export { default as claping } from "./icons/claping.svg";
export { default as sun } from "./icons/sun.svg";

export { default as scanner } from "./icons/scanner.svg";

// new things use

export { default as Monad } from "./icons/MonadLogo.svg";
export { default as Notification } from "./icons/notification.svg";
export { default as Message } from "./icons/message-question.svg";
export { default as analysis } from "./icons/analysis.svg";
export { default as screenImage } from "./icons/screenImage.svg";
export { default as campaign } from "./icons/campaign.svg";
export { default as campaignSVG } from "./icons/campaignSVG.svg";
export { default as notificationSVG } from "./icons/notificationSVG.svg";

export { default as about } from "./icons/about.svg";
export { default as HelpSvg } from "./icons/Help.svg";
