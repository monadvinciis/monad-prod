import { useQuery } from "react-query";
// import axios from "../services/axios";

interface Props {
  id: string;
}

const fetchMedia = async (id: string) => {
  try {
    if (!id) return undefined;
    // const data = await axios.get(`https://ipfs.io/ipfs/${id}`);
    // console.log("fetchMedia data ", data);
    // return data;

    let regExp =
      /^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|shorts\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    let match = id.match(regExp);
    if (match && match[2].length === 11) {
      // console.log("match[2] : ", match[2]);
      let s = match[2];
      return {
        url: `//www.youtube.com/embed/${s}`,
        fileType: "youtube",
      };
    }
    var tarea_regex = /^(http|https)/;
    if (tarea_regex.test(String(id).toLowerCase()) === true) {
      return {
        url: id,
        fileType: "iframe",
      };
    }
    const videoResponse = await fetch(`https://ipfs.io/ipfs/${id}`);
    if (!videoResponse.ok) {
      throw new Error("Failed to fetch IPFS video data");
    }
    const videoData = await videoResponse.blob();
    return {
      url: URL.createObjectURL(videoData),
      fileType: videoData?.type?.split("/")[0],
    };
  } catch (error) {
    return undefined;
  }
};

export function useMedia({ id }: Props) {
  return useQuery([`nft-${id}`], () => fetchMedia(id), {
    staleTime: 60 * 1000 * 60, // 1hr cache, since the nft details does not change.
    refetchOnWindowFocus: undefined,
  });
}
