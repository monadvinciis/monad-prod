import { Route, Routes } from "react-router-dom";
import {
  CreatePassword,
  ForgetPassword,
  SetupAccount,
  SignUp,
  Signin,
  Screens,
  Dashboard,
  CreateNewScreen,
  ScreenDetailPage,
  MyCampaigns,
  CreateCampaign,
  CampaignDetailsPage,
  Setting,
  MyWallet,
  Help,
  Notification,
  EditScreen,
  TestPlayer,
  NewCampaignCreate,
  RootPage,
} from "../Pages";
import { Page404 } from "../Pages/404";
import { Footer, NavBar, Sidebar } from "../components/newCommans";
import { Stack } from "@chakra-ui/react";
import { useSelector } from "react-redux";

export const PublicRoutes = () => {
  const userSignin = useSelector((state: any) => state.userSignin);
  const { userInfo } = userSignin;

  return (
    <div>
      {window.location.pathname.split("/")[1] !== "myplaylistfullscreen" ? (
        <Stack height="100vh" width="100%" flexGrow="1" flexDir="row">
          {userInfo && <Sidebar />}
          <Stack width="100%">
            <NavBar />
            <Stack height="90vh" width="100%">
              <Routes>
                <Route path="/" element={<RootPage />} />
                <Route path="/dashboard" element={<Dashboard />} />
                <Route path="/my/screens" element={<Screens />} />
                <Route path="/screen/create" element={<CreateNewScreen />} />
                <Route path="/screen/editScreen/:id" element={<EditScreen />} />
                <Route
                  path="/screen/screenDetail/:id"
                  element={<ScreenDetailPage />}
                />
                <Route path="/my/campaigns" element={<MyCampaigns />} />
                <Route path="/create-campaign" element={<CreateCampaign />} />
                <Route
                  path="/newcreatecampaign"
                  element={<NewCampaignCreate />}
                />
                <Route
                  path="/campaigns/details/:id"
                  element={<CampaignDetailsPage />}
                />
                <Route path="/setting" element={<Setting />} />
                <Route path="/my/wallet" element={<MyWallet />} />

                <Route path="/contactUs" element={<Help />} />
                <Route path="/notification" element={<Notification />} />

                <Route path="/signin" element={<Signin />} />
                <Route path="/signup" element={<SignUp />} />
                <Route
                  path="/create-reset-password/:email/:name"
                  element={<CreatePassword />}
                />
                <Route path="/setupAccount" element={<SetupAccount />} />
                <Route path="/forgetPassword" element={<ForgetPassword />} />

                <Route path="*" element={<Page404 />} />
              </Routes>
            </Stack>
          </Stack>
          <Footer />
        </Stack>
      ) : (
        <Routes>
          <Route path="/myplaylistfullscreen/:id" element={<TestPlayer />} />
        </Routes>
      )}
    </div>
  );
};
