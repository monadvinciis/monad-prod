import {
  ADD_NEW_QUERY_FAIL,
  ADD_NEW_QUERY_REQUEST,
  ADD_NEW_QUERY_RESET,
  ADD_NEW_QUERY_SUCCESS,
  GET_ALL_QUERY_BY_USER_FAIL,
  GET_ALL_QUERY_BY_USER_REQUEST,
  GET_ALL_QUERY_BY_USER_RESET,
  GET_ALL_QUERY_BY_USER_SUCCESS,
} from "../Constants/queryConstants";

export function addQueryReducer(state = {}, action) {
  switch (action.type) {
    case ADD_NEW_QUERY_REQUEST:
      return { loading: true };
    case ADD_NEW_QUERY_SUCCESS:
      return { loading: false, success: true, query: action.payload };
    case ADD_NEW_QUERY_FAIL:
      return { loading: false, error: action.payload };
    case ADD_NEW_QUERY_RESET:
      return {};
    default:
      return state;
  }
}

export function getAllQueryByUserReducer(state = { queries: [] }, action) {
  switch (action.type) {
    case GET_ALL_QUERY_BY_USER_REQUEST:
      return { loading: true };
    case GET_ALL_QUERY_BY_USER_SUCCESS:
      return { loading: false, success: true, queries: action.payload };
    case GET_ALL_QUERY_BY_USER_FAIL:
      return { loading: false, error: action.payload };
    case GET_ALL_QUERY_BY_USER_RESET:
      return {};
    default:
      return state;
  }
}
