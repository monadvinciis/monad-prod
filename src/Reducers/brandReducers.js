import {
  CREATE_BRAND_FAIL,
  CREATE_BRAND_REQUEST,
  CREATE_BRAND_RESET,
  CREATE_BRAND_SUCCESS,
  GET_BRAND_DETAILS_FAIL,
  GET_BRAND_DETAILS_REQUEST,
  GET_BRAND_DETAILS_RESET,
  GET_BRAND_DETAILS_SUCCESS,
  UPDATE_BRAND_FAIL,
  UPDATE_BRAND_REQUEST,
  UPDATE_BRAND_RESET,
  UPDATE_BRAND_SUCCESS,
} from "../Constants/brandConstants";

export function createNewBrandReducers(state = {}, action) {
  switch (action.type) {
    case CREATE_BRAND_REQUEST:
      return { loading: true };
    case CREATE_BRAND_SUCCESS:
      return { loading: false, success: true, brand: action.payload };
    case CREATE_BRAND_FAIL:
      return { loading: false, error: action.payload };
    case CREATE_BRAND_RESET:
      return {};
    default:
      return state;
  }
}

export function brandDetailsGetReducer(state = {}, action) {
  switch (action.type) {
    case GET_BRAND_DETAILS_REQUEST:
      return { loading: true };
    case GET_BRAND_DETAILS_SUCCESS:
      return { loading: false, success: true, brand: action.payload };
    case GET_BRAND_DETAILS_FAIL:
      return { loading: false, error: action.payload };
    case GET_BRAND_DETAILS_RESET:
      return {};
    default:
      return state;
  }
}

export function updateBrandDetailsReducer(state = {}, action) {
  switch (action.type) {
    case UPDATE_BRAND_REQUEST:
      return { loading: true };
    case UPDATE_BRAND_SUCCESS:
      return { loading: false, success: true, brand: action.payload };
    case UPDATE_BRAND_FAIL:
      return { loading: false, error: action.payload };
    case UPDATE_BRAND_RESET:
      return {};
    default:
      return state;
  }
}

export function allBrandsGetReducer(state = {}, action) {
  switch (action.type) {
    case "GET_ALL_BRANDS_REQUEST":
      return { loading: true };
    case "GET_ALL_BRANDS_SUCCESS":
      return { loading: false, success: true, allBrands: action.payload };
    case "GET_ALL_BRANDS_FAIL":
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}
