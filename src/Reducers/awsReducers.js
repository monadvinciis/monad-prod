export function getFileUrlToUploadFileOnAWSReducer(state = {}, action) {
  switch (action.type) {
    case "GET_URL_TO_UPLOAD_FILE_ON_AWS_REQUEST":
      return { loading: true };
    case "GET_URL_TO_UPLOAD_FILE_ON_AWS_SUCCESS":
      return {
        loading: false,
        success: true,
        url: action.payload.url,
        awsURL: action.payload.awsURL,
      };
    case "GET_URL_TO_UPLOAD_FILE_ON_AWS_FAIL":
      return { loading: false, error: action.payload };
    case "GET_URL_TO_UPLOAD_FILE_ON_AWS_RESET":
      return {};
    default:
      return state;
  }
}
